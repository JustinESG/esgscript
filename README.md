# ESG - Roleplay Script Repository (Section 1.a)
---
## General rules
* **always pull before pushing any commits**!
* only push if you are able to successfully compile the whole script in its current state
* the more commits, the easier it is to track changes and thus finding bugs
* **never** rebase or merge branches manually, use the `git-flow` plugin instead
* **never** push any commits to the `master` branch
* the `development`->`master` branch merge is to be done by the head of this development section or any higher member of the development team
* always use informative and descriptive commit messages
## Branches
**Please take a look [here][git-branching-model] for a full explanation of the branching model we are using.**

- `master`: contains only stable versions, which are (or were) running on the server
- `develop`: all changes not related to any currently developed feature are done here
- `feature/<name>`: features which are being worked on
    - these branches are always based on the `develop` branch  
    - when a feature is finished, it is merged back into the `develop` branch

__Always make use of the `git-flow` plugin to create and finish `feature` branches, never merge or create them manually!__


[git-branching-model]: http://nvie.com/posts/a-successful-git-branching-model/