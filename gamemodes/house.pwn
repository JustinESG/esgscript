#include <a_samp>
#include <crashdetect>
#include <a_mysql>
#include <a_zones>
#include <mSelection>
#include <streamer>
#include <sscanf2>
#include <foreach>
#include <zcmd>

#define MAX_HOUSES      (250)
#define MAX_FURNITURE   (30)

#define DIALOG_FURNI        0
#define SELECT_FURNI        0

enum HouseInfo
{
	ID,
	
	Owner[MAX_PLAYER_NAME],
	Bought,
	
	Locked,
	Street,

	FurniCount,
	Price,
	
	eX,
	eY,
	eZ,
	
	eInterior,
	eVirtualWorld,
	
	iX,
	iY,
	iZ,

	iInterior,
	iVirtualWorld
};

new House[MAX_HOUSES][HouseInfo], houseCount, furniCount;

enum hFurniData
{
	FurniType,
	FurniName[32],
	FurniModel
};

enum FurnitureInfo
{
	ID,
	
	Object,
	HouseID,
	
	fX,
	fY,
	fZ,
	
	fRX,
	fRY,
	fRZ,
	
	fInterior,
	fVirtualWorld
};

new Furniture[MAX_FURNITURE][MAX_HOUSES][FurnitureInfo];

#include "../filterscripts/server/furniturelist.pwn"

new mysql;

public OnGameModeInit()
{
    mysql_log(LOG_ALL);
	mysql = mysql_connect("localhost", "root", "newserver", "mysql");

    if(mysql_errno(mysql) != 0) print("Could not connect to database!");

	return true;
}

AddFurniture(id, houseid, object, Float: feX, Float: feY, Float: feZ, Float: feRX, Float: feRY, Float: feRZ, feInterior, feVirtualWorld)
{
	new query[200];

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `furniture` (`ID`, `Object`, `HouseID`, `fX`, `fY`, `fZ`, `fRX`, `fRY`, `fRZ`, `fInterior`, `fVirtualWorld`)");
	mysql_format(mysql, query, sizeof(query), "%s (VALUES", query);

	Furniture[id][Object] = object;
	Furniture[id][HouseID] = houseid;

	Furniture[id][fX] = feX;
	Furniture[id][fY] = feY;
	Furniture[id][fZ] = feZ;
	
	Furniture[id][fRX] = feRX;
	Furniture[id][fRY] = feRY;
	Furniture[id][fRZ] = feRZ;

	Furniture[id][fInterior] = feInterior;
	Furniture[id][fVirtualWorld] = feVirtualWorld;

	House[houseid][FurniCount] ++;
	furniCount ++;

	return true;
}

CMD:furni(playerid, params[])
{
	new string[120];

    for (new i = 0; i < sizeof(hFurniType); i ++)
	{
        format(string, sizeof(string), "%s\n%s", string, hFurniType[i]);
	}

	ShowPlayerDialog(playerid, DIALOG_FURNI, DIALOG_STYLE_LIST, "Furniture", string, "Select", "Cancel");

	return true;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    if(dialogid == DIALOG_FURNI)
	{
	    if(response)
	    {
			new itemcount, items[sizeof(FurniInfo)], string[120];

			for(new i = 0; i < sizeof(FurniInfo); i ++)
			{
			    if(FurniInfo[i][FurniType] == listitem)
			    {
					items[itemcount ++] = FurniInfo[i][FurniModel];
				}
			}

            format(string, sizeof(string), "%s", hFurniType[listitem]);
		    ShowModelSelectionMenuEx(playerid, items, itemcount, string, SELECT_FURNI, 16.0, 0.0, -55.0, 1.0, 0x8AB2DABB, 0xB4C9DE99, 0xDA8A8AAA);
		}
	}

	return false;
}

public OnPlayerModelSelectionEx(playerid, response, extraid, modelid)
{
	if(extraid == SELECT_FURNI && response)
	{
		printf("%d");
	}

	return true;
}
