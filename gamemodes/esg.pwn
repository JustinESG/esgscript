/*
 * IMPORTANT NOTES
 * 1. don't use 'Account[playerid][Name]' in callback 'OnPlayerConnect',
 *    since this variable is not guaranteed to be initialized when called there
 *
 * ADDITIONAL CALLBACKS
 * - OnCharacterSpawn: called when a user selects his character and
 *                     hits the 'Select Char' textdraw button
 * - OnSystemInit:     callback to initialize a custom system
 *                     is called after initializing all 
 *                     core systems (like MySQL) in OnGameModeInit
 */

#include <a_samp>

// SA-MP CONSTANTS (have to be re-defined before including fixes.inc)
#undef MAX_PLAYERS
#define MAX_PLAYERS (50)

#include <fixes>

#include <YSI4\YSI_Coding\y_inline> //include this before a_mysql
#include <a_mysql>
#include <a_zones>
#include <sscanf2>
#include <streamer>
#include <zcmd>
#include <geoip>
#include <timestamp>
#include <YSI4\YSI_Server\y_colours>
#include <YSI4\YSI_Data\y_iterate>
#include <YSI4\YSI_Coding\y_hooks>

native WP_Hash(buffer[], len, const str[]); // Whirlpool plugin


//////////////////////////// CONSTANT EXPRESSIONS //////////////////////////////

// General Server Information:
#define SERVER_NAME     	"EastSide Gaming - Roleplay (0.3.7)"
#define SERVER_MODE     	"ES-RP"
#define SERVER_MAP      	"Los Santos"
#define SERVER_REV          "0.5 Alpha"
#define SERVER_URL          "eastsidegaming.net"



// Spawn Location (SAN FIERRO):
#define SERVER_SPAWN_X     (-1531.005)
#define SERVER_SPAWN_Y     (-422.8018)
#define SERVER_SPAWN_Z     (0006.1155)
#define SERVER_SPAWN_A     (134.64780)

// VARS USED IN MORE THAN 1 MODULE
new Text: ClockTD, Text: LogoTD;

// Server-side settings

enum ServerInfoEnum
{
	bool: Closed,
	bool: WhiteListOnly
};

new gServerInfo[ServerInfoEnum] = {false, false};

enum // DIALOGS
{
	DIALOG_REGISTER,
	DIALOG_REGISTER_EMAIL,
	DIALOG_REGISTER_PASS,
	DIALOG_LOGIN,
	DIALOG_NOTWHITELISTED,
 	DIALOG_GPS,
 	DIALOG_GATE_CODE,
	DIALOG_PHONE_TEXT_NUMBER,
	DIALOG_PHONE_CALL_NUMBER,
	DIALOG_PHONE_TEXT_TEXT,
	DIALOG_STYLE_BOOMBOX_SEL,
	DIALOG_STYLE_CHANNEL_SEL,
	DIALOG_CHAR_CREATE_SELECT_NAME,
	DIALOG_CHARACTER_DELETE
};

// COLOR DEFINES:
#define COLOR_FACTION       	(0x00FFFFFF)
#define HEX_FACTION             "{00FFFF}"
#define COLOR_BLUE				(0x63C9FFFF)
#define HEX_BLUE                "{63C9FF}"

#define COLOR_INFO				(COLOR_BLUE)
#define HEX_INFO                HEX_BLUE
#define COLOR_ERROR				(0xFF6347AA)
#define HEX_ERROR               "{FF6347}"

#define COLOR_GREEN 			(0x33AA33AA)
#define HEX_GREEN               "{33AA33}"
#define COLOR_ORANGE 			(0xFFCC33AA)
#define HEX_ORANGE              "{FFCC33}"

#define HEX_AQUA                "{4AFFF6}"


// MYSQL VARS
new 
	mysql, // MySQL SAMP database handle
	MysqlUcp; //MySQL UCP database handle


// MODULES

//#include "modules/security.pwn"            // Security System.
#include "modules/account.pwn"          	// Registration/MySQL function(s) module
#include "modules/character.pwn"           // Contains the character system: creating / selecting.
#include "modules/admin.pwn"          		// Admin module: report system and commands
#include "ESG/main/weapons.pwn" 			// Weapon Damage System by (Mads)
#include "modules/names.pwn"               // Custom name script and basic /mask command

//#include "modules/leveling.pwn"          	// Leveling system (LevelUp and StartLevelingTimer function)
//#include "modules/radio.pwn"           	// Boombox and MP3 system with seperate play systems.
//#include "modules/toys.pwn" 				// Contains complete toy system

/*		// Business System Rev. 1 [By Mione] //

	Documentation:
		"saving.pwn" 	- 	Contains MySQL framework for the business system (loading/saving) + creation command
		"entrance.pwn" 	- 	Contains entrance functions and OnPlayerKeyStateChange hooks for entry by "F"/"ENTER"
		"buystuff.pwn" 	- 	Contains buying functions and ondialogresponse/mselectionex hooks and /buy command

	Commands:
		"c_own.pwn" 	- 	Contains owner related commands for businesses. Also biz management commands.
		"c_adm.pwn"		- 	Contains admin related commands for businesses, mainly admin management.
*/
/*
#include "ESG/biz/saving.pwn"              //
#include "ESG/biz/entrance.pwn"            //
#include "ESG/biz/buystuff.pwn"            //

#include "ESG/biz/c_own.pwn"               //
#include "ESG/biz/c_adm.pwn"               //
*/

/*		// ATM System Rev. 1 [By Mione] //

	Documentation:
		"saving.pwn" 	- 	Contains MySQL framework for ATM system; loading/saving & creation
		"editing.pwn" 	- 	ATM placement; for new and edited ATMs (OPEditDynamicObject hooked)
		"textds.pwn" 	- 	ATM textdraws, OnPlayerConnect & OnPlayerClickPTD hooked; (main atm system)
		"money.pwn" 	- 	Contains bank system and money anticheat (Relevant to ATM System).
			
	Commands:
		"c_use.pwn" 	- 	Usage commands; /atm, /refreshatmdraws and isplayernear/getclosestatm functions
		"c_adm.pwn" 	- 	Admin management commands
*/

#include "ESG/atm/saving.pwn"              //
#include "ESG/atm/editing.pwn"             //
#include "ESG/atm/textds.pwn"              //
#include "modules/money.pwn"           	//

#include "ESG/atm/c_use.pwn"               //
#include "ESG/atm/c_adm.pwn"               //

// ==== // Communication modules that do not have more than one module associated to them: // ==== //

//#include "ESG/main/phone.pwn"              // Phone System; OnPlayerKeyStateChange/OnDialogResponse/OnPlayerClickPlayerTextDraw hooked; contains phone textdraws.
#include "ESG/main/death.pwn"              // Death System: OnPlayerConnect/OnPlayerSpawn/OnPlayerDeath hooked; Contains DEATHSPAWN_X/Y/Z/A constant expressions.
#include "ESG/main/chat.pwn"				// Communication Script: OnPlayerText hooked)

/*		// Phone Booth System Rev. 1 [By Mione] //

	Documentation:
		"saving.pwn" 	- 	Contains MySQL framework for phone booth system; creation/loading/saving + vars for placement
		"editing.pwn" 	-  	Placement commands and functions; contains a OnPlayerEditDynamicObject hook

	Commands:
		"c_use.pwn" 	- 	Usage commands; /boothcall, /boothhangup and IsPlayerNearBooth/GetClosestBooth functions
		"c_adm.pwn" 	- 	Admin management commands
*/
/*
#include "ESG/phb/saving.pwn"				//
#include "ESG/phb/editing.pwn"             //

#include "ESG/phb/c_use.pwn"               //
#include "ESG/phb/c_adm.pwn"               //
*/

/*		// Vehicle System Rev. 1 [By Mione] //

	Documentation:
		"saving.pwn" 	- Contains MySQL framework for vehicle system (contains mod loading/applying)
		"modding.pwn" 	- Contains vehicle modding system (applying, saving, enum data + mod arrays)
		"dealer.pwn" 	- Contains complete dealership system (inc. textdraws and browsing system)
		"timers.pwn" 	- Contains fuel and battery timers (inc system) and /engine command.
		"renting.pwn" 	- Contains rental system along w/ OnPlayerStateChange and /rentcar, /unrentcar
		
		"speedo.pwn" 	- Speedometer: OnPlayerUpdate/OnPlayerStateChange hooked + textdraws
		"dmv.pwn" 		- DMV System: OnPlayerExitVehicle/OnPlayerEnterDynamicCP hooked (array contains locations)
		"gps.pwn" 		- GPS system; OPStateChange/OPEnterDynamicCP/OPDialogResponse hooked

	Commands:
		"c_main.pwn" 	- Contains main vehicle commands; usable by every player
		"c_own.pwn" 	- Contains player vehicle commands; usable by vehicle owners
		"c_adm.pwn" 	- Contains admin vehicle commands (only vehicle system commands)
*/

#include "ESG/car/saving.pwn" 				//
#include "ESG/car/modding.pwn" 			//
#include "ESG/car/dealer.pwn"              //
#include "ESG/car/timers.pwn"              //
#include "ESG/car/renting.pwn"             //

#include "ESG/car/c_main.pwn"              //
#include "ESG/car/c_own.pwn"               //
#include "ESG/car/c_adm.pwn"               //

// Independant Modules that do not need the above modules to function:
#include "ESG/car/speedo.pwn"              //
#include "ESG/car/dmv.pwn"          		//
#include "ESG/car/gps.pwn"             	//


#include "modules/factions.pwn"         	// Contains complete faction system
#include "modules/time.pwn"                // Everything to do with time updates.

#include "modules/mapping.pwn"           	// Custom mapping system (and loading) including custom gate script
//#include "modules/walkytalky.pwn"          // Walkytalky/radio script including 3 commands (/r(adio), /radiofreq, /radioslot)



#define MYSQL_HOST		"159.253.3.223"
#define MYSQL_USER		"samp_rp"
#define MYSQL_SAMP_DB	"esg_samprp"
#define MYSQL_UCP_DB	"esg_online"
#define MYSQL_PASS		"vEHXLuxUyq2mDy54"


main() { }

// ####################################################### //
// ############# // GAMEMODE INITIALISERS // ############# //
// ####################################################### //

Mode_Init()
{
	SendRconCommand("hostname " SERVER_NAME);
	SendRconCommand("mapname " SERVER_MAP);
	SetGameModeText(SERVER_MODE);
}

// Temp static labels until label script is finished
Mode_LoadStaticLabels()
{
	// START Bank Labels
	CreateDynamic3DTextLabel("San Fierro Bank\n{E0E0E0}Type /pass to enter.", 0x6CDAF8FF, -1599.9169, 873.8834, 9.2298, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, 0, 0, -1, 20.0);
	CreateDynamic3DTextLabel("San Fierro Bank\n{E0E0E0}Type /pass to exit.", 0x6CDAF8FF, 1957.5806, -2274.8992, 13.5629, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, 701, 1, -1, 20.0);
	// END Bank Labels

	// START Hospital Labels
	CreateDynamic3DTextLabel("San Fierro Hospital\n{E0E0E0}Type /pass to enter.", 0x6CDAF8FF, DEATHSPAWN_X, DEATHSPAWN_Y, DEATHSPAWN_Z, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 0, 0, -1, 20.0);
	CreateDynamic3DTextLabel("San Fierro Hospital\n{E0E0E0}Type /pass to exit.", 0x6CDAF8FF, 2146.0874, -2439.2534, 13.5570, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 800, 2, -1, 20.0);
	// END Hospital Labels
	
	// START Hospital Elevator Labels
	CreateDynamic3DTextLabel("Elevator\n{E0E0E0}Type /pass to use.", 0x6CDAF8FF, 2098.0425, -2439.4502, 13.5570, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 800, 2, -1, 20.0); // bottom
	CreateDynamic3DTextLabel("Elevator\n{E0E0E0}Type /pass to use.", 0x6CDAF8FF, 2098.0093, -2439.2966, 17.8170, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 800, 2, -1, 20.0); // upper
	// END Hospital Elevator Labels

	// START DMV Labels
	CreateDynamic3DTextLabel("San Fierro DMV Center\n{E0E0E0}Type /startdriverstest to start.", 0x6CDAF8FF, -1880.8655, 822.6258, 35.1775, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, 0, 0, -1, 20.0);
	// END DMV Labels

	// START Mall Labels
	CreateDynamic3DTextLabel("San Fierro Mall\n{E0E0E0}Type /pass to enter.", 0x6CDAF8FF, SERVER_SPAWN_X, SERVER_SPAWN_Y, SERVER_SPAWN_Z, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, 0, 0, -1, 20.0);
	CreateDynamic3DTextLabel("San Fierro Mall\n{E0E0E0}Type /pass to enter.", 0x6CDAF8FF, 1938.6051, -2409.2183, 13.6270, 20.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 1, 700, 1, -1, 20.0);
	// END Mall Labels
}

Mode_LoadStaticPickups()
{
	// START Bank Pickup
	CreateDynamicPickup(1274, 19, 1957.0490, -2286.0330, 13.5785, 701, 1, -1, 100.0);
	// END Bank Pickup
}

Mode_InitMysql()
{
	mysql_log(LOG_ALL); // enables MySQL debugging
	
	printf("Establishing MySQL connection...");
	
	printf("  connecting to database %s...", MYSQL_SAMP_DB);
    mysql = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_SAMP_DB, MYSQL_PASS);
	new conn_errno_sampdb = mysql_errno(mysql);
    if(conn_errno_sampdb != 0)
		printf("    FAILED! (error #%d)", conn_errno_sampdb);
	else if(mysql != 1) //'mysql' has to be the default connection handle
	{
		printf("    FAILED! (error: not default handle)");
		conn_errno_sampdb = -2;
	}
	else
		printf("    success!");
	
	printf("  connecting to database %s...", MYSQL_UCP_DB);
    MysqlUcp = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_UCP_DB, MYSQL_PASS, .pool_size = 0);
	new conn_errno_ucpdb = mysql_errno(MysqlUcp);
    if(conn_errno_ucpdb != 0)
		printf("    FAILED! (error #%d)", conn_errno_ucpdb);
	else
		printf("    success!");
	
	if(conn_errno_sampdb || conn_errno_ucpdb)
	{
		printf("Failed to establish MySQL connection; shutting down...\n");
	    SendRconCommand("exit");
		return 0;
	}
 	else
		printf("Connection to MySQL server successfully established.\n");
		
	return 1;
}

public OnGameModeInit()
{
	AddPlayerClass(0, 0.0, 0.0, 5.0, 0.0, 0, 0, 0, 0, 0, 0);
	DisableInteriorEnterExits();
	ShowPlayerMarkers(PLAYER_MARKERS_MODE_OFF);
	EnableStuntBonusForAll(false);
	ShowNameTags(false);
	DisableNameTagLOS();
	SetWeather(14);
	
	Mode_Init();
    if(!Mode_InitMysql())
		return 0;
	Mode_LoadStaticLabels();
	Mode_LoadStaticPickups();
	
	InitialiseClock();
	
	SQL_LoadVehicles();
//	SQL_LoadPhoneBooths();
//	SQL_LoadBizzes();
	SQL_LoadATMs();
	SQL_LoadWeaponSystem();
	
	LoadLogoTextDraw();
	
	CallLocalFunction("OnSystemInit", "");
	
	return 1;
}



// ################################################## //
// ############# // HOOKED FUNCTIONS // ############# //
// ################################################## //
#include <YSI4\YSI_Coding\y_hooks>

hook OnPlayerConnect(playerid)
{
	SetPlayerColor(playerid, 0xa3a3a3ff);
	return 1;
}


new bool: welcomeMsg[MAX_PLAYERS];

hook OnCharacterSpawn(playerid)
{
	new string[70];

	if(!Account[playerid][NewUser])
	{
		if(!welcomeMsg[playerid])
		{
			format(string, sizeof(string), "[WELCOME]{E0E0E0} Welcome back, {63C9FF}%s{E0E0E0}!", GetPlayerFirstName(playerid));
			SendClientMessage(playerid, COLOR_BLUE, string);
			
			welcomeMsg[playerid] = true;
		}
	}
	return 1;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(newkeys & KEY_SECONDARY_ATTACK)
	{
		return cmd_pass(playerid);
	}
	return 1;
}


// ################################################## //
// ############# // GENERAL COMMANDS // ############# //
// ################################################## //

// TEMP command until label system is finished
CMD:pass(playerid)
{
	// START BANK /PASS POINTS
	if(IsPlayerInRangeOfPoint(playerid, 5.0, -1600.2062, 873.6846, 9.2298) && !IsPlayerInAnyVehicle(playerid))
	{
	    SetPlayerPos(playerid, 1958.0394, -2275.2795, 13.5629);
	    SetPlayerFacingAngle(playerid, 183.5283);
	    
	    SetPlayerVirtualWorld(playerid, 701);
	    SetPlayerInterior(playerid, 1);
	    
	    SetCameraBehindPlayer(playerid);
	    SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Welcome to the bank! To access your bank account, you can use the following commands: {63C9FF}/deposit, /withdraw, /savings{E0E0E0}!");
	}

	if(IsPlayerInRangeOfPoint(playerid, 3.0, 1958.0394, -2275.2795, 13.5629) && !IsPlayerInAnyVehicle(playerid))
	{
	    SetPlayerPos(playerid, -1600.2062, 873.6846, 9.2298);
		SetPlayerFacingAngle(playerid, 177.8881);
	    
	    SetPlayerVirtualWorld(playerid, 0);
	    SetPlayerInterior(playerid, 0);
	    
	    SetCameraBehindPlayer(playerid);
	}
	// END BANK /PASS POINTS

	// START HOSPITAL /PASS POINTS
	if(IsPlayerInRangeOfPoint(playerid, 5.0, DEATHSPAWN_X, DEATHSPAWN_Y, DEATHSPAWN_Z) && !IsPlayerInAnyVehicle(playerid))
	{
	    SetPlayerPos(playerid,  2146.0874, -2439.2534, 13.5570);
	    SetPlayerFacingAngle(playerid, 87.1000);

	    SetPlayerVirtualWorld(playerid, 800);
	    SetPlayerInterior(playerid, 2);

	    SetCameraBehindPlayer(playerid);
	}

	if(IsPlayerInRangeOfPoint(playerid, 3.0, 2146.0874, -2439.2534, 13.5570) && !IsPlayerInAnyVehicle(playerid))
	{
		SetPlayerPos(playerid, DEATHSPAWN_X, DEATHSPAWN_Y, DEATHSPAWN_Z);
		SetPlayerFacingAngle(playerid, DEATHSPAWN_A);
		SetCameraBehindPlayer(playerid);

	    SetPlayerVirtualWorld(playerid, 0);
	    SetPlayerInterior(playerid, 0);

	    SetCameraBehindPlayer(playerid);
	}
	// END HOSPITAL /PASS POINTS

	// START HOSPITAL ELEVATOR /PASS POINTS
	if(IsPlayerInRangeOfPoint(playerid, 3.0, 2098.0425, -2439.4502, 13.5570) && !IsPlayerInAnyVehicle(playerid))
	{
	    SetPlayerPos(playerid, 2098.0093, -2439.2966, 17.8170);
	    SetCameraBehindPlayer(playerid);
	}

	if(IsPlayerInRangeOfPoint(playerid, 3.0, 2098.0093, -2439.2966, 17.8170) && !IsPlayerInAnyVehicle(playerid))
	{
		SetPlayerPos(playerid, 2098.0425, -2439.4502, 13.5570);
	    SetCameraBehindPlayer(playerid);
	}
	// END HOSPITAL ELEVATOR /PASS POINTS


	// START MALL /PASS POINTS
	if(IsPlayerInRangeOfPoint(playerid, 3.0, SERVER_SPAWN_X, SERVER_SPAWN_Y, SERVER_SPAWN_Z) && !IsPlayerInAnyVehicle(playerid))
	{
	    SetPlayerPos(playerid, 1938.6051, -2409.2183, 13.6270);
	    SetPlayerFacingAngle(playerid, 180.1049);
	    
	    SetPlayerVirtualWorld(playerid, 700);
	    SetPlayerInterior(playerid, 1);
	    
	    SetCameraBehindPlayer(playerid);
	}

	if(IsPlayerInRangeOfPoint(playerid, 3.0, 1938.6051, -2409.2183, 13.6270) && !IsPlayerInAnyVehicle(playerid))
	{
		SetPlayerPos(playerid,  SERVER_SPAWN_X, SERVER_SPAWN_Y, SERVER_SPAWN_Z);
		SetPlayerVirtualWorld(playerid, 0);
	    SetPlayerInterior(playerid, 0);

	    SetCameraBehindPlayer(playerid);
	}
	// END MALL /PASS POINTS

	return true;
}

CMD:factionhelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Faction commands available to you:");

	SendClientMessage(playerid, COLOR_BLUE, "[PLAYERS]{D9D9D9} /f(action), /factionlist");
	SendClientMessage(playerid, COLOR_BLUE, "[TIER 2]{F2F2F2} /factionrank, /factiontier, /factionchat");
	SendClientMessage(playerid, COLOR_BLUE, "[TIER 1]{D9D9D9} /factionlist, /factionname");
	
	return true;
}

CMD:carhelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Vehicle commands available to you:");

	SendClientMessage(playerid, COLOR_BLUE, "[PLAYER OWNED]{F2F2F2} /carbuy, /carlist, /cargive, /carpark, /cargunslots, /carscrap, /engine, /lock");
	SendClientMessage(playerid, COLOR_BLUE, "[ALL VEHICLES]{D9D9D9} /carhood, /cartrunk, /carlights,  /carstore, /cartake");
	SendClientMessage(playerid, COLOR_BLUE, "[MISCELLANEOUS]{F2F2F2} /rentcar, /unrentcar, /startdrivingtest, /gps");
	
	return true;
}

CMD:bizhelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Business commands available to you:");

	SendClientMessage(playerid, COLOR_BLUE, "[PLAYER OWNED]{F2F2F2} /bizname, /bizlock, /biztill, /bizfee");
	SendClientMessage(playerid, COLOR_BLUE, "[EVERY BIZ]{D9D9D9} /buy, /bizbuy, /bizsell, /forcebizreparse");

	return true;
}

CMD:phonehelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Phone commands available to you:");
	SendClientMessage(playerid, COLOR_BLUE, "[HELP]{F2F2F2}, /phone (or NUM4), /togphone, /boothcall, /boothhangup");
	
	return true;
}

CMD:moneyhelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Money commands available to you:");
	SendClientMessage(playerid, COLOR_BLUE, "[HELP]{F2F2F2} /atm, /refreshatmtextdraws, /deposit, /withdraw, /savings, /emptysavings");
	
	return true;
}

CMD:chathelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Chat commands available to you:");
	SendClientMessage(playerid, COLOR_BLUE, "[HELP]{F2F2F2} /me, /do, /b, /o(oc), /s(hout), /yell, /low, /w(hisper), /pm");

	return true;
}

CMD:radiohelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Radio commands available to you:");
	SendClientMessage(playerid, COLOR_BLUE, "[HELP]{F2F2F2} /r(adio), /radioslots, /r(adio), /radioslots, /radioslot, /radiofreq");

	return true;
}

CMD:weaponhelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Weapon commands available to you:");

	SendClientMessage(playerid, COLOR_BLUE, "[PLAYERS]{F2F2F2} /gunslots, /pickupgun");
	SendClientMessage(playerid, COLOR_BLUE, "[MANAGEMENT]{F2F2F2} /storegun, /takegun, /dropgun");

	return true;
}

CMD:gunhelp(playerid, params[])
	return cmd_weaponhelp(playerid, params);

CMD:toyhelp(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "Weapon commands available to you:");

	SendClientMessage(playerid, COLOR_BLUE, "[PLAYERS]{F2F2F2} /buytoyslot, /toys, /reparsetoyinfo");
	SendClientMessage(playerid, COLOR_BLUE, "[MANAGEMENT]{D9D9D9} /weartoy, /hidetoy, /edittoy, /removetoy");
	
	return true;
}

CMD:help(playerid, params[])
{
	SendClientMessage(playerid, COLOR_BLUE, "List of commands currently available to you");

	SendClientMessage(playerid, 0xF2F2F2AA, "/changepass, /changename, /stats, /mask, /pass, /admins");
	SendClientMessage(playerid, 0xD9D9D9AA, "/mp3, /mp3play, /boombox(play/pickup), /medikit, /radiohelp, /weaponhelp");
	SendClientMessage(playerid, 0xF2F2F2AA, "/factionhelp, /carhelp, /bizhelp, /chathelp, /phonehelp, /moneyhelp");

	if(Account[playerid][Admin] > 0) SendClientMessage(playerid, COLOR_BLUE, "[ADMIN]:{EBEBEB} /ahelp");

	return true;
}
/*
CMD:stats(playerid, params[])
	return ShowStats(playerid, playerid);
*/
// ################################################## //
// ############# // CUSTOM FUNCTIONS // ############# //
// ################################################## //
/*
ShowStats(playerid, toplayerid)
{
	new string[144], holder[60];

	format(string, sizeof(string), "[Statistics of %s]", Account[playerid][Name]);
	SendClientMessage(toplayerid, COLOR_BLUE, string);

	// START mask boolean to string
	if(!pMask[playerid]) format(string, sizeof(string), "No");
	else if(pMask[playerid]) format(string, sizeof(string), "Yes");
	// END mask boolean to string

	// START Gender to string
	if(Character[playerid][PlayerChar[playerid]][Gender] == 1) format(holder, sizeof(holder), "Male");
	else if(Character[playerid][PlayerChar[playerid]][Gender] == 2) format(holder, sizeof(holder), "Female");
	// END Gender to string

	format(string, sizeof(string), "[SQL ID: %d] [Username: %s] [IP: %s] [Gender: %s] [Skin ID: %d] [Mask: %s] [Mask ID: %d]", Player[playerid][ID], Account[playerid][Name], IP[playerid], holder, GetPlayerSkin(playerid), string, Player[playerid][MaskID]);
	SendClientMessage(toplayerid, 0xF2F2F2AA, string);

	format(string, sizeof(string), "[Level: %d] [Hours played: %d] [Experience info: %d/%d] [Money: %d (SQL)] [Bank Account: %d] [Savings: %d]", Player[playerid][Level], Player[playerid][Hours], Player[playerid][Experience], Player[playerid][ExpLeft], Player[playerid][Money], Player[playerid][BankAmount], Player[playerid][BankSavings]);
	SendClientMessage(toplayerid, 0xD9D9D9AA, string);

	// START "empty" Faction name + abbreviation
	new factionid = Character[playerid][PlayerChar[playerid]][FactionID];
	if(strlen(Character[playerid][PlayerChar[playerid]][Rank]) < 1) format(Player[playerid][Rank], 16, "None");
	if(Player[playerid][FactionID] == 999) format(string, sizeof(string), "[NONE] Civilian");
	else format(string, sizeof(string), "[%s] %s", Faction[factionid][ShortName], Faction[factionid][Name]);
	// END "empty" Faction name + abbreviation

	// START Radio boolean to string
	if(Player[playerid][HasRadio] == 0) format(holder, sizeof(holder), "No");
	else if(Player[playerid][HasRadio] == 1) format(holder, sizeof(holder), "Yes");
	// END Radio boolean to string
	
	format(string, sizeof(string), "[Faction: [ID: %d] %s] [Rank: %s] [Tier: %d] [Radio: %s] [Radio Slot: %d] [Radio Freq 1: %d]", Player[playerid][FactionID], string, Player[playerid][Rank], Player[playerid][Tier], holder, RadioSlot[playerid], Player[playerid][RadioFreq][0]);
	SendClientMessage(toplayerid, 0xF2F2F2AA, string);

	// START phone boolean to string
	if(Player[playerid][Phone] == 0) format(string, sizeof(string), "No");
	else if(Player[playerid][Phone] == 1) format(string, sizeof(string), "Yes");
	// END phone boolean to string

	// START mp3 boolean to string
	if(Player[playerid][HasMP3] == 0) format(holder, sizeof(holder), "No");
	else if(Player[playerid][HasMP3] == 1) format(holder, sizeof(holder), "Yes");
	// END mp3 boolean to string
	
	format(string, sizeof(string), "[Radio Freq 2: %d] [Business: %d (SQL ID)] [MP3: %s] [Phone: %s] [Phone Number: %d] [Medikits: %d]", Player[playerid][RadioFreq][1], Player[playerid][BizOwner], holder, string, Player[playerid][PhoneNumber], Player[playerid][Medikits]);
	SendClientMessage(toplayerid, 0xD9D9D9AA, string);

	// START drivings license boolean to string
	if(Player[playerid][DrivingLicense] == 0) format(string, sizeof(string), "No");
	else if(Player[playerid][DrivingLicense] == 1) format(string, sizeof(string), "Yes");
	// END drivings license boolean to string

	// START weapon license boolean to string
	if(Player[playerid][WeaponLicense] == 0) format(holder, sizeof(holder), "No");
	else if(Player[playerid][WeaponLicense] == 1) format(holder, sizeof(holder), "Yes");
	// END weapon license boolean to string

	format(string, sizeof(string), "[Drivers License: %s] [Weapon License: %s] [Trousers: %s] [Ammo: %d] [Jacket: %s] [Ammo: %d]", string, holder, GetWeaponNameEx(Player[playerid][TrouserSlot]), Player[playerid][TrouserAmmo], GetWeaponNameEx(Player[playerid][JacketSlot]), Player[playerid][JacketAmmo]);
	SendClientMessage(toplayerid, 0xF2F2F2AA, string);

	return true;
}*/

IsNumeric(const string[])
{
    for (new i = 0, j = strlen(string); i < j; i++)
    {
        if (string[i] > '9' || string[i] < '0') return false;
    }

    return true;
}

LoadLogoTextDraw()
{
	LogoTD = TextDrawCreate(497.0, 97.5, "Eastside Gaming ~g~Roleplay");
	TextDrawLetterSize(LogoTD, 0.325999, 1.284741);
	TextDrawAlignment(LogoTD, 1);
	TextDrawColor(LogoTD, 0x9EDADEFF);
	TextDrawSetShadow(LogoTD, 0);
	TextDrawSetOutline(LogoTD, 1);
	TextDrawBackgroundColor(LogoTD, 51);
	TextDrawFont(LogoTD, 1);
	TextDrawSetProportional(LogoTD, 1);
}

// Name Functions:
GetPlayerFirstName(playerid)
{
    new szName[MAX_PLAYER_NAME], iPos;
    GetPlayerName(playerid, szName, sizeof(szName));

    iPos = strfind(szName, "_");
    if(iPos != -1) szName[iPos] = EOS;

    return szName;
}

GetRoleplayName(playerid)
{
	new name[MAX_PLAYER_NAME];
	GetPlayerName(playerid, name, sizeof(name));

	if(pMasked[playerid])
	{
		for(new i; i < MAX_PLAYER_NAME; i++)
			if(name[i] == '_') name[i] = ' ';
	}
	else
	{
		format(name, sizeof(name), "Stranger [%d]", 
			Character[playerid][MaskID]);
	}
	
	return name;
}

// ################################################# //
// ############# // PUBLIC FUNCIONS // ############# //
// ################################################# //

forward KickPlayer(playerid);
public KickPlayer(playerid)
{
	// Blank kick function for delayed actions
	return Kick(playerid);
}
