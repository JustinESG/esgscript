#include <YSI4\YSI_Coding\y_hooks>

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(newkeys & KEY_SECONDARY_ATTACK)
	{
	    if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	    {
			new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), string[150];

			if(bInfo[id][Locked] == 1)
			    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This business is locked!");

			if(bInfo[id][EntryFee] > GetPlayerMoney(playerid))
			{
				format(string, sizeof(string), "[ERROR]:{E0E0E0} You don't have enough money to enter this business! You need $%d.", bInfo[id][EntryFee]);
				return SendClientMessage(playerid, COLOR_ERROR, string);
			}

			GivePlayerMoney(playerid, - bInfo[id][EntryFee]);
			bInfo[id][Till] += bInfo[id][EntryFee];

			mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `Till` = '%d' WHERE `ID` = '%d'", bInfo[id][Till], id + 1);
			mysql_tquery(mysql, string, "", "");

			SetPlayerPos(playerid, bInfo[id][iX], bInfo[id][iY], bInfo[id][iZ]);

			SetPlayerInterior(playerid, bInfo[id][iInterior]);
			SetPlayerVirtualWorld(playerid, bInfo[id][iVirtualWorld]);

			if(!bInfo[id][Bought])
				format(string, sizeof(string), "[INFO]:{E0E0E0} This business is for sale! You can purchase it for {5AA35F}$%d.", bInfo[id][Price]);

			else if(bInfo[id][Bought])
				format(string, sizeof(string), "[INFO]:{E0E0E0} This business is owned by %s.", bInfo[id][Owner]);

            SendClientMessage(playerid, COLOR_BLUE, string);
	    }

		else if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 2))
		{
			new id = ReturnNearestBizToPlayer(playerid, 3.0, 2);
			SetPlayerPos(playerid, bInfo[id][eX], bInfo[id][eY], bInfo[id][eZ]);

			SetPlayerInterior(playerid, bInfo[id][eInterior]);
			SetPlayerVirtualWorld(playerid, bInfo[id][eVirtualWorld]);
		}

	    else return false;
	}

	if(newkeys & KEY_NO)
	{
	    SendClientMessage(playerid, COLOR_INFO, "Hooked like a muhfucka");
	}
	return 1;
}


IsPlayerNearBusinessPoint(playerid, Float: range, type)
{
	if(type == 0 || type > 2 || type == -1)
		return printf("PID: %d just returned false when called \"IsPlayerNearBusinessPoint\" for type is %d and not 1 or 2.", playerid, type);

	switch(type)
	{
		case 1:
		{
			for(new i; i < MAX_BUSINESSES ; i ++) if(bizInit[i])
			{
			    if(IsPlayerInRangeOfPoint(playerid, range, bInfo[i][eX], bInfo[i][eY], bInfo[i][eZ]) &&
				GetPlayerVirtualWorld(playerid) == bInfo[i][eVirtualWorld] && GetPlayerInterior(playerid) == bInfo[i][eInterior])
			    {
			        return true;
			    }
			}
		}

		case 2:
		{
			for(new i; i < MAX_BUSINESSES ; i ++) if(bizInit[i])
		    {
			    if(IsPlayerInRangeOfPoint(playerid, range, bInfo[i][iX], bInfo[i][iY], bInfo[i][iZ]) &&
				GetPlayerVirtualWorld(playerid) == bInfo[i][iVirtualWorld] && GetPlayerInterior(playerid) == bInfo[i][iInterior])
			    {
			        return true;
			    }
			}
		}
	}

	return false;
}

ReturnNearestBizToPlayer(playerid, Float: range, type)
{
	if(type == 0 || type > 2 || type == -1)
		return printf("PID: %d just returned false when called \"IsPlayerNearBusinessPoint\" for type is %d and not 1 or 2.", playerid, type);

	switch(type)
	{
		case 1:
		{
			for(new i; i < MAX_BUSINESSES ; i ++) if(bizInit[i])
		    {
			    if(IsPlayerInRangeOfPoint(playerid, range, bInfo[i][eX], bInfo[i][eY], bInfo[i][eZ]) &&
				GetPlayerVirtualWorld(playerid) == bInfo[i][eVirtualWorld] && GetPlayerInterior(playerid) == bInfo[i][eInterior])
			    {
			        printf("Returned %d", i);
			        return i;
				}
			}
		}

		case 2:
		{
			for(new i; i < MAX_BUSINESSES ; i ++) if(bizInit[i])
		    {
			    if(IsPlayerInRangeOfPoint(playerid, range, bInfo[i][iX], bInfo[i][iY], bInfo[i][iZ]) &&
				GetPlayerVirtualWorld(playerid) == bInfo[i][iVirtualWorld] && GetPlayerInterior(playerid) == bInfo[i][iInterior])
			    {
			        return i;
				}
			}
		}
	}
   	printf("Returned %d", 999);
 	return 999;
}
