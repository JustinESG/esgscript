#include <YSI4\YSI_Coding\y_hooks>

#define DIALOG_FOOD_BUY      	(350)
#define DIALOG_BAR_BUY      	(351)
#define DIALOG_247_BUY      	(352)
#define DIALOG_AMMU_BUY      	(353)
#define DIALOG_CLOTH_BUY      	(354)
#define DIALOG_TOY_BUY      	(356)
#define DIALOG_HARDW_BUY      	(357)
#define DIALOG_ELECT_BUY      	(358)

#define MENU_CLOTHES_BUY        (350)

new bool: ConsumingItem[MAX_PLAYERS];

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	new string[120];

	if(dialogid == DIALOG_FOOD_BUY)
	{
	    if(response)
	    {
	        if(ConsumingItem[playerid])
	            return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're already consuming an item!");

	        switch(listitem)
	        {
	        	case 0: // Pizza
				{
				    if(GetPlayerMoneyEx(playerid) <= 150)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $150 for a pizza!");

				    TakePlayerMoney(playerid, 150);

					SetPlayerAttachedObject(playerid, 5, 2702, 6, 0.064, 0.00, 0.00, 70.1, 178.5, -17.84, 1.00, 1.00, 1.00);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 20.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a pizza for $150. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 1: // Hotdog
				{
				    if(GetPlayerMoneyEx(playerid) <= 200)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $200 for a hotdog!");

				    TakePlayerMoney(playerid, 200);

			 		SetPlayerAttachedObject(playerid, 5, 19346, 6, 0.058, 0.00, 0.00, 93.0, 0.00, 0.00, 1.00, 0.697, 1.00);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 30.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a hotdog for $200. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

			    case 2: // Burger
				{
				    if(GetPlayerMoneyEx(playerid) <= 250)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $250 for a burger!");

				    TakePlayerMoney(playerid, 250);

					SetPlayerAttachedObject(playerid, 5, 2703, 6, 0.068, 0.017, 0.00, 0.00, 169.6, 0.00, 0.775, 0.769, 0.715); //0.635, 0.443, 0.581);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 40.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a burger for $250. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 3: // Sprunk
				{
				    if(GetPlayerMoneyEx(playerid) <= 150)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $150 for a Sprunk!");

				    TakePlayerMoney(playerid, 150);

			 		SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_SPRUNK);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 25.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a Sprunk for $150. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 4: // Burger Cola
				{
				    if(GetPlayerMoneyEx(playerid) <= 300)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $300 for a Burger Shot Cola!");

				    TakePlayerMoney(playerid, 300);

					SetPlayerAttachedObject(playerid, 5, 2647, 6, 0.078, 0.00, 0.00, -14.44, 176.80, 1.8, 0.775, 0.769, 0.715);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 45.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a Burger Shot Cola for $300. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 5: // Beer
				{
				    if(GetPlayerMoneyEx(playerid) <= 500)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $500 for a beer!");

				    TakePlayerMoney(playerid, 500);

					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_BEER);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 10.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a beer for $500. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 6: // Wine
				{
				    if(GetPlayerMoneyEx(playerid) <= 500)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $500 for wine!");

				    TakePlayerMoney(playerid, 500);

					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_WINE);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 10.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought some wine for $500. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}
	        }

	       	ConsumingItem[playerid] = true;
		    ApplyAnimation(playerid, "FOOD", "EAT_Burger", 4.1, 1, 1, 1, 1, 1, 1);
	    }
	}

	if(dialogid == DIALOG_BAR_BUY)
	{
	    if(response)
	    {
	        if(ConsumingItem[playerid])
	            return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're already consuming an item!");

	        switch(listitem)
			{
				case 0: // Sprunk
				{
				    if(GetPlayerMoneyEx(playerid) <= 150)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $150 for a Sprunk!");

				    TakePlayerMoney(playerid, 150);

			 		SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_SPRUNK);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 25.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a Sprunk for $150. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 1: // Burger Cola
				{
				    if(GetPlayerMoneyEx(playerid) <= 300)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $300 for a Burger Shot Cola!");

				    TakePlayerMoney(playerid, 300);

					SetPlayerAttachedObject(playerid, 5, 2647, 6, 0.078, 0.00, 0.00, -14.44, 176.80, 1.8, 0.775, 0.769, 0.715);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 45.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a Burger Shot Cola for $300. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 2: // Beer
				{
				    if(GetPlayerMoneyEx(playerid) <= 500)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $500 for a beer!");

				    TakePlayerMoney(playerid, 500);

					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_BEER);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 10.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought a beer for $500. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}

				case 3: // Wine
				{
				    if(GetPlayerMoneyEx(playerid) <= 500)
				        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $500 for wine!");

				    TakePlayerMoney(playerid, 500);

					SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_WINE);
					SetTimerEx("ConsumeItem", 30000, false, "ii", playerid, 10.0);

					format(string, sizeof(string), "[INFO]{E0E0E0} You bought some wine for $500. You have $%d left.", GetPlayerMoneyEx(playerid));
					SendClientMessage(playerid, COLOR_BLUE, string);
				}
	        }

	       	ConsumingItem[playerid] = true;
		    ApplyAnimation(playerid, "FOOD", "EAT_Burger", 4.1, 1, 1, 1, 1, 1, 1);
	    }
	}

	if(dialogid == DIALOG_247_BUY)
	{
	    if(response)
	    {
	        switch(listitem)
	        {
	            case 0: // Mask
	            {
	                if(GetPlayerMoneyEx(playerid) >= 2500)
	                {
						TakePlayerMoney(playerid, 2500);

						Character[playerid][PlayerChar[playerid]][HasMask] = 1;

						new maskid = 1250 + random(658);
						Character[playerid][PlayerChar[playerid]] = maskid;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a mask for $2500. You have $%d left.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);

						if(Player[playerid][HasMask] != 0)
						{
							format(string, sizeof(string), "[INFO]{E0E0E0} You already have a mask! Your mask ID has been changed. It now is: %d. (/mask)", maskid);
						    SendClientMessage(playerid, COLOR_BLUE, string);
						}
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $2,500 for a mask!");
	            }

				case 1: // Medikit
	            {
	                if(GetPlayerMoneyEx(playerid) >= 3000)
	                {
	                	if(Player[playerid][Medikits] > 3)
							return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't hold more than three medikits!");

						TakePlayerMoney(playerid, 3000);
						Player[playerid][Medikits] ++;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a medikit for $3000. You have $%d left. Use it by typing /medikit.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $3,000 for a medikit!");
	            }

				case 2: // Boombox
				{
	                if(GetPlayerMoneyEx(playerid) >= 5000)
	                {
	                	if(HasBoombox[playerid])
							return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You already have a boombox! You can only hold one.");

						TakePlayerMoney(playerid, 5000);
						HasBoombox[playerid] = true;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a boombox for $5000. You have $%d left. Use it by typing /boombox.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $5,000 for a boombox!");
	            }

	            case 3:
	            {
	                if(GetPlayerMoneyEx(playerid) >= 500)
	                {
	                    if(Player[playerid][HasCiggy] > 10)
	                        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You already have a full pack of cigarettes!");

						TakePlayerMoney(playerid, 500);
						Player[playerid][HasCiggy] = 10;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a pack of cigarettes (10) for $500. You have $%d left. Use it by typing /ciggy.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
					}

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $500 for a pack of cigarettes!");
	            }
	        }
	    }
	}

	if(dialogid == DIALOG_HARDW_BUY)
	{
	    if(response)
	    {
	        switch(listitem)
	        {
	            case 0: //Radio
	            {
	                if(GetPlayerMoneyEx(playerid) >= 2000)
	                {
                    	if(Player[playerid][HasRadio])
							return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You already have a radio (walky-talky)!");

						TakePlayerMoney(playerid, 2000);

						Player[playerid][HasRadio] = 1;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a radio for $2000. You have $%d left. Use it by typing /r.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $2,000 for a radio!");
	            }

				case 1: // Car Battery
	            {
	                if(GetPlayerMoneyEx(playerid) >= 4500)
	                {
	                	if(Player[playerid][CarBattery] != 0)
							return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't hold more than one car battery!");

						TakePlayerMoney(playerid, 4500);
						Player[playerid][CarBattery] = 1;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a car battery for $4500. You have $%d left. Use it by typing /replacecarbattery.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $4,500 for a car battery!");
	            }
	        }
	    }
	}

	if(dialogid == DIALOG_ELECT_BUY)
	{
	    if(response)
	    {
	        switch(listitem)
	        {
	            case 0: // Phone
	            {
	                if(GetPlayerMoneyEx(playerid) >= 1000)
	                {
						TakePlayerMoney(playerid, 1000);

						Player[playerid][Phone] = 1;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a phone for $1000. You have $%d left. Use it by typing /phone.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);

                        new phonenr = 100050 + random(899850) + 2;

						if(Player[playerid][Phone] != 0)
						{
							format(string, sizeof(string), "[INFO]{E0E0E0} You already have phone! Your phone number has been changed. It now is: %d.", phonenr);
						    SendClientMessage(playerid, COLOR_BLUE, string);
						}
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $1,000 for a cell-phone!");
	            }

				case 1: //GPS
	            {
	                if(GetPlayerMoneyEx(playerid) >= 2500)
	                {
	                	if(Player[playerid][HasGPS] != 0)
							return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't hold more than one GPS!");

						TakePlayerMoney(playerid, 2500);
						Player[playerid][HasGPS] = 1;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a GPS for $2500. You have $%d left. Use it by typing /gps.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $2,500 for a GPS!");
	            }

				case 2: // MP3
				{
	               if(GetPlayerMoneyEx(playerid) >= 4000)
	                {
	                	if(Player[playerid][HasMP3] != 0)
							return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't hold more than one MP3!");

						TakePlayerMoney(playerid, 4000);
						Player[playerid][HasMP3] = 1;

						format(string, sizeof(string), "[INFO]{E0E0E0} You bought a MP3 for $4000. You have $%d left. Use it by typing /mp3.", GetPlayerMoneyEx(playerid));
						SendClientMessage(playerid, COLOR_BLUE, string);
	                }

	                else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $4000 for a MP3!");
				}
		    }
		}
	}
	return 1;
}

hook OnPlayerModelSelectionEx(playerid, response, extraid, modelid)
{
    if(extraid == MENU_CLOTHES_BUY)
    {
        if(response)
        {
			if(GetPlayerMoneyEx(playerid) <= 750)
				return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You need at least $750 to buy new clothes!");

			new string[120];

			SetPlayerSkin(playerid, modelid);
			TakePlayerMoney(playerid, 750);

			format(string, sizeof(string), "[INFO]{E0E0E0} You have been charged $750. You have $%d left. Please select your new skin.", GetPlayerMoneyEx(playerid));
			SendClientMessage(playerid, COLOR_BLUE, string);
        }
    }
	return 1;
}

forward ConsumeItem(playerid, Float: amount);
public ConsumeItem(playerid, Float: amount)
{
	new Float: health;
	GetPlayerHealth(playerid, health);

	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_NONE);
	ApplyAnimation(playerid, "goggles", "goggles_put_on", 4.1, 0, 0, 0, 0, 1, 1);

	RemovePlayerAttachedObject(playerid, 5);
	SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have consumed the item you just bought!");

	ConsumingItem[playerid] = false;

	if(health <= 60.0)
	{
	    SetPlayerHealth(playerid, health + amount);
	}

	else SetPlayerHealth(playerid, 100.0);

	return true;
}

///////////////////////////////////////////////////////////////////////////////
/////////                        BUY/SELL COMMANDS                    /////////
///////////////////////////////////////////////////////////////////////////////

CMD:buy(playerid, params[])
{
	if(IsPlayerNearBusinessPoint(playerid, 5.0, 2))
	{
	    new id = ReturnNearestBizToPlayer(playerid, 5.0, 2);

	    if(id == 999)
	        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a business buy point!");

		switch(bInfo[id][Type])
		{
		    // Food store:
		    case 0:
		    {
		        return ShowPlayerDialog(playerid, DIALOG_FOOD_BUY, DIALOG_STYLE_LIST, "Business: Food Store", "Pizza ($15)\nHotdog ($20)\nBurger ($25)\nSprunk ($15)\nCoke ($30)\nBeer ($50)\nWine($50)", "Buy", "Cancel");
		    }

			// Bar/Club:
		    case 1:
		    {
		        return ShowPlayerDialog(playerid, DIALOG_BAR_BUY, DIALOG_STYLE_LIST, "Business: Bar/Club", "Sprunk ($15)\nCoke ($30)\nBeer ($50)\nWine($50)", "Buy", "Cancel");
		    }

			// Grocery Store:
		    case 2:
		    {
		        return ShowPlayerDialog(playerid, DIALOG_247_BUY, DIALOG_STYLE_LIST, "Business: Grocery Store", "Mask ($2500)\nMedikit ($3000)\nBoombox (50000)\nPack of Cigarettes ($500)", "Buy", "Cancel");
		    }

			// Ammunation:
		    case 3:
		    {
				return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This business type (Ammunation) has no commands associated to it!");
		    }

			// Clothing Store:
		    case 4:
			 {
			    new skins[299], skincount;

			    for(new i = 1; i < sizeof(skins); i ++)
			    {
			        if(!IsFactionSkin(i))
			        {
			        	skins[skincount ++] = i;
					}
				}

            	ShowModelSelectionMenuEx(playerid, skins, skincount, "Clothes", MENU_CLOTHES_BUY);
		    }

			// Toy Store:
		    case 5:
		    {
				if(EditingAttachedObject[playerid])
				    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're already editing an attached object!");

				ShowPlayerDialog(playerid, DIALOG_TOYSEL, DIALOG_STYLE_LIST, "Select a toy", "Watches\nGlasses\nCaps\nHats\nBandanas\nMasks\nHair", "Select", "Cancel");
		    }

			// Hardware Store:
		    case 6:
		    {
		        return ShowPlayerDialog(playerid, DIALOG_HARDW_BUY, DIALOG_STYLE_LIST, "Business: Hardware Store", "Radio ($2000)\nCar Battery ($4500)", "Buy", "Cancel");
		    }

			// Electronics Store:
		    case 7:
		    {
		        return ShowPlayerDialog(playerid, DIALOG_ELECT_BUY, DIALOG_STYLE_LIST, "Business: Electronics", "Cellphone ($1000)\nGPS ($2500)\nMP3 ($4000)", "Buy", "Cancel");
		    }
		}
	}

	return true;
}
