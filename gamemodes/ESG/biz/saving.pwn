#define MAX_BUSINESSES 40

enum BizInfo
{
	ID,

	Name[MAX_PLAYER_NAME],
	Street,

	Owner[MAX_PLAYER_NAME],
	Bought,

	EntryFee,
	Till,
	Locked,

	Type,
	Price,

	Float: eX,
	Float: eY,
	Float: eZ,

	eInterior,
	eVirtualWorld,

	Float: iX,
	Float: iY,
	Float: iZ,

	iInterior,
	iVirtualWorld,
	iType
};

new bInfo[MAX_BUSINESSES][BizInfo];

// Note to self: bCount has to be 1 to prevent duplicate PRIMARY key errors
new bCount = 1, Text3D: bLabel[MAX_BUSINESSES];
new bool: bizInit[MAX_BUSINESSES];


enum bArrayInfo
{
	TypeID,
	TypeName[24]
};

new bizTypeName[][bArrayInfo] =
{
	{0, "Food Store"},
	{1, "Bar/Club"},
	{2, "Grocery Store"},
	{3, "Ammunation"},
	{4, "Clothing Store"},
	{5, "Toy Store"},
	{6, "Hardware Store"},
	{7, "Electronics Store"}
};

enum bInteriorInfo
{
	TypeID,
	SelID,

	Float: tyX,
	Float: tyY,
	Float: tyZ,

	tyInt
};

new bIntInfo[][bInteriorInfo] =
{
	// Food Ints
	{0,     1, 		375.962463,		-65.816848,		1001.507812,    10},
	{0,     2, 		369.579528,		-4.487294,		1001.858886,    9},
	{0,     3, 		373.825653,		-117.270904,	1001.499511,    5},
	{0,     4, 		381.169189,		-188.803024,	1000.632812,    17},

	// Bar/Clubs
	{1,     1, 		457.304748,		-88.428497,		999.55468,    	4},
	{1,     2, 		-227.027999,	1401.229980,	27.765625,    	18},
	{1,     3,		501.980987,		-69.150199,		998.757812,    	11},
	{1,     4, 		493.390991,		-22.722799,		1000.679687,    17},

	// 24/7s
	{2, 	1, 		-25.884498, 	-185.868988, 	1003.546875, 	17},
	{2, 	2, 		6.091179, 		-29.271898, 	1003.549438, 	10},
	{2, 	3, 		-30.946699, 	-89.609596, 	1003.546875, 	18},
	{2, 	4, 		-25.132598, 	-139.066986, 	1003.546875, 	16},

	// Ammu-nations
	{3,     1,      286.148986,		-40.644397,		1001.515625, 	1},
	{3,     2,      286.800994,		-82.547599,		1001.515625,    4},
	{3,		3,    	296.919982,		-108.071998,	1001.515625, 	6},
	{3,     4,  	314.820983,		-141.431991,	999.601562,     7},
	{3,     5,      316.524993,		-167.706985,	999.593750,     6},

	// Clothing Stores
	{4,     1,      207.737991,		-109.019996,	1005.132812, 	15},
	{4,     2,      204.332992,		-166.694992,	1000.523437, 	14},
	{4,     3,      207.054992,		-138.804992,	1003.507812, 	3},
	{4,     4,      203.777999,		-48.492397,		1001.804687,    1},
	{4,     5,      226.293991,		-7.431529,		1002.210937, 	5},
	{4,     6,      161.391006,		-93.159156,		1001.804687, 	18},
	{5,     7,      1956.5640, 		-2251.7424, 	13.1254,        2},

	// Toy Store
	{5,     1,      1956.5640, 		-2251.7424, 	13.1254,        2},

	// Hardware Store
	{6,     1,      1718.7136, 		-2457.1069, 	13.2640,        5},

	// Electronic Store
	{7,     1,      1718.7136, 		-2457.1069, 	13.2640,        5},
	{7,     2,     	1951.8973, 		-2311.6238, 	13.1851,        2}
};

SQL_LoadBizzes()
{
	mysql_function_query(mysql, "SELECT * FROM `businesses`", true, "SQL_LoadBusiness", "");
}

forward SQL_LoadBusiness();
public SQL_LoadBusiness()
{
	new rows, fields;
	cache_get_data(rows, fields, mysql);

	if(!cache_num_rows()) return printf("[BUSINESS] cache_get_row_count returned false. There are no businesses to load.\n");
	printf("[BUSINESSES] cache_get_row_count returned %d rows (businesses) to load.\n", cache_get_row_count());

	for(new i = 0; i != cache_get_row_count(); i++)
	{
	   	bInfo[i][ID] = 				bCount;

	    cache_get_field_content(i, "Name", bInfo[i][Name], mysql, MAX_PLAYER_NAME);
	    cache_get_field_content(i, "Owner", bInfo[i][Owner], mysql, MAX_PLAYER_NAME);

	    bInfo[i][Street] = 			cache_get_field_content_int(i, "Street");
	    bInfo[i][Bought] = 			cache_get_field_content_int(i, "Bought");

	    bInfo[i][EntryFee] = 		cache_get_field_content_int(i, "EntryFee");
	    bInfo[i][Till] = 			cache_get_field_content_int(i, "Till");
	    bInfo[i][Locked] = 			cache_get_field_content_int(i, "Locked");

	    bInfo[i][Type] = 			cache_get_field_content_int(i, "Type");
	    bInfo[i][Price] = 			cache_get_field_content_int(i, "Price");

		bInfo[i][eX] = 				cache_get_field_content_float(i, "eX");
		bInfo[i][eY] = 				cache_get_field_content_float(i, "eY");
		bInfo[i][eZ] = 				cache_get_field_content_float(i, "eZ");

	    bInfo[i][eInterior] = 		cache_get_field_content_int(i, "eInterior");
	    bInfo[i][eVirtualWorld] = 	cache_get_field_content_int(i, "eVirtualWorld");

		bInfo[i][iX] = 				cache_get_field_content_float(i, "iX");
		bInfo[i][iY] = 				cache_get_field_content_float(i, "iY");
		bInfo[i][iZ] = 				cache_get_field_content_float(i, "iZ");

	    bInfo[i][iInterior] = 		cache_get_field_content_int(i, "iInterior");
	    bInfo[i][iVirtualWorld] = 	cache_get_field_content_int(i, "iVirtualWorld");
	    bInfo[i][iType] = 			cache_get_field_content_int(i, "iType");

       	printf("Loaded business ID %d (%s) with type %d.", bCount, bInfo[i][Name], bInfo[i][Type]);

		bizInit[i] = true; // Use a var instead of enum

		ReloadBusinessLabel(i);
		bCount ++;
	}

	printf("\n[BUSINESSES] Loaded %d businesses!\n", cache_get_row_count());

	return 1;
}

CMD:bizcreate(playerid, params[])
{
	new string[90], name[24], zonename[16], type, inttype, price, street, Float: X, Float: Y, Float: Z, Float: jX, Float: jY, Float: jZ, jWorld, jViWorld;

	if(Account[playerid][Admin] < 3) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "s[24]iii", name, type, inttype, price)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizcreate [name] [type] [interior (relevant to type)] [price]");
	if(type > 7) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} There are only seven business types!");

	switch(type)
	{
	    case 0 .. 2: if(inttype > 4) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than four interiors.");
	    case 3: if(inttype > 5) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than five interiors.");
	    case 4: if(inttype > 7) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than seven interiors.");
		case 5: if(inttype > 1) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than one interiors.");
		case 6: if(inttype > 1) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than one interiors.");
		case 7: if(inttype > 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than two interiors.");
	}

	GetPlayerPos(playerid, X, Y, Z);
	street = 200 + random(300);

	for(new i; i < sizeof(bIntInfo); i ++)
	{
		if(bIntInfo[i][TypeID] == type && bIntInfo[i][SelID] == inttype)
		{
			jX = 		bIntInfo[i][tyX];
		    jY = 		bIntInfo[i][tyY];
		    jZ = 		bIntInfo[i][tyZ];

			jWorld = 	bIntInfo[i][tyInt];
			jViWorld =  30 + random(70);
		}
	}

	GetZone(bInfo[bCount][eX], bInfo[bCount][eY], zonename);

	format(string, sizeof(string), "[%d] %s{F2F2F2}\n[%s]\n[%d, %s]", bCount, name, bizTypeName[type][TypeName], street, zonename);
	bLabel[bCount] = CreateDynamic3DTextLabel(string, 0x00F2FF88, X, Y, Z, 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), -1, 10.0);

	SQL_AddBusiness(name, street, type, price, X, Y, Z, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid), jX, jY, jZ, jWorld, jViWorld, type);

	format(string, sizeof(string), "%f %f %f", X, Y, Z);
	SendClientMessage(playerid, COLOR_ERROR, string), print(string);

	return true;
}

SQL_AddBusiness(name[], street, type, price, Float: ex, Float: ez, Float: ey, eint, evw, Float: ix, Float: iy, Float: iz, iint, ivw, itype)
{
	new query[500];

	format(bInfo[bCount][Name], MAX_PLAYER_NAME, "%s", name);
	bInfo[bCount][Street] = street;

	format(bInfo[bCount][Owner], MAX_PLAYER_NAME, "None");
	bInfo[bCount][Bought] = 0;

	bInfo[bCount][EntryFee] = 0;
	bInfo[bCount][Till] = 0;
	bInfo[bCount][Locked] = 0;

	bInfo[bCount][Type] = type;
	bInfo[bCount][Price] = price;

	bInfo[bCount][eX] = ex;
	bInfo[bCount][eY] = ey;
	bInfo[bCount][eZ] = ez;

	bInfo[bCount][eInterior] = eint;
	bInfo[bCount][eVirtualWorld] = evw;

	bInfo[bCount][iX] = ix;
	bInfo[bCount][iY] = iy;
	bInfo[bCount][iZ] = iz;

	bInfo[bCount][iInterior] = iint;
	bInfo[bCount][iVirtualWorld] = ivw;
	bInfo[bCount][iType] = itype;

	bizInit[bCount] = true;

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `businesses` (`ID`, `Name`, `Owner`, `Bought`, `EntryFee`, `Till`, `Locked`, `Street`, `Type`, `Price`, `eX`, `eY`, `eZ`, `eInterior`, `eVirtualWorld`");
	mysql_format(mysql, query, sizeof(query), "%s, `iX`, `iY`, `iZ`, `iInterior`, `iVirtualWorld`, `iType`) VALUES (%d, '%s', 'None', 0, 0, 0, 0, %d, %d, %d, %f, %f, %f, %d, %d, %f, %f, %f, %d, %d, %d)",

		query,

		bCount, name, street, type, price,
		ex, ez, ey, eint, evw, ix, iy, iz, iint, ivw,

		itype
	);

	mysql_tquery(mysql, query, "", "");

	bCount ++;

	return true;
}

ReloadBusinessLabel(id)
{
	new string[100], zonename[16];

    GetZone(bInfo[id][eX], bInfo[id][eY], zonename);

	// Note to self: streamer info is saved in the plugin memory, not in the
	// actual filter script. Removing them on OnFS_Exit isn't mandatory

    if (IsValidDynamic3DTextLabel(bLabel[id]))
	    DestroyDynamic3DTextLabel(bLabel[id]);

	format(string, sizeof(string), "[%d] %s{F2F2F2}\n[%s]\n[%d, %s]", bInfo[id][ID], bInfo[id][Name], bizTypeName[bInfo[id][iType]][TypeName], bInfo[id][Street], zonename);
	bLabel[id] = CreateDynamic3DTextLabel(string, 0x00F2FF88, bInfo[id][eX], bInfo[id][eY], bInfo[id][eZ], 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, bInfo[id][eVirtualWorld], bInfo[id][eInterior], -1, 10.0);

	return true;
}
