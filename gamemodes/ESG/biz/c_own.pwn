CMD:bizsell(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	{
		new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), string[144], tempval;

		if(Player[playerid][BizOwner] == id && bInfo[id][Bought] != 0)
		{
		    bInfo[id][Owner] = EOS;
		    bInfo[id][Bought] = 0;

			tempval = bInfo[id][Price] / 50;
			GivePlayerMoney(playerid, tempval);

			format(string, sizeof(string), "[INFO]:{E0E0E0} You have just sold your business: %s (%d) and got 50 percent of the market price in return. ($%d)", bInfo[id][Name], id, tempval);
			SendClientMessage(playerid, COLOR_BLUE, string);

			mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `Owner` = '0', `Bought` = '0' WHERE `ID` = '%d'", id + 1);
			mysql_tquery(mysql, string, "", "");

			Player[playerid][BizOwner] = 999;
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You don't own this business!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You are either not near a business entrance or you are driving a vehicle.");

	return true;
}

CMD:bizbuy(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	{
		new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), string[120];

		if(bInfo[id][Price] > GetPlayerMoney(playerid))
        	return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You don't have enough money to purchase this business!");

		if(bInfo[id][Bought] == 1)
        	return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This business is already owned by someone!");

		if(Player[playerid][BizOwner] == 999)
        	return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You already own a business!");

		format(bInfo[id][Owner], MAX_PLAYER_NAME, Account[playerid][Name]);
		bInfo[id][Bought] = 1;

		GivePlayerMoney(playerid, - bInfo[id][Price]);
		Player[playerid][BizOwner] = id + 1;

		mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `Owner` = '%s', `Bought` = '1' WHERE `ID` = '%d'", Account[playerid][Name], id + 1);
		mysql_tquery(mysql, string, "", "");

		format(string, sizeof(string), "[INFO]:{E0E0E0} You have successfully bought %s (%d) for $%d! You can manage your business using /bizmanage.", bInfo[id][Name], id, bInfo[id][Price]);
		SendClientMessage(playerid, COLOR_BLUE, string);
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "You are either not near a business entrance or you are driving a vehicle.");

	return true;
}

///////////////////////////////////////////////////////////////////////////////
/////////                         MANAGING COMMANDS                   /////////
///////////////////////////////////////////////////////////////////////////////

CMD:bizfee(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	{
		new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), fee, string[95];

 	 	if(Player[playerid][BizOwner] == id)
		{
			if(sscanf(params, "i", fee)) return SendClientMessage(playerid, COLOR_ERROR, "/bizfee [fee]");
			if(fee > 2500 || fee < 500) return SendClientMessage(playerid, COLOR_ERROR, "Fee can't be less than $500 or higher than $2,500!");

			bInfo[id][EntryFee] = fee;

			format(string, sizeof(string), "[INFO]:{E0E0E0} You have set your business fee to \"%d\".", bInfo[id][EntryFee]);
			SendClientMessage(playerid, COLOR_BLUE, string);

			mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `EntryFee` = '%d' WHERE `ID` = '%d'", fee, id + 1);
			mysql_tquery(mysql, string, "", "");

			ReloadBusinessLabel(id);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You don't own this business!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You are either not near a business entrance or you are driving a vehicle.");

	return true;
}

CMD:biztill(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	{
		new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), amount, string[95];

  		if(Player[playerid][BizOwner] == id)
		{
			if(sscanf(params, "i", amount)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizname [amount]");
			if(amount > bInfo[id][Till]) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You don't have that much money in your business till!");

			bInfo[id][Till] -= amount;
			GivePlayerMoney(playerid, amount);

			format(string, sizeof(string), "[INFO]:{E0E0E0} You withdrawn \"$%d\" from your business till. You have \"$%d\" left.", amount, bInfo[id][Till]);
			SendClientMessage(playerid, COLOR_BLUE, string);

			mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `Till` = '%d' WHERE `ID` = '%d'", bInfo[id][Till], id + 1);
			mysql_tquery(mysql, string, "", "");

			ReloadBusinessLabel(id);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "You don't own this business!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "You are either not near a business entrance or you are driving a vehicle.");

	return true;
}

CMD:bizname(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	{
		new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), newname[MAX_PLAYER_NAME], string[95];

  		if(Player[playerid][BizOwner] == id)
		{
			if(sscanf(params, "s[24]", newname)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizname [name]");
			if(strlen(newname) > MAX_PLAYER_NAME) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} Name can't be longer than 24 characters!");

			// Format "inputtext" into the biz name var to avoid array error
			format(bInfo[id][Name], MAX_PLAYER_NAME, "%s", newname);

			mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `Name` = '%s' WHERE `ID` = '%d'", newname, id + 1);
			mysql_tquery(mysql, string, "", "");

			format(string, sizeof(string), "[INFI]:{E0E0E0} You have successfully changed your properties's (id: %d) name to \"%s\".", id, bInfo[id][Name]);
			SendClientMessage(playerid, COLOR_BLUE, string);

			ReloadBusinessLabel(id);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You don't own this business!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You are either not near a business entrance or you are driving a vehicle.");

	return true;
}

CMD:bizlock(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid) && IsPlayerNearBusinessPoint(playerid, 3.0, 1))
	{
		new id = ReturnNearestBizToPlayer(playerid, 3.0, 1), string[65];

        if(Player[playerid][BizOwner] == id)
		{
		    switch(bInfo[id][Locked])
		    {
		      	case 0:
				{
				  	bInfo[id][Locked] = true;
					format(string, sizeof(string), "{D93232}locked{ffffff}");
				}

			    case 1:
				{
					bInfo[id][Locked] = false;
					format(string, sizeof(string), "{4DA83D}unlocked{ffffff}");
				}
			}

		    format(string, sizeof(string), "[INFO]:{E0E0E0} You have just %s your business.", string);
		    SendClientMessage(playerid, COLOR_BLUE, string);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You don't own this business!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You are either not near a business entrance or you are driving a vehicle.");

	return true;
}
