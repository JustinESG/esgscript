
CMD:bizsettype(playerid, params[])
{
	new string[90], query[200], bizid, type, inttype, Float: jX, Float: jY, Float: jZ, jWorld;

	if(Player[playerid][Admin] < 3) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "iii", bizid, type, inttype)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizsettype [id] [type] [interior (relevant to type)]");
	if(type > 7) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} There are only seven business types!");

	switch(type)
	{
	    case 0 .. 2: if(inttype > 4) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than four interiors.");
	    case 3: if(inttype > 5) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than five interiors.");
	    case 4: if(inttype > 6) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} This type doesn't support more than six interiors.");
	}

	for(new i; i < sizeof(bIntInfo); i ++)
	{
		if(bIntInfo[i][TypeID] == type && bIntInfo[i][SelID] == inttype)
		{
			jX = 		bIntInfo[i][tyX];
		    jY = 		bIntInfo[i][tyY];
		    jZ = 		bIntInfo[i][tyZ];

			jWorld = 	bIntInfo[i][tyInt];
		}
	}

	mysql_format(mysql, query, sizeof(query), "UPDATE `businesses` SET `Type` = '%d', iX = '%f', iY = '%f', iZ = '%f', `iInterior` = '%d' WHERE `ID` = '%d'", type, jX, jY, jZ, jWorld, bizid);
	mysql_tquery(mysql, query, "", "");

	bizid --;

	bInfo[bizid][Type] = type;
	bInfo[bizid][iType] = type;

	bInfo[bizid][iX] = jX;
	bInfo[bizid][iY] = jY;
	bInfo[bizid][iZ] = jZ;

	bInfo[bizid][iInterior] = jWorld;

	ReloadBusinessLabel(bizid);

	format(string, sizeof(string), "[INFO]:{E0E0E0} You have set business ID %d's type to %d with new int %d.", bizid, type, inttype);
	SendClientMessage(playerid, COLOR_BLUE, string);

	if(Player[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has changed business ID %d's type to %d with new int %d.", playerid, Account[playerid][Name], bizid, type, inttype);
		SendAdminMsgEx(string);
	}

	return true;
}

CMD:bizmove(playerid, params[])
{
	new bizid, string[130], Float: ppX, Float: ppY, Float: ppZ;

	if(Player[playerid][Admin] < 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "i", bizid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizmove [id]");
	if(!bizInit[bizid]) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} The ID you specified is not a valid business.");

	mysql_format(mysql, string, sizeof(string), "UPDATE `businesses` SET `eX` = '%f', `eY` = '%f', `eZ` = '%f' WHERE `ID` = '%d'", ppX, ppY, ppZ, bizid);
	mysql_tquery(mysql, string, "", "");

	bizid --;

	GetPlayerPos(playerid, ppX, ppY, ppZ);

	bInfo[bizid][eX] = 		ppX;
	bInfo[bizid][eY] = 		ppY;
	bInfo[bizid][eZ] = 		ppZ;

	bInfo[bizid][eInterior] = 		GetPlayerInterior(playerid);
	bInfo[bizid][eVirtualWorld] = 	GetPlayerVirtualWorld(playerid);

	ReloadBusinessLabel(bizid);

	format(string, sizeof(string), "[INFO]:{E0E0E0} You have moved biz ID %d to your position.", bizid);
	SendClientMessage(playerid, COLOR_BLUE, string);

	if(Player[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has moved to business ID %d to their location.", playerid, Account[playerid][Name], bizid);
		SendAdminMsgEx(string);
	}

	return true;
}

CMD:bizgoto(playerid, params[])
{
	new bizid, string[130];

	if(Player[playerid][Admin] < 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "i", bizid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizmove [id]");
	if(!bizInit[bizid]) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} The ID you specified is not a valid business.");

	bizid --;

	SetPlayerPos(playerid, bInfo[bizid][eX], bInfo[bizid][eY], bInfo[bizid][eZ]);
	SetPlayerInterior(playerid, bInfo[bizid][eInterior]);
	SetPlayerVirtualWorld(playerid, bInfo[bizid][eVirtualWorld]);

	format(string, sizeof(string), "[INFO]:{E0E0E0} You have teleported to business ID %d.", bizid);
	SendClientMessage(playerid, COLOR_BLUE, string);

	if(Player[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has teleported to business ID %d.", playerid, Account[playerid][Name], bizid);
		SendAdminMsgEx(string);
	}

	return true;
}

// NOTE: Don't use /bizdelete until you know how to reset AI.
CMD:bizdelete(playerid, params[])
{
	SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} Command variable is set to UNUSED.");

/*	new bizid, string[130];

	if(Player[playerid][Admin] < 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "i", bizid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /bizmove [id]");
	if(!bizInit[bizid]) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} The ID you specified is not a valid business.");

	mysql_format(mysql, string, sizeof(string), "DELETE FROM `businesses` WHERE ID = '%d'", bizid + 1);
	mysql_tquery(mysql, string, "", "");

	bizid --;

	format(string, sizeof(string), "[INFO]:{E0E0E0} You have deleted business ID %d.", bizid);
	SendClientMessage(playerid, COLOR_BLUE, string);

	DestroyDynamic3DTextLabel(bLabel[bizid]);

	new x[BizInfo];
	bInfo[bizid] = x;

	bizInit[bizid] = false;

	if(Player[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has removed business ID %d.", playerid, Account[playerid][Name], bizid);
		SendAdminMsgEx(string);
	}
*/
	return true;
}

CMD:forcebizreparse(playerid, params[])
{
	new string[122];

	SendClientMessage(playerid, COLOR_BLUE, "Business list reparse:");

	for(new i; i < MAX_BUSINESSES; i ++)
	{
	    if(bizInit[i])
	    {
		    format(string, sizeof(string), "[BIZ]:{E0E0E0} %d ; %s ; %s ; %d ; %d", bInfo[i][ID], bInfo[i][Name], bInfo[i][Owner], bInfo[i][Type], bInfo[i][Price], bInfo[i][EntryFee]);
		    SendClientMessage(playerid, COLOR_BLUE, string);
		}
	}

	return true;
}
