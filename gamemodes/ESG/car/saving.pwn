#undef MAX_VEHICLES
#define MAX_VEHICLES    (750)

// NOTE TO SELF: Enums start from 0, vehicles start from 1.
// VehSlot1 ID 0 works for vehicle ID 1. While VehSlot2
// ID 3 works for ID 4.

enum VehicleInfo
{
	ID,
	ModelID,
	FactionID,

	Owner[MAX_PLAYER_NAME],
	LicensePlate[10],
	Type,
	Locked,

	Float: vX,
	Float: vY,
	Float: vZ,
	Float: vA,

	Color1,
	Color2,

	fuel,
	alarm, // level 1 = 60 sec (5k), level 2 = 90 (10k), level 3 = 120 (15k)
	insurance,
	battery,
	explodes,

	gunSlot1,
	gunSlotAmmo1,
	gunSlot2,
	gunSlotAmmo2,
	gunSlot3,
	gunSlotAmmo3,

	HasMods,
	PaintjobID,

	modComponent1, modComponent2, modComponent3, modComponent4, modComponent5, modComponent6,
	modComponent7, modComponent8, modComponent9, modComponent10, modComponent11, modComponent12,
	modComponent13, modComponent14, modComponent15, modComponent16, modComponent17
};

new Vehicle[MAX_VEHICLES][VehicleInfo], vCount;


CreateStaticVehicle(playerid, modelid, owner[MAX_PLAYER_NAME], type, licenseplate[10], Float: vXx, Float: vYx, Float: vZx, Float: vAx, col1, col2)
{
	new query[800];

	if(playerid == 999)
	    printf("Vehicle with no owner created!");

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `vehicles` (`ModelID`, `FactionID`, `Owner`, `LicensePlate`, `Type`, `Locked`, `vX`, `vY`, `vZ`, `vA`, `Color1`, `Color2`");
	mysql_format(mysql, query, sizeof(query), "%s, `fuel`, `insurance`, `explodes`, `battery`, `gunSlot1`, `gunSlot2`, `gunSlot3`, `HasMods`, `PaintjobID`", query);

	mysql_format(mysql, query, sizeof(query), "%s, `modComponent1`, `modComponent2`, `modComponent3`, `modComponent4`, `modComponent5`, `modComponent6`, `modComponent7`", query);
	mysql_format(mysql, query, sizeof(query), "%s, `modComponent8`, `modComponent9`, `modComponent10`, `modComponent11`, `modComponent12`, `modComponent13`, `modComponent14`", query);
	mysql_format(mysql, query, sizeof(query), "%s, `modComponent15`, `modComponent16`, `modComponent17`, `alarm`) VALUES ('%d', '998', '%s', '%s', '%d', 0, '%f', '%f', '%f', '%f', '%d', '%d'", query, modelid, owner, licenseplate, type, vXx, vYx, vZx, vAx, col1, col2);

	mysql_format(mysql, query, sizeof(query), "%s, 100, 2, 0, 100, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)", query);

	mysql_tquery(mysql, query);

	Vehicle[vCount][ModelID] = 		modelid;
	Vehicle[vCount][Owner] = 		owner;
	Vehicle[vCount][LicensePlate] = licenseplate;
	Vehicle[vCount][Type] = 		type;

	Vehicle[vCount][vX] = vXx;
	Vehicle[vCount][vY] = vYx;
	Vehicle[vCount][vZ] = vZx;
	Vehicle[vCount][vA] = vAx;

	Vehicle[vCount][Color1] = col1;
	Vehicle[vCount][Color2] = col2;

	Vehicle[vCount][PaintjobID] = 3; // 3 = no paintjobid

	Vehicle[vCount][insurance] = 2;
	Vehicle[vCount][fuel] = 	100;
	Vehicle[vCount][battery] = 	100;

	new tempvar = CreateVehicle(modelid, vXx, vYx, vZx, vAx, col1, col2, -1);
	SetVehicleNumberPlate(tempvar, licenseplate);
	SetVehicleToRespawn(tempvar);

	if(type == 2)
	{
		if(Character[playerid][VehSlot1] == 999)
	    	Character[playerid][VehSlot1] = vCount;

		else if(Character[playerid][VehSlot1] != 999 && Character[playerid][VehSlot2] == 999)
			Character[playerid][VehSlot2] = vCount;
	}

	printf("Added vehicle ID %d with type %d at x: %f y: %f z: %f", vCount, type, vXx, vYx, vZx, vAx);

	vCount ++;

	return true;
}

SQL_LoadVehicles()
{
	mysql_function_query(mysql, "SELECT * FROM `vehicles`", true, "SQL_LoadVehicle", "");
}

forward SQL_LoadVehicle();
public SQL_LoadVehicle()
{
	new rows, fields;
	cache_get_data(rows, fields, mysql);

	if(!cache_num_rows()) return printf("[VEHICLES] cache_get_row_count returned false. There are no vehicles to load.\n");
	printf("[VEHICLES] cache_get_row_count returned %d rows (vehicles) to load.\n", cache_get_row_count());

 	for(new i; i != cache_get_row_count(); i++)
	{
	   	Vehicle[i][ID] =                cache_insert_id();
	   	Vehicle[i][ModelID] = 			cache_get_field_content_int(i, "ModelID");
	   	Vehicle[i][FactionID] = 		cache_get_field_content_int(i, "FactionID");

	    cache_get_field_content(i, "Owner", 		Vehicle[i][Owner], mysql, MAX_PLAYER_NAME);
	    cache_get_field_content(i, "LicensePlate",	Vehicle[i][LicensePlate], mysql, 10);

        Vehicle[i][Type] = 				cache_get_field_content_int(i, "Type");
        Vehicle[i][Locked] = 			cache_get_field_content_int(i, "Locked");

        Vehicle[i][vX] = 				cache_get_field_content_float(i, "vX");
        Vehicle[i][vY] = 				cache_get_field_content_float(i, "vY");
        Vehicle[i][vZ] = 				cache_get_field_content_float(i, "vZ");
        Vehicle[i][vA] = 				cache_get_field_content_float(i, "vA");

        Vehicle[i][Color1] = 			cache_get_field_content_int(i, "Color1");
        Vehicle[i][Color2] = 			cache_get_field_content_int(i, "Color2");

        Vehicle[i][fuel] = 				cache_get_field_content_int(i, "fuel");
        Vehicle[i][alarm] = 			cache_get_field_content_int(i, "alarm");
        Vehicle[i][insurance] = 		cache_get_field_content_int(i, "insurance");
        Vehicle[i][battery] = 			cache_get_field_content_int(i, "battery");
        Vehicle[i][explodes] = 			cache_get_field_content_int(i, "explodes");

        Vehicle[i][gunSlot1] = 			cache_get_field_content_int(i, "gunSlot1");
        Vehicle[i][gunSlot2] = 			cache_get_field_content_int(i, "gunSlot2");
        Vehicle[i][gunSlot3] = 			cache_get_field_content_int(i, "gunSlot3");

		Vehicle[i][HasMods] =           cache_get_field_content_int(i, "HasMods");
		Vehicle[i][PaintjobID] =       	cache_get_field_content_int(i, "PaintjobID");

		Vehicle[i][modComponent1] = 	cache_get_field_content_int(i, "modComponent1");
		Vehicle[i][modComponent2] = 	cache_get_field_content_int(i, "modComponent2");
		Vehicle[i][modComponent3] = 	cache_get_field_content_int(i, "modComponent3");
		Vehicle[i][modComponent4] = 	cache_get_field_content_int(i, "modComponent4");
		Vehicle[i][modComponent5] = 	cache_get_field_content_int(i, "modComponent5");
		Vehicle[i][modComponent6] = 	cache_get_field_content_int(i, "modComponent6");
		Vehicle[i][modComponent7] = 	cache_get_field_content_int(i, "modComponent7");
		Vehicle[i][modComponent8] = 	cache_get_field_content_int(i, "modComponent8");
		Vehicle[i][modComponent9] = 	cache_get_field_content_int(i, "modComponent9");
		Vehicle[i][modComponent10] = 	cache_get_field_content_int(i, "modComponent10");
		Vehicle[i][modComponent11] = 	cache_get_field_content_int(i, "modComponent11");
		Vehicle[i][modComponent12] = 	cache_get_field_content_int(i, "modComponent12");
		Vehicle[i][modComponent13] = 	cache_get_field_content_int(i, "modComponent13");
		Vehicle[i][modComponent14] = 	cache_get_field_content_int(i, "modComponent14");
		Vehicle[i][modComponent15] = 	cache_get_field_content_int(i, "modComponent15");
		Vehicle[i][modComponent16] = 	cache_get_field_content_int(i, "modComponent16");
		Vehicle[i][modComponent17] = 	cache_get_field_content_int(i, "modComponent17");

		printf("Loaded vehicle ID %d with model %d, type %d and license plate %s", i, Vehicle[i][ModelID], Vehicle[i][Type], Vehicle[i][LicensePlate]);

		new vid = CreateVehicle(Vehicle[i][ModelID], Vehicle[i][vX], Vehicle[i][vY], Vehicle[i][vZ], Vehicle[i][vA], Vehicle[i][Color1], Vehicle[i][Color2], -1);
		SetVehicleNumberPlate(vid, Vehicle[i][LicensePlate]);
		SetVehicleToRespawn(vid);

        SetVehicleParamsEx(vid, false, false, false, false, false, false, false);
		ChangeVehicleColor(vid, Vehicle[i][Color1], Vehicle[i][Color2]);

		if(Vehicle[i][Locked])
		{
		    SetVehicleParamsEx(vid, false, false, false, true, false, false, false);
		}

		if(Vehicle[i][HasMods])
		{
		    ApplyModsToVehicle(vid, vid);
		}

        vCount ++;
	}

	printf("\n[VEHICLES] All %d rows (vehicles) have been loaded.\n", cache_get_row_count());

	return true;
}

ApplyModsToVehicle(vehicleid, enumid)
{
    ChangeVehiclePaintjob(vehicleid, Vehicle[enumid][PaintjobID]);

	if(Vehicle[enumid][modComponent1] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent1]);

	if(Vehicle[enumid][modComponent2] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent2]);

	if(Vehicle[enumid][modComponent3] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent3]);

	if(Vehicle[enumid][modComponent4] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent4]);

	if(Vehicle[enumid][modComponent5] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent5]);

	if(Vehicle[enumid][modComponent6] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent6]);

	if(Vehicle[enumid][modComponent7] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent7]);

	if(Vehicle[enumid][modComponent8] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent8]);

	if(Vehicle[enumid][modComponent9] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent9]);

	if(Vehicle[enumid][modComponent10] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent10]);

	if(Vehicle[enumid][modComponent11] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent11]);

	if(Vehicle[enumid][modComponent12] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent12]);

	if(Vehicle[enumid][modComponent13] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent13]);

	if(Vehicle[enumid][modComponent14] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent14]);

	if(Vehicle[enumid][modComponent15] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent15]);

	if(Vehicle[enumid][modComponent16] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent16]);

	if(Vehicle[enumid][modComponent17] != 0)
	    AddVehicleComponent(vehicleid, Vehicle[enumid][modComponent17]);

	return true;
}

// Misc Functions:

GetNearestVehicleToPlayer(playerid)
{
	new Float: vXex, Float: vYex, Float: vZex;

	for(new i; i < MAX_VEHICLES; i ++)
	{
	    if(IsValidVehicle(i))
	    {
			GetVehiclePos(i, vXex, vYex, vZex);

			if(IsPlayerInRangeOfPoint(playerid, 2.0, vXex, vYex, vZex))
			{
			    return i;
			}
	    }
	}

	return true;
}

GetVehicleNameFromModelID(modelid)
{ // Credits to Y_Less, Edited by Mione
	static const scVehicleNames[][18] =
	{
		"Landstalker", "Bravura", "Buffalo", "Linerunner", "Perrenial", "Sentinel", "Dumper", "Firetruck", "Trashmaster", "Stretch", "Manana", "Infernus",
		"Voodoo", "Pony", "Mule", "Cheetah", "Ambulance", "Leviathan", "Moonbeam", "Esperanto", "Taxi", "Washington", "Bobcat", "Mr Whoopee", "BF Injection",
		"Hunter", "Premier", "Enforcer", "Securicar", "Banshee", "Predator", "Bus", "Rhino", "Barracks", "Hotknife", "Trailer 1", "Previon", "Coach", "Cabbie",
		"Stallion", "Rumpo", "RC Bandit", "Romero", "Packer", "Monster", "Admiral", "Squalo", "Seasparrow", "Pizzaboy", "Tram", "Trailer 2", "Turismo", "Speeder",
		"Reefer", "Tropic", "Flatbed", "Yankee", "Caddy", "Solair", "Berkley's RC Van", "Skimmer", "PCJ-600", "Faggio", "Freeway", "RC Baron", "RC Raider", "Glendale",
		"Oceanic", "Sanchez", "Sparrow", "Patriot", "Quad", "Coastguard", "Dinghy", "Hermes", "Sabre", "Rustler", "ZR-350", "Walton", "Regina", "Comet", "BMX",
		"Burrito", "Camper", "Marquis", "Baggage", "Dozer", "Maverick", "News Chopper", "Rancher", "FBI Rancher", "Virgo", "Greenwood", "Jetmax", "Hotring", "Sandking",
		"Blista Compact", "Police Maverick", "Boxville", "Benson", "Mesa", "RC Goblin", "Hotring Racer A", "Hotring Racer B", "Bloodring Banger", "Rancher", "Super GT", "Elegant", "Journey",
		"Bike", "Mountain Bike", "Beagle", "Cropdust", "Stunt", "Tanker", "Roadtrain", "Nebula", "Majestic", "Buccaneer", "Shamal", "Hydra", "FCR-900", "NRG-500", "HPV1000", "Cement Truck",
		"Tow Truck", "Fortune", "Cadrona", "FBI Truck", "Willard", "Forklift", "Tractor", "Combine", "Feltzer", "Remington", "Slamvan", "Blade", "Freight", "Streak", "Vortex", "Vincent", "Bullet",
		"Clover", "Sadler", "Firetruck LA", "Hustler", "Intruder", "Primo", "Cargobob", "Tampa", "Sunrise", "Merit", "Utility", "Nevada", "Yosemite", "Windsor", "Monster A",
		"Monster B", "Uranus", "Jester", "Sultan", "Stratum", "Elegy", "Raindance", "RC Tiger", "Flash", "Tahoma", "Savanna", "Bandito", "Freight Flat", "Streak Carriage", "Kart",
		"Mower", "Duneride", "Sweeper", "Broadway", "Tornado", "AT-400", "DFT-30", "Huntley", "Stafford", "BF-400", "Newsvan", "Tug", "Trailer 3", "Emperor", "Wayfarer", "Euros",
		"Hotdog", "Club", "Freight Carriage", "Trailer 3", "Andromada", "Dodo", "RC Cam", "Launch", "LS Police Car", "SF Police Car", "LV Police Car", "Police Ranger",
		"Picador", "S.W.A.T. Van", "Alpha", "Phoenix", "Glendale", "Sadler", "Luggage Trailer A", "Luggage Trailer B", "Stair Trailer", "Boxville", "Farm Plow", "Utility Trailer"
	}, scOnFoot[18] = "None";

	if (modelid > 0)
	{
	 	return scVehicleNames[modelid - 400];
	}

	else return scOnFoot;
}
