#include <YSI4\YSI_Coding\y_hooks>

enum GPSInfo
{
	gpsDetails[6],

	Float: gpsX,
	Float: gpsY,
	Float: gpsZ
};

new GPSArray[][GPSInfo] =
{
	{"Bank", 	1908.8239, -2326.9221, 13.6463},
	{"24/7", 	1983.3857, -2393.8784, 13.5469},
	{"UrNan", 	1983.3857, -2393.8784, 13.5469},
	{"Goobr", 	1983.3857, -2393.8784, 13.5469},
	{"Nerd", 	1983.3857, -2393.8784, 13.5469}
}, GPSObject[MAX_PLAYERS], GPSTimer[MAX_PLAYERS], GPSCP[MAX_PLAYERS], bool: UsingGPS[MAX_PLAYERS];

forward Float:PointAngle(playerid, Float:xa, Float:ya, Float:xb, Float:yb);


hook OnPlayerStateChange(playerid, newstate, oldstate)
{
    if(oldstate == PLAYER_STATE_DRIVER && newstate == PLAYER_STATE_ONFOOT && UsingGPS[playerid]) // Player entered a vehicle as a driver
    {
        SendClientMessage(playerid, COLOR_BLUE, "[GPS]:{E0E0E0} Your GPS only works inside of a vehicle.");

	    KillTimer(GPSTimer[playerid]);
        UsingGPS[playerid] = false;

 		DestroyDynamicCP(GPSCP[playerid]);
 		DestroyObject(GPSObject[playerid]);
    }
	return 1;
}

hook OnPlayerEnterDynamicCP(playerid, checkpointid)
{
	if(checkpointid == GPSCP[playerid])
	{
        SendClientMessage(playerid, COLOR_BLUE, "[GPS]:{E0E0E0} You have reached your destination.");

	    KillTimer(GPSTimer[playerid]);
        UsingGPS[playerid] = false;

 		DestroyDynamicCP(GPSCP[playerid]);
 		DestroyObject(GPSObject[playerid]);
	}
	return 1;
}

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	switch(dialogid)
	{
		case DIALOG_GPS:
		{
		    if(response)
		    {
			    GPSCP[playerid] = CreateDynamicCP(GPSArray[listitem][gpsX], GPSArray[listitem][gpsY], GPSArray[listitem][gpsZ], 5.0, 0, 0, -1, 400.0);
			    GPSTimer[playerid] = SetTimerEx("GPSTick", 1000, true, "ii", playerid, listitem);
			}
		}
	}
	return 1;
}


CMD:gps(playerid, params[])
{
	new string[700], zone[32];

	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You must be in a vehicle in order to use this command.");

	GPSObject[playerid] = CreateObject(1318, 0, 0, 0, 0.0, 0.0, 0);
	UsingGPS[playerid] = true;

	for(new i; i < sizeof(GPSArray); i ++)
	{
		GetZone(GPSArray[i][gpsX], GPSArray[i][gpsY], zone);
	    format(string, sizeof(string), "%s\n[%s] %s", string, zone, GPSArray[i][gpsDetails]);
	}

	return ShowPlayerDialog(playerid, DIALOG_GPS, DIALOG_STYLE_LIST, "Select your destination", string, "Select", "Cancel");
}

forward GPSTick(playerid, arrayid);
public GPSTick(playerid, arrayid)
{
	new Float: VPos[3], Float: Rotation;
	GetVehiclePos(GetPlayerVehicleID(playerid),VPos[0],VPos[1],VPos[2]);

	Rotation = PointAngle(playerid, VPos[0], VPos[1], GPSArray[arrayid][gpsX], GPSArray[arrayid][gpsY]);
	AttachObjectToVehicle(GPSObject[playerid], GetPlayerVehicleID(playerid), 0.0, 0.0, 1.0, 0.0, 90.0, Rotation);

	return true;
}

// xa, ya = starting, xb, yb = destination
public Float: PointAngle(playerid, Float:xa, Float:ya, Float:xb, Float:yb)
{
	new Float:carangle, Float:xc, Float:yc, Float:angle;
	xc = floatabs(floatsub(xa,xb));
	yc = floatabs(floatsub(ya,yb));
	if (yc == 0.0 || xc == 0.0)
	{
		if(yc == 0 && xc > 0) angle = 0.0;
		else if(yc == 0 && xc < 0) angle = 180.0;
		else if(yc > 0 && xc == 0) angle = 90.0;
		else if(yc < 0 && xc == 0) angle = 270.0;
		else if(yc == 0 && xc == 0) angle = 0.0;
	}
	else
	{
		angle = atan(xc/yc);
		if(xb > xa && yb <= ya) angle += 90.0;
		else if(xb <= xa && yb < ya) angle = floatsub(90.0, angle);
		else if(xb < xa && yb >= ya) angle -= 90.0;
		else if(xb >= xa && yb > ya) angle = floatsub(270.0, angle);
	}
	GetVehicleZAngle(GetPlayerVehicleID(playerid), carangle);
	return floatadd(angle, -carangle);
}
