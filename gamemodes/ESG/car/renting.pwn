#include <YSI4\YSI_Coding\y_hooks>

new vRented[MAX_VEHICLES];
new bool: vRenting[MAX_PLAYERS];
new RentedVehicle[MAX_PLAYERS];

hook OnPlayerStateChange(playerid, newstate, oldstate)
{
	if(newstate == PLAYER_STATE_DRIVER)
	{
	    new vid = GetPlayerVehicleID(playerid), enumid = vid - 1;
	    
	    if(Vehicle[enumid][Type] == 1)
	    {
			if(!vRented[enumid])
			{
			    SetVehicleParamsEx(vid, false, false, false, false, false, false, false);
			    SendClientMessage(playerid, COLOR_BLUE, "[RENT CAR]{E0E0E0} This vehicle is a rental car! You can rent it by using /rentcar!");
			}

			else if(vRented[enumid]) return SendClientMessage(playerid, COLOR_ERROR, "[RENT CAR]{E0E0E0} This vehicle has already been rented. Wait until it gets respawned or the owner unrents it.");
	    }

	    if(Vehicle[enumid][Type] == 2)
	    {
			if(Character[playerid][VehSlot1] != enumid && Character[playerid][VehSlot2] != enumid)
				return SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You don't own this vehicle! Type /hotwire to start hotwiring it!");

			else if(Character[playerid][VehSlot1] == enumid || Character[playerid][VehSlot2] == enumid)
			    return SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Welcome back to your vehicle!");
		}

 	    if(Vehicle[enumid][Type] == 3)
	    {
			if(Character[playerid][FactionID] != Vehicle[enumid][FactionID])
				return SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You don't own this vehicle! Type /hotwire to start hotwiring it!");

			else if(Character[playerid][FactionID] == Vehicle[enumid][FactionID])
				return SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Your faction owns this vehicle, as thus you have full control of it.");
	    }
	}
	return 1;
}


CMD:rentcar(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new vid = GetPlayerVehicleID(playerid), enumid = vid - 1;

	if(vRented[enumid])
		return SendClientMessage(playerid, COLOR_ERROR, "[RENT CAR]{E0E0E0} This vehicle has already been rented. Wait until it gets respawned or the owner unrents it.");

	SetVehicleParamsEx(vid, true, false, false, false, false, false, false);

	vRented[enumid] = true;
	vRenting[playerid] = true;

	RentedVehicle[playerid] = vid;

	SendClientMessage(playerid, COLOR_BLUE, "[RENT CAR]{E0E0E0} You have successfully rented this vehicle. Make sure to /unrentcar it when you are done using it!");

	FuelTimer[vid] = SetTimerEx("FuelTimerTick", 1200000, true, "i", vid);
	BatteryTimer[vid] = SetTimerEx("BatteryTimerTick", 2700000, true, "i", vid);

	return true;
}

CMD:unrentcar(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new vid = GetPlayerVehicleID(playerid), enumid = vid - 1;

	if(!vRented[enumid])
		return SendClientMessage(playerid, COLOR_ERROR, "[RENT CAR]{E0E0E0} This vehicle isn't been rented yet!");

	if(vRenting[playerid] &&  RentedVehicle[playerid] == GetPlayerVehicleID(playerid))
	{
		vRented[enumid] = false;
		vRenting[playerid] = false;

		KillTimer(FuelTimer[vid]);
		KillTimer(BatteryTimer[vid]);

		SetVehicleToRespawn(vid);
		RentedVehicle[playerid] = 0;
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[RENT CAR]{E0E0E0} You're not renting this vehicle!");

	return true;
}

