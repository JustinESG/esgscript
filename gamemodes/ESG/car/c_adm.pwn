CMD:carreparse(playerid, params[])
{
	new string[80];

	if(Account[playerid][Admin] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not an administrator!");

	for(new i; i < 5;  i ++)
	{
        format(string, sizeof(string), "ID: %d Veh ID: %d Name: %s  Alarm: %d Insur: %d Explodes: %d Fuel: %d Battery: %d", i, i +1, GetVehicleNameFromModelID(GetVehicleModel(i + 1)), Vehicle[i][alarm], Vehicle[i][insurance], Vehicle[i][explodes], Vehicle[i][fuel], Vehicle[i][battery]);
		SendClientMessage(playerid, -1, string);
	}

	return true;
}

CMD:gotocar(playerid, params[])
{
	new id;

	if(Account[playerid][Admin] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not an administrator!");

	if(sscanf(params, "i", id))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /gotocar [id]");

	if(!IsValidVehicle(id))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This vehicle doesn't exist!");

	new Float: vx, Float: vy, Float: vz;
	GetVehiclePos(id, vx, vy, vz);

	SetPlayerPos(playerid, vx, vy, vz);

	return true;
}

CMD:entercar(playerid, params[])
{
	new vehicleid, seatid, str[128];
	
	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "ii", vehicleid, seatid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /entercar [vehicleid] [seat: 0 = driver, 1-3 = passenger]");
	
	if(!IsValidVehicle(vehicleid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} The vehicle ID you entered does not exist!");
	
	if(seatid > 3 && GetVehicleModel(vehicleid) != 431) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} There aren't more than four seats in a vehicle!");
	
	
	PutPlayerInVehicle(playerid, vehicleid, seatid);
	
	format(str, sizeof(str), 
		"[AMSG] (%d) %s has entered vehicle ID %d's seat %d.", 
		playerid, Account[playerid][Name], vehicleid, seatid);
	SendAdminMsgEx(str);
	
	return true;
}

CMD:putplayerincar(playerid, params[])
{
	new targetid, vehicleid, seatid, str[128];
	
	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "uii", targetid, vehicleid, seatid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /putplayerincar [playerid/name] [vehicleid] [seat: 0 = driver, 1-3 = passenger]");
	
	if(!IsValidVehicle(vehicleid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} The vehicle ID you entered does not exist!");
	
	if(seatid > 3 && GetVehicleModel(vehicleid) != 431) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} There aren't more than four seats in a vehicle!");
	
	
	PutPlayerInVehicle(targetid, vehicleid, seatid);
	
	format(str, sizeof(str), 
		"[INFO]:{E0E0E0} Admin (%d) %s has just put you inside of vehicle ID %d.", 
		playerid, Account[playerid][Name], vehicleid);
	SendClientMessage(targetid, COLOR_BLUE, str);

	format(str, sizeof(str), 
		"[AMSG] (%d) %s has put player (%d) %s inside of vehicle ID %d's seat %d.", 
		playerid, Account[playerid][Name], targetid, Account[targetid][Name], vehicleid, seatid);
	SendAdminMsgEx(str);
	
	return true;
}

CMD:getvehicle(playerid, params[])
{
	new 
		vehicleid, 
		str[128], 
		Float: pX, Float: pY, Float: pZ, 
		driverid = GetVehicleDriver(vehicleid);

	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");

	if(sscanf(params, "i", vehicleid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /getvehicle [vehicleid]");
	
	if(!IsValidVehicle(vehicleid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} The vehicle ID you entered does not exist!");
	
	
	GetPlayerPos(playerid, pX, pY, pZ);
	SetVehiclePos(vehicleid, pX, pY, pZ);
	
	if(IsVehicleOccupied(vehicleid))
	{
	    PutPlayerInVehicle(driverid, vehicleid, 0);

	    format(str, sizeof(str), 
			"[INFO]:{E0E0E0} The vehicle you are in has been teleported to admininistrator (%d) %s.", 
			playerid, Account[playerid][Name]);
		SendClientMessage(driverid, COLOR_BLUE, str);
	}

	format(str, sizeof(str), 
		"[AMSG] (%d) %s has just teleported vehicle ID %d to himself.", 
		playerid, Account[playerid][Name], vehicleid);
	SendAdminMsgEx(str);

	return true;
}

CMD:gotovehicle(playerid, params[])
{
	new 
		vehicleid, 
		str[128], 
		Float: pX, Float: pY, Float: pZ;
	
	if(!Account[playerid][Admin])
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "i", vehicleid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /gotovehicle [vehicleid]");
	
	if(!IsValidVehicle(vehicleid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} The vehicle ID you entered does not exist!");
	
	
	GetVehiclePos(vehicleid, pX, pY, pZ);
	SetPlayerPos(playerid, pX, pY, pZ);
	
	if(IsVehicleOccupied(vehicleid))
	{
		new driverid = GetVehicleDriver(vehicleid);
	    SendClientMessage(playerid, COLOR_BLUE, 
			"[INFO]:{E0E0E0} The vehicle you have teleported to has occupants!");
	    format(str, sizeof(str), 
			"[INFO]:{E0E0E0} The driver of the vehicle is: {63C9FF} (%d) %s{E0E0E0}.", 
			driverid, Account[driverid][Name]);
		SendClientMessage(playerid, COLOR_BLUE, str);
	}

	format(str, sizeof(str), 
		"[AMSG] (%d) %s has just teleported to vehicle ID %d.",
		playerid, Account[playerid][Name], vehicleid);
	SendAdminMsgEx(str);

	return true;
}

CMD:removeplayerfromcar(playerid, params[])
{
	new 
		targetid, 
		str[128];
	
	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "u", targetid))
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /removeplayerfromcar [player/name]");
	
	if(!IsPlayerConnected(targetid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	
	RemovePlayerFromVehicle(targetid);
	
	format(str, sizeof(str), 
		"[INFO]:{E0E0E0} You have been ejected from your vehicle by admin (%d) %s!", 
		playerid, Account[playerid][Name]);
	SendClientMessage(playerid, COLOR_BLUE, str);
	
	format(str, sizeof(str), 
		"[AMSG] (%d) %s has just ejected player (%d) %s from their vehicle!", 
		playerid, Account[playerid][Name], targetid, Account[targetid][Name]);
	SendAdminMsgEx(str);
	
	return true;
}

CMD:acreateveh(playerid, params[])
{
	new Float: pX, Float: pY, Float: pZ, Float: pA, vid, type, color[2], lp[10], string[90];

	if(Account[playerid][Admin] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not an administrator!");

	if(sscanf(params, "iiii", vid, type, color[0], color[1]))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /car [modelid] [color] [color]");

	GetPlayerPos(playerid, pX, pY, pZ);
	GetPlayerFacingAngle(playerid, pA);

	// License Plate Shitty Algorithm
	new temp = random(4500500) + random(4900500);
	format(lp, sizeof(lp), "SF_%d", temp);

	CreateStaticVehicle(999, vid, "The State", 2, lp, pX, pY, pZ, pA, color[0], color[1]);
	PutPlayerInVehicle(playerid, vCount, 0);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have created vehicle ID %d at your location! Please move it to", vCount);
	SendClientMessage(playerid, COLOR_BLUE, string);

	format(string, sizeof(string), "a appropiate location and type /asavecar for the spawning coordinates to be updated.");
	SendClientMessage(playerid, 0xE0E0E0FF, string);

	return true;
}

CMD:asavecar(playerid, params[])
{
	if(Account[playerid][Admin] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not an administrator!");

	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new Float: x, Float: y, Float: z, Float: a, vehicleid = GetPlayerVehicleID(playerid), query[200];

	GetVehiclePos(vehicleid, x, y, z);
	GetVehicleZAngle(vehicleid, a);

	mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `vX` = '%f', `vY` = '%f', `vZ` = '%f', `vA` = '%f' WHERE `ID` = '%d'", x, y, z, a, vehicleid);
	mysql_tquery(mysql, query, "", "");

	new model = GetVehicleModel(vehicleid);

	DestroyVehicle(vehicleid);
	CreateVehicle(model, x, y, z, a, Vehicle[vehicleid -1][Color1], Vehicle[vehicleid -1][Color2], -1);
	PutPlayerInVehicle(playerid, vehicleid, 0);

	return true;
}
