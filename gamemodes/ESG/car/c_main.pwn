CMD:carlist(playerid, params[])
{
	new 
		string[144],
		
		slot1 = Character[playerid][VehSlot1], 
		slot2 = Character[playerid][VehSlot2];

	if(slot1 < 999)
	{
		format(string, sizeof(string), "[SLOT 1]{E0E0E0} [(ID: %d) %s] [Alarm Level: %d] [Insurance: %d] [Dents: %d] [Fuel: %d] [Battery: %d]", slot1, GetVehicleNameFromModelID(GetVehicleModel(slot1 + 1)), Vehicle[slot1][alarm], Vehicle[slot1][insurance], Vehicle[slot1][explodes], Vehicle[slot1][fuel], Vehicle[slot1][battery]);
	}

	else format(string, sizeof(string), "[SLOT 2]{E0E0E0} None");

	SendClientMessage(playerid, COLOR_BLUE, string);

    if(slot2 < 999)
    {
		format(string, sizeof(string), "[SLOT 2]{E0E0E0} [(ID: %d) %s] [Alarm Level: %d] [Insurance: %d] [Dents: %d] [Fuel: %d] [Battery: %d]", slot2, GetVehicleNameFromModelID(GetVehicleModel(slot2 + 1)), Vehicle[slot2][alarm], Vehicle[slot2][insurance], Vehicle[slot2][explodes], Vehicle[slot2][fuel], Vehicle[slot2][battery]);
	}
	else format(string, sizeof(string), "[SLOT 2]{E0E0E0} None");

	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:cartrunk(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new vehicleid = GetPlayerVehicleID(playerid), engine, lights, alarmx, doors, bonnet, boot, objective, string[100];
	GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);

	if(!boot)
	{
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have opened your vehicle's trunk.");

		format(string, sizeof(string), "* %s has just opened their %s's trunk.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

	    SetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, true, objective);
	}

	if(boot)
	{
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have closed your vehicle's trunk.");

		format(string, sizeof(string), "* %s has just closed their %s's trunk.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

	    SetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, false, objective);
	}

	return true;
}

CMD:carhood(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new vehicleid = GetPlayerVehicleID(playerid), engine, lights, alarmx, doors, bonnet, boot, objective, string[100];
	GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);

	if(!bonnet)
	{
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have opened your vehicle's hood.");

		format(string, sizeof(string), "* %s has just opened their %s's hood.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

	    SetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, true, boot, objective);
	}

	if(bonnet)
	{
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have closed your vehicle's hood.");

		format(string, sizeof(string), "* %s has just closed their %s's hood.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

	    SetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, false, boot, objective);
	}

	return true;
}

CMD:carlights(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new vehicleid = GetPlayerVehicleID(playerid), engine, lights, alarmx, doors, bonnet, boot, objective, string[100];
	GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);

	if(lights)
	{
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have turned your car lights off.");

		format(string, sizeof(string), "* %s has just turned their %s's lights off.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

	    SetVehicleParamsEx(vehicleid, engine, false, alarmx, doors, bonnet, boot, objective);
	}

	if(!lights)
	{
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have turned your car lights on.");

		format(string, sizeof(string), "* %s has just turned their %s's lights on.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

	    SetVehicleParamsEx(vehicleid, engine, true, alarmx, doors, bonnet, boot, objective);
	}

	return true;
}

