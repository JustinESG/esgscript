#include <YSI4\YSI_Coding\y_hooks>

#define DEALER_BIKE      	(0)
#define DEALER_LUXURY     	(1)
#define DEALER_SALOON    	(2)
#define DEALER_SUV      	(3)
#define DEALER_LOWRIDER    	(4)
#define DEALER_AIRPLANE     (5)

#define DIALOG_SELCAR       (951)

enum DealershipInfo
{
	Model,
	Price
};

new pAccessingDealerShip[MAX_PLAYERS];

new BikeDealerArray[][DealershipInfo] =
{
	{461, 45000}, // pcj-600
	{462, 8500}, // faggio
	{468, 25000}, // sanchez
	{521, 45000}, // fcr-900
	{581, 50000}, //bf-400
	{463, 30000}, // freeway
	{586, 30000}, // wayfarer
	{509, 7500}, // bike
	{481, 7000}, // bmx
	{510, 6500} // mt. bike
};

new LuxuryDealerArray[][DealershipInfo] =
{
	{562, 85000}, // elegy
	{559, 85000}, // jester
	{602, 70000}, // alpha
	{429, 95000}, // banshee
	{402, 65000}, // buffalo
	{541, 105000}, // bullet
	{415, 125000}, // cheetah
	{587, 70000}, // euros
	{603, 90000}, // phoenix
	{477, 80000}, // uranus
	{480, 90000}, // comet
	{555, 95000}, // windsor
	{419, 100000}, // esperanto
	{560, 100000}, // sultan
	{580, 89700} // stafford
};

new SaloonDealerArray[][DealershipInfo] =
{
	{439, 55000}, // stallion
	{533, 56000}, // feltzer
	{558, 65000}, // uranus
	{475, 60000}, // sabre
	{565, 60000}, // flash
	{589, 55000}, // club
	{445, 53900}, // admiral
	{401, 50000}, // bravura
	{527, 52500}, // cadrona
	{542, 60000}, // clover
	{507, 58000}, // elegant
	{585, 53500}, // emperor
	{526, 62500}, // fortune
	{466, 52000}, // glendale
	{546, 48500}, // intruder
	{410, 43500}, // manana
	{551, 65000}, // merit
	{516, 45000}, // nebula
	{426, 50000}, // premier
	{436, 40000}, // previon
	{547, 43500}, // primo
	{405, 50000}, // sentinel
	{550, 54000}, // sunrise
	{540, 56500}, // vincent
	{529, 56500}, // willard
	{421, 56500}, // washington
	{404, 45000}, // perenniel
	{458, 50000}, // solair
	{561, 65000}, // stratum
	{418, 50000} // moonbeam
};

new SUVDealerArray[][DealershipInfo] =
{
	{422, 45000}, // bobcat
	{600, 45000}, // picador
	{554, 50000}, // yosemite
	{518, 52500}, // buccaneer
	{489, 55000}, // rancher 1
	{500, 60000}, // mesa
	{400, 60000}, // landstalker
	{579, 60000} // huntley
};

new LowriderDealerArray[][DealershipInfo] =
{
	{496, 57500}, // blista compact
	{517, 52500}, // majestic
	{474, 60000}, // hermes
	{492, 40000}, // greenwood
	{467, 45000}, // oceanic
	{549, 50000}, // tampa
	{491, 43500}, // virgo
	{566, 50000}, // tahoma
	{412, 60000}, // voodoo
	{576, 47500}, // tornado
	{567, 65000}, // savanna
	{535, 50000}, // slamvan
	{534, 45000}, // remington
	{575, 50000}, // broadway
	{536, 60000}, // blade
	{479, 57500} // regina
};

new AirDealerArray[][DealershipInfo] =
{
	{487, 250000}, // maverick
	{593, 250000} // dood
};

new DealershipBrowsingInt[MAX_PLAYERS];
new PurchasingCar[MAX_PLAYERS];
new PurchasingCarPrice[MAX_PLAYERS];

new PlayerText:VehDashTD[7][MAX_PLAYERS];
new PlayerText: DealershipTD[18][MAX_PLAYERS];

CMD:carbuy(playerid, params[])
{
	ShowPlayerDialog(playerid, DIALOG_SELCAR, DIALOG_STYLE_LIST, "Vehicle Dealership", "Bike\nLuxury\nSaloon\n4X4\nLowrider\nAirplanes", "Select", "Cancel");
	SelectTextDraw(playerid, 0xffaaaaaa);

	return true;
}

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == DIALOG_SELCAR)
	{
	    if(response)
	    {
	        pAccessingDealerShip[playerid] = listitem;

	        DestroyDealerShipTextDraws(playerid);
			LoadDealerShipTextDraws(playerid);
			ShowDealerShipTextDraws(playerid);
	    }
	}
	return 1;
}

hook OnPlayerClickPlayerTD(playerid, PlayerText:playertextid)
{
	if(playertextid == DealershipTD[9][playerid]) // left
	{
	    new id = DealershipBrowsingInt[playerid] --;

	    if(id == 0)
		{
			switch(pAccessingDealerShip[playerid])
			{
 				case DEALER_BIKE: 			DealershipBrowsingInt[playerid] = sizeof(BikeDealerArray) - 1;
				case DEALER_LUXURY: 		DealershipBrowsingInt[playerid] = sizeof(LuxuryDealerArray) - 1;
				case DEALER_SALOON:     	DealershipBrowsingInt[playerid] = sizeof(SaloonDealerArray) - 1;
				case DEALER_SUV:        	DealershipBrowsingInt[playerid] = sizeof(SUVDealerArray) - 1;
				case DEALER_LOWRIDER:       DealershipBrowsingInt[playerid] = sizeof(LowriderDealerArray) - 1;
				case DEALER_AIRPLANE:   	DealershipBrowsingInt[playerid] = sizeof(AirDealerArray) - 1;
			}
		}

		UpdateDealershipDraw(playerid, id);
	}

	if(playertextid == DealershipTD[10][playerid]) // right
	{
    	new id = DealershipBrowsingInt[playerid] ++;

		switch(pAccessingDealerShip[playerid])
		{
			case DEALER_BIKE:
			{
				if(id == sizeof(BikeDealerArray) - 1)
    				DealershipBrowsingInt[playerid] = 0;
			}

			case DEALER_LUXURY:
			{
				if(id == sizeof(LuxuryDealerArray) - 1)
    				DealershipBrowsingInt[playerid] = 0;
			}

			case DEALER_SALOON:
			{
				if(id == sizeof(SaloonDealerArray) - 1)
    				DealershipBrowsingInt[playerid] = 0;
			}

			case DEALER_SUV:
			{
				if(id == sizeof(SUVDealerArray) - 1)
    				DealershipBrowsingInt[playerid] = 0;
			}

			case DEALER_LOWRIDER:
			{
				if(id == sizeof(LowriderDealerArray) - 1)
    				DealershipBrowsingInt[playerid] = 0;
			}

			case DEALER_AIRPLANE:
			{
				if(id == sizeof(AirDealerArray) - 1)
    				DealershipBrowsingInt[playerid] = 0;
			}
		}

		UpdateDealershipDraw(playerid, id);
	}

	if(playertextid == DealershipTD[16][playerid]) // buy
	{
	    new lp[10], string[120], id = PurchasingCar[playerid];
	
       	if(Character[playerid][VehSlot1] != 999 && Character[playerid][VehSlot2] != 999)
			return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You have no vehicle slots left!");

		format(string, sizeof(string), "[INFO]{E0E0E0} You have purchased a %s for $%d. To park it, use /carpark.", GetVehicleNameFromModelID(id), PurchasingCarPrice[playerid]);
		SendClientMessage(playerid, COLOR_BLUE, string);

  		// License Plate Shitty Algorithm
		new temp = random(4500500) + random(4900500);
		format(lp, sizeof(lp), "SF_%d", temp);

	    HideDealerShipTextDraws(playerid);
	    CancelSelectTextDraw(playerid);

		if(IsValidVehicleModel(id))
		{
			new ownername[MAX_PLAYER_NAME];
			format(ownername, sizeof ownername, Account[playerid][Name]);
	        CreateStaticVehicle(playerid, id, ownername, 2, lp, -1991.4955, 254.4221, 34.7118, 0.0, random(255), random(255));
			PutPlayerInVehicle(playerid, vCount, 0);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "The model ID of the vehicle you are trying to purchase is invalid! Please try again!");
	}

	if(playertextid == DealershipTD[17][playerid]) // close
	{
	    PurchasingCar[playerid] = 0;
	    DealershipBrowsingInt[playerid] = 0;

	    HideDealerShipTextDraws(playerid);
	    CancelSelectTextDraw(playerid);
	}
	return 1;
}


UpdateDealershipDraw(playerid, id)
{
	new string[20];

	switch(pAccessingDealerShip[playerid])
	{
		case DEALER_BIKE:
		{
		    PlayerTextDrawHide(playerid, DealershipTD[3][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], BikeDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[3][playerid]);

		    PlayerTextDrawHide(playerid, DealershipTD[4][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], BikeDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[4][playerid]);

		    PlayerTextDrawSetString(playerid, DealershipTD[6][playerid], GetVehicleNameFromModelID(BikeDealerArray[id][Model]));

		    format(string, sizeof(string), "%d", BikeDealerArray[id][Price]);
		    PlayerTextDrawSetString(playerid, DealershipTD[8][playerid], string);

		    PurchasingCar[playerid] = BikeDealerArray[id][Model];
		    PurchasingCarPrice[playerid] = BikeDealerArray[id][Price];
		}

		case DEALER_LUXURY:
		{
		    PlayerTextDrawHide(playerid, DealershipTD[3][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], LuxuryDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[3][playerid]);

		    PlayerTextDrawHide(playerid, DealershipTD[4][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], LuxuryDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[4][playerid]);

		    PlayerTextDrawSetString(playerid, DealershipTD[6][playerid], GetVehicleNameFromModelID(LuxuryDealerArray[id][Model]));

		    format(string, sizeof(string), "%d", LuxuryDealerArray[id][Price]);
		    PlayerTextDrawSetString(playerid, DealershipTD[8][playerid], string);

		    PurchasingCar[playerid] = LuxuryDealerArray[id][Model];
		    PurchasingCarPrice[playerid] = LuxuryDealerArray[id][Price];
		}

		case DEALER_SALOON:
		{
		    PlayerTextDrawHide(playerid, DealershipTD[3][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], SaloonDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[3][playerid]);

		    PlayerTextDrawHide(playerid, DealershipTD[4][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], SaloonDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[4][playerid]);

		    PlayerTextDrawSetString(playerid, DealershipTD[6][playerid], GetVehicleNameFromModelID(SaloonDealerArray[id][Model]));

		    format(string, sizeof(string), "%d", SaloonDealerArray[id][Price]);
		    PlayerTextDrawSetString(playerid, DealershipTD[8][playerid], string);

		    PurchasingCar[playerid] = SaloonDealerArray[id][Model];
		    PurchasingCarPrice[playerid] = SaloonDealerArray[id][Price];
		}

		case DEALER_SUV:
		{
		    PlayerTextDrawHide(playerid, DealershipTD[3][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], SUVDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[3][playerid]);

		    PlayerTextDrawHide(playerid, DealershipTD[4][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], SUVDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[4][playerid]);

		    PlayerTextDrawSetString(playerid, DealershipTD[6][playerid], GetVehicleNameFromModelID(SUVDealerArray[id][Model]));

		    format(string, sizeof(string), "%d", SUVDealerArray[id][Price]);
		    PlayerTextDrawSetString(playerid, DealershipTD[8][playerid], string);

		    PurchasingCar[playerid] = SUVDealerArray[id][Model];
		    PurchasingCarPrice[playerid] = SUVDealerArray[id][Price];
		}

		case DEALER_LOWRIDER:
		{
		    PlayerTextDrawHide(playerid, DealershipTD[3][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], LowriderDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[3][playerid]);

		    PlayerTextDrawHide(playerid, DealershipTD[4][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], LowriderDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[4][playerid]);

		    PlayerTextDrawSetString(playerid, DealershipTD[6][playerid], GetVehicleNameFromModelID(LowriderDealerArray[id][Model]));

		    format(string, sizeof(string), "%d", LowriderDealerArray[id][Price]);
		    PlayerTextDrawSetString(playerid, DealershipTD[8][playerid], string);

		    PurchasingCar[playerid] = LowriderDealerArray[id][Model];
		    PurchasingCarPrice[playerid] = LowriderDealerArray[id][Price];
		}

		case DEALER_AIRPLANE:
		{
		    PlayerTextDrawHide(playerid, DealershipTD[3][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], AirDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[3][playerid]);

		    PlayerTextDrawHide(playerid, DealershipTD[4][playerid]);
			PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], AirDealerArray[id][Model]);
		    PlayerTextDrawShow(playerid, DealershipTD[4][playerid]);

		    PlayerTextDrawSetString(playerid, DealershipTD[6][playerid], GetVehicleNameFromModelID(AirDealerArray[id][Model]));

		    format(string, sizeof(string), "%d", AirDealerArray[id][Price]);
		    PlayerTextDrawSetString(playerid, DealershipTD[8][playerid], string);

		    PurchasingCar[playerid] = AirDealerArray[id][Model];
		    PurchasingCarPrice[playerid] = AirDealerArray[id][Price];
		}
	}

	return true;
}


ShowDealerShipTextDraws(playerid)
{
	for(new i; i < sizeof(DealershipTD); i ++)
	{
	    PlayerTextDrawShow(playerid, DealershipTD[i][playerid]);
	}

	return true;
}

HideDealerShipTextDraws(playerid)
{
	for(new i; i < sizeof(DealershipTD); i ++)
	{
	    PlayerTextDrawHide(playerid, DealershipTD[i][playerid]);
	}

	return true;
}

DestroyDealerShipTextDraws(playerid)
{
	for(new i; i < sizeof(DealershipTD); i ++)
	{
	    PlayerTextDrawDestroy(playerid, DealershipTD[i][playerid]);
	}

	return true;
}

LoadDealerShipTextDraws(playerid)
{
	DealershipTD[0][playerid] = CreatePlayerTextDraw(playerid, 497.000000, 111.425933, "usebox");
	PlayerTextDrawLetterSize(playerid, DealershipTD[0][playerid], 0.000000, 24.680854);
	PlayerTextDrawTextSize(playerid, DealershipTD[0][playerid], 174.333328, 0.000000);
	PlayerTextDrawAlignment(playerid, DealershipTD[0][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[0][playerid], 0);
	PlayerTextDrawUseBox(playerid, DealershipTD[0][playerid], true);
	PlayerTextDrawBoxColor(playerid, DealershipTD[0][playerid], 0x33333355);
	PlayerTextDrawSetShadow(playerid, DealershipTD[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[0][playerid], 0);
	PlayerTextDrawFont(playerid, DealershipTD[0][playerid], 0);

	DealershipTD[1][playerid] = CreatePlayerTextDraw(playerid, 493.999877, 114.329635, "usebox");
	PlayerTextDrawLetterSize(playerid, DealershipTD[1][playerid], 0.000000, 24.027782);
	PlayerTextDrawTextSize(playerid, DealershipTD[1][playerid], 177.333312, 0.000000);
	PlayerTextDrawAlignment(playerid, DealershipTD[1][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[1][playerid], 0);
	PlayerTextDrawUseBox(playerid, DealershipTD[1][playerid], true);
	PlayerTextDrawBoxColor(playerid, DealershipTD[1][playerid], 0x93C4C455);
	PlayerTextDrawSetShadow(playerid, DealershipTD[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[1][playerid], 0);
	PlayerTextDrawFont(playerid, DealershipTD[1][playerid], 0);

	DealershipTD[2][playerid] = CreatePlayerTextDraw(playerid, 236.000045, 117.392532, "Purchase A Vehicle");
	PlayerTextDrawLetterSize(playerid, DealershipTD[2][playerid], 0.504999, 1.853036);
	PlayerTextDrawAlignment(playerid, DealershipTD[2][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[2][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[2][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[2][playerid], 2);
	PlayerTextDrawSetProportional(playerid, DealershipTD[2][playerid], 1);

	// MODEL BOX
	DealershipTD[3][playerid] = CreatePlayerTextDraw(playerid, 200, 120, "usebox");
	PlayerTextDrawFont(playerid, DealershipTD[3][playerid], TEXT_DRAW_FONT_MODEL_PREVIEW);
	PlayerTextDrawLetterSize(playerid, DealershipTD[3][playerid], 0.000000, 14.030247);
	PlayerTextDrawUseBox(playerid, DealershipTD[3][playerid], true);
	PlayerTextDrawTextSize(playerid, DealershipTD[3][playerid], 150.0, 127.0);
	PlayerTextDrawBoxColor(playerid, DealershipTD[3][playerid], 0x00000000);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[3][playerid], 0);
	PlayerTextDrawSetPreviewModel(playerid, DealershipTD[3][playerid], 19300);
	PlayerTextDrawSetPreviewRot(playerid, DealershipTD[3][playerid], -10.0, 0.0, -40.0, 0.9);
	PlayerTextDrawSetPreviewVehCol(playerid, DealershipTD[3][playerid], 193, 192);

	DealershipTD[4][playerid] = CreatePlayerTextDraw(playerid, 187, 180, "usebox");
	PlayerTextDrawFont(playerid, DealershipTD[4][playerid], TEXT_DRAW_FONT_MODEL_PREVIEW);
	PlayerTextDrawLetterSize(playerid, DealershipTD[4][playerid], 0.000000, 14.030247);
	PlayerTextDrawUseBox(playerid, DealershipTD[4][playerid], true);
	PlayerTextDrawTextSize(playerid, DealershipTD[4][playerid], 150.0, 127.0);
	PlayerTextDrawBoxColor(playerid, DealershipTD[4][playerid], 0x00000000);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[4][playerid], 0);
	PlayerTextDrawSetPreviewModel(playerid, DealershipTD[4][playerid], 1239);
	PlayerTextDrawSetPreviewRot(playerid, DealershipTD[4][playerid], -10.0, 0.0, -130.0, 0.9);
	PlayerTextDrawSetPreviewVehCol(playerid, DealershipTD[4][playerid], 193, 192);

	DealershipTD[5][playerid] = CreatePlayerTextDraw(playerid, 361.333282, 154.725906, "Name:");
	PlayerTextDrawLetterSize(playerid, DealershipTD[5][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, DealershipTD[5][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[5][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[5][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[5][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[5][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[5][playerid], 2);
	PlayerTextDrawSetProportional(playerid, DealershipTD[5][playerid], 1);

	DealershipTD[6][playerid] = CreatePlayerTextDraw(playerid, 361.333374, 171.733352, "\0");
	PlayerTextDrawLetterSize(playerid, DealershipTD[6][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, DealershipTD[6][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[6][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[6][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[6][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[6][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[6][playerid], 1);
	PlayerTextDrawSetProportional(playerid, DealershipTD[6][playerid], 1);

	DealershipTD[7][playerid] = CreatePlayerTextDraw(playerid, 361.333343, 202.014831, "Price:");
	PlayerTextDrawLetterSize(playerid, DealershipTD[7][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, DealershipTD[7][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[7][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[7][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[7][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[7][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[7][playerid], 2);
	PlayerTextDrawSetProportional(playerid, DealershipTD[7][playerid], 1);

	DealershipTD[8][playerid] = CreatePlayerTextDraw(playerid, 361.666595, 217.362945, "\0");
	PlayerTextDrawLetterSize(playerid, DealershipTD[8][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, DealershipTD[8][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[8][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[8][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[8][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[8][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[8][playerid], 1);
	PlayerTextDrawSetProportional(playerid, DealershipTD[8][playerid], 1);

	DealershipTD[9][playerid] = CreatePlayerTextDraw(playerid, 348.000091, 248.474014, "LD_BEAT:left");
	PlayerTextDrawLetterSize(playerid, DealershipTD[9][playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, DealershipTD[9][playerid], 18.999998, 21.155551);
	PlayerTextDrawAlignment(playerid, DealershipTD[9][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[9][playerid], 0xBBC2C488);
	PlayerTextDrawSetShadow(playerid, DealershipTD[9][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[9][playerid], 1);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[9][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[9][playerid], 4);
	PlayerTextDrawSetProportional(playerid, DealershipTD[9][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, DealershipTD[9][playerid], 1);

	DealershipTD[10][playerid] = CreatePlayerTextDraw(playerid, 469.666748, 248.474105, "LD_BEAT:right");
	PlayerTextDrawLetterSize(playerid, DealershipTD[10][playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, DealershipTD[10][playerid], 18.999998, 21.155551);
	PlayerTextDrawAlignment(playerid, DealershipTD[10][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[10][playerid], 0xBBC2C488);
	PlayerTextDrawSetShadow(playerid, DealershipTD[10][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[10][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[10][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[10][playerid], 4);
	PlayerTextDrawSetProportional(playerid, DealershipTD[10][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, DealershipTD[10][playerid], 1);

	DealershipTD[11][playerid] = CreatePlayerTextDraw(playerid, 378.0, 251.792510, "Select Car");
	PlayerTextDrawLetterSize(playerid, DealershipTD[11][playerid], 0.332000, 1.284741);
	PlayerTextDrawAlignment(playerid, DealershipTD[11][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[11][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[11][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[11][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[11][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[11][playerid], 2);
	PlayerTextDrawSetProportional(playerid, DealershipTD[11][playerid], 1);

	DealershipTD[12][playerid] = CreatePlayerTextDraw(playerid, 292.666656, 294.774078, "usebox");
	PlayerTextDrawLetterSize(playerid, DealershipTD[12][playerid], 0.000000, 2.413374);
	PlayerTextDrawTextSize(playerid, DealershipTD[12][playerid], 191.000000, 0.000000);
	PlayerTextDrawAlignment(playerid, DealershipTD[12][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[12][playerid], 0);
	PlayerTextDrawUseBox(playerid, DealershipTD[12][playerid], true);
	PlayerTextDrawBoxColor(playerid, DealershipTD[12][playerid], 0x33333355);
	PlayerTextDrawSetShadow(playerid, DealershipTD[12][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[12][playerid], 0);
	PlayerTextDrawFont(playerid, DealershipTD[12][playerid], 0);

	DealershipTD[13][playerid] = CreatePlayerTextDraw(playerid, 479.999877, 294.773986, "usebox");
	PlayerTextDrawLetterSize(playerid, DealershipTD[13][playerid], 0.000000, 2.356581);
	PlayerTextDrawTextSize(playerid, DealershipTD[13][playerid], 378.666595, 0.000000);
	PlayerTextDrawAlignment(playerid, DealershipTD[13][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[13][playerid], 0);
	PlayerTextDrawUseBox(playerid, DealershipTD[13][playerid], true);
	PlayerTextDrawBoxColor(playerid, DealershipTD[13][playerid], 0x33333355);
	PlayerTextDrawSetShadow(playerid, DealershipTD[13][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[13][playerid], 0);
	PlayerTextDrawFont(playerid, DealershipTD[13][playerid], 0);

	DealershipTD[14][playerid] = CreatePlayerTextDraw(playerid, 291.333374, 296.018463, "usebox");
	PlayerTextDrawLetterSize(playerid, DealershipTD[14][playerid], 0.000000, 2.099380);
	PlayerTextDrawTextSize(playerid, DealershipTD[14][playerid], 192.333343, 0.000000);
	PlayerTextDrawAlignment(playerid, DealershipTD[14][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[14][playerid], 0);
	PlayerTextDrawUseBox(playerid, DealershipTD[14][playerid], true);
	PlayerTextDrawBoxColor(playerid, DealershipTD[14][playerid], 0xBBC2C455);
	PlayerTextDrawSetShadow(playerid, DealershipTD[14][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[14][playerid], 0);
	PlayerTextDrawFont(playerid, DealershipTD[14][playerid], 0);

	DealershipTD[15][playerid] = CreatePlayerTextDraw(playerid, 478.666625, 296.018585, "usebox");
	PlayerTextDrawLetterSize(playerid, DealershipTD[15][playerid], 0.000000, 2.026129);
	PlayerTextDrawTextSize(playerid, DealershipTD[15][playerid], 380.000000, 0.000000);
	PlayerTextDrawAlignment(playerid, DealershipTD[15][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[15][playerid], 0);
	PlayerTextDrawUseBox(playerid, DealershipTD[15][playerid], true);
	PlayerTextDrawBoxColor(playerid, DealershipTD[15][playerid], 0xBBC2C455);
	PlayerTextDrawSetShadow(playerid, DealershipTD[15][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[15][playerid], 0);
	PlayerTextDrawFont(playerid, DealershipTD[15][playerid], 0);

	DealershipTD[16][playerid] = CreatePlayerTextDraw(playerid, 198.000030, 298.251861, "Purchase");
	PlayerTextDrawLetterSize(playerid, DealershipTD[16][playerid], 0.415666, 1.533630);
	PlayerTextDrawTextSize(playerid, DealershipTD[16][playerid], 290.0, 15.0);
	PlayerTextDrawAlignment(playerid, DealershipTD[16][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[16][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[16][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[16][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[16][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[16][playerid], 2);
	PlayerTextDrawSetProportional(playerid, DealershipTD[16][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, DealershipTD[16][playerid], 1);

	DealershipTD[17][playerid] = CreatePlayerTextDraw(playerid, 396.333312, 297.837005, "Cancel");
	PlayerTextDrawLetterSize(playerid, DealershipTD[17][playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, DealershipTD[17][playerid], 470.0, 15.0);
	PlayerTextDrawAlignment(playerid, DealershipTD[17][playerid], 1);
	PlayerTextDrawColor(playerid, DealershipTD[17][playerid], -1);
	PlayerTextDrawSetShadow(playerid, DealershipTD[17][playerid], 0);
	PlayerTextDrawSetOutline(playerid, DealershipTD[17][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DealershipTD[17][playerid], 51);
	PlayerTextDrawFont(playerid, DealershipTD[17][playerid], 2);
	PlayerTextDrawSetProportional(playerid, DealershipTD[17][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, DealershipTD[17][playerid], 1);
}

IsValidVehicleModel(model)
{
    if(model >= 400 && model <= 611)
    {
        return true;
    }

    return false;
}
