#include <YSI4\YSI_Coding\y_hooks>

forward Float:GetVehicleSpeed(vehicleid);


//TODO: hooking OnPlayerUpdate is a bad idea! [Alex]
hook OnPlayerUpdate(playerid)
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		new string[60];

		format(string, sizeof(string), "KMP/H:~w~ %0.0f", GetVehicleSpeed(GetPlayerVehicleID(playerid)));
		PlayerTextDrawSetString(playerid, VehDashTD[1][playerid], string);
	}
	return 1;
}

hook OnPlayerStateChange(playerid, newstate, oldstate)
{
	if(newstate == PLAYER_STATE_DRIVER || newstate == PLAYER_STATE_PASSENGER)
	{
	    DestroyVehicleDashboardTD(playerid);
	 	LoadVehicleDashboardTextDraws(playerid);
	 	
	    ShowVehicleDashboardTextDraws(playerid);
	    
		new string[20], enumid = GetPlayerVehicleID(playerid) - 1;
	    
	    PlayerTextDrawSetString(playerid, VehDashTD[0][playerid], GetVehicleNameFromModelID(GetVehicleModel(GetPlayerVehicleID(playerid))));

	    format(string, sizeof(string), "FUEL: ~w~%d", Vehicle[enumid][fuel]);
	    PlayerTextDrawSetString(playerid, VehDashTD[2][playerid], string);

	    format(string, sizeof(string), "BATT: ~w~%d", Vehicle[enumid][battery]);
		PlayerTextDrawSetString(playerid, VehDashTD[3][playerid], string);
	}

	if(newstate == PLAYER_STATE_ONFOOT && oldstate == PLAYER_STATE_DRIVER || oldstate == PLAYER_STATE_PASSENGER)
	{
		HideVehicleDashboardTextDraws(playerid);
	}
	return 1;
}

public Float: GetVehicleSpeed(vehicleid)
{
    new Float:Vel[3];

    GetVehicleVelocity(vehicleid, Vel[0], Vel[1], Vel[2]);

    Vel[0] *= 175;
    Vel[1] *= 175;
    Vel[2] *= 175;

    return Float: VectorSize(Vel[0], Vel[1], Vel[2]);
}

HideVehicleDashboardTextDraws(playerid)
{
	for(new i; i < sizeof(VehDashTD); i ++)
	{
		PlayerTextDrawHide(playerid, VehDashTD[i][playerid]);
	}

	return true;
}

ShowVehicleDashboardTextDraws(playerid)
{
	for(new i; i < sizeof(VehDashTD); i ++)
	{
		PlayerTextDrawShow(playerid, VehDashTD[i][playerid]);
	}

	return true;
}

DestroyVehicleDashboardTD(playerid)
{
	for(new i; i < sizeof(VehDashTD); i ++)
	{
		PlayerTextDrawDestroy(playerid, VehDashTD[i][playerid]);
	}

	return true;
}

LoadVehicleDashboardTextDraws(playerid)
{
	VehDashTD[0][playerid] = CreatePlayerTextDraw(playerid, 154.0, 350.933410, "Vehicle Name");
	PlayerTextDrawLetterSize(playerid, VehDashTD[0][playerid], 0.432333, 1.604148);
	PlayerTextDrawAlignment(playerid, VehDashTD[0][playerid], 1);
	PlayerTextDrawColor(playerid, VehDashTD[0][playerid], 0x00FBFFFF);
	PlayerTextDrawSetShadow(playerid, VehDashTD[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid, VehDashTD[0][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, VehDashTD[0][playerid], 51);
	PlayerTextDrawFont(playerid, VehDashTD[0][playerid], 1);
	PlayerTextDrawSetProportional(playerid, VehDashTD[0][playerid], 1);

	VehDashTD[1][playerid] = CreatePlayerTextDraw(playerid, 154.0, 367.940795, "KM/PH:");
	PlayerTextDrawLetterSize(playerid, VehDashTD[1][playerid], 0.379333, 1.575111);
	PlayerTextDrawAlignment(playerid, VehDashTD[1][playerid], 1);
	PlayerTextDrawColor(playerid, VehDashTD[1][playerid], 0x00FBFFFF);
	PlayerTextDrawSetShadow(playerid, VehDashTD[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid, VehDashTD[1][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, VehDashTD[1][playerid], 51);
	PlayerTextDrawFont(playerid, VehDashTD[1][playerid], 1);
	PlayerTextDrawSetProportional(playerid, VehDashTD[1][playerid], 1);

	VehDashTD[2][playerid] = CreatePlayerTextDraw(playerid, 154.0, 381.629608, "FUEL:");
	PlayerTextDrawLetterSize(playerid, VehDashTD[2][playerid], 0.447333, 1.600000);
	PlayerTextDrawAlignment(playerid, VehDashTD[2][playerid], 1);
	PlayerTextDrawColor(playerid, VehDashTD[2][playerid], 0x00FBFFFF);
	PlayerTextDrawSetShadow(playerid, VehDashTD[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid, VehDashTD[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, VehDashTD[2][playerid], 51);
	PlayerTextDrawFont(playerid, VehDashTD[2][playerid], 1);
	PlayerTextDrawSetProportional(playerid, VehDashTD[2][playerid], 1);

	VehDashTD[3][playerid] = CreatePlayerTextDraw(playerid, 154.0, 394.903625, "BATT:");
	PlayerTextDrawLetterSize(playerid, VehDashTD[3][playerid], 0.409333, 1.529481);
	PlayerTextDrawAlignment(playerid, VehDashTD[3][playerid], 1);
	PlayerTextDrawColor(playerid, VehDashTD[3][playerid], 0x00FBFFFF);
	PlayerTextDrawSetShadow(playerid, VehDashTD[3][playerid], 0);
	PlayerTextDrawSetOutline(playerid, VehDashTD[3][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, VehDashTD[3][playerid], 51);
	PlayerTextDrawFont(playerid, VehDashTD[3][playerid], 1);
	PlayerTextDrawSetProportional(playerid, VehDashTD[3][playerid], 1);

	return true;
}
