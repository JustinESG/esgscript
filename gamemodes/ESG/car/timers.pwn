new FuelTimer[MAX_VEHICLES];
new BatteryTimer[MAX_VEHICLES];

CMD:engine(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new vehicleid = GetPlayerVehicleID(playerid);
	
	if(Character[playerid][VehSlot1] == vehicleid - 1 || Character[playerid][VehSlot2] == vehicleid -  1)
	{
	    new engine, lights, alarmx, doors, bonnet, boot, objective, string[100];
		GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);

		if(engine)
		{
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have turned your engine off.");

			format(string, sizeof(string), "* %s has just turned their %s's engine off.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
            SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

		    SetVehicleParamsEx(vehicleid, false, lights, alarmx, doors, bonnet, boot, objective);
		    KillTimer(FuelTimer[vehicleid]);
		}

		if(!engine)
		{
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have turned your engine on.");

			format(string, sizeof(string), "* %s has just turned their %s's engine on.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
            SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

		    SetVehicleParamsEx(vehicleid, true, lights, alarmx, doors, bonnet, boot, objective);

			FuelTimer[vehicleid] = SetTimerEx("FuelTimerTick", 1200000, true, "i", vehicleid);
			BatteryTimer[vehicleid] = SetTimerEx("BatteryTimerTick", 2700000, true, "i", vehicleid);
		}
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}

forward FuelTimerTick(vehicleid);
public FuelTimerTick(vehicleid)
{
	new enumidex = vehicleid - 1;

	if(-- Vehicle[enumidex][fuel] > 0)
	{
	    new string[20];

	    format(string, sizeof(string), "FUEL: ~w~%d", Vehicle[enumidex][fuel]);

        foreach(new i: Player)
        {
		    if(GetPlayerVehicleID(i) == vehicleid)
		    {
		        PlayerTextDrawSetString(i, VehDashTD[2][i], string);
		    }
		}
	}

	else if(Vehicle[enumidex][fuel] == 0)
	{
	    new engine, lights, alarmx, doors, bonnet, boot, objective;

		// Get every value again to ensure nothing else but the engine gets changed
		GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);
	    SetVehicleParamsEx(vehicleid, false, lights, alarmx, doors, bonnet, boot, objective);

 		foreach(new i: Player)
        {
		    if(GetPlayerVehicleID(i) == vehicleid && GetPlayerState(i) == PLAYER_STATE_DRIVER)
		    {
		    	SendClientMessage(i, COLOR_ERROR, "[VEHICLE]{e0e0e0} Your vehicle has ran out of fuel.");
		    }
		}

		KillTimer(FuelTimer[vehicleid]);
	}

	return true;
}

forward BatteryTimerTick(vehicleid);
public BatteryTimerTick(vehicleid)
{
	new enumidex = vehicleid - 1;

	if(-- Vehicle[enumidex][battery] > 0)
	{
	    new string[20];

	    format(string, sizeof(string), "BATT: ~w~%d", Vehicle[enumidex][battery]);

        foreach(new i: Player)
        {
		    if(GetPlayerVehicleID(i) == vehicleid)
		    {
		        PlayerTextDrawSetString(i, VehDashTD[3][i], string);
		    }
		}
	}

	else if(Vehicle[enumidex][battery] == 0)
	{
	    new engine, lights, alarmx, doors, bonnet, boot, objective;

		// Get every value again to ensure nothing else but the engine gets changed
		GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);
	    SetVehicleParamsEx(vehicleid, false, lights, alarmx, doors, bonnet, boot, objective);

 		foreach(new i: Player)
        {
		    if(GetPlayerVehicleID(i) == vehicleid && GetPlayerState(i) == PLAYER_STATE_DRIVER)
		    {
		    	SendClientMessage(i, COLOR_ERROR, "[VEHICLE]{e0e0e0} Your vehicle's battery has depleted.");
		    }
		}

		KillTimer(BatteryTimer[vehicleid]);
	}

	return true;
}
