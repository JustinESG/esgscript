CMD:carscrap(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new 
		vehicleid = GetPlayerVehicleID(playerid), enumid = vehicleid - 1;

	if(Character[playerid][VehSlot1] == enumid || Character[playerid][VehSlot2] == enumid)
	{
		new query[100], string[80], money;

		mysql_format(mysql, query, sizeof(query), "DELETE FROM `vehicles` WHERE `ID` = '%d'", vehicleid);
		mysql_tquery(mysql, query, "", "");

		// Reset AI counter
		mysql_tquery(mysql, "ALTER TABLE `vehicles` AUTO_INCREMENT = 1", "", "");

		DestroyVehicle(GetPlayerVehicleID(playerid));

		if(money == 0) money += 12500 + random(17500);

		format(string, sizeof(string), "[INFO]{E0E0E0} You have just scrapped your vehicle and got $%d in return!", money);
		SendClientMessage(playerid, COLOR_BLUE, string);

		GivePlayerMoneyEx(playerid, money);
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}

CMD:cargive(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new 
		vehicleid = GetPlayerVehicleID(playerid);

	if(Character[playerid][VehSlot1] == vehicleid - 1 || Character[playerid][VehSlot2] == vehicleid -  1)
	{
	    new targetid, slot, string[100], query[100];

	    if(sscanf(params, "ui", targetid, slot))
	  	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /cargive [playerid/name] [slot]");

		if(!IsPlayerConnected(targetid))
	  	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The ID you entered is invalid.");
		
		if(Character[targetid][VehSlot1] != 999 && Character[targetid][VehSlot2] != 999)
	  	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} Your target has no car slots left.");

		mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `Owner` = '%s' WHERE `ID` = '%d'", Account[targetid][Name], vehicleid);
		mysql_tquery(mysql, query, "", "");

		if(Character[targetid][VehSlot1] != 999)
		{
		    if(slot == 1)
		    {
			    Character[playerid][VehSlot1] = Character[playerid][VehSlot1];

				format(string, sizeof(string), "[INFO]{E0E0E0} You have been given %s's %s.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
				SendClientMessage(playerid, COLOR_BLUE, string);

				Character[playerid][VehSlot1] = 999;
			}

			else if(slot == 2)
			{
				Character[targetid][VehSlot1] = Character[playerid][VehSlot2];

				format(string, sizeof(string), "[INFO]{E0E0E0} You have been given %s's %s.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
				SendClientMessage(playerid, COLOR_BLUE, string);

				Character[playerid][VehSlot2] = 999;
			}
		}

		else if(Character[targetid][VehSlot1] == 999 && Character[targetid][VehSlot2] != 999)
		{
		    if(slot == 1)
		    {
			    Character[targetid][VehSlot2] = Character[playerid][VehSlot1];

				format(string, sizeof(string), "[INFO]{E0E0E0} You have been given %s's %s.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
				SendClientMessage(playerid, COLOR_BLUE, string);

				Character[playerid][VehSlot1] = 999;
			}

			else if(slot == 2)
			{
				Character[targetid][VehSlot2] = Character[playerid][VehSlot2];

				format(string, sizeof(string), "[INFO]{E0E0E0} You have been given %s's %s.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
				SendClientMessage(playerid, COLOR_BLUE, string);

				Character[playerid][VehSlot2] = 999;
			}
		}
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}

/*

CMD:cargunslots(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new 
		vehicleid = GetPlayerVehicleID(playerid), enumid = vehicleid - 1;

	if(Character[playerid][VehSlot1] == enumid || Character[playerid][VehSlot2] == enumid)
	{
	    new string[100];

	    format(string, sizeof(string), "[SLOT 1]{E0E0E0} %s with %d ammo.", GetWeaponNameEx(Vehicle[enumid][gunSlot1]), Vehicle[enumid][gunSlotAmmo1]);
	    if(Vehicle[enumid][gunSlot1] == 0) format(string, sizeof(string), "[SLOT 1]{E0E0E0}Empty");
	    SendClientMessage(playerid, COLOR_BLUE, string);

	    format(string, sizeof(string), "[SLOT 2]{E0E0E0} %s with %d ammo.", GetWeaponNameEx(Vehicle[enumid][gunSlot2]), Vehicle[enumid][gunSlotAmmo2]);
	    if(Vehicle[enumid][gunSlot2] == 0) format(string, sizeof(string), "[SLOT 2]{E0E0E0} Empty");
	    SendClientMessage(playerid, COLOR_BLUE, string);

	    format(string, sizeof(string), "[SLOT 3]{E0E0E0} %s with %d ammo.", GetWeaponNameEx(Vehicle[enumid][gunSlot3]), Vehicle[enumid][gunSlotAmmo3]);
	    if(Vehicle[enumid][gunSlot3] == 0) format(string, sizeof(string), "[SLOT 3]{E0E0E0} Empty");
	    SendClientMessage(playerid, COLOR_BLUE, string);
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}

CMD:cartake(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new 
		vehicleid = GetPlayerVehicleID(playerid), enumid = vehicleid - 1, string[100];

	if(Character[playerid][VehSlot1] == enumid || Character[playerid][VehSlot2] == enumid)
	{
		new slot, query[120];

		if(Character[playerid][HoldingWeapon] != 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're already holding a weapon!");

		if(sscanf(params, "i", slot))
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /vtake [slot]");

		if(slot < 1 || slot > 3)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} Can't go lower than slot 1, or higher than slot 3!");

		if(slot == 1 && Vehicle[enumid][gunSlot1] == 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The slot you are trying to take a weapon from is empty!");

		if(slot == 2 && Vehicle[enumid][gunSlot2] == 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The slot you are trying to take a weapon from is empty!");

		if(slot == 3 && Vehicle[enumid][gunSlot3] == 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The slot you are trying to take a weapon from is empty!");

		if(slot == 1)
		{
        	GivePlayerWeaponEx(playerid, Vehicle[enumid][gunSlot1], Vehicle[enumid][gunSlotAmmo1]);

			format(string, sizeof(string), "* %s has just taken their %s from their %s.", GetRoleplayName(playerid), GetWeaponNameEx(Character[playerid][HoldingWeapon]), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
	        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

		    Vehicle[enumid][gunSlot1] = 0;
		    Vehicle[enumid][gunSlotAmmo1] = 0;

		    mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `gunSlot1` = '0', `gunSlotAmmo1` = '0' WHERE `ID` = '%d'", vehicleid);
			mysql_tquery(mysql, query, "", "");
		}

		if(slot == 2)
		{
        	GivePlayerWeaponEx(playerid, Vehicle[enumid][gunSlot2], Vehicle[enumid][gunSlotAmmo2]);

			format(string, sizeof(string), "* %s has just taken their %s from their %s.", GetRoleplayName(playerid), GetWeaponNameEx(Character[playerid][HoldingWeapon]), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
	        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

		    Vehicle[enumid][gunSlot2] = 0;
		    Vehicle[enumid][gunSlotAmmo2] = 0;

		    mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `gunSlot2` = '0', `gunSlotAmmo2` = '0' WHERE `ID` = '%d'", vehicleid);
			mysql_tquery(mysql, query, "", "");
		}

		if(slot == 3)
		{
        	GivePlayerWeaponEx(playerid, Vehicle[enumid][gunSlot3], Vehicle[enumid][gunSlotAmmo3]);

			format(string, sizeof(string), "* %s has just taken their %s from their %s.", GetRoleplayName(playerid), GetWeaponNameEx(Character[playerid][HoldingWeapon]), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
	        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

		    Vehicle[enumid][gunSlot3] = 0;
		    Vehicle[enumid][gunSlotAmmo3] = 0;

		    mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `gunSlot3` = '0', `gunSlotAmmo3` = '0' WHERE `ID` = '%d'", vehicleid);
			mysql_tquery(mysql, query, "", "");
		}
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}

CMD:carstore(playerid, params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	new 
		vehicleid = GetPlayerVehicleID(playerid), enumid = vehicleid - 1, string[100];

	if(Character[playerid][VehSlot1] == enumid || Character[playerid][VehSlot2] == enumid)
	{
		new slot, query[120];

		if(Character[playerid][HoldingWeapon] == 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have any weapon!");

		if(sscanf(params, "i", slot))
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /vstore [slot]");

		if(slot < 1 || slot > 3)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} Can't go lower than slot 1, or higher than slot 3!");

		if(slot == 1 && Vehicle[enumid][gunSlot1] != 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The slot you are trying to store a weapon in isn't empty!");

		if(slot == 2 && Vehicle[enumid][gunSlot2] != 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The slot you are trying to store a weapon in isn't empty!");

		if(slot == 3 && Vehicle[enumid][gunSlot3] != 0)
		    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The slot you are trying to store a weapon in isn't empty!");

		if(slot == 1 && Vehicle[enumid][gunSlot1] == 0)
		{
		    Vehicle[enumid][gunSlot1] = Character[playerid][HoldingWeapon];
		    Vehicle[enumid][gunSlotAmmo1] = Character[playerid][hWeaponAmmo];

		    mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `gunSlot1` = '%d', `gunSlotAmmo1` = '%d' WHERE `ID` = '%d'", Character[playerid][HoldingWeapon], Character[playerid][hWeaponAmmo], vehicleid);
			mysql_tquery(mysql, query, "", "");
		}

		if(slot == 2 && Vehicle[enumid][gunSlot2] == 0)
		{
		    Vehicle[enumid][gunSlot2] = Character[playerid][HoldingWeapon];
		    Vehicle[enumid][gunSlotAmmo2] = Character[playerid][hWeaponAmmo];

		    mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `gunSlot2` = '%d', `gunSlotAmmo2` = '%d' WHERE `ID` = '%d'", Character[playerid][HoldingWeapon], Character[playerid][hWeaponAmmo], vehicleid);
			mysql_tquery(mysql, query, "", "");
		}

		if(slot == 3 && Vehicle[enumid][gunSlot3] == 0)
		{
		    Vehicle[enumid][gunSlot3] = Character[playerid][HoldingWeapon];
		    Vehicle[enumid][gunSlotAmmo3] = Character[playerid][hWeaponAmmo];

		    mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `gunSlot3` = '%d', `gunSlotAmmo3` = '%d' WHERE `ID` = '%d'", Character[playerid][HoldingWeapon], Character[playerid][hWeaponAmmo], vehicleid);
			mysql_tquery(mysql, query, "", "");
		}

		format(string, sizeof(string), "* %s has just stored their %s into their %s.", GetRoleplayName(playerid), GetWeaponNameEx(Character[playerid][HoldingWeapon]), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
        SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

        RemovePlayerWeapon(playerid, Character[playerid][HoldingWeapon]);
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}
*/

CMD:carlock(playerid, params[])
{
	new 
		
		vehicleid = GetNearestVehicleToPlayer(playerid), 
		query[100], 
		Float: vx1, Float: vy1, Float: vz1, 
		Float: vx2, Float: vy2, Float: vz2;

	GetVehiclePos(Character[playerid][VehSlot1] + 1, vx1, vy1, vz1);
	GetVehiclePos(Character[playerid][VehSlot2] + 1, vx2, vy2, vz2);

	if(IsPlayerInRangeOfPoint(playerid, 3.0, vx1, vy1, vz1) || IsPlayerInRangeOfPoint(playerid, 3.0, vx2, vy2, vz2))
	{
		if(Character[playerid][VehSlot1] == vehicleid - 1 || Character[playerid][VehSlot2] == vehicleid -  1)
		{
		    new enumid = vehicleid - 1, engine, lights, alarmx, doors, bonnet, boot, objective, string[80];
			GetVehicleParamsEx(vehicleid, engine, lights, alarmx, doors, bonnet, boot, objective);

		    if(Vehicle[enumid][Locked])
		    {
		        Vehicle[enumid][Locked] = false;

				format(string, sizeof(string), "* %s has just unlocked their %s.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
	            SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

				SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have unlocked your vehicle.");
			    SetVehicleParamsEx(vehicleid, engine, lights, alarmx, false, bonnet, boot, objective);
		    }

			else if(!Vehicle[enumid][Locked])
		    {
		        Vehicle[enumid][Locked] = true;

				format(string, sizeof(string), "* %s has just locked their %s.", GetRoleplayName(playerid), GetVehicleNameFromModelID(GetVehicleModel(vehicleid)));
	            SendLocalMessageEx(playerid, COLOR_ACTION, string, 7.0);

				SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have locked your vehicle.");
			    SetVehicleParamsEx(vehicleid, engine, lights, alarmx, true, bonnet, boot, objective);
		    }

			mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `Locked` = '%d' WHERE `ID` = '%d'", Vehicle[enumid][Locked], vehicleid);
			mysql_tquery(mysql, query, "", "");
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a vehicle you own!");

	return true;
}

CMD:carpark(playerid, params[])
{
	new 
		
		vehicleid = GetPlayerVehicleID(playerid), 
		Float: x, Float: y, Float: z, Float: a, 
		query[200];

	if(!IsPlayerInAnyVehicle(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not in a vehicle!");

	if(Character[playerid][VehSlot1] == vehicleid - 1 || Character[playerid][VehSlot2] == vehicleid -  1)
	{
		GetVehiclePos(vehicleid, x, y, z);
		GetVehicleZAngle(vehicleid, a);

        SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have parked your vehicle.");

		mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `vX` = '%f', `vY` = '%f', `vZ` = '%f', `vA` = '%f' WHERE `ID` = '%d'", x, y, z, a, vehicleid);
		mysql_tquery(mysql, query, "", "");

		new model = GetVehicleModel(vehicleid);

		DestroyVehicle(vehicleid);
		CreateVehicle(model, x, y, z, a, Vehicle[vehicleid -1][Color1], Vehicle[vehicleid -1][Color2], -1);
		PutPlayerInVehicle(playerid, vehicleid, 0);
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't own this vehicle!");

	return true;
}

