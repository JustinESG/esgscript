#include <YSI4\YSI_Coding\y_hooks>


new const fbumper[23][0] = { {1117}, {1152}, {1153}, {1155}, {1157}, {1160}, {1165}, {1167}, {1169},
	{1170}, {1171}, {1172}, {1173}, {1174}, {1175}, {1179}, {1181}, {1182}, {1185}, {1188},
	{1189}, {1192}, {1193} };

new const rbumper[22][0] = { {1140}, {1141}, {1148}, {1149}, {1150}, {1151}, {1154}, {1156}, {1159},
    {1161}, {1166}, {1168}, {1176}, {1177}, {1178}, {1180}, {1183}, {1184}, {1186}, {1187},
	{1190}, {1191} };

new const exhaust[28][0] = { {1018}, {1019}, {1020}, {1021}, {1022}, {1028}, {1029}, {1037}, {1043},
    {1044}, {1045}, {1046}, {1059}, {1064}, {1065}, {1066}, {1089}, {1092}, {1104}, {1105},
	{1113}, {1114}, {1126}, {1127}, {1129}, {1132}, {1135}, {1136} };

new const lskirt[21][0] = { {1007}, {1026}, {1031}, {1036}, {1039}, {1042}, {1047}, {1048}, {1056},
	{1057}, {1069}, {1070}, {1090}, {1093}, {1106}, {1108}, {1118}, {1119}, {1133}, {1122},
	{1134} };

new const rskirt[21][0] = { {1017}, {1027}, {1030}, {1040}, {1041}, {1051}, {1052}, {1062}, {1063},
	{1071}, {1072}, {1094}, {1095}, {1099}, {1101}, {1102}, {1107}, {1120}, {1121}, {1124},
	{1137} }, spoiler[20][0] = { {1000}, {1001}, {1002}, {1003}, {1014}, {1015}, {1016}, {1023}, {1058},
	{1060}, {1049}, {1050}, {1138}, {1139}, {1146}, {1147}, {1158}, {1162}, {1163}, {1164} };

new const rscoop[17][0] = { {1006}, {1032}, {1033}, {1035}, {1038}, {1053}, {1054}, {1055}, {1061},
    {1067}, {1068}, {1088}, {1091}, {1103}, {1128}, {1130}, {1131} };

new const wheels[17][0] = { {1025}, {1073},  {1074}, {1075}, {1076}, {1077}, {1078}, {1079}, {1080},
    {1081}, {1082}, {1083}, {1084}, {1085}, {1096}, {1097}, {1098} };

new const bventr[2][0] = { {1142}, {1144} }, bventl[2][0] = { {1143}, {1145} }, fbbars[2][0] = { {1115}, {1116} };
new const modlights[2][0] = { {1013}, {1024} }, nitro[3][0] = { {1008}, {1009}, {1010} }, hydraulics[1][0] = { {1087} };
new const g_Base[1][0] = { {1086} }, bscoop[4][0] = { {1004}, {1005}, {1011}, {1012} }, rbbars[4][0] = { {1109}, {1110}, {1123}, {1125} };

hook OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	new query[200], enumid = vehicleid - 1;

	mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `color1` = '%d', `color2` = '%d' WHERE `ID` = '%d'", color1, color2, vehicleid);
	mysql_tquery(mysql, query, "", "");

	Vehicle[enumid][Color1] = color1;
	Vehicle[enumid][Color2] = color2;
	return 1;
}

hook OnVehicleSpawn(vehicleid)
{
	new enumid = vehicleid - 1;

	ApplyModsToVehicle(vehicleid, enumid);
	return 1;
}

hook OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	new query[200], enumid = vehicleid - 1;

	mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `PaintjobID` = '%d' WHERE `ID` = '%d'", paintjobid, vehicleid);
	mysql_tquery(mysql, query, "", "");

	Vehicle[enumid][PaintjobID] = paintjobid;
	ChangeVehiclePaintjob(vehicleid, paintjobid);
	return 1;
}

hook OnVehicleMod(playerid, vehicleid, componentid)
{
    SaveComponent(playerid, vehicleid, componentid);
	return 1;
}

SaveComponent(playerid, vehicleid, componentid)
{
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
	{
		new query[1500];

        for(new s; s < 20; s ++)
		{
     		if(componentid == spoiler[s][0])
       			Vehicle[vehicleid][modComponent1] = componentid;
   	    }

        for(new s; s < 3; s ++)
		{
     		if(componentid == nitro[s][0])
       			Vehicle[vehicleid][modComponent2] = componentid;
   	    }

        for(new s; s < 23; s ++)
		{
     		if(componentid == fbumper[s][0])
       			Vehicle[vehicleid][modComponent3] = componentid;
   	    }

        for(new s; s < 22; s ++)
		{
     		if(componentid == rbumper[s][0])
       			Vehicle[vehicleid][modComponent4] = componentid;
   	    }

        for(new s; s < 28; s ++)
		{
     		if(componentid == exhaust[s][0])
       			Vehicle[vehicleid][modComponent5] = componentid;
   	    }

        for(new s; s < 2; s ++)
		{
     		if(componentid == bventr[s][0])
       			Vehicle[vehicleid][modComponent6] = componentid;
   	    }

        for(new s; s < 2; s ++)
		{
     		if(componentid == bventl[s][0])
       			Vehicle[vehicleid][modComponent7] = componentid;
   	    }

        for(new s; s < 4; s ++)
		{
     		if(componentid == bscoop[s][0])
       			Vehicle[vehicleid][modComponent8] = componentid;
   	    }

        for(new s; s < 17; s ++)
		{
     		if(componentid == rscoop[s][0])
       			Vehicle[vehicleid][modComponent9] = componentid;
   	    }

        for(new s; s < 21; s ++)
		{
     		if(componentid == lskirt[s][0])
       			Vehicle[vehicleid][modComponent10] = componentid;
   	    }

        for(new s; s < 21; s ++)
		{
     		if(componentid == rskirt[s][0])
       			Vehicle[vehicleid][modComponent11] = componentid;
   	    }

        for(new s; s < 1; s ++)
		{
     		if(componentid == hydraulics[s][0])
       			Vehicle[vehicleid][modComponent12] = componentid;
   	    }

        for(new s; s < 1; s ++)
		{
     		if(componentid == g_Base[s][0])
       			Vehicle[vehicleid][modComponent13] = componentid;
   	    }

        for(new s; s < 4; s ++)
		{
     		if(componentid == rbbars[s][0])
       			Vehicle[vehicleid][modComponent14] = componentid;
   	    }

        for(new s; s < 2; s ++)
		{
     		if(componentid == fbbars[s][0])
       			Vehicle[vehicleid][modComponent15] = componentid;
   	    }

        for(new s; s < 17; s ++)
		{
     		if(componentid == wheels[s][0])
       			Vehicle[vehicleid][modComponent16] = componentid;
   	    }

        for(new s; s < 2; s ++)
		{
     		if(componentid == modlights[s][0])
       			Vehicle[vehicleid][modComponent17] = componentid;
   	    }

		mysql_format(mysql, query, sizeof(query), "UPDATE `vehicles` SET `modComponent1` = '%d', `modComponent2` = '%d', `modComponent3` = '%d', `modComponent4` = '%d'",
			Vehicle[vehicleid][modComponent1], Vehicle[vehicleid][modComponent2], Vehicle[vehicleid][modComponent3], Vehicle[vehicleid][modComponent4]);

		mysql_format(mysql, query, sizeof(query), "%s, `modComponent5` = '%d', `modComponent6` = '%d', `modComponent7` = '%d', `modComponent8` = '%d'",
			query, Vehicle[vehicleid][modComponent5], Vehicle[vehicleid][modComponent6], Vehicle[vehicleid][modComponent7], Vehicle[vehicleid][modComponent8]);

		mysql_format(mysql, query, sizeof(query), "%s, `modComponent9` = '%d', `modComponent10` = '%d', `modComponent11` = '%d', `modComponent12` = '%d'",
			query, Vehicle[vehicleid][modComponent9], Vehicle[vehicleid][modComponent10], Vehicle[vehicleid][modComponent11], Vehicle[vehicleid][modComponent12]);

		mysql_format(mysql, query, sizeof(query), "%s, `modComponent13` = '%d', `modComponent14` = '%d', `modComponent15` = '%d', `modComponent16` = '%d', `modComponent17` = '%d' WHERE `ID` = '%d'",
			query, Vehicle[vehicleid][modComponent13], Vehicle[vehicleid][modComponent14], Vehicle[vehicleid][modComponent15], Vehicle[vehicleid][modComponent16], Vehicle[vehicleid][modComponent17], vehicleid);

		mysql_tquery(mysql, query, "", "");

		new enumid = vehicleid --;

		Vehicle[enumid][HasMods] = true;
 	}

	return true;
}
