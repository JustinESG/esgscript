#include <YSI4\YSI_Coding\y_hooks>

//AddPlayerClass(0,-1881.5887,822.4144,35.1772,220.0475,0,0,0,0,0,0); // dmvloc

enum DMVInfo
{
	Float: X,
	Float: Y,
	Float: Z
}

new DMVArray[][DMVInfo] =
{
	{-1895.6023, 867.4259, 34.8937},
	{-1880.8647, 915.1783, 34.8844},
	{-1812.1528, 915.1389, 24.6125},
	{-1790.8585, 948.0328, 24.6097},
	{-1791.1630, 1086.9943, 45.1752},
	{-1729.1752, 1099.2748, 45.1644},
	{-1709.4700, 1170.7302, 25.4865},
	{-1798.1548, 1190.3302, 24.8499},
	{-1868.8201, 1182.0150, 44.8537},
	{-1888.5220, 1159.9380, 45.1717},
	{-1887.3827, 1045.1519, 45.1718},
	{-1902.3964, 947.0983, 34.8944},
	{-1988.9742, 933.9285, 45.1717},
	{-2010.0834, 852.6645, 45.1718},
	{-1917.2898, 837.9581, 35.6385},
	{-1876.6674, 833.3923, 34.9560}
};

new PlayerText: DMV_TD[MAX_PLAYERS];
new DMVtimer[MAX_PLAYERS];
new DMVtime[MAX_PLAYERS];

new bool: takingDMVtest[MAX_PLAYERS];
new DMVcar[MAX_PLAYERS];
new DMVcheckpoint[MAX_PLAYERS];
new DMVprogress[MAX_PLAYERS];

hook OnPlayerExitVehicle(playerid, vehicleid)
{
	if(vehicleid == DMVcar[playerid] && takingDMVtest[playerid])
	{
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You have failed your drivings test for leaving the test vehicle! Try again!");
        DestroyDynamicCP(DMVcheckpoint[playerid]);

		takingDMVtest[playerid] = false;
		DMVprogress[playerid] = 0;
		DestroyVehicle(DMVcar[playerid]);

		KillTimer(DMVtimer[playerid]);
		PlayerTextDrawHide(playerid, DMV_TD[playerid]);
	}
	return 1;
}

hook OnPlayerEnterDynamicCP(playerid, checkpointid)
{
	if(checkpointid == DMVcheckpoint[playerid])
	{
		new id = DMVprogress[playerid] ++, Float: vHealth;

		if(id < sizeof(DMVArray))
		{
			DestroyDynamicCP(DMVcheckpoint[playerid]);
			DMVcheckpoint[playerid] = CreateDynamicCP(DMVArray[id][X], DMVArray[id][Y], DMVArray[id][Z], 10.0, 0, 0, playerid, 200.0);
		}

		else if(id == sizeof(DMVArray))
		{
			DestroyDynamicCP(DMVcheckpoint[playerid]);
			GetVehicleHealth(DMVcar[playerid], vHealth);

		    if(vHealth > 980)
		    {
            	SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Congratulations, you {629E64}passed{E0E0E0} your drivings test! You have been given your drivings license.");
				Character[playerid][DrivingLicense] = 1;
			}

			else SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You have failed your drivings test for damaging your vehicle. Try again!");

			DMVprogress[playerid] = 0;
			DestroyVehicle(DMVcar[playerid]);

            takingDMVtest[playerid] = false;
			KillTimer(DMVtimer[playerid]);
			PlayerTextDrawHide(playerid, DMV_TD[playerid]);
		}
	}
	return 1;
}


CMD:startdrivingtest(playerid, params[])
{
	if(IsPlayerInRangeOfPoint(playerid, 5.0, -1881.5887, 822.4144, 35.1772))
	{
		if(!takingDMVtest[playerid])
		{
			DMVcar[playerid] = CreateVehicle(405, -1895.6161, 822.7810, 34.8906, 0.4789, 24, 1, 1);
			SetVehicleParamsEx(DMVcar[playerid], true, true, false, true, false, false, false);

	        SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} To complete the DMV test, drive through all checkpoints while taking minimal damage and before the time runs out!");
	        takingDMVtest[playerid] = true;

		    LoadDMVTextdraws(playerid);
			PlayerTextDrawShow(playerid, DMV_TD[playerid]);

			DMVtime[playerid] = 80;
			DMVtimer[playerid] = SetTimerEx("DMVTick", 1000, true, "i", playerid);

			PutPlayerInVehicle(playerid, DMVcar[playerid], 0);

			new id = DMVprogress[playerid];
			DMVcheckpoint[playerid] = CreateDynamicCP(DMVArray[id][X], DMVArray[id][Y], DMVArray[id][Z], 10.0, 0, 0, playerid, 200.0);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're already taking a DMV test!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not at the DMV center!");

	return true;
}

forward DMVTick(playerid);
public DMVTick(playerid)
{
	new string[80];

	if(-- DMVtime[playerid] > 0)
	{
	    format(string, sizeof(string), "Test time left:~n~~b~%d ~w~seconds", DMVtime[playerid]);
		PlayerTextDrawSetString(playerid, DMV_TD[playerid], string);
	}

	if(DMVtime[playerid] == 0)
	{
		SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You have failed your drivings test for running out of time. Try again!");
        DestroyDynamicCP(DMVcheckpoint[playerid]);

		DMVprogress[playerid] = 0;
		DestroyVehicle(DMVcar[playerid]);

        takingDMVtest[playerid] = false;
		KillTimer(DMVtimer[playerid]);
		PlayerTextDrawHide(playerid, DMV_TD[playerid]);
	}

	return true;
}

LoadDMVTextdraws(playerid)
{
	DMV_TD[playerid] = CreatePlayerTextDraw(playerid, 430.666717, 231.881469, "Test time left:~n~~b~80 ~w~seconds");
	PlayerTextDrawLetterSize(playerid, DMV_TD[playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, DMV_TD[playerid], 1);
	PlayerTextDrawColor(playerid, DMV_TD[playerid], -1);
	PlayerTextDrawSetShadow(playerid, DMV_TD[playerid], 0);
	PlayerTextDrawSetOutline(playerid, DMV_TD[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, DMV_TD[playerid], 51);
	PlayerTextDrawFont(playerid, DMV_TD[playerid], 1);
	PlayerTextDrawSetProportional(playerid, DMV_TD[playerid], 1);

	return true;
}
