/*
	Author: Mads
	Date: 21-02-2015
	For: EastSideGaming
	Description: Custom damages and weapons
	File: weapons.pwn
*/
#define 	MAX_WEAPONS 	30
#define 	MAX_AMMUNATIONS	30

new TazeSpark[MAX_PLAYERS];

enum ammunationInfo {
	ID,
	Name[50],
	DamagePoints
}
new Ammunation[MAX_AMMUNATIONS][ammunationInfo];

enum weaponInfo {
	ID,
	Name[50],
	Model,
	AmmunationID,
	Accuracy // %
}
new Weapons[MAX_WEAPONS][weaponInfo];

SQL_LoadWeaponSystem() {
	mysql_tquery(mysql, "SELECT * FROM `weapons`", "SQL_LoadDynamicWeapons");
	mysql_tquery(mysql, "SELECT * FROM `weapon_ammunation`", "SQL_LoadDynamicAmmunation");
	return 1;
}

CMD:givetazer(playerid, params[]) {
	GivePlayerWeapon(playerid, 23, 100);
	Character[playerid][HoldingWeapon] = 18;
	return 1;
}

CMD:giveak(playerid, params[]) {
	GivePlayerWeapon(playerid, 30, 100);
	Character[playerid][HoldingWeapon] = 8;
	return 1;
}

CMD:givem4(playerid, params[]) {
	GivePlayerWeapon(playerid, 31, 100);
	Character[playerid][HoldingWeapon] = 9;
	return 1;
}

CMD:sethp(playerid, params[]) {
	new target;
	if(!sscanf(params, "u", target)) {
		SetPlayerHealth(target, 100);
	}
	return 1;
}

CMD:setarmour(playerid, params[]) {
	new target;
	if(!sscanf(params, "u", target)) {
		SetPlayerArmour(target, 100);
	}
	return 1;
}

hook OnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart) {
	new Float:health, Float:armour, Float:damage;

	GetPlayerHealth(playerid, health);
	GetPlayerArmour(playerid, armour);

	new weapon = Character[playerid][HoldingWeapon];
	new ammunation = Weapons[weapon][AmmunationID];

	if(bodypart == BODY_PART_HEAD) {
		SetPlayerHealth(playerid, 0);
		return 1;
	}

	if(bodypart == BODY_PART_LEFT_LEG || bodypart == BODY_PART_RIGHT_LEG && weapon != 19 && weapon != 18) {
		// Disable running
	}

	if(bodypart == BODY_PART_LEFT_ARM || bodypart == BODY_PART_RIGHT_ARM && weapon != 19 && weapon != 18) {
		// Lower accuracy
	}

	if(weapon == 19) { // Rubber Bullets

	} else if(weapon == 18) {// Tazer
		new Float:x, Float:y, Float:z;
		GetPlayerPos(playerid, x, y, z);
		ClearAnimations(playerid);
		ApplyAnimation(playerid, "PED", "KO_skid_front", 4.1, 0, 1, 1, 1, 0);
		TazeSpark[playerid] = CreateObject(18717, x, y, z-3, 0, 0, 0);
		SetTimerEx("DestroySpark", 1200, 0, "i", playerid);
		SetTimerEx("RemoveTaze", 5000, 0, "i", playerid);
	}

	damage = Ammunation[ammunation][DamagePoints];

	new accuracyRandom = random(100);

	if(accuracyRandom >= Weapons[weapon][Accuracy]) {// ex. accuracyRandom is < 80 = 20% chance of not hitting
		SendClientMessage(playerid, 0xFFFFFF, "Miss");
		return 1;
	}
	
	if(armour) {
		if(armour > damage) 
			SetPlayerArmour(playerid, armour - damage);
		else {
			SetPlayerArmour(playerid, 0.0);

			new Float:damage_health = damage - armour;
			if(armour >= health)
				SetPlayerHealth(playerid, 0.0);
			else
				SetPlayerHealth(playerid, health - damage_health);
		}
	} else {
		if(damage >= health)
			SetPlayerHealth(playerid, 0.0);
		else
			SetPlayerHealth(playerid, health - damage);
	}
	
	return 1;
}

forward SQL_LoadDynamicWeapons();
public SQL_LoadDynamicWeapons(){
	new rows, fields;
	cache_get_data(rows, fields);
	new i;
	for(i = 0; i < rows; i++) {
		Weapons[i][ID] = cache_get_field_content_int(i, "ID");
		cache_get_field_content(i, "Name", Weapons[i][Name], .max_len = 50);
		Weapons[i][Model] = cache_get_field_content_int(i, "Model");
		Weapons[i][AmmunationID] = cache_get_field_content_int(i, "AmmunationID");
		Weapons[i][Accuracy] = cache_get_field_content_int(i, "Accuracy");
	}
	printf("[SQL_LoadDynamicWeapons] Loaded %d weapons, and added to the server", i);
	return 1;
}

forward SQL_LoadDynamicAmmunation();
public SQL_LoadDynamicAmmunation() {
	new rows, fields;
	cache_get_data(rows, fields);
	new i;
	for(i = 0; i < rows; i++) {
		Ammunation[i][ID] = cache_get_field_content_int(i, "ID");
		cache_get_field_content(i, "Name", Ammunation[i][Name], .max_len = 50);
		Ammunation[i][DamagePoints] = cache_get_field_content_int(i, "DamagePoints");
	}
	printf("[SQL_LoadDynamicAmmunation] Loaded %d types of ammunation, and added to the server", i);
	return 1;
}

forward DestroySpark(playerid);
public DestroySpark(playerid) {
	DestroyObject(TazeSpark[playerid]);
	return 1;
}

forward ClearTaze(playerid);
public ClearTaze(playerid) {
	TogglePlayerControllable(playerid, 1);
	ClearAnimations(playerid);
	return 1;
}