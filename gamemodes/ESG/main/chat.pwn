#include <YSI4\YSI_Coding\y_hooks>

#define COLOR_ACTION                (0xC2A2DAAA)
#define COLOR_LOCALOOC              (0xA3A3A3AA)
#define COLOR_GLOBALOOC             (0x92D6DEAA)
#define COLOR_MSG_TO                (0xF2F246aa)
#define COLOR_MSG_FROM				(0xF2DE46aa)

#define COLOR_NEARBY_A              (0xDBDBDBAA)
#define COLOR_NEARBY_B              (0xCCCCCCAA)
#define COLOR_NEARBY_C              (0xB3B3B3AA)
#define COLOR_NEARBY_D              (0x949494AA)

hook OnPlayerText(playerid, text[])
{
// Ku-kuru-kurukuru-kurukuru~             -- http://youtu.be/x4RxkZSpTNQ

    new string[144];
    
	if(DeathMode[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't speak while being in death mode!");
    
/*	if(IsCalling[playerid])
	{
	    format(string, sizeof(string), "%s says (phone): %s", GetRoleplayName(playerid), text);
	    SendLocalMessageSay(playerid, string, 10.0);

	 	format(string, sizeof(string), "%s says (phone): %s", GetRoleplayName(playerid), text);
	    ReturnSplittedMessage(CallingWith[playerid], playerid, COLOR_MSG_FROM, string, 1);
	}*/

	else
	{
	    format(string, sizeof(string), "%s says: %s", GetRoleplayName(playerid), text);
	    SendLocalMessageSay(playerid, string, 10.0);
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////// --- IC COMMANDS --- /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

CMD:w(playerid, params[])
{
	new targetid, text[90], string[144];

	if(sscanf(params, "us[90]", targetid, text))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /w(hisper) [playerid/name] [text]");

	format(string, sizeof(string), "Whisper to %s: %s", GetRoleplayName(targetid), text);
	ReturnSplittedMessage(playerid, playerid, COLOR_MSG_TO, string, 1); // to

	format(string, sizeof(string), "%s whispers: %s", GetRoleplayName(playerid), text);
	ReturnSplittedMessage(targetid, -1, COLOR_MSG_FROM, string, 1); // from

	return true;
}

CMD:whisper(playerid, params[])
	return cmd_w(playerid, params);

CMD:me(playerid, params[])
{
    new text[92], string[144];

    if(sscanf(params, "s[92]", text)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /me [text]");

    format(string, sizeof(string), "%s %s", GetRoleplayName(playerid), text);
    SendLocalMessage(playerid, COLOR_ACTION, string, 10.0);

    return true;
}

CMD:do(playerid, params[])
{
    new text[92], string[144];

    if(sscanf(params, "s[92]", text)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /do [text]");

    format(string, sizeof(string), "%s (( %s ))", text, GetRoleplayName(playerid));
    SendLocalMessage(playerid, COLOR_ACTION, string, 10.0);

    return true;
}

CMD:low(playerid, params[])
{
    new text[92], string[144];

    if(sscanf(params, "s[92]", text)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /low [text]");

    format(string, sizeof(string), "%s [low]: %s", GetRoleplayName(playerid), text);
    SendLocalMessage(playerid, 0xe0e0e0aa, string, 10.0);

    return true;
}

CMD:yell(playerid, params[])
{
    new text[92], string[144];

    if(sscanf(params, "s[92]", text)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /yell - /s(hout) [text]");

    format(string, sizeof(string), "%s yells: %s", GetRoleplayName(playerid), text);
    SendLocalMessageSay(playerid, string, 10.0);

    return true;
}

CMD:shout(playerid, params[])
	return cmd_yell(playerid, params);

CMD:s(playerid, params[])
	return cmd_yell(playerid, params);

////////////////////////////////////////////////////////////////////////////////
///////////////////////////// --- OOC COMMANDS --- /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

CMD:pm(playerid, params[])
{
	new targetid, text[90], string[144];

	if(sscanf(params, "us[90]", targetid, text))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /pm [playerid/name] [text]");

	format(string, sizeof(string), "PM -> (%d) %s: %s", targetid, GetRoleplayName(targetid), text);
	ReturnSplittedMessage(playerid, playerid, COLOR_MSG_TO, string, 1); // to

	format(string, sizeof(string), "PM <- (%d) %s: %s", playerid, GetRoleplayName(playerid), text);
	ReturnSplittedMessage(targetid, -1, COLOR_MSG_FROM, string, 1); // from

	return true;
}

CMD:b(playerid, params[])
{
    new text[92], string[144];

    if(sscanf(params, "s[92]", text)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /b [text]");

	format(string, sizeof(string), "(( [%d] %s: %s ))", playerid, GetRoleplayName(playerid), text);

	if(IsOnAdminDuty(playerid))
	{
		format(string, sizeof(string), "(( [%d] {83D444}%s{A3A3A3}: %s ))", playerid, GetRoleplayName(playerid), text);
	}

    SendLocalMessage(playerid, COLOR_LOCALOOC, string, 10.0);

    return true;
}

CMD:o(playerid, params[])
{
	new text[92], string[144];

    if(sscanf(params, "s[92]", text)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /o(oc) [text]");

    format(string, sizeof(string), "{( [%d] %s: %s )}", playerid, GetRoleplayName(playerid), text);
	ReturnSplittedMessage(playerid, playerid, COLOR_GLOBALOOC, string, 2);

	return true;
}

CMD:ooc(playerid, params[])
	return cmd_o(playerid, params);

////////////////////////////////////////////////////////////////////////////////
//////////////////////////////// --- FUNCTIONS --- /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

ReturnSplittedMessage(playerid, senderid, color, text[], type)
{
    if(strlen(text) <= 70) SendClientMessage(playerid, color, text);

    else if(strlen(text) > 70)
    {
        // Was "105" before, change back if shit fucks up
		if(strlen(text) > 144)
			return SendClientMessage(senderid, COLOR_ERROR, "[ERROR]:{E0E0E0} Message format request can't be completed because the string exceeds the limit!");

        new texts[74];

        strmid(texts, text, 70, 144);
      	strdel(text, 70, 144);

//      strins(text, " ..", 70, 3);
//		printf("START %s END", text);

        if(type == 1) // Local
        {
            SendClientMessage(playerid, color, text);
            SendClientMessage(playerid, color, texts);
        }

        else if(type == 2) // Global
        {
            SendClientMessageToAll(color, text);
            SendClientMessageToAll(color, texts);
        }
    }

    return true;
}

SendLocalMessage(playerid, color, text[], Float: range)
{
    new Float: psX, Float: psY, Float: psZ;
    GetPlayerPos(playerid, psX, psY, psZ);

    foreach(new i: Player)
    {
        if(IsPlayerInRangeOfPoint(i, range, psX, psY, psZ))
        {
            ReturnSplittedMessage(i, playerid, color, text, 1);
        }
    }

    return true;
}

SendLocalMessageSay(playerid, text[], Float: range)
{
    new Float: psX, Float: psY, Float: psZ;
    GetPlayerPos(playerid, psX, psY, psZ);

    foreach(new i: Player)
    {
        if(IsPlayerInRangeOfPoint(i, range, psX, psY, psZ))
        {
            if(range > 2)
            	return SendClientMessage(i, COLOR_NEARBY_A, text);

            if(range > 4)
            	return SendClientMessage(i, COLOR_NEARBY_B, text);
            	
            if(range > 6)
            	return SendClientMessage(i, COLOR_NEARBY_C, text);
            	
            if(range > 8)
            	return SendClientMessage(i, COLOR_NEARBY_D, text);
        }
    }

    return true;
}

SendLocalMessageEx(playerid, color, text[], Float: range)
{
    new Float: psX, Float: psY, Float: psZ;
    GetPlayerPos(playerid, psX, psY, psZ);

    foreach(new i: Player)
    {
        if(IsPlayerInRangeOfPoint(i, range, psX, psY, psZ))
        {
			SendClientMessage(i, color, text);
        }
    }

    return true;
}
