#include <YSI4\YSI_Coding\y_hooks>

#define DEATHSPAWN_X        (-2650.1941)
#define DEATHSPAWN_Y        (698.4097)
#define DEATHSPAWN_Z        (27.4727)
#define DEATHSPAWN_A        (356.7799)

#define DEATHMODE_TIME      (20)

new bool: DeathMode[MAX_PLAYERS];
new bool: Revived[MAX_PLAYERS];
new Float: DeathPos[MAX_PLAYERS][3];
new PlayerText: DeathTD[MAX_PLAYERS];
new DeathTimer[MAX_PLAYERS];
new DeathTime[MAX_PLAYERS];
new DeathSkin[MAX_PLAYERS];

hook OnPlayerConnect(playerid)
{
    //PreloadAnimLib(playerid, "ped");
	LoadDeathTextDraw(playerid);
	return 1;
}

hook OnCharacterSpawn(playerid)
{
	if(DeathMode[playerid])
	{
		return PutPlayerInDeathMode(playerid);
	}
	return 1;
}

hook OnPlayerDeath(playerid, killerid, reason)
{
	new Float: dx, Float: dy, Float: dz;
	GetPlayerPos(playerid, dx, dy, dz);

	if(killerid != INVALID_PLAYER_ID)
	{
		if(!DeathMode[playerid])
		{
		    DeathMode[playerid] = true;
			DeathTime[playerid] = DEATHMODE_TIME;
			DeathSkin[playerid] = GetPlayerSkin(playerid);

			DeathPos[playerid][0] = dx;
			DeathPos[playerid][1] = dy;
			DeathPos[playerid][2] = dz;
		}
	}
	return 1;
}

CMD:medikit(playerid, params[])
{
	new targetid, Float: tdX, Float: tdY, Float: tdZ, string[100];
	GetPlayerPos(targetid, tdX, tdY, tdZ);

	if(Character[playerid][Medikits] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a medikit!");

	if(sscanf(params, "u", targetid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /medikit [player/name]");

	if(!IsPlayerConnected(targetid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} The name or playerid you entered is invalid.");

	if(!DeathMode[targetid])
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This player isn't in death mode at the moment!");

	if(targetid == playerid)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't revive yourself!");

	if(IsPlayerInRangeOfPoint(playerid, 3.0, tdX, tdY, tdZ))
	{
		Revived[playerid] = true;
		KillDeathModedPlayer(playerid);
		
		Character[playerid][Medikits] --;

		format(string, sizeof(string), "[INFO]{E0E0E0} You have been revived by %s!", GetRoleplayName(playerid));
		SendClientMessage(targetid, COLOR_BLUE, string);
		
		format(string, sizeof(string), "[INFO]{E0E0E0} You have revived %s! You have %d medikit(s) left.", GetRoleplayName(targetid), Character[playerid][Medikits]);
		SendClientMessage(playerid, COLOR_BLUE, string);
		
		format(string, sizeof(string), "* %s has just revived %s using a medikit!", GetRoleplayName(playerid), GetRoleplayName(targetid));
		SendLocalMessage(playerid, 0xC2A2DAAA, string, 10.0);
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near the player you're trying to revive!");

	return true;
}

forward DeathTick(playerid);
public DeathTick(playerid)
{
	new string[50];

	if(DeathMode[playerid])
	{
	    if(-- DeathTime[playerid] > 0)
	    {
			format(string, sizeof(string), "~n~Time until death:~n~~r~%d~w~ seconds left", DeathTime[playerid]);
			PlayerTextDrawSetString(playerid, DeathTD[playerid], string);

			SetPlayerHealth(playerid, 98305);
	    }

		else if(DeathTime[playerid] <= 0)
		{
		    KillDeathModedPlayer(playerid);
		}
	}

	else KillDeathModedPlayer(playerid);

	return true;
}

PutPlayerInDeathMode(playerid)
{
	SetPlayerSkin(playerid, DeathSkin[playerid]);
	SetPlayerMoneyEx(playerid, Character[playerid][Money]);
	
    SetPlayerPos(playerid, DeathPos[playerid][0], DeathPos[playerid][1], DeathPos[playerid][2]);
	TogglePlayerControllable(playerid, false);

	SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} If someone doesn't revive you before the time expires, you will die!");
	PlayerTextDrawShow(playerid, DeathTD[playerid]);

	DeathTimer[playerid] = SetTimerEx("DeathTick", 1000, true, "i", playerid);
    ApplyAnimation(playerid, "PARACHUTE", "FALL_skyDive_DIE", 4.1, 0, 0, 0, 1, 1, 1);

	return true;
}

KillDeathModedPlayer(playerid)
{
    PlayerTextDrawHide(playerid, DeathTD[playerid]);
	KillTimer(DeathTimer[playerid]);

	if(!Revived[playerid])
	{
		SetPlayerPos(playerid, DEATHSPAWN_X, DEATHSPAWN_Y, DEATHSPAWN_Z);
		SetPlayerFacingAngle(playerid, DEATHSPAWN_A);
		SetCameraBehindPlayer(playerid);
	}

	TogglePlayerControllable(playerid, true);
	SetPlayerHealth(playerid, 85);

	DeathMode[playerid] = false;
    Revived[playerid] = false;

    ApplyAnimation(playerid, "PED", "getup", 4.1, 0, 0, 0, 0, 1, 1);

	return true;
}

LoadDeathTextDraw(playerid)
{
	DeathTD[playerid] = CreatePlayerTextDraw(playerid, 376.666625, 316.918487, "~n~Time until death:~n~~r~320~w~ seconds left");
	PlayerTextDrawLetterSize(playerid, DeathTD[playerid], 0.405000, 1.529481);
	PlayerTextDrawAlignment(playerid, DeathTD[playerid], 1);
	PlayerTextDrawColor(playerid, DeathTD[playerid], -1);
	PlayerTextDrawSetShadow(playerid, DeathTD[playerid], 0);
	PlayerTextDrawSetOutline(playerid, DeathTD[playerid], 1);
	PlayerTextDrawBackgroundColor(playerid, DeathTD[playerid], 51);
	PlayerTextDrawFont(playerid, DeathTD[playerid], 1);
	PlayerTextDrawSetProportional(playerid, DeathTD[playerid], 1);
}
