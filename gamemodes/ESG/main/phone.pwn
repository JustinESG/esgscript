#include <YSI4\YSI_Coding\y_hooks>

new PlayerText: PhoneTD[31][MAX_PLAYERS];
new PlayerText: TextSign[MAX_PLAYERS];
new PlayerText: PhoneSign[MAX_PLAYERS];

new bool: PendingTextMsg[MAX_PLAYERS];
new PendingTextMsgText[MAX_PLAYERS][90];
new PendingTextMsgNumber[MAX_PLAYERS];

new bool: IsCalling[MAX_PLAYERS];
new bool: PendingCall[MAX_PLAYERS];
new CallingWith[MAX_PLAYERS];

new bool: PhoneTogged[MAX_PLAYERS];
new DiallingNumber[MAX_PLAYERS];

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(newkeys & KEY_ANALOG_LEFT)
	{
	    return cmd_phone(playerid);
	}
	return 1;
}

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    new number, string[60], smstext[90];

	switch(dialogid)
	{
	    case DIALOG_PHONE_CALL_NUMBER:
	    {
	        if(!response) return SelectTextDraw(playerid, 0xA4E5F3FF);

			if(response)
			{
				if(!IsNumeric(inputtext))
					return ShowPlayerDialog(playerid, DIALOG_PHONE_CALL_NUMBER, DIALOG_STYLE_INPUT, "Call", "Only numbers are allowed!", "Confirm", "Cancel");

			    if(strlen(inputtext) > 6 || strlen(inputtext) < 6)
					return ShowPlayerDialog(playerid, DIALOG_PHONE_CALL_NUMBER, DIALOG_STYLE_INPUT, "Call", "Enter the number of the individual you want to call\nA number can only be 6 digits long!", "Confirm", "Cancel");

            	if(IsCalling[playerid] || IsCalling[GetPlayerByNumber(number)])
    				return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You, or the number you are trying to call, are already connected to a phone call.");

			    number = strval(inputtext);

				HidePhone(playerid);
			    ShowPhone(playerid, 3);

			    format(string, sizeof(string), "Calling...~n~~w~%d", number);
			    PlayerTextDrawSetString(playerid, PhoneTD[26][playerid], string);

                CallNumber(playerid, number);
			    SelectTextDraw(playerid, 0xA4E5F3FF);
			}
	    }

	    case DIALOG_PHONE_TEXT_NUMBER:
	    {
	        if(!response) return SelectTextDraw(playerid, 0xA4E5F3FF);

	        if(response)
        	{
			    if(strlen(inputtext) > 6 || strlen(inputtext) < 6)
					return ShowPlayerDialog(playerid, DIALOG_PHONE_TEXT_NUMBER, DIALOG_STYLE_INPUT, "SMS", "Enter the number of the individual you want to text\nA number can only be 6 digits long!", "Confirm", "Cancel");

				number = strval(inputtext);
                DiallingNumber[playerid] = number;

				ShowPlayerDialog(playerid, DIALOG_PHONE_TEXT_TEXT, DIALOG_STYLE_INPUT, "SMS", "Enter your message", "Confirm", "Cancel");
	        }
	    }

	    case DIALOG_PHONE_TEXT_TEXT:
	    {
	        if(!response) return SelectTextDraw(playerid, 0xA4E5F3FF);

			if(response)
			{
		        if(strlen(inputtext) > 72)
		            return ShowPlayerDialog(playerid, DIALOG_PHONE_TEXT_TEXT, DIALOG_STYLE_INPUT, "SMS", "Enter your message\nCan't have more than 72 characters!", "Confirm", "Cancel");

		        format(smstext, sizeof(smstext), "%s", inputtext);

				if(strlen(smstext) >= 18) strins(smstext, "~n~", 18, 3);
		        if(strlen(smstext) >= 36) strins(smstext, "~n~", 39, 3);
		        if(strlen(smstext) >= 54) strins(smstext, "~n~", 60, 3);

				HidePhone(playerid);
			    ShowPhone(playerid, 4);

				format(string, sizeof(string), "Message to ~b~%d", DiallingNumber[playerid]);
				SendTextMessage(Character[playerid][PlayerChar[playerid]][PhoneNumber], DiallingNumber[playerid], smstext);

				PlayerTextDrawSetString(playerid, PhoneTD[28][playerid], string);
		        PlayerTextDrawSetString(playerid, PhoneTD[29][playerid], smstext);

				SelectTextDraw(playerid, 0xA4E5F3FF);
			}
	    }
	}
	return 1;
}

hook OnPlayerClickPlayerTD(playerid, PlayerText:playertextid)
{
	if(playertextid == PhoneTD[18][playerid]) // close phone button
	{
	    HidePhone(playerid);
	    CancelSelectTextDraw(playerid);
	}

	if(playertextid == PhoneTD[16][playerid]) // go to menu button
	{
	    HidePhone(playerid);
	    ShowPhone(playerid, 2);
	}

	if(playertextid == PhoneTD[23][playerid]) // menu cancel button
	{
	    HidePhone(playerid);
	    ShowPhone(playerid, 1);
	}

	if(playertextid == PhoneTD[22][playerid]) // text number button
	{
		CancelSelectTextDraw(playerid);
		ShowPlayerDialog(playerid, DIALOG_PHONE_TEXT_NUMBER, DIALOG_STYLE_INPUT, "SMS", "Enter the number of the individual you want to text", "Confirm", "Cancel");
	}

	if(playertextid == PhoneTD[21][playerid]) // call number button
	{
		CancelSelectTextDraw(playerid);
		ShowPlayerDialog(playerid, DIALOG_PHONE_CALL_NUMBER, DIALOG_STYLE_INPUT, "Call", "Enter the number of the individual you want to call", "Confirm", "Cancel");
	}

	if(playertextid == PhoneTD[27][playerid]) // hangup button
	{
	    EndCall(playerid);

	    HidePhone(playerid);
	    ShowPhone(playerid, 2);
	}

	if(playertextid == PhoneTD[27][playerid]) // show sms button
	{
	    HidePhone(playerid);
	    ShowPhone(playerid, 2);
	}

	if(playertextid == PhoneTD[30][playerid]) // hide/send sms button
	{
	    HidePhone(playerid);
	    ShowPhone(playerid, 2);
	}
	return 1;
}

CMD:togphone(playerid, params[])
{
	if(Character[playerid][PlayerChar[playerid]][PhoneNumer] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a phone!");

	if(PhoneTogged[playerid])
	{
		PhoneTogged[playerid] = false;
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have turned your phone on.");
	}

	else if(!PhoneTogged[playerid])
	{
		PhoneTogged[playerid] = true;
		SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have turned your phone off.");
	}

	return true;
}

CMD:phone(playerid)
{
	new string[90];

	PlayerTextDrawHide(playerid, TextSign[playerid]);
	PlayerTextDrawHide(playerid, PhoneSign[playerid]);

	DestroyPhoneTextDraws(playerid);
	LoadPhoneTextDraws(playerid);

	if(PhoneTogged[playerid])
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} Your phone is currently turned off! /togphone");

	if(Player[playerid][Phone] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a phone!");

	if(!PendingTextMsg[playerid] && !PendingCall[playerid])
		return ShowPhone(playerid, 1);

	else if(PendingCall[playerid])
	{
		format(string, sizeof(string), "Calling...~n~~w~%d", Player[CallingWith[playerid]][PhoneNumber]);
		PlayerTextDrawSetString(playerid, PhoneTD[26][playerid], string);

		IsCalling[playerid] = true;
		PendingCall[playerid] = false;

		ShowPhone(playerid, 3);
	}

	else if(PendingTextMsg[playerid])
	{
	    format(string, sizeof(string), "Message from ~b~%d", PendingTextMsgNumber[playerid]);

		PlayerTextDrawSetString(playerid, PhoneTD[28][playerid], string);
		PlayerTextDrawSetString(playerid, PhoneTD[29][playerid], PendingTextMsgText[playerid]);

		PendingTextMsg[playerid] = false;

	    ShowPhone(playerid, 4);
	}

	return true;
}

SendTextMessage(fromnumber, number, sms[90])
{
	new string[80], id = GetPlayerByNumber(number), playerid = GetPlayerByNumber(fromnumber);

	if(id == 999)
	{
		HidePhone(playerid);
	    ShowPhone(playerid, 2);

		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This number does not exist!");
	}

    if(Player[id][PhoneNumber] == number)
    {
		PlayerTextDrawShow(id, TextSign[id]);
		PendingTextMsg[id] = true;

		PendingTextMsgText[id] = sms;
		PendingTextMsgNumber[id] = fromnumber;

		format(string, sizeof(string), "[INFO]{E0E0E0} You have just recieved a text message from %d!", fromnumber);
		SendClientMessage(id, COLOR_BLUE, string);
    }

	return true;
}

GetPlayerByNumber(number)
{
	foreach(new i: Player)
	{
	    if(Player[i][PhoneNumber] == number)
	    {
			return i;
		}

		else return 999;
	}

	return false;
}

EndCall(playerid)
{
	new id = CallingWith[playerid];

	PendingCall[playerid] = false;
	PendingCall[id] = false;

	IsCalling[playerid] = false;
	IsCalling[id] = false;

	HidePhone(id), ShowPhone(id, 1);
	HidePhone(playerid), ShowPhone(playerid, 1);

	SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You hung up.");
	SendClientMessage(id, COLOR_BLUE, "[INFO]{E0E0E0} They hung up.");

	PlayerTextDrawHide(id, PhoneSign[id]);
	PlayerTextDrawHide(playerid, PhoneSign[id]);

	return true;
}

CallNumber(playerid, number)
{
	new string[60], id = GetPlayerByNumber(number);

	if(id == 999 || PhoneTogged[id])
	{
		HidePhone(playerid);
	    ShowPhone(playerid, 2);

		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This number does not exist!");
	}

	PlayerTextDrawShow(id, PhoneSign[id]);

	PendingCall[id] = true;
	CallingWith[id] = playerid;

	format(string, sizeof(string), "[INFO]{E0E0E0} Your phone is being called by %d.", Player[playerid][PhoneNumber]);
	SendClientMessage(id, COLOR_BLUE, string);

	return true;
}

DestroyPhoneTextDraws(playerid)
{
	for(new i; i < sizeof(PhoneTD); i ++)
	{
	    PlayerTextDrawDestroy(playerid, PhoneTD[i][playerid]);
	}

	return true;
}

HidePhone(playerid)
{
	for(new i; i < sizeof(PhoneTD); i ++)
	{
	    PlayerTextDrawHide(playerid, PhoneTD[i][playerid]);
	}

	return true;
}

ShowPhone(playerid, screen)
{
	switch(screen)
	{
	    case 1: // main frame
	    {
		    for(new i; i < 21; i ++)
		    {
		        PlayerTextDrawShow(playerid, PhoneTD[i][playerid]);
		    }
	    }

		case 2: // menu frame
		{
		    for(new i; i < 16; i ++)
		    {
		        PlayerTextDrawShow(playerid, PhoneTD[i][playerid]);
		    }

			PlayerTextDrawShow(playerid, PhoneTD[21][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[22][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[23][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[24][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[25][playerid]);
		}

		case 3: // phone frame
		{
		    for(new i; i < 16; i ++)
		    {
		        PlayerTextDrawShow(playerid, PhoneTD[i][playerid]);
		    }

			PlayerTextDrawShow(playerid, PhoneTD[26][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[27][playerid]);
		}

		case 4: // text frame
		{
		    for(new i; i < 16; i ++)
		    {
		        PlayerTextDrawShow(playerid, PhoneTD[i][playerid]);
		    }

			PlayerTextDrawShow(playerid, PhoneTD[28][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[29][playerid]);
			PlayerTextDrawShow(playerid, PhoneTD[30][playerid]);
		}
	}

	SelectTextDraw(playerid, 0xA4E5F3FF);

	return true;
}

LoadPhoneTextDraws(playerid)
{
	PhoneTD[0][playerid] = CreatePlayerTextDraw(playerid, 611.666870, 334.596252, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[0][playerid], 0.000000, 14.953300);
	PlayerTextDrawTextSize(playerid, PhoneTD[0][playerid], 507.666748, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[0][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[0][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[0][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[0][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[0][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[0][playerid], 0);

	PhoneTD[1][playerid] = CreatePlayerTextDraw(playerid, 511.000030, 379.811096, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[1][playerid], 0.000000, 9.039714);
	PlayerTextDrawTextSize(playerid, PhoneTD[1][playerid], 504.333404, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[1][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[1][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[1][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[1][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[1][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[1][playerid], 0);

	PhoneTD[2][playerid] = CreatePlayerTextDraw(playerid, 502.333526, 311.525604, "(");
	PlayerTextDrawLetterSize(playerid, PhoneTD[2][playerid], 0.582333, 20.158821);
	PlayerTextDrawAlignment(playerid, PhoneTD[2][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[2][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[2][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[2][playerid], 1);
	PlayerTextDrawSetProportional(playerid, PhoneTD[2][playerid], 1);

	PhoneTD[3][playerid] = CreatePlayerTextDraw(playerid, 508.999786, 380.640716, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[3][playerid], 0.000000, -3.728187);
	PlayerTextDrawTextSize(playerid, PhoneTD[3][playerid], 507.999847, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[3][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[3][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[3][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[3][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[3][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[3][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[3][playerid], 0);

	PhoneTD[4][playerid] = CreatePlayerTextDraw(playerid, 505.333465, 316.918457, "(");
	PlayerTextDrawLetterSize(playerid, PhoneTD[4][playerid], 0.471333, 8.303407);
	PlayerTextDrawAlignment(playerid, PhoneTD[4][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[4][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[4][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[4][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[4][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[4][playerid], 1);
	PlayerTextDrawSetProportional(playerid, PhoneTD[4][playerid], 1);

	PhoneTD[5][playerid] = CreatePlayerTextDraw(playerid, 608.667297, 321.896209, ")");
	PlayerTextDrawLetterSize(playerid, PhoneTD[5][playerid], 0.470333, 5.797922);
	PlayerTextDrawAlignment(playerid, PhoneTD[5][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[5][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[5][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[5][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[5][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[5][playerid], 1);
	PlayerTextDrawSetProportional(playerid, PhoneTD[5][playerid], 1);

	PhoneTD[6][playerid] = CreatePlayerTextDraw(playerid, 610.999877, 313.599975, ")");
	PlayerTextDrawLetterSize(playerid, PhoneTD[6][playerid], 0.476666, 16.384004);
	PlayerTextDrawAlignment(playerid, PhoneTD[6][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[6][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[6][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[6][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[6][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[6][playerid], 1);
	PlayerTextDrawSetProportional(playerid, PhoneTD[6][playerid], 1);

	PhoneTD[7][playerid] = CreatePlayerTextDraw(playerid, 616.332885, 367.781555, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[7][playerid], 0.000000, 11.539299);
	PlayerTextDrawTextSize(playerid, PhoneTD[7][playerid], 603.666564, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[7][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[7][playerid], 286331391);
	PlayerTextDrawUseBox(playerid, PhoneTD[7][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[7][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[7][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[7][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[7][playerid], 0);

	PhoneTD[8][playerid] = CreatePlayerTextDraw(playerid, 613.333496, 341.648132, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[8][playerid], 0.000000, 2.789914);
	PlayerTextDrawTextSize(playerid, PhoneTD[8][playerid], 604.666809, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[8][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[8][playerid], 286331391);
	PlayerTextDrawUseBox(playerid, PhoneTD[8][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[8][playerid], 286331391);
	PlayerTextDrawSetShadow(playerid, PhoneTD[8][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[8][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[8][playerid], 0);

	PhoneTD[9][playerid] = CreatePlayerTextDraw(playerid, 609.666564, 361.974151, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[9][playerid], 0.000000, 6.458642);
	PlayerTextDrawTextSize(playerid, PhoneTD[9][playerid], 511.666656, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[9][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[9][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[9][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[9][playerid], 858993663);
	PlayerTextDrawSetShadow(playerid, PhoneTD[9][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[9][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[9][playerid], 0);

	PhoneTD[10][playerid] = CreatePlayerTextDraw(playerid, 608.666748, 363.218658, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[10][playerid], 0.000000, 6.182098);
	PlayerTextDrawTextSize(playerid, PhoneTD[10][playerid], 513.000061, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[10][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[10][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[10][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[10][playerid], -2004317953);
	PlayerTextDrawSetShadow(playerid, PhoneTD[10][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[10][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[10][playerid], 0);

	PhoneTD[11][playerid] = CreatePlayerTextDraw(playerid, 556.000000, 431.662933, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[11][playerid], 0.000000, 0.789507);
	PlayerTextDrawTextSize(playerid, PhoneTD[11][playerid], 515.666625, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[11][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[11][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[11][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[11][playerid], 1145324799);
	PlayerTextDrawSetShadow(playerid, PhoneTD[11][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[11][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[11][playerid], 0);

	PhoneTD[12][playerid] = CreatePlayerTextDraw(playerid, 605.665954, 431.662994, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[12][playerid], 0.000000, 0.789503);
	PlayerTextDrawTextSize(playerid, PhoneTD[12][playerid], 565.332580, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[12][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[12][playerid], 1431655935);
	PlayerTextDrawUseBox(playerid, PhoneTD[12][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[12][playerid], 1145324799);
	PlayerTextDrawSetShadow(playerid, PhoneTD[12][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[12][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[12][playerid], 0);

	PhoneTD[13][playerid] = CreatePlayerTextDraw(playerid, 568.333557, 441.618652, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[13][playerid], 0.000000, -1.407611);
	PlayerTextDrawTextSize(playerid, PhoneTD[13][playerid], 602.333251, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[13][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[13][playerid], 0);
	PlayerTextDrawUseBox(playerid, PhoneTD[13][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[13][playerid], -2004317953);
	PlayerTextDrawSetShadow(playerid, PhoneTD[13][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[13][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[13][playerid], 0);

	PhoneTD[14][playerid] = CreatePlayerTextDraw(playerid, 555.666870, 432.077728, "usebox");
	PlayerTextDrawLetterSize(playerid, PhoneTD[14][playerid], 0.000000, 0.684569);
	PlayerTextDrawTextSize(playerid, PhoneTD[14][playerid], 516.333496, 0.000000);
	PlayerTextDrawAlignment(playerid, PhoneTD[14][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[14][playerid], 1717987071);
	PlayerTextDrawUseBox(playerid, PhoneTD[14][playerid], true);
	PlayerTextDrawBoxColor(playerid, PhoneTD[14][playerid], -2004317953);
	PlayerTextDrawSetShadow(playerid, PhoneTD[14][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[14][playerid], 0);
	PlayerTextDrawFont(playerid, PhoneTD[14][playerid], 0);

	PhoneTD[15][playerid] = CreatePlayerTextDraw(playerid, 531.000061, 338.903839, "iPokia");
	PlayerTextDrawLetterSize(playerid, PhoneTD[15][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, PhoneTD[15][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[15][playerid], -522132790);
	PlayerTextDrawSetShadow(playerid, PhoneTD[15][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[15][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[15][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[15][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[15][playerid], 1);

	// Start screen

	PhoneTD[17][playerid] = CreatePlayerTextDraw(playerid, 519.999816, 384.118652, "Internic Network");
	PlayerTextDrawLetterSize(playerid, PhoneTD[17][playerid], 0.214666, 1.118815);
	PlayerTextDrawAlignment(playerid, PhoneTD[17][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[17][playerid], 858993578);
	PlayerTextDrawSetShadow(playerid, PhoneTD[17][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[17][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[17][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[17][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[17][playerid], 1);

	PhoneTD[16][playerid] = CreatePlayerTextDraw(playerid, 515.333251, 410.666778, "Menu");
	PlayerTextDrawLetterSize(playerid, PhoneTD[16][playerid], 0.309666, 1.118815);
	PlayerTextDrawAlignment(playerid, PhoneTD[16][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[16][playerid], 714);
	PlayerTextDrawSetShadow(playerid, PhoneTD[16][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[16][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[16][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[16][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[16][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[16][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[16][playerid], 550.0, 12.0);

	PhoneTD[18][playerid] = CreatePlayerTextDraw(playerid, 568.333129, 411.081451, "Close");
	PlayerTextDrawLetterSize(playerid, PhoneTD[18][playerid], 0.295666, 1.077333);
	PlayerTextDrawAlignment(playerid, PhoneTD[18][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[18][playerid], 714);
	PlayerTextDrawSetShadow(playerid, PhoneTD[18][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[18][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[18][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[18][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[18][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[18][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[18][playerid], 608.0, 12.0);

	PhoneTD[19][playerid] = CreatePlayerTextDraw(playerid, 515.000000, 359.644531, "~g~iii");
	PlayerTextDrawLetterSize(playerid, PhoneTD[19][playerid], 0.296666, 1.330371);
	PlayerTextDrawAlignment(playerid, PhoneTD[19][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[19][playerid], 1431655850);
	PlayerTextDrawSetShadow(playerid, PhoneTD[19][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[19][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[19][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[19][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[19][playerid], 1);

	PhoneTD[20][playerid] = CreatePlayerTextDraw(playerid, 586.000061, 360.474243, "~g~86%");
	PlayerTextDrawLetterSize(playerid, PhoneTD[20][playerid], 0.252999, 1.135407);
	PlayerTextDrawAlignment(playerid, PhoneTD[20][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[20][playerid], 1431655850);
	PlayerTextDrawSetShadow(playerid, PhoneTD[20][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[20][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[20][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[20][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[20][playerid], 1);

	// Menu screen

	PhoneTD[21][playerid] = CreatePlayerTextDraw(playerid, 522.0, 395.0, "Call");
	PlayerTextDrawLetterSize(playerid, PhoneTD[21][playerid], 0.3, 1.0);
	PlayerTextDrawAlignment(playerid, PhoneTD[21][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[21][playerid], -1);
	PlayerTextDrawSetShadow(playerid, PhoneTD[21][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[21][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[21][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[21][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[21][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[21][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[21][playerid], 550.0, 10.0);

	PhoneTD[22][playerid] = CreatePlayerTextDraw(playerid, 570.0, 395.0, "Text");
	PlayerTextDrawLetterSize(playerid, PhoneTD[22][playerid], 0.3, 1.0);
	PlayerTextDrawAlignment(playerid, PhoneTD[22][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[22][playerid], -1);
	PlayerTextDrawSetShadow(playerid, PhoneTD[22][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[22][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[22][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[22][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[22][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[22][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[22][playerid], 600.0, 10.0);

	PhoneTD[23][playerid] = CreatePlayerTextDraw(playerid, 559.000061, 411.081451, "Cancel");
	PlayerTextDrawLetterSize(playerid, PhoneTD[23][playerid], 0.295666, 1.077332);
	PlayerTextDrawAlignment(playerid, PhoneTD[23][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[23][playerid], 714);
	PlayerTextDrawSetShadow(playerid, PhoneTD[23][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[23][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[23][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[23][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[23][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[23][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[23][playerid], 608.0, 12.0);

	PhoneTD[24][playerid] = CreatePlayerTextDraw(playerid, 575.0, 372.0, "LD_CHAT:goodcha");
	PlayerTextDrawLetterSize(playerid, PhoneTD[24][playerid], 0.45, 1.600000);
	PlayerTextDrawTextSize(playerid, PhoneTD[24][playerid], 21.333330, 22.814823);
	PlayerTextDrawAlignment(playerid, PhoneTD[24][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[24][playerid], -1);
	PlayerTextDrawSetShadow(playerid, PhoneTD[24][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[24][playerid], 1);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[24][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[24][playerid], 4);
	PlayerTextDrawSetProportional(playerid, PhoneTD[24][playerid], 1);

	PhoneTD[25][playerid] = CreatePlayerTextDraw(playerid, 525.0, 372.0, "LD_CHAT:badchat");
	PlayerTextDrawLetterSize(playerid, PhoneTD[25][playerid], 0.45, 1.600000);
	PlayerTextDrawTextSize(playerid, PhoneTD[25][playerid], 21.333330, 22.814823);
	PlayerTextDrawAlignment(playerid, PhoneTD[25][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[25][playerid], -1);
	PlayerTextDrawSetShadow(playerid, PhoneTD[25][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[25][playerid], 1);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[25][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[25][playerid], 4);
	PlayerTextDrawSetProportional(playerid, PhoneTD[25][playerid], 1);

	// Call screen

	PhoneTD[26][playerid] = CreatePlayerTextDraw(playerid, 517.666564, 369.600097, "Calling...~n~~w~number");
	PlayerTextDrawLetterSize(playerid, PhoneTD[26][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, PhoneTD[26][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[26][playerid], 1431655935);
	PlayerTextDrawSetShadow(playerid, PhoneTD[26][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[26][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[26][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[26][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[26][playerid], 1);

	PhoneTD[27][playerid] = CreatePlayerTextDraw(playerid, 559.000061, 411.081451, "~r~Hangup");
	PlayerTextDrawLetterSize(playerid, PhoneTD[27][playerid], 0.295666, 1.077333);
	PlayerTextDrawAlignment(playerid, PhoneTD[27][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[27][playerid], 714);
	PlayerTextDrawSetShadow(playerid, PhoneTD[27][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[27][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[27][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[27][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[27][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[27][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[27][playerid], 608.0, 12.0);

	// Text screen

	PhoneTD[28][playerid] = CreatePlayerTextDraw(playerid, 516.333190, 362.133392, "Message from ~b~number");
	PlayerTextDrawLetterSize(playerid, PhoneTD[28][playerid], 0.184999, 0.849185);
	PlayerTextDrawAlignment(playerid, PhoneTD[28][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[28][playerid], 1431655935);
	PlayerTextDrawSetShadow(playerid, PhoneTD[28][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[28][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[28][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[28][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[28][playerid], 1);

	PhoneTD[29][playerid] = CreatePlayerTextDraw(playerid, 517.333435, 375.822174, "smstext");
	PlayerTextDrawLetterSize(playerid, PhoneTD[29][playerid], 0.187666, 0.782815);
	PlayerTextDrawAlignment(playerid, PhoneTD[29][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[29][playerid], -1);
	PlayerTextDrawSetShadow(playerid, PhoneTD[29][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[29][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[29][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[29][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[29][playerid], 1);

	PhoneTD[30][playerid] = CreatePlayerTextDraw(playerid, 559.000061, 411.081451, "Cancel");
	PlayerTextDrawLetterSize(playerid, PhoneTD[30][playerid], 0.295666, 1.077333);
	PlayerTextDrawAlignment(playerid, PhoneTD[30][playerid], 1);
	PlayerTextDrawColor(playerid, PhoneTD[30][playerid], 714);
	PlayerTextDrawSetShadow(playerid, PhoneTD[30][playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneTD[30][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PhoneTD[30][playerid], 51);
	PlayerTextDrawFont(playerid, PhoneTD[30][playerid], 2);
	PlayerTextDrawSetProportional(playerid, PhoneTD[30][playerid], 1);
    PlayerTextDrawSetSelectable(playerid, PhoneTD[30][playerid], 1);
	PlayerTextDrawTextSize(playerid, PhoneTD[30][playerid], 608.0, 12.0);

	TextSign[playerid] = CreatePlayerTextDraw(playerid, 123.000030, 326.874114, "LD_CHAT:goodcha");
	PlayerTextDrawLetterSize(playerid, TextSign[playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, TextSign[playerid], 19.999994, 19.911134);
	PlayerTextDrawAlignment(playerid, TextSign[playerid], 1);
	PlayerTextDrawColor(playerid, TextSign[playerid], -1);
	PlayerTextDrawSetShadow(playerid, TextSign[playerid], 0);
	PlayerTextDrawSetOutline(playerid, TextSign[playerid], 1);
	PlayerTextDrawBackgroundColor(playerid, TextSign[playerid], 51);
	PlayerTextDrawFont(playerid, TextSign[playerid], 4);
	PlayerTextDrawSetProportional(playerid, TextSign[playerid], 1);

	PhoneSign[playerid] = CreatePlayerTextDraw(playerid, 123.000030, 326.874114, "LD_CHAT:badchat");
	PlayerTextDrawLetterSize(playerid, PhoneSign[playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, PhoneSign[playerid], 19.999994, 19.911134);
	PlayerTextDrawAlignment(playerid, PhoneSign[playerid], 1);
	PlayerTextDrawColor(playerid, PhoneSign[playerid], -1);
	PlayerTextDrawSetShadow(playerid, PhoneSign[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PhoneSign[playerid], 1);
	PlayerTextDrawBackgroundColor(playerid, PhoneSign[playerid], 51);
	PlayerTextDrawFont(playerid, PhoneSign[playerid], 4);
	PlayerTextDrawSetProportional(playerid, PhoneSign[playerid], 1);

	return true;
}
