/*
	Author: Mads
	Date: 19-02-2015
	For: EastSideGaming
	Description: Creating spots for vehicles for selling
	File: showcars.pwn
*/

/* - MYSQL FORMAT: 
	0. (int) Unique ID
	1. (int) Dealership ID
	2. (int) Vehicle ID
	3. (float) Position X
	4. (float) Position Y
	5. (float) Position Z
	6. (float) Position Angle
*/