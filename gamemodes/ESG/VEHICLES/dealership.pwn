/*
	Author: Mads
	Date: 19-02-2015
	For: EastSideGaming
	Description: Creation of a Dealership & Managing a dealership
	File: dealership.pwn
*/

/*
	Notes
	- Car pricing is going to be dynamic
*/

/* - MYSQL FORMAT: 
	0. (int) Unique ID
	1. (string) Name
	2. (int) Type
	3. (float) Position X
	4. (float) Position Y
	5. (float) Position Z
*/
#define 	DEALER_TYPE_BIKES			1
#define 	DEALER_TYPE_CONVERTIABLES	2
#define 	DEALER_TYPE_INDUSTRIALS		3
#define 	DEALER_TYPE_LOWRIDERS 		4
#define 	DEALER_TYPE_OFFROAD 		5
#define 	DEALER_TYPE_SALOONS 		6
#define 	DEALER_TYPE_SPORT 			7
#define 	DEALER_TYPE_STATION_WAGONS 	8
#define 	DEALER_TYPE_AIRPLANES 		9

enum VehicleInformation {
	vehicleName[32],
	ModelID,
	Price,
	Type
}

new DealerVehicles[][VehicleInformation] = 
{
	{"Bike", 509, 0, DEALER_TYPE_BIKES},
	{"BMX", 481, 0, DEALER_TYPE_BIKES},
	{"Mountain Bike", 510, 0, DEALER_TYPE_BIKES},
	{"Faggio", 462, 0, DEALER_TYPE_BIKES},
	{"BF-400", 581, 0, DEALER_TYPE_BIKES},
	{"NRG-500", 522, 0, DEALER_TYPE_BIKES},
	{"PCJ-600", 461, 0, DEALER_TYPE_BIKES},
	{"FCR-900", 521, 0, DEALER_TYPE_BIKES},
	{"Freeway", 463, 0, DEALER_TYPE_BIKES},
	{"Wayfarer", 586, 0, DEALER_TYPE_BIKES},
	{"Sanchez", 468, 0, DEALER_TYPE_BIKES},
	{"Quad Bike", 471, 0, DEALER_TYPE_BIKES}
};

GetVehicleDealerType(vehicle[]) {
	for(new i = 0; i < sizeof(DealerVehicles)) {
		if(strcmp(DealerVehicles[i][vehicleName], vehicle) == 0)
			return DealerVehicles[i][Type];
	}
	return true;
}

GetVehiclePrice(vehicle[]) {
	for(new i = 0; i < sizeof(DealerVehicles)) {
		if(strcmp(DealerVehicles[i][vehicleName, vehicle)) == 0)
			return DealerVehicles[i][Price];
	}
	return true;
}