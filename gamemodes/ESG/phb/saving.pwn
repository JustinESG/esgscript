#define     MAX_BOOTHS      	(25)
#define     PHONE_BOOTH_OBJ     (1216)

enum PhoneBoothInfo
{
	ID,

	Object,
	Text3D: Label,

	Float: pbX,
	Float: pbY,
	Float: pbZ,

	Float: pbRX,
	Float: pbRY,
	Float: pbRZ,

	pbInterior,
	pbVirtualWorld,
	Number
};

new PhoneBooth[MAX_BOOTHS][PhoneBoothInfo], pbCount;
new tempPlObj[MAX_PLAYERS], bool: tempPlEditing[MAX_PLAYERS], bool: PlEditing[MAX_PLAYERS], PlEditingID[MAX_PLAYERS]; // PLACEMENT VARIABLES

SQL_LoadPhoneBooths()
{
	mysql_function_query(mysql, "SELECT * FROM `phonebooths`", true, "SQL_LoadPhoneBooth", "");
}

CMD:createbooth(playerid, params[])
{
	new Float: pX, Float: pY, Float: pZ;
	GetPlayerPos(playerid, pX, pY, pZ);

	if(Player[playerid][Admin] < 3)
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You're not an administrator or your admin level isn't high enough!");

	if(tempPlEditing[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You are already editing a phone booth!");

	tempPlObj[playerid] = CreateDynamicObject(PHONE_BOOTH_OBJ, pX + 1, pY + 1, pZ, 0.0, 0.0, 0.0);
	EditDynamicObject(playerid, tempPlObj[playerid]);

	SendClientMessage(playerid, COLOR_BLUE, "[PHONE BOOTH]:{E0E0E0} Move the object to your desired location and type /confirmboothpos to create the phone booth!");
	SendClientMessage(playerid, COLOR_BLUE, "[PHONE BOOTH]:{D12E2E} Note:{E0E0E0} don't lower the Z coordinate, the script does this automatically for you!");

	tempPlEditing[playerid] = true;
	printf("[PHONE BOOTHS]: ID %d (%s) started placing a phone booth", playerid, Account[playerid][Name]);

	if(Player[playerid][Admin] > 3)
	{
	    new string[144];
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has started placing a phone booth.", playerid, Account[playerid][Name]);
		SendAdminMsgEx(string);
	}

	return true;
}

CreatePhoneBooth(id, Float: x, Float: y, Float: z, Float: rx, Float: ry, Float: rz, virtualworld, interior)
{
	new query[600], number = 6000 + random(999) + random(3);

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `phonebooths` (`ID`, `pbX`, `pbY`, `pbZ`, `pbRX`, `pbRY`, `pbRZ`, `pbVirtualWorld`, `pbInterior`, `Number`)");
	mysql_format(mysql, query, sizeof(query), "%s VALUES ('%d', %f, %f, %f, %f, %f, %f, %d, %d, %d)",

		query, id,
		x, y, z, rx, ry, rz,
		virtualworld, interior, number
	);

	mysql_tquery(mysql, query, "", "");

	PhoneBooth[id][ID] = id;
	PhoneBooth[id][Number] = number;

	PhoneBooth[id][pbX] = x;
	PhoneBooth[id][pbY] = y;
	PhoneBooth[id][pbZ] = z;

	PhoneBooth[id][pbRX] = rx;
	PhoneBooth[id][pbRY] = ry;
	PhoneBooth[id][pbRZ] = rz;

	PhoneBooth[id][pbVirtualWorld] = virtualworld;
	PhoneBooth[id][pbInterior] = interior;

	format(query, sizeof(query), "(({E0E0E0} Phone Booth: %d {4AD5E7})){E0E0E0}\n{4AD5E7}(({E0E0E0} /boothcall {4AD5E7})){E0E0E0}", id);
	PhoneBooth[id][Label] = CreateDynamic3DTextLabel(query, 0x4AD5E7FF, x, y, z, 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, virtualworld, interior);
	PhoneBooth[id][Object] = CreateDynamicObject(PHONE_BOOTH_OBJ , x, y, z, rx, ry, rz, virtualworld, interior, -1, 200.0, 100.0);

	pbCount ++;

	return true;
}

forward SQL_LoadPhoneBooth();
public SQL_LoadPhoneBooth()
{
	new fields, rows, labelstr[100];
	cache_get_data(rows, fields);

	if(!cache_get_row_count()) return print("[PHONE BOOTHS]: cache_get_row_count returned false. There are no phone booths to load.\n");
 	printf("[PHONE BOOTHS]: cache_get_row_count returned %d rows. There are %d rows (phone booths) to load.\n", cache_get_row_count());

	for(new i = 0; i != cache_get_row_count(); i++)
	{
	   	PhoneBooth[i][ID] = 			cache_insert_id();

		PhoneBooth[i][pbX] = 				cache_get_field_content_float(i, "pbX");
		PhoneBooth[i][pbY] = 				cache_get_field_content_float(i, "pbY");
		PhoneBooth[i][pbZ] = 				cache_get_field_content_float(i, "pbZ");

		PhoneBooth[i][pbRX] = 				cache_get_field_content_float(i, "pbRX");
		PhoneBooth[i][pbRY] = 				cache_get_field_content_float(i, "pbRY");
		PhoneBooth[i][pbRZ] = 				cache_get_field_content_float(i, "pbRZ");

		PhoneBooth[i][pbInterior] = 		cache_get_field_content_int(i, "pbInterior");
		PhoneBooth[i][pbVirtualWorld] = 	cache_get_field_content_int(i, "pbVirtualWorld");

		PhoneBooth[i][Number] = 			cache_get_field_content_int(i, "Number");

		printf("Loaded phone booth with ID %d and number %d", i, PhoneBooth[i][Number]);
		format(labelstr, sizeof(labelstr), "(({E0E0E0} Phone Booth: %d {4AD5E7})){E0E0E0}\n{4AD5E7}(({E0E0E0} /boothcall {4AD5E7})){E0E0E0}", i);

		PhoneBooth[i][Label] = CreateDynamic3DTextLabel(labelstr, 0x4AD5E7FF,  PhoneBooth[i][pbX], PhoneBooth[i][pbY],PhoneBooth[i][pbZ], 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, PhoneBooth[i][pbVirtualWorld], PhoneBooth[i][pbInterior]);
		PhoneBooth[i][Object] = CreateDynamicObject(PHONE_BOOTH_OBJ , PhoneBooth[i][pbX], PhoneBooth[i][pbY], PhoneBooth[i][pbZ], PhoneBooth[i][pbRX], PhoneBooth[i][pbRY], PhoneBooth[i][pbRZ], PhoneBooth[i][pbVirtualWorld], PhoneBooth[i][pbInterior], -1, 200.0, 100.0);

		pbCount ++;
	}

	printf("\n[PHONE BOOTHS]: Loaded %d phone booths!\n", cache_get_row_count());

	return true;
}
