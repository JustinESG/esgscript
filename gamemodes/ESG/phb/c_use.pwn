IsPlayerNearPhoneBooth(playerid)
{
	for(new i; i < MAX_BOOTHS; i ++)
	{
	    if(IsPlayerInRangeOfPoint(playerid, 3.0, PhoneBooth[i][pbX], PhoneBooth[i][pbY], PhoneBooth[i][pbZ]))
	    {
	        return true;
	    }
	}

	return false;
}

GetClosestPhoneBoothToPlayer(playerid)
{
	for(new i; i < MAX_BOOTHS; i ++)
	{
	    if(IsPlayerInRangeOfPoint(playerid, 3.0, PhoneBooth[i][pbX], PhoneBooth[i][pbY], PhoneBooth[i][pbZ]))
	    {
	        return i;
	    }
	}

	return false;
}

CMD:boothcall(playerid, params[])
{
	new number;

	if(!IsPlayerNearPhoneBooth(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a phone boots!");

	if(sscanf(params, "i", number))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /boothcall [number]");

	if(GetPlayerByNumber(number) == 999)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This number does not exist!");

	if(IsCalling[playerid] || IsCalling[GetPlayerByNumber(number)])
        return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You, or the number you are trying to call, are already connected to a phone call.");

	CallNumber(playerid, number);

	TogglePlayerControllable(playerid, false);

	return true;
}

CMD:boothhangup(playerid, params[])
{
	if(!IsPlayerNearPhoneBooth(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a phone boots!");

	if(IsCalling[playerid])
	{
		EndCall(playerid);
		TogglePlayerControllable(playerid, true);
	}

	return true;
}
