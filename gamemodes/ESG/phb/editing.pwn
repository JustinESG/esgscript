#include <YSI4\YSI_Coding\y_hooks>

hook OnPlayerEditDynObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz)
{
	if(response && tempPlEditing[playerid] && objectid == tempPlObj[playerid])
	{
		SetDynamicObjectPos(objectid, x, y, z);
		SetDynamicObjectRot(objectid, rx, ry, rz);
	}
	return 1;
}

CMD:editbooth(playerid, params[])
{
	new id, Float: pX, Float: pY, Float: pZ;
	GetPlayerPos(playerid, pX, pY, pZ);

	if(Player[playerid][Admin] < 2)
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You're not an administrator or your admin level isn't high enough!");

	if(sscanf(params, "i", id))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /editbooth [id]");

	if(tempPlEditing[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You are already editing a phone booth!");

	tempPlObj[playerid] = CreateDynamicObject(PHONE_BOOTH_OBJ, pX + 1, pY + 1, pZ, 0.0, 0.0, 0.0);
	EditDynamicObject(playerid, tempPlObj[playerid]);

	SendClientMessage(playerid, COLOR_BLUE, "[PHONE BOOTH]:{E0E0E0} Move the object to your desired location and type /confirmboothpos to create the phone booth!");
	SendClientMessage(playerid, COLOR_BLUE, "[PHONE BOOTH]:{D12E2E} Note:{E0E0E0} don't lower the Z coordinate, the script does this automatically for you!");

    PlEditingID[playerid] = id;

	tempPlEditing[playerid] = true;
	PlEditing[playerid] = true;

	printf("[PHONE BOOTHS]: ID %d (%s) started placing a phone booth", playerid, Account[playerid][Name]);

	if(Player[playerid][Admin] > 3)
	{
	    new string[144];
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has started editing phone booth ID %d.", playerid, Account[playerid][Name], id);
		SendAdminMsgEx(string);
	}

	return true;
}

CMD:confirmboothpos(playerid, params[])
{
	new Float: pX, Float: pY, Float: pZ, Float: pRX, Float: pRY, Float: pRZ, query[200], id;

	GetDynamicObjectPos(tempPlObj[playerid], pX, pY, pZ);
	GetDynamicObjectRot(tempPlObj[playerid], pRX, pRY, pRZ);

    DestroyDynamicObject(tempPlObj[playerid]);

	if(Player[playerid][Admin] < 2)
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You're not an administrator or your admin level isn't high enough!");

	if(!tempPlEditing[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You're not editing a phone booth!");

	if(!PlEditing[playerid])
	{
	 	CreatePhoneBooth(pbCount, pX, pY, pZ - 0.4, pRX, pRY, pRZ, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));

		format(query, sizeof(query), "[PHONE BOOTH]:{E0E0E0} ID: %d has been created!", pbCount);
		SendClientMessage(playerid, COLOR_BLUE, query);

		tempPlEditing[playerid] = false;
		printf("Created Phone Booth %d at coords. %f %f %f with rot. %f %f %f, ", pbCount, pX, pY, pZ, pRX, pRY, pRZ);

		if(Player[playerid][Admin] > 3)
		{
		    new string[144];
			format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has just created phone booth ID %d.", playerid, Account[playerid][Name], pbCount);
			SendAdminMsgEx(string);
		}
	}

	else if(PlEditing[playerid])
	{
		format(query, sizeof(query), "[PHONE BOOTH]:{E0E0E0} ID: %d has been edited!", PlEditingID[playerid]);
		SendClientMessage(playerid, COLOR_BLUE, query);

		mysql_format(mysql, query, sizeof(query), "UPDATE `phonebooths` SET `pbX` = '%f', `pbY` = '%f', `pbZ` = '%f', `pbRX` = '%f', `pbRY` = '%f', `pbRZ` = '%f' WHERE `ID` = '%d'", pX, pY, pZ, pRX, pRY, pRZ, PlEditingID[playerid] + 1);
		mysql_tquery(mysql, query, "", "");

		printf("Created Phone Booth %d at coords. %f %f %f with rot. %f %f %f, ", pbCount, pX, pY, pZ, pRX, pRY, pRZ);
		format(query, sizeof(query), "(({E0E0E0} Phone Booth: %d {4AD5E7})){E0E0E0}\n{4AD5E7}(({E0E0E0} /boothcall {4AD5E7})){E0E0E0}", PlEditingID[playerid]);

        DestroyDynamicObject(PhoneBooth[id][Object]);
        DestroyDynamic3DTextLabel(PhoneBooth[id][Label]);


		PhoneBooth[id][Label] = CreateDynamic3DTextLabel(query, 0x4AD5E7FF, pX, pY, pZ, 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
		PhoneBooth[id][Object] = CreateDynamicObject(PHONE_BOOTH_OBJ, pX, pY, pZ, pRX, pRY, pRZ, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), -1, 200.0, 100.0);

        tempPlEditing[playerid] = false;
		PlEditing[playerid] = false;

		if(Player[playerid][Admin] > 3)
		{
		    new string[144];
			format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has edited (saved) phone booth ID %d.", playerid, Account[playerid][Name], PlEditing[playerid]);
			SendAdminMsgEx(string);
		}

		PlEditingID[playerid] = 0;
	}

	return true;
}
