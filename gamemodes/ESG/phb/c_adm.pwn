CMD:gotobooth(playerid, params[])
{
	new id, Float: pX, Float: pY, Float: pZ;

	if(Player[playerid][Admin] < 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[PHONE BOOTH]:{E0E0E0} You're not an administrator or your admin level isn't high enough!");

	if(sscanf(params, "i", id))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /gotobooth [id]");

    GetDynamicObjectPos(PhoneBooth[id][Object], pX, pY, pZ);
	SetPlayerPos(playerid, pX + 1, pY + 1, pZ);

	SetPlayerVirtualWorld(playerid, PhoneBooth[id][pbVirtualWorld]);
	SetPlayerInterior(playerid, PhoneBooth[id][pbInterior]);

	if(Player[playerid][Admin] > 3)
	{
	    new string[144];
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has teleported to phonebooth ID %d.", playerid, Account[playerid][Name], id);
		SendAdminMsgEx(string);
	}

	return true;
}
