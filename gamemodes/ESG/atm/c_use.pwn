CMD:atm(playerid, params[])
{
	new string[110];

	if(IsPlayerNearATM(playerid))
	{
		if(!AccessingATM[playerid])
		{
		    if(strlen(ATMvalue[playerid]) == 0) format(string, sizeof(string), "\"empty\"");
		    else if(strlen(ATMvalue[playerid]) != 0) format(string, sizeof(string), "%s", ATMvalue[playerid]);

			if(Account[playerid][Admin] >= 1)
			{
			    format(string, sizeof(string), "[ADMIN]:{E0E0E0} Accessing ATM ID %d [ATM variable: int: %d str: %s]", GetATMNearPlayer(playerid), strval(ATMvalue[playerid]), string);
			    SendClientMessage(playerid, COLOR_BLUE, string);
			}

		    AccessingATM[playerid] = true;
			ShowATMTextDraws(playerid);

			printf("Player ID (%d) %s accessed ATM ID %d", playerid, Account[playerid][Name], GetATMNearPlayer(playerid));
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're already accessing ATM textdraws. If you are not and think this message is wrong, use /refreshatmtextdraws.");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not near any ATM!");

	return true;
}

CMD:refreshatmtextdraws(playerid, params[])
{
	DeleteATMTextDraws(playerid);
	LoadATMTextDraws(playerid);

	AccessingATM[playerid] = false;

	SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} ATM textdraws have been refreshed.");
	printf("Player ID (%d) %s just refreshed their ATM textdraws.", playerid, Account[playerid][Name]);

	return true;
}

IsPlayerNearATM(playerid)
{
	for(new i; i < ATMCount; i ++)
	{
	    if(IsPlayerInRangeOfPoint(playerid, 3.0, ATM_Info[i][aX], ATM_Info[i][aY], ATM_Info[i][aZ]) &&
 		GetPlayerInterior(playerid) == ATM_Info[i][Interior] && GetPlayerVirtualWorld(playerid) == ATM_Info[i][World])
 		{
 		    return true;
 		}
	}

	return false;
}

GetATMNearPlayer(playerid)
{
	for(new i; i < ATMCount; i ++)
	{
	    if(IsPlayerInRangeOfPoint(playerid, 3.0, ATM_Info[i][aX], ATM_Info[i][aY], ATM_Info[i][aZ]) &&
 		GetPlayerInterior(playerid) == ATM_Info[i][Interior] && GetPlayerVirtualWorld(playerid) == ATM_Info[i][World])
 		{
 		    return i;
 		}
	}

	return 999;
}
