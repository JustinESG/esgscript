#define ATM_OBJECT      (2942)
#define MAX_ATMS        (25)

enum AtmInfo
{
	ID,

	Object,
	Text3D: Label,

	Float: aX,
	Float: aY,
	Float: aZ,

	Float: aRX,
	Float: aRY,
	Float: aRZ,

	World,
	Interior
};

new ATM_Info[MAX_ATMS][AtmInfo], ATMCount, bool: EditingATM[MAX_PLAYERS], Editing_ATM_ID[MAX_PLAYERS];
new ATMtempObj[MAX_PLAYERS], bool: ATMplacingObj[MAX_PLAYERS];

CMD:atmcreate(playerid, params[])
{
	new Float: pX, Float: pY, Float: pZ;

	if(Account[playerid][Admin] < 3)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");

	if(ATMplacingObj[playerid] || EditingATM[playerid])
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're already editing a ATM!");

	GetPlayerPos(playerid, pX, pY, pZ);

	ATMtempObj[playerid] = CreateDynamicObject(1337, pX + 1, pY + 1, pZ, 0.0, 0.0, 0.0);
    EditDynamicObject(playerid, ATMtempObj[playerid]);

	SendClientMessage(playerid, COLOR_BLUE, "[ADMIH]{E0E0E0} Please move the ATM to a proper location and type /atmconfirmpos!");
	ATMplacingObj[playerid] = true;

	return true;
}

CreateATM(aID, Float: X, Float: Y, Float: Z, Float: RX, Float: RY, Float: RZ, interior, vworld)
{
	new query[500];

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `atms` (`ID`, `aX`, `aY`, `aZ`, `aRX`, `aRY`, `aRZ`, `Interior`, `World`)");
	mysql_format(mysql, query, sizeof(query), "%s VALUES (%d, %f, %f, %f, %f, %f, %f, %d, %d)",	query, aID, X, Y, Z, RX, RY, RZ, interior, vworld);
	mysql_tquery(mysql, query, "", "");

	ATM_Info[ATMCount][aX] = X;
	ATM_Info[ATMCount][aY] = Y;
	ATM_Info[ATMCount][aZ] = Z;

	ATM_Info[ATMCount][aRX] = RX;
	ATM_Info[ATMCount][aRY] = RY;
	ATM_Info[ATMCount][aRZ] = RZ;

	ATM_Info[ATMCount][World] = vworld;
	ATM_Info[ATMCount][Interior] = interior;

 	format(query, sizeof(query), "(({E0E0E0} ATM ID: %d {4AD5E7}))\n(({E0E0E0} /atm to use {4AD5E7}))", aID);
	ATM_Info[ATMCount][Label] = CreateDynamic3DTextLabel(query, 0x4AD5E7FF, X, Y, Z, 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, interior, vworld);
	ATM_Info[ATMCount][Object] = CreateDynamicObject(ATM_OBJECT, X, Y, Z, RX, RY, RZ, vworld, interior, COLOR_INFO, 200.0, 300.0);
	ATMCount ++;

	return true;
}

SQL_LoadATMs()
{
	ATMCount = 1;
	mysql_function_query(mysql, "SELECT * FROM `atms`", true, "SQL_LoadATM", "");
}

forward SQL_LoadATM();
public SQL_LoadATM()
{
	new rows, fields, labelstring[110];
	cache_get_data(rows, fields, mysql);

	if(!cache_num_rows()) return printf("[ATM] cache_get_row_count returned false. There are no ATMs to load.\n");
	printf("[ATM] cache_get_row_count returned %d rows (ATMs) to load.\n", cache_get_row_count());

	for(new i = 0; i != cache_get_row_count(); i++)
	{
	   	ATM_Info[i][ID] = 			cache_insert_id();

		ATM_Info[i][aX] = 			cache_get_field_content_float(i, "aX");
		ATM_Info[i][aY] = 			cache_get_field_content_float(i, "aY");
		ATM_Info[i][aZ] = 			cache_get_field_content_float(i, "aZ");

		ATM_Info[i][aRX] = 			cache_get_field_content_float(i, "aRX");
		ATM_Info[i][aRY] = 			cache_get_field_content_float(i, "aRY");
		ATM_Info[i][aRZ] = 			cache_get_field_content_float(i, "aRZ");

		ATM_Info[i][World] =        cache_get_field_content_int(i, "World");
		ATM_Info[i][Interior] =   	cache_get_field_content_int(i, "Interior");

		printf("Loaded ATM ID %d at %f %f %f.", ATMCount, ATM_Info[i][aX], ATM_Info[i][aY], ATM_Info[i][aZ]);
		format(labelstring, sizeof(labelstring), "(({E0E0E0} ATM ID: %d {4AD5E7}))\n(({E0E0E0} /atm to use {4AD5E7}))", ATMCount);

		ATM_Info[i][Object] = CreateDynamicObject(ATM_OBJECT, ATM_Info[i][aX], ATM_Info[i][aY], ATM_Info[i][aZ], ATM_Info[i][aRX], ATM_Info[i][aRY], ATM_Info[i][aRZ], ATM_Info[i][World], ATM_Info[i][Interior], COLOR_INFO, 200.0, 300.0);
		ATM_Info[i][Label] = CreateDynamic3DTextLabel(labelstring, 0x4AD5E7FF, ATM_Info[i][aX], ATM_Info[i][aY], ATM_Info[i][aZ], 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, ATM_Info[i][World], ATM_Info[i][Interior]);

		ATMCount ++;
	}

	printf("\n[ATM] %d ATMs have been loaded.\n", cache_get_row_count());

	return true;
}
