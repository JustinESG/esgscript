#include <YSI4\YSI_Coding\y_hooks>

new PlayerText: ATM_TD[33][MAX_PLAYERS];
new ATMvalue[MAX_PLAYERS][15];
new bool: AccessingATM[MAX_PLAYERS];

hook OnPlayerConnect(playerid)
{
	LoadATMTextDraws(playerid);
	return 1;
}

hook OnPlayerClickPlayerTD(playerid, PlayerText:playertextid)
{
	if(playertextid == ATM_TD[32][playerid])
	{
	    format(ATMvalue[playerid], sizeof(ATMvalue), "888, 888, 888");

	    HideATMTextDraws(playerid);
		AccessingATM[playerid] = false;

		CancelSelectTextDraw(playerid);
	}

	if(playertextid == ATM_TD[22][playerid]) return AddATMValue(playerid, 0);
	if(playertextid == ATM_TD[23][playerid]) return AddATMValue(playerid, 1);
	if(playertextid == ATM_TD[24][playerid]) return AddATMValue(playerid, 2);
	if(playertextid == ATM_TD[25][playerid]) return AddATMValue(playerid, 3);
	if(playertextid == ATM_TD[26][playerid]) return AddATMValue(playerid, 4);
	if(playertextid == ATM_TD[27][playerid]) return AddATMValue(playerid, 5);
	if(playertextid == ATM_TD[28][playerid]) return AddATMValue(playerid, 6);
	if(playertextid == ATM_TD[29][playerid]) return AddATMValue(playerid, 7);
	if(playertextid == ATM_TD[30][playerid]) return AddATMValue(playerid, 8);
	if(playertextid == ATM_TD[31][playerid]) return AddATMValue(playerid, 9);
	
	if(playertextid == ATM_TD[7][playerid])
	{
	    new oldAmount[15];

	    format(oldAmount, sizeof(oldAmount), "%s", ATMvalue[playerid]);

		if(Character[playerid][BankAmount] >= strval(ATMvalue[playerid]))
		{
			if(strlen(ATMvalue[playerid]) > 3)
			{
				strdel(ATMvalue[playerid], 3, 5);

				if(strlen(ATMvalue[playerid]) > 6)
				{
					strdel(ATMvalue[playerid], 6, 8);
				}
			}

			new correctAmount = strval(ATMvalue[playerid]), string[144];

			if(Character[playerid][BankAmount] < strval(ATMvalue[playerid]))
				return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have that much money in your bank account!");

            if(Character[playerid][BankAmount] >= strval(ATMvalue[playerid]))
            {
				Character[playerid][BankAmount] -= correctAmount;
				Character[playerid][Money] += correctAmount;
				GivePlayerMoney(playerid, correctAmount);
			}

			printf("[ATM DEBUG]: Player (%d) %s STRVAL: %d - STR: %s - INT: %d", playerid, Account[playerid][Name], strval(ATMvalue[playerid]), ATMvalue[playerid], correctAmount);
			printf("[ATM DEBUG]: Player (%d) %s withdrew %d from their bank account. Money left: %d", playerid, Account[playerid][Name], correctAmount, Character[playerid][BankAmount]);

			format(string, sizeof(string), "[INFO]{E0E0E0} You have just withdrawn \"$%d\" from your bank account. You have \"%d\" dollar remaining.", correctAmount, Character[playerid][BankAmount]);
			SendClientMessage(playerid, COLOR_BLUE, string);

			format(ATMvalue[playerid], sizeof(oldAmount), "%s", oldAmount);
		}

		else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have that much money in your bank account!");
	}

	if(playertextid == ATM_TD[8][playerid])
	{
	    new string[60];

	    format(string, sizeof(string), "[INFO]{E0E0E0} Your current balance is \"$%d\".", Character[playerid][BankAmount]);
	    SendClientMessage(playerid, COLOR_BLUE, string);
	}

	if(playertextid == ATM_TD[2][playerid])
	{
	    strdel(ATMvalue[playerid], strlen(ATMvalue[playerid]) - 1, strlen(ATMvalue[playerid]));

		switch(strlen(ATMvalue[playerid]))
		{
		    case 5: strdel(ATMvalue[playerid], strlen(ATMvalue[playerid]) - 2, strlen(ATMvalue[playerid]));
		    case 10: strdel(ATMvalue[playerid], strlen(ATMvalue[playerid]) - 2, strlen(ATMvalue[playerid]));
		}

		PlayerTextDrawSetString(playerid, ATM_TD[4][playerid], ATMvalue[playerid]);
	}
	return 1;
}

AddATMValue(playerid, number)
{
	new tempstr[2];

	if(strlen(ATMvalue[playerid]) > 12)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You can't withdraw more than $ 999, 999, 999!");

	format(tempstr, sizeof(tempstr), "%d", number);
    strins(ATMvalue[playerid], tempstr, strlen(ATMvalue[playerid]));

	PlayerTextDrawSetString(playerid, ATM_TD[4][playerid], ATMvalue[playerid]);

	switch(strlen(ATMvalue[playerid]))
	{
	    case 3: strins(ATMvalue[playerid], ", ", strlen(ATMvalue[playerid]));
	    case 8: strins(ATMvalue[playerid], ", ", strlen(ATMvalue[playerid]));
	}

	return true;
}

ShowATMTextDraws(playerid)
{
	ATMvalue[playerid] = "\0";

	for(new i; i < sizeof(ATM_TD); i ++)
	{
		PlayerTextDrawShow(playerid, ATM_TD[i][playerid]);
	}

    SelectTextDraw(playerid, 0x00FF00FF);

	return true;
}

HideATMTextDraws(playerid)
{
	for(new i; i < sizeof(ATM_TD); i ++)
	{
		PlayerTextDrawHide(playerid, ATM_TD[i][playerid]);
	}

	return true;
}

DeleteATMTextDraws(playerid)
{
	for(new i; i < sizeof(ATM_TD); i ++)
	{
		PlayerTextDrawDestroy(playerid, ATM_TD[i][playerid]);
	}

	return true;
}

LoadATMTextDraws(playerid)
{
	ATM_TD[0][playerid] = CreatePlayerTextDraw(playerid, 430.332916, 127.603721, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[0][playerid], 0.000000, 25.565639);
	PlayerTextDrawTextSize(playerid, ATM_TD[0][playerid], 231.999923, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[0][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[0][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[0][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[0][playerid], 1145324799);
	PlayerTextDrawSetShadow(playerid, ATM_TD[0][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[0][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[0][playerid], 0);

	ATM_TD[1][playerid] = CreatePlayerTextDraw(playerid, 428.333068, 129.263000, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[1][playerid], 0.000000, 25.130250);
	PlayerTextDrawTextSize(playerid, ATM_TD[1][playerid], 233.999984, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[1][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[1][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[1][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[1][playerid], 2004318122);
	PlayerTextDrawSetShadow(playerid, ATM_TD[1][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[1][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[1][playerid], 0);

	ATM_TD[2][playerid] = CreatePlayerTextDraw(playerid, 415, 180.5, "-");
	PlayerTextDrawLetterSize(playerid, ATM_TD[2][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, ATM_TD[2][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[2][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[2][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[2][playerid], 1);
	PlayerTextDrawSetProportional(playerid, ATM_TD[2][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[2][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[2][playerid], 425.0, 15.0);

	ATM_TD[3][playerid] = CreatePlayerTextDraw(playerid, 425.666687, 131.337066, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[3][playerid], 0.000000, 7.011727);
	PlayerTextDrawTextSize(playerid, ATM_TD[3][playerid], 236.999984, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[3][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[3][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[3][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[3][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[3][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[3][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[3][playerid], 0);

	ATM_TD[4][playerid] = CreatePlayerTextDraw(playerid, 260.333435, 160.948196, "888, 888, 888");
	PlayerTextDrawLetterSize(playerid, ATM_TD[4][playerid], 0.484666, 2.504296);
	PlayerTextDrawAlignment(playerid, ATM_TD[4][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[4][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[4][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[4][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[4][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[4][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[4][playerid], 1);

	ATM_TD[5][playerid] = CreatePlayerTextDraw(playerid, 331.333435, 199.781478, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[5][playerid], 0.000000, 2.738065);
	PlayerTextDrawTextSize(playerid, ATM_TD[5][playerid], 237.000000, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[5][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[5][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[5][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[5][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[5][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[5][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[5][playerid], 0);

	ATM_TD[6][playerid] = CreatePlayerTextDraw(playerid, 425.666687, 199.781494, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[6][playerid], 0.000000, 2.771400);
	PlayerTextDrawTextSize(playerid, ATM_TD[6][playerid], 330.666717, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[6][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[6][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[6][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[6][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[6][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[6][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[6][playerid], 0);

	ATM_TD[7][playerid] = CreatePlayerTextDraw(playerid, 247.999954, 204.503738, "Withdraw");
	PlayerTextDrawLetterSize(playerid, ATM_TD[7][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, ATM_TD[7][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[7][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[7][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[7][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[7][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[7][playerid], 1);
	PlayerTextDrawSetProportional(playerid, ATM_TD[7][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[7][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[7][playerid], 315.0, 15.0);

	ATM_TD[8][playerid] = CreatePlayerTextDraw(playerid, 350.666625, 204.503738, "Balance");
	PlayerTextDrawLetterSize(playerid, ATM_TD[8][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, ATM_TD[8][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[8][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[8][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[8][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[8][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[8][playerid], 1);
	PlayerTextDrawSetProportional(playerid, ATM_TD[8][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[8][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[8][playerid], 410.0, 15.0);

	ATM_TD[9][playerid] = CreatePlayerTextDraw(playerid, 243.666687, 131.081451, "Welcome to your bank account,~n~please select an option below.");
	PlayerTextDrawLetterSize(playerid, ATM_TD[9][playerid], 0.338666, 1.205925);
	PlayerTextDrawAlignment(playerid, ATM_TD[9][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[9][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[9][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[9][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[9][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[9][playerid], 1);
	PlayerTextDrawSetProportional(playerid, ATM_TD[9][playerid], 1);

	ATM_TD[10][playerid] = CreatePlayerTextDraw(playerid, 244.666625, 165.511123, "$");
	PlayerTextDrawLetterSize(playerid, ATM_TD[10][playerid], 0.449999, 1.600000);
	PlayerTextDrawAlignment(playerid, ATM_TD[10][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[10][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[10][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[10][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[10][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[10][playerid], 1);
	PlayerTextDrawSetProportional(playerid, ATM_TD[10][playerid], 1);

	ATM_TD[11][playerid] = CreatePlayerTextDraw(playerid, 286.666564, 230.062957, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[11][playerid], 0.000000, 3.969756);
	PlayerTextDrawTextSize(playerid, ATM_TD[11][playerid], 241.000061, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[11][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[11][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[11][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[11][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[11][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[11][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[11][playerid], 0);

	ATM_TD[12][playerid] = CreatePlayerTextDraw(playerid, 331.666656, 230.062988, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[12][playerid], 0.000000, 3.969750);
	PlayerTextDrawTextSize(playerid, ATM_TD[12][playerid], 285.999969, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[12][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[12][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[12][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[12][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[12][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[12][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[12][playerid], 0);

	ATM_TD[13][playerid] = CreatePlayerTextDraw(playerid, 375.999908, 230.063003, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[13][playerid], 0.000000, 3.956995);
	PlayerTextDrawTextSize(playerid, ATM_TD[13][playerid], 330.999908, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[13][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[13][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[13][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[13][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[13][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[13][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[13][playerid], 0);

	ATM_TD[14][playerid] = CreatePlayerTextDraw(playerid, 421.000091, 230.063018, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[14][playerid], 0.000000, 3.949175);
	PlayerTextDrawTextSize(playerid, ATM_TD[14][playerid], 375.333465, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[14][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[14][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[14][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[14][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[14][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[14][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[14][playerid], 0);

	ATM_TD[15][playerid] = CreatePlayerTextDraw(playerid, 286.333343, 271.544464, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[15][playerid], 0.000000, 4.015842);
	PlayerTextDrawTextSize(playerid, ATM_TD[15][playerid], 241.000015, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[15][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[15][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[15][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[15][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[15][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[15][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[15][playerid], 0);

	ATM_TD[16][playerid] = CreatePlayerTextDraw(playerid, 331.333343, 271.544494, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[16][playerid], 0.000000, 4.003084);
	PlayerTextDrawTextSize(playerid, ATM_TD[16][playerid], 285.999969, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[16][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[16][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[16][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[16][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[16][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[16][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[16][playerid], 0);

	ATM_TD[17][playerid] = CreatePlayerTextDraw(playerid, 375.666595, 271.544464, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[17][playerid], 0.000000, 3.990329);
	PlayerTextDrawTextSize(playerid, ATM_TD[17][playerid], 331.000000, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[17][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[17][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[17][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[17][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[17][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[17][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[17][playerid], 0);

	ATM_TD[18][playerid] = CreatePlayerTextDraw(playerid, 421.000061, 271.544494, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[18][playerid], 0.000000, 4.023662);
	PlayerTextDrawTextSize(playerid, ATM_TD[18][playerid], 375.333404, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[18][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[18][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[18][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[18][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[18][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[18][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[18][playerid], 0);

	ATM_TD[19][playerid] = CreatePlayerTextDraw(playerid, 285.999938, 313.440643, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[19][playerid], 0.000000, 3.969754);
	PlayerTextDrawTextSize(playerid, ATM_TD[19][playerid], 241.000030, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[19][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[19][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[19][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[19][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[19][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[19][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[19][playerid], 0);

	ATM_TD[20][playerid] = CreatePlayerTextDraw(playerid, 331.333343, 313.440765, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[20][playerid], 0.000000, 3.969750);
	PlayerTextDrawTextSize(playerid, ATM_TD[20][playerid], 285.666687, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[20][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[20][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[20][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[20][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[20][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[20][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[20][playerid], 0);

	ATM_TD[21][playerid] = CreatePlayerTextDraw(playerid, 420.666656, 313.855529, "usebox");
	PlayerTextDrawLetterSize(playerid, ATM_TD[21][playerid], 0.000000, 3.903088);
	PlayerTextDrawTextSize(playerid, ATM_TD[21][playerid], 330.999938, 0.000000);
	PlayerTextDrawAlignment(playerid, ATM_TD[21][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[21][playerid], 0);
	PlayerTextDrawUseBox(playerid, ATM_TD[21][playerid], true);
	PlayerTextDrawBoxColor(playerid, ATM_TD[21][playerid], 102);
	PlayerTextDrawSetShadow(playerid, ATM_TD[21][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[21][playerid], 0);
	PlayerTextDrawFont(playerid, ATM_TD[21][playerid], 0);

	ATM_TD[22][playerid] = CreatePlayerTextDraw(playerid, 256.666595, 237.688888, "0");
	PlayerTextDrawLetterSize(playerid, ATM_TD[22][playerid], 0.515999, 2.077036);
	PlayerTextDrawAlignment(playerid, ATM_TD[22][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[22][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[22][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[22][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[22][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[22][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[22][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[22][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[22][playerid], 270.0, 15.0);

	ATM_TD[23][playerid] = CreatePlayerTextDraw(playerid, 305.000122, 239.348144, "1");
	PlayerTextDrawLetterSize(playerid, ATM_TD[23][playerid], 0.482333, 1.890369);
	PlayerTextDrawAlignment(playerid, ATM_TD[23][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[23][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[23][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[23][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[23][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[23][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[23][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[23][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[23][playerid], 310.0, 15.0);

	ATM_TD[24][playerid] = CreatePlayerTextDraw(playerid, 348.0, 239.763015, "2");
	PlayerTextDrawLetterSize(playerid, ATM_TD[24][playerid], 0.465333, 1.861333);
	PlayerTextDrawAlignment(playerid, ATM_TD[24][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[24][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[24][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[24][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[24][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[24][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[24][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[24][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[24][playerid], 360.0, 15.0);

	ATM_TD[25][playerid] = CreatePlayerTextDraw(playerid, 392.666748, 239.762985, "3");
	PlayerTextDrawLetterSize(playerid, ATM_TD[25][playerid], 0.476666, 1.828147);
	PlayerTextDrawAlignment(playerid, ATM_TD[25][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[25][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[25][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[25][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[25][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[25][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[25][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[25][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[25][playerid], 405.0, 15.0);

	ATM_TD[26][playerid] = CreatePlayerTextDraw(playerid, 255.999984, 278.340728, "4");
	PlayerTextDrawLetterSize(playerid, ATM_TD[26][playerid], 0.482666, 2.135111);
	PlayerTextDrawAlignment(playerid, ATM_TD[26][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[26][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[26][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[26][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[26][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[26][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[26][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[26][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[26][playerid], 270.0, 15.0);

	ATM_TD[27][playerid] = CreatePlayerTextDraw(playerid, 303.333374, 279.999969, "5");
	PlayerTextDrawLetterSize(playerid, ATM_TD[27][playerid], 0.473333, 1.919407);
	PlayerTextDrawAlignment(playerid, ATM_TD[27][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[27][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[27][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[27][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[27][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[27][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[27][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[27][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[27][playerid], 320.0, 15.0);

	ATM_TD[28][playerid] = CreatePlayerTextDraw(playerid, 348.0, 280.829559, "6");
	PlayerTextDrawLetterSize(playerid, ATM_TD[28][playerid], 0.475666, 1.836444);
	PlayerTextDrawAlignment(playerid, ATM_TD[28][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[28][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[28][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[28][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[28][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[28][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[28][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[28][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[28][playerid], 360.0, 15.0);

	ATM_TD[29][playerid] = CreatePlayerTextDraw(playerid, 392.666687, 280.414764, "7");
	PlayerTextDrawLetterSize(playerid, ATM_TD[29][playerid], 0.459666, 1.923555);
	PlayerTextDrawAlignment(playerid, ATM_TD[29][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[29][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[29][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[29][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[29][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[29][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[29][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[29][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[29][playerid], 405.0, 15.0);

	ATM_TD[30][playerid] = CreatePlayerTextDraw(playerid, 255.333374, 320.5, "8");
	PlayerTextDrawLetterSize(playerid, ATM_TD[30][playerid], 0.473000, 1.965036);
	PlayerTextDrawAlignment(playerid, ATM_TD[30][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[30][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[30][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[30][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[30][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[30][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[30][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[30][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[30][playerid], 270.0, 15.0);

	ATM_TD[31][playerid] = CreatePlayerTextDraw(playerid, 302.000061, 320.5, "9");
	PlayerTextDrawLetterSize(playerid, ATM_TD[31][playerid], 0.458666, 1.940148);
	PlayerTextDrawAlignment(playerid, ATM_TD[31][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[31][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[31][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[31][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[31][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[31][playerid], 2);
	PlayerTextDrawSetProportional(playerid, ATM_TD[31][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[31][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[31][playerid], 320.0, 15.0);

	ATM_TD[32][playerid] = CreatePlayerTextDraw(playerid, 349.000030, 321.481475, "Cancel");
	PlayerTextDrawLetterSize(playerid, ATM_TD[32][playerid], 0.432333, 1.757629);
	PlayerTextDrawAlignment(playerid, ATM_TD[32][playerid], 1);
	PlayerTextDrawColor(playerid, ATM_TD[32][playerid], -1);
	PlayerTextDrawSetShadow(playerid, ATM_TD[32][playerid], 0);
	PlayerTextDrawSetOutline(playerid, ATM_TD[32][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, ATM_TD[32][playerid], 51);
	PlayerTextDrawFont(playerid, ATM_TD[32][playerid], 1);
	PlayerTextDrawSetProportional(playerid, ATM_TD[32][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, ATM_TD[32][playerid], 1);
	PlayerTextDrawTextSize(playerid, ATM_TD[32][playerid], 405.0, 15.0);
}
