#include <YSI4\YSI_Coding\y_hooks>

hook OnPlayerEditDynObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz)
{
	if(response && ATMplacingObj[playerid] && objectid == ATMtempObj[playerid])
	{
		SetDynamicObjectPos(objectid, x, y, z);
		SetDynamicObjectRot(objectid, rx, ry, rz);
	}
	return 1;
}


CMD:atmconfirmpos(playerid, params[])
{
	new Float: ax, Float: ay, Float: az, Float: arx, Float: ary, Float: arz;

	if(Account[playerid][Admin] < 3)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");

	if(!ATMplacingObj[playerid] || !ATMtempObj[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're currently not editing an ATM.");

	if(ATMplacingObj[playerid] || ATMtempObj[playerid] != COLOR_INFO)
	{
		GetDynamicObjectPos(ATMtempObj[playerid], ax, ay, az);
		GetDynamicObjectRot(ATMtempObj[playerid], arx, ary, arz);

		printf("ATM conf. coords: (id) %d, (x) %f, (y) %f, (z) %f(rx) %f (ry) %f (rz) %f", ATMCount, ax, ay, az, arx, ary, arz);
		SendClientMessage(playerid, COLOR_BLUE, "[ADMIH]{E0E0E0} ATM has been successfully confirmed and placed at coordinates.");

		if(!EditingATM[playerid])
		{
		    CreateATM(ATMCount, ax, ay, az, arx, ary, arz, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
		}

		else if(EditingATM[playerid])
		{
		    new atmid = Editing_ATM_ID[playerid], query[600];

			ATM_Info[atmid][aX] = ax;
			ATM_Info[atmid][aY] = ay;
			ATM_Info[atmid][aZ] = az;

			ATM_Info[atmid][aRX] = arx;
			ATM_Info[atmid][aRY] = ary;
			ATM_Info[atmid][aRZ] = arz;

			ATM_Info[atmid][World] = GetPlayerVirtualWorld(playerid);
			ATM_Info[atmid][Interior] = GetPlayerInterior(playerid);

		    format(query, sizeof(query), "(({E0E0E0} ATM ID: %d {4AD5E7}))\n(({E0E0E0} /atm to use {4AD5E7}))", atmid + 1);

			ATM_Info[atmid][Object] = CreateDynamicObject(ATM_OBJECT, ax, ay, az, arx, ary, arz, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid), -1, 200.0, 300.0);
			ATM_Info[atmid][Label] = CreateDynamic3DTextLabel(query, 0x4AD5E7FF, ax, ay, az, 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));

			mysql_format(mysql, query, sizeof(query), "UPDATE `atms` SET `aX` = '%f', `aY` = '%f', `aZ` = '%f', `aRX` = '%f', `aRY` = '%f', `aRZ` = '%f', `World` = '%d', `Interior` = '%d' WHERE `ID` = '%d'", ax, ay, az, arx, ary, arz, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), atmid + 1);
			mysql_tquery(mysql, query, "", "");

			EditingATM[playerid] = false;
		}

	    DestroyDynamicObject(ATMtempObj[playerid]);

	    ATMplacingObj[playerid] = false;
	    ATMtempObj[playerid] = -1;
	}

	return true;
}
