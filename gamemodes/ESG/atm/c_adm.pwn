CMD:atmmove(playerid, params[])
{
	new atmid, Float: x, Float: y, Float: z, string[90];

	if(Account[playerid][Admin] < 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "i", atmid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /atmmove [id]");
	if(ATMplacingObj[playerid] || EditingATM[playerid]) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're already editing a ATM!");
	if(atmid >= ATMCount) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} The ATM ID you entered does not exist!");

	DestroyDynamicObject(ATM_Info[atmid][Object]);
	DestroyDynamic3DTextLabel(ATM_Info[atmid][Label]);

	GetPlayerPos(playerid, x, y, z);

	ATMtempObj[playerid] = CreateDynamicObject(1337, x + 1, y + 1, z, 0.0, 0.0, 0.0);
    EditDynamicObject(playerid, ATMtempObj[playerid]);

	SendClientMessage(playerid, COLOR_BLUE, "[ADMIH]{E0E0E0} Please move the ATM to a proper location and type /atmconfirmpos!");

    Editing_ATM_ID[playerid] = atmid;
	ATMplacingObj[playerid] = true;
	EditingATM[playerid] = true;

	if(Account[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has teleported to ATM ID %d.", playerid, Account[playerid][Name], atmid);
		SendAdminMsgEx(string);
	}

	return true;
}

CMD:atmgoto(playerid, params[])
{
	new atmid, string[90];

	if(Account[playerid][Admin] < 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "i", atmid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /atmgoto [id]");
	if(atmid >= ATMCount) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} The ATM ID you entered does not exist!");

	SetPlayerPos(playerid, ATM_Info[atmid][aX], ATM_Info[atmid][aY], ATM_Info[atmid][aZ]);
	SetPlayerInterior(playerid, ATM_Info[atmid][Interior]);
	SetPlayerVirtualWorld(playerid, ATM_Info[atmid][World]);

	format(string, sizeof(string), "[ADMIH]{E0E0E0} You have teleported to ATM ID %d.", atmid);
	SendClientMessage(playerid, COLOR_BLUE, string);

	if(Account[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has teleported to ATM ID %d.", playerid, Account[playerid][Name], atmid);
		SendAdminMsgEx(string);
	}

	return true;
}

CMD:atmdelete(playerid, params[])
{
	new atmid, string[130];

	if(Account[playerid][Admin] < 2) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an admin!");
	if(sscanf(params, "i", atmid)) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /atm [id]");
	if(atmid >= ATMCount) return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} The ATM ID you entered does not exist!");

	format(string, sizeof(string), "[ADMIH]{E0E0E0} You have deleted ATM ID %d.", atmid);
	SendClientMessage(playerid, COLOR_BLUE, string);

	mysql_format(mysql, string, sizeof(string), "DELETE FROM `atms` WHERE ID = '%d'", atmid);
	mysql_tquery(mysql, string, "", "");

	DestroyDynamicObject(ATM_Info[atmid][Object]);
	DestroyDynamic3DTextLabel(ATM_Info[atmid][Label]);

	new x[AtmInfo];
	ATM_Info[atmid] = x;

	if(Account[playerid][Admin] > 3)
	{
		format(string, sizeof(string), "[ADMIN WATCH] (%d) %s has removed ATM ID %d.", playerid, Account[playerid][Name], atmid);
		SendAdminMsgEx(string);
	}

	return true;
}
