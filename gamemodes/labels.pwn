#include <a_samp>
#include <a_mysql>
#include <streamer>
#include <sscanf2>
#include <zcmd>

#define COLOR_BLUE				(0x63C9FFFF)
#define COLOR_ERROR				(0xFF6347AA)

#define MAX_LABELS      (500)

#define LABEL_TYPE_TEXT     (0)
#define LABEL_TYPE_PASS     (1)

enum LabelInfo
{
	ID,
	Text3D: LabelHolder,
	
	Text[64],
	Type,

	Float: labelX,
	Float: labelY,
	Float: labelZ,

	Float: labeltpX,
	Float: labeltpY,
	Float: labeltpZ,

	Float: labeldd,

	labTestlos,
	labVirtualWorld,
	labInterior,
	
	labtpVirtualWorld,
	labtpInterior
};

new Label[MAX_LABELS][LabelInfo];
new labelCount;

#define MYSQL_HOST  "localhost"
#define MYSQL_USER  "root"
#define MYSQL_DB  	"sublimity"
#define MYSQL_PASS  "mysql"

static mysql;

main()
{
    mysql_log(LOG_ALL); // Enables console debugging
    mysql = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_DB, MYSQL_PASS);

    AddPlayerClass(0, -2624.3801, 1374.6218, 7.1105, 90.0, 0, 0, 0, 0, 0, 0);
	SQL_LoadLabels();

    if(mysql_errno(mysql) != 0) return print("Could not connect to database!");

	return printf("Connected to database with user \"%s\" and database \"%s\"", MYSQL_USER, MYSQL_DB);
}

IsPlayerNearLabel(playerid, point)
{
	for(new i; i < MAX_LABELS; i ++)
	{
		if(IsPlayerInRangeOfPoint(playerid, 5.0, Label[i][labelX], Label[i][labelY], Label[i][labelZ]) &&
		GetPlayerVirtualWorld(playerid) == Label[i][labVirtualWorld] && GetPlayerInterior(playerid) == Label[i][labInterior] && point == 0)
		{
			return true;
		}

		else if(IsPlayerInRangeOfPoint(playerid, 5.0, Label[i][labeltpX], Label[i][labeltpY], Label[i][labeltpZ]) &&
		GetPlayerVirtualWorld(playerid) == Label[i][labtpVirtualWorld] && GetPlayerInterior(playerid) == Label[i][labtpInterior] && point == 1)
		{
			return true;
		}
	}

	return false;
}

GetClosestLabelToPlayer(playerid)
{
	for(new i; i < MAX_LABELS; i ++)
	{
	    if(IsPlayerInRangeOfPoint(playerid, 5.0, Label[i][labelX], Label[i][labelY], Label[i][labelZ]))
	    {
	        return i;
	    }
	    
	    else if(IsPlayerInRangeOfPoint(playerid, 5.0, Label[i][labeltpX], Label[i][labeltpY], Label[i][labeltpZ]))
	    {
	        return i;
	    }
	}

	return 999;
}

CMD:pass(playerid, params[])
{
	new i = GetClosestLabelToPlayer(playerid);
	
	if(i == 999)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a label!");

	if(IsPlayerNearLabel(playerid, 0))
	{
	    SetPlayerPos(playerid, Label[i][labeltpX], Label[i][labeltpY], Label[i][labeltpZ]);

	    SetPlayerInterior(playerid, Label[i][labtpInterior]);
	    SetPlayerVirtualWorld(playerid, Label[i][labtpVirtualWorld]);
	}

	else if(IsPlayerNearLabel(playerid, 1))
	{
	    SetPlayerPos(playerid, Label[i][labelX], Label[i][labelY], Label[i][labelZ]);

	    SetPlayerInterior(playerid, Label[i][labInterior]);
	    SetPlayerVirtualWorld(playerid, Label[i][labVirtualWorld]);
	}

	return true;
}

CMD:createlabel(playerid, params[])
{
	new text[64], testlos, type, string[120];

	if(sscanf(params, "s[64]ii", text, type, testlos))
	    return SendClientMessage(playerid, COLOR_ERROR, "/createlabel [text] [type] [testlos]");

	if(type > 2)
	    return SendClientMessage(playerid, COLOR_ERROR, "There are only two types!");

	new Float: pX, Float: pY, Float: pZ;
	GetPlayerPos(playerid, pX, pY, pZ);

	format(string, sizeof(string), "[INFO]{E0E0E0} Created label ID %d with text %s (and type %d) at your location!", labelCount, text, type);
	SendClientMessage(playerid, COLOR_BLUE, string);

	SQL_AddLabel(text, type, pX, pY, pZ, 10.0, testlos, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));

	return true;
}

CMD:labeltppoint(playerid, params[])
{
	new id, query[200], Float: pX, Float: pY, Float: pZ;

	if(sscanf(params, "i", id))
	    return SendClientMessage(playerid, COLOR_ERROR, "/labeltppoint [id]");

	new enumid = id - 1;
	GetPlayerPos(playerid, pX, pY, pZ);

	if(Label[enumid][Type] != 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "This label can't get a teleport point!");

	Label[enumid][labeltpX] = pX;
	Label[enumid][labeltpY] = pY;
	Label[enumid][labeltpZ] = pZ;
	
	Label[enumid][labtpVirtualWorld] = GetPlayerVirtualWorld(playerid);
	Label[enumid][labtpInterior] = GetPlayerInterior(playerid);

	mysql_format(mysql, query, sizeof(query), "UPDATE `labels` SET `labeltpX` = '%f', `labeltpY` = '%f', `labeltpZ` = '%f'", pX, pY, pZ);
	mysql_format(mysql, query, sizeof(query), "%s, `labtpVirtualWorld` = '%d', `labtpInterior` = '%d' WHERE `ID` = '%d'",
		query, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), id);

	mysql_tquery(mysql, query, "", "");

	ReloadLabel(enumid);

	return true;
}

SQL_AddLabel(lText[64], type, Float: lX, Float: lY, Float: lZ, Float: ldd, lTestlos, lVirtualWorld, lInterior)
{
	new query[500];
	
	mysql_format(mysql, query, sizeof(query), "INSERT INTO `labels` (`Text`, `Type`, `labelX`, `labelY`, `labelZ`, `labeldd`, `labTestlos`, `labVirtualWorld`, `labInterior`)");
	mysql_format(mysql, query, sizeof(query), "%s VALUES ('%s', '%d', '%f', '%f', '%f', '%f', '%d', '%d', '%d')", query, lText, type, lX, lY, lZ, ldd, lTestlos, lVirtualWorld, lInterior);

	mysql_tquery(mysql, query, "", "");

   	Label[labelCount][ID] = labelCount;

   	Label[labelCount][Type] = type;

   	Label[labelCount][Text] = lText;

    Label[labelCount][labelX] = lX;
    Label[labelCount][labelY] = lY;
    Label[labelCount][labelZ] = lZ;

    Label[labelCount][labeldd] = ldd;
	Label[labelCount][labTestlos] =  lTestlos;

   	Label[labelCount][labVirtualWorld] = lVirtualWorld;
   	Label[labelCount][labInterior] = lInterior;

	ReloadLabel(labelCount);
	
	printf("Created label ID %d with type %d and text %s.", Label[labelCount][ID], Label[labelCount][Type], Label[labelCount][Text]);

	labelCount ++;

	return true;
}

SQL_LoadLabels()
{
	mysql_function_query(mysql, "SELECT * FROM `labels`", true, "SQL_LoadLabel", "");
}

ReloadLabel(labelid)
{
	new string[120];

	if(!Label[labelid][LabelHolder])
		DestroyDynamic3DTextLabel(Label[labelid][LabelHolder]);

	switch(Label[labelid][Type])
	{
	    case LABEL_TYPE_TEXT:
	    {
	        format(string, sizeof(string), "[%d] %s", Label[labelid][ID] + 1, Label[labelid][Text]);

    		Label[labelid][LabelHolder] =
				CreateDynamic3DTextLabel(string, 0x9EDE62FF,
					Label[labelid][labelX], Label[labelid][labelY], Label[labelid][labelZ],
					Label[labelid][labeldd], INVALID_PLAYER_ID, INVALID_VEHICLE_ID,
					Label[labelid][labTestlos], Label[labelid][labVirtualWorld], Label[labelid][labInterior], -1);
	    }

    	case LABEL_TYPE_PASS:
	    {
	        format(string, sizeof(string), "[%d] %s\n/pass", Label[labelid][ID] + 1, Label[labelid][Text]);

    		Label[labelid][LabelHolder] =
				CreateDynamic3DTextLabel(string, 0x5BA6CFFF,
					Label[labelid][labelX], Label[labelid][labelY], Label[labelid][labelZ],
					Label[labelid][labeldd], INVALID_PLAYER_ID, INVALID_VEHICLE_ID,
					Label[labelid][labTestlos], Label[labelid][labVirtualWorld], Label[labelid][labInterior], -1);

    		Label[labelid][LabelHolder] =
				CreateDynamic3DTextLabel(string, 0x5BA6CFFF,
					Label[labelid][labeltpX], Label[labelid][labeltpY], Label[labelid][labeltpZ],
					Label[labelid][labeldd], INVALID_PLAYER_ID, INVALID_VEHICLE_ID,
					Label[labelid][labTestlos], Label[labelid][labtpVirtualWorld], Label[labelid][labtpInterior], -1);
	    }
	}

	return true;
}

forward SQL_LoadLabel();
public SQL_LoadLabel()
{
	new rows, fields;
	cache_get_data(rows, fields, mysql);

	if(!cache_num_rows()) return printf("[LABELS] cache_get_row_count returned false. There are no labels to load.");
	printf("[LABELS] cache_get_row_count returned %d rows (labels) to load.", cache_get_row_count());

 	for(new i; i != cache_get_row_count(); i++)
	{
	   	Label[i][ID] =                	cache_insert_id();

	   	Label[i][Type] = 				cache_get_field_content_int(i, "Type");

	    cache_get_field_content(i, "Text", Label[i][Text], mysql, 64);

        Label[i][labelX] = 				cache_get_field_content_float(i, "labelX");
        Label[i][labelY] = 				cache_get_field_content_float(i, "labelY");
        Label[i][labelZ] = 				cache_get_field_content_float(i, "labelZ");

        Label[i][labeltpX] = 			cache_get_field_content_float(i, "labeltpX");
        Label[i][labeltpY] = 			cache_get_field_content_float(i, "labeltpY");
        Label[i][labeltpZ] = 			cache_get_field_content_float(i, "labeltpZ");

        Label[i][labeldd] = 			cache_get_field_content_float(i, "labeldd");
		Label[i][labTestlos] = 			cache_get_field_content_int(i, "labTestlos");
	   	
	   	Label[i][labVirtualWorld] = 	cache_get_field_content_int(i, "labVirtualWorld");
	   	Label[i][labInterior] = 		cache_get_field_content_int(i, "labInterior");

	   	Label[i][labtpVirtualWorld] = 	cache_get_field_content_int(i, "labtpVirtualWorld");
	   	Label[i][labtpInterior] = 		cache_get_field_content_int(i, "labtpInterior");

        ReloadLabel(i);

		printf("Loaded label ID %d with type %d and text %s.", Label[i][ID], Label[i][Type], Label[i][Text]);

        labelCount ++;
	}

	return true;
}
