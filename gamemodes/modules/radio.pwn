#include <YSI4\YSI_Coding\y_hooks>

#define MAX_BOOMBOXES     			(30)

new bool: PlacedBoomBox[MAX_PLAYERS];
new bool: WearingMP3[MAX_PLAYERS];
new bool: HasBoombox[MAX_PLAYERS];
new PlacedBoomBoxID[MAX_PLAYERS];

enum RadioInfo
{
	ID, Name[16], URL[21]
};

new RadioArray[][RadioInfo] = {
	{0, "Pop", 				"http://goo.gl/kOgsrz"},
	{1, "Rock ", 			"http://goo.gl/D7ExXO"},
	{2, "Country", 			"http://goo.gl/jlsPHI"},
	{3, "Hip Hop", 			"http://goo.gl/D7vJoq"},
	{4, "Dance", 			"http://goo.gl/pRQedj"} // Note to self, this radio fucking rocks. (181.FM / ENERGY 93)
};

enum BoomboxInfo
{
	ID,

	Placer[MAX_PLAYER_NAME],
	bool: Placed,

	Object, // stores object
	URL[16],

	Float: bbX,
	Float: bbY,
	Float: bbZ,

	bbVirtualWorld,
	bbInterior,
};

new Boombox[MAX_BOOMBOXES][BoomboxInfo], bbCount;

hook OnCharacterSpawn(playerid)
{
	PreloadAnimLib(0, "BSKTBALL");
	return 1;
}

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	switch(dialogid)
	{
	    case DIALOG_STYLE_BOOMBOX_SEL:
	    {
			if(response)
			{
		    	new id = GetBoomboxIDClosestToPlayer(playerid);

				if(id == 999)
				    return false;

				PlayAudioStreamForPlayer(playerid, RadioArray[listitem][URL], Boombox[id][bbX], Boombox[id][bbY], Boombox[id][bbZ], 5.0, 1);
			}
	    }

		case DIALOG_STYLE_CHANNEL_SEL:
		{
			if(response)
			{
				PlayAudioStreamForPlayer(playerid, RadioArray[listitem][URL]);
			}
		}
	}
	return 1;
}


CMD:boombox(playerid, params[])
{
	new Float: pX, Float: pY, Float: pZ;
	GetPlayerPos(playerid, pX, pY, pZ);

	if(!HasBoombox[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a boombox! Visit a 24/7 and purchase one.");

	if(PlacedBoomBox[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You already dropped a boombox! Pick up your previous one if you wish to place another.");

	Boombox[bbCount][Object] = CreateDynamicObject(2226, pX, pY, pZ - 1, 0, 0, 65, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), -1, 200.0);

	Boombox[bbCount][bbX] = pX;
	Boombox[bbCount][bbY] = pY;
	Boombox[bbCount][bbZ] = pZ;

	Boombox[bbCount][bbVirtualWorld] = GetPlayerVirtualWorld(playerid);
	Boombox[bbCount][bbInterior] = GetPlayerInterior(playerid);

	Boombox[bbCount][Placed] = true;

	PlacedBoomBox[playerid] = true;
	PlacedBoomBoxID[playerid] = bbCount;

	bbCount ++;

	ApplyAnimation(playerid, "BSKTBALL", "BBALL_pickup", 4.1, 0, 0, 0, 0, 1000, 1);
	SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have dropped a boombox! You can manage it by using /boomboxplay, and pick it up by using /boomboxpickup.");

	return true;
}

CMD:boomboxplay(playerid, params[])
{
	new string[120];

	if(IsPlayerNearBoomBox(playerid))
	{
	    if(GetBoomboxIDClosestToPlayer(playerid) == PlacedBoomBoxID[playerid])
	    {
			for(new i; i < sizeof(RadioArray); i ++)
			{
			    format(string, sizeof(string), "%s\nID: %d - %s", string, i, RadioArray[i][Name]);
			}

			ShowPlayerDialog(playerid, DIALOG_STYLE_BOOMBOX_SEL, DIALOG_STYLE_LIST, "Boombox", string, "Select", "Cancel");
		}

	    else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This isn't your boombox!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a boombox!");

	return true;
}

CMD:boomboxpickup(playerid, params[])
{
	if(IsPlayerNearBoomBox(playerid))
	{
	    if(GetBoomboxIDClosestToPlayer(playerid) == PlacedBoomBoxID[playerid])
	    {
	        new id = GetBoomboxIDClosestToPlayer(playerid);

	        PlacedBoomBox[playerid] = false;
	        Boombox[id][Placed] = false;

	        DestroyDynamicObject(Boombox[id][Object]);

	        foreach(new i: Player)
	        {
		        if(IsPlayerNearSpecificBoombox(i, id))
		        {
		            StopAudioStreamForPlayer(i);
				}
			}

	        bbCount --;

			ApplyAnimation(playerid, "BSKTBALL", "BBALL_pickup", 4.1, 0, 0, 0, 0, 1000, 1);
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have picked up your boombox!");
	    }

	    else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This isn't your boombox!");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near a boombox!");

	return true;
}


CMD:mp3(playerid, params[])
{
	if(Player[playerid][HasMP3])
	{
		if(!WearingMP3[playerid])
		{
			SetPlayerAttachedObject(playerid, 0, 19421, 2, 0.034, 0, 0, -92, 174, 90.613, 1, 1, 1);
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have put on your MP3! Use /mp3play to choose a channel.");

			WearingMP3[playerid] = true;
		}

		else if(WearingMP3[playerid])
		{
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} You have removed your MP3!");

		    RemovePlayerAttachedObject(playerid, 0);
			StopAudioStreamForPlayer(playerid);

		    WearingMP3[playerid] = false;
		}
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a MP3.");

	return true;
}

CMD:mp3play(playerid, params[])
{
	new string[120];

	if(Player[playerid][HasMP3] && WearingMP3[playerid])
	{
		for(new i; i < sizeof(RadioArray); i ++)
		{
		    format(string, sizeof(string), "%s\nID: %d - %s", string, i, RadioArray[i][Name]);
		}

		ShowPlayerDialog(playerid, DIALOG_STYLE_BOOMBOX_SEL, DIALOG_STYLE_LIST, "Boombox", string, "Select", "Cancel");
	}

	else return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You either don't have an MP3, or you're not wearing it.");

	return true;
}

GetBoomboxIDClosestToPlayer(playerid)
{
	for(new i; i < MAX_BOOMBOXES; i ++)
	{
	    if(Boombox[i][Placed])
	    {
	        if(IsPlayerInRangeOfPoint(playerid, 5.0, Boombox[i][bbX], Boombox[i][bbY], Boombox[i][bbZ]))
	        {
	            return i;
	        }
	    }
	}

	return 999;
}

IsPlayerNearBoomBox(playerid)
{
	for(new i; i < MAX_BOOMBOXES; i ++)
	{
	    if(Boombox[i][Placed])
	    {
	        if(IsPlayerInRangeOfPoint(playerid, 1.0, Boombox[i][bbX], Boombox[i][bbY], Boombox[i][bbZ]))
	        {
	            return true;
	        }
	    }
	}

	return false;
}

IsPlayerNearSpecificBoombox(playerid, boomboxid)
{
    if(Boombox[boomboxid][Placed])
    {
        if(IsPlayerInRangeOfPoint(playerid, 1.0, Boombox[boomboxid][bbX], Boombox[boomboxid][bbY], Boombox[boomboxid][bbZ]))
        {
            return true;
        }
    }

	return false;
}

PreloadAnimLib(playerid, animlib[])
{
    ApplyAnimation(playerid, animlib, "null", 0.0, 0, 0, 0, 0, 0);
}
