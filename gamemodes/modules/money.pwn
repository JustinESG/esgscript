CMD:withdraw(playerid, params[])
{
	new amount, string[144];

	if(!IsPlayerInRangeOfPoint(playerid, 3.0, 1957.0490, -2286.0330, 13.5785) && GetPlayerInterior(playerid) != 1 && GetPlayerVirtualWorld(playerid) != 701)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near the correct bank booth!");

	if(sscanf(params, "i", amount))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /withdraw [amount]");

	if(amount > Character[playerid][BankAmount])
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have that much money in your bank account!");

	Character[playerid][BankAmount] -= amount;
    GivePlayerMoneyEx(playerid, amount);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have just withdrawn {63C9FF}\"$ %d\"{E0E0E0}. You have {63C9FF}\"$ %d\"{E0E0E0} left in your bank account.", amount, Character[playerid][BankAmount]);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:deposit(playerid, params[])
{
	new amount, string[144];

	if(!IsPlayerInRangeOfPoint(playerid, 3.0, 1957.0490, -2286.0330, 13.5785) && GetPlayerInterior(playerid) != 1 && GetPlayerVirtualWorld(playerid) != 701)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near the correct bank booth!");

	if(sscanf(params, "i", amount))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /deposit [amount]");

	if(amount > GetPlayerMoneyEx(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have that much money!");

	Character[playerid][BankAmount] += amount;
	GivePlayerMoneyEx(playerid, -amount);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have just deposited {63C9FF}\"$ %d\"{E0E0E0}. You have {63C9FF}\"$ %d\"{E0E0E0} in your bank account.", amount, Character[playerid][BankAmount]);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:savings(playerid, params[])
{
	new amount, string[144];

	if(!IsPlayerInRangeOfPoint(playerid, 3.0, 1957.0490, -2286.0330, 13.5785) && GetPlayerInterior(playerid) != 1 && GetPlayerVirtualWorld(playerid) != 701)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near the correct bank booth!");

	if(sscanf(params, "i", amount))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /savings [amount]");

	if(amount > GetPlayerMoneyEx(playerid))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have that much money!");

	if(amount < 25000)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You have to deposit a minimum of $20,000 into your savings account!");

	Character[playerid][BankSavings] += amount;
	GivePlayerMoneyEx(playerid, -amount);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have just deposited {63C9FF}\"$ %d\"{E0E0E0} into your savings account.", amount);
	SendClientMessage(playerid, COLOR_BLUE, string);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have {63C9FF}\"$ %d\"{E0E0E0} in your bank account. You can access your savings after 10 paydays.", Character[playerid][BankAmount]);
	SendClientMessage(playerid, COLOR_BLUE, string);

	Character[playerid][SavingsCooldown] = 10;

	return true;
}

CMD:emptysavings(playerid, params[])
{
	new string[144];

	if(!IsPlayerInRangeOfPoint(playerid, 3.0, 1957.0490, -2286.0330, 13.5785) && GetPlayerInterior(playerid) != 1 && GetPlayerVirtualWorld(playerid) != 701)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're not near the correct bank booth!");
	
	if(Character[playerid][SavingsCooldown] > 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're still on savings cooldown! Wait until the next payday to withdraw your savings.");

	format(string, sizeof(string), "[INFO]{E0E0E0} You have just withdrawn {63C9FF}\"$ %d\"{E0E0E0} from your savings account.", Character[playerid][BankSavings]);
	SendClientMessage(playerid, COLOR_BLUE, string);

	format(string, sizeof(string), "[INFO]{E0E0E0} You can redeposit money into your savings account after two paydays.", Character[playerid][BankAmount]);
	SendClientMessage(playerid, COLOR_BLUE, string);

	GivePlayerMoneyEx(playerid, Character[playerid][BankSavings]);

	Character[playerid][BankSavings] = 0;
	Character[playerid][SavingsCooldown] = 2;

	return true;
}

// Anti Cheat Functions:

TakePlayerMoney(playerid, amount)
{
	Character[playerid][Money] -= amount;
	GivePlayerMoney(playerid, -amount);

	return true;
}

GivePlayerMoneyEx(playerid, amount)
{
	Character[playerid][Money] += amount;
	GivePlayerMoney(playerid, amount);

	return true;
}

SetPlayerMoneyEx(playerid, amount)
{
	ResetPlayerMoney(playerid);

	Character[playerid][Money] = amount;
	GivePlayerMoney(playerid, amount);

	return true;
}

GetPlayerMoneyEx(playerid)
{
	if(GetPlayerMoney(playerid) > Character[playerid][Money])
	{
		new anticheatstring[120];

		format(anticheatstring, sizeof(anticheatstring), "Player [%d] %s might be money hacking! [Has $%d, should have $%d!]", playerid, Account[playerid][Name], GetPlayerMoney(playerid), Character[playerid][Money]);
	    SendAdminMsgEx(anticheatstring), print(anticheatstring);
	}

	return Character[playerid][Money];
}
