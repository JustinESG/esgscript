#include <YSI4\YSI_Coding\y_hooks>

new Text3D: NameLabel[MAX_PLAYERS];
new bool: pMasked[MAX_PLAYERS];
new bool: pMask[MAX_PLAYERS];

CMD:mask(playerid, params[])
{
	Delete3DTextLabel(NameLabel[playerid]);

	if(!pMask[playerid])
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a mask!");

	switch(pMasked[playerid])
	{
		case false: pMasked[playerid] = true;
		case true: pMasked[playerid] = false;
	}

	NameLabel[playerid] = Create3DTextLabel(GetRoleplayName(playerid), 0xCFCFCFFF, 0.0, 0.0, 0.0, 4.0, -1, 1);
    Attach3DTextLabelToPlayer(NameLabel[playerid], playerid, 0.0, 0.0, 0.15);

	return true;
}

hook OnCharacterSpawn(playerid)
{
	NameLabel[playerid] = Create3DTextLabel(GetRoleplayName(playerid), 0xCFCFCFFF, 0.0, 0.0, 0.0, 4.0, -1, 1);
    Attach3DTextLabelToPlayer(NameLabel[playerid], playerid, 0.0, 0.0, 0.15);
    
    printf("(%d) %s has been attached to a custom nametag!", playerid, Account[playerid][Name]);
	return 1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	Delete3DTextLabel(NameLabel[playerid]);
	return 1;
}
