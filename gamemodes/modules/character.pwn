/*
	Author: Mads
	Date: 14-02-2015
	For: EastSideGaming
	Description: A Character Creation system.
*/
#include <YSI4\YSI_Coding\y_hooks>


#define MAX_CHARACTERS 3


enum GenderType
{
	NONE,
	MALE,
	FEMALE
}
enum CharacterData 
{
	ID,
	Name[MAX_PLAYER_NAME],
	GenderType:Gender,
	CharacterLocked,
	Skin,
	Level,
	Money,
	Rank,
	HoldingWeapon,
	BankAmount,
	BankSavings,
	SavingsCooldown,
	FactionID,
	Tier,
	PhoneNumber[11],
	DrivingLicense,
	WeaponLicense,
	hWeaponAmmo,
	TrouserSlot,
	TrouserAmmo,
	JacketSlot,
	JacketAmmo,
	BizOwner,
	Medikits,
	HasGPS,
	HasCiggy,
	HasMask,
	MaskID,
	SQL_ToyInitiate,
	VehSlot1,
	VehSlot2,
	Float:PosX,
	Float:PosY,
	Float:PosZ,
	Float:PosA,
	PosInt,
	PosVW,
}
new Character[MAX_PLAYERS][CharacterData];

enum CachedCharData
{
	ID,
	Name[MAX_PLAYER_NAME],
	Skin
}
static CachedChars[MAX_PLAYERS][MAX_CHARACTERS][CachedCharData];

static SelectedCharIndex[MAX_PLAYERS];

enum CreatingCharEnum 
{
	ID,
	Name[MAX_PLAYER_NAME],
	GenderType:Gender,
	Age,
	Skin,
	Spawn,
	
	SkinIndex,
	SpawnIndex
}
static CreatingCharData[MAX_PLAYERS][CreatingCharEnum];


enum SpawnInfo
{
	Name[32],
	Float:PosX,
	Float:PosY,
	Float:PosZ,
	Float:PosA
}
static SpawnLocations[][SpawnInfo] = {
	{"Airport", 0.0, 0.0, 5.0, 0.0}
};

static MaleSkins[] = {
	17,18,19,20,29,26,46,299,114,188,250
};

static FemaleSkins[] = {
	9,10,11,12,13,31,38,39,40,41,53,54,55,56,63,64,69,75,76,77,85,87,88,89,90,
	91,92,93,129,130,131,138,139,140,141,145,148,150,151,152,157,169,172,178,
	190,191,192,193,194,195,196,197,198,199,201,205,207,211,214,215,216,218,
	219,224,225,226,231,232,233,237,238,243,244,245,246,251,256,257,263,298
};

/*
	Character Selection
*/
enum CharSelectionEnum
{
	PlayerText:Textdraw0,
	PlayerText:Textdraw1,
	PlayerText:SelectChar,
	PlayerText:CreateChar,
	PlayerText:DeleteChar,
	PlayerText:LeftArrow,
	PlayerText:RightArrow,
	PlayerText:Box,
	PlayerText:CurrentChar
}
new PlayerText:CharSelectTD[MAX_PLAYERS][CharSelectionEnum];

/*
	Character Creation
*/
enum CharacterCreationEnum
{
	PlayerText:Box,
	PlayerText:Title,
	PlayerText:Info,
	PlayerText:Name,
	PlayerText:Gender,
	PlayerText:Age,
	PlayerText:Clothes,
	PlayerText:GenderLeft,
	PlayerText:AgeLeft,
	PlayerText:ClothesLeft,
	PlayerText:GenderRight,
	PlayerText:AgeRight,
	PlayerText:ClothesRight,
	PlayerText:Spawn,
	PlayerText:SpawnLeft,
	PlayerText:SpawnRight,
	PlayerText:ButtonText,
	PlayerText:ButtonBox,
	PlayerText:NameValue,
	PlayerText:GenderValue,
	PlayerText:AgeValue,
	PlayerText:ClothesValue,
	PlayerText:SpawnValue
}
new PlayerText:CharCreateTD[MAX_PLAYERS][CharacterCreationEnum];


hook OnPlayerClickPlayerTD(playerid, PlayerText:clickedid) 
{
	if(_:clickedid != INVALID_TEXT_DRAW) 
	{
		if(clickedid == CharCreateTD[playerid][NameValue]) 
		{
			ShowPlayerDialog(playerid, DIALOG_CHAR_CREATE_SELECT_NAME, DIALOG_STYLE_INPUT, "Select a name", "Please select a name for character in format: FirstName_LastName", "Ok", "");
			CancelSelectTextDraw(playerid);
		}
		else if(clickedid == CharCreateTD[playerid][GenderLeft] 
			|| clickedid == CharCreateTD[playerid][GenderRight]) 
		{
			if(CreatingCharData[playerid][Gender] == MALE)
			{
				PlayerTextDrawSetString(playerid, CharCreateTD[playerid][GenderValue], "Female");
				CreatingCharData[playerid][Gender] = FEMALE;

				new skin_idx = CreatingCharData[playerid][SkinIndex] = random(sizeof(FemaleSkins));
				CreatingCharData[playerid][Skin] = FemaleSkins[skin_idx];
			} 
			else
			{
				PlayerTextDrawSetString(playerid, CharCreateTD[playerid][GenderValue], "Male");
				CreatingCharData[playerid][Gender] = MALE;

				new skin_idx = CreatingCharData[playerid][SkinIndex] = random(sizeof(MaleSkins));
				CreatingCharData[playerid][Skin] = MaleSkins[skin_idx];
			}
			
			SetPlayerSkin(playerid, CreatingCharData[playerid][Skin]);
			
			new skinid_str[4];
			valstr(skinid_str, CreatingCharData[playerid][Skin]);
			PlayerTextDrawSetString(playerid, CharCreateTD[playerid][ClothesValue], skinid_str);
			
			CancelSelectTextDraw(playerid);
			SelectTextDraw(playerid, 0xFFFFFFFF);
		} 
		else if(clickedid == CharCreateTD[playerid][AgeLeft]
			|| clickedid == CharCreateTD[playerid][AgeRight])
		{
			new age = CreatingCharData[playerid][Age];
			if(clickedid == CharCreateTD[playerid][AgeLeft])
				age--;
			else
				age++;
			
			if(age < 18)
				age = 18;
			else if(age > 70)
				age = 70;
			
			if(age != CreatingCharData[playerid][Age])
			{
				new age_str[3];
				valstr(age_str, age);
				PlayerTextDrawSetString(playerid, CharCreateTD[playerid][AgeValue], age_str);
				CreatingCharData[playerid][Age] = age;
			}
			CancelSelectTextDraw(playerid);
			SelectTextDraw(playerid, 0xFFFFFFFF);
		}
		else if(clickedid == CharCreateTD[playerid][ClothesLeft]
			|| clickedid == CharCreateTD[playerid][ClothesRight])
		{
			new skin_idx = CreatingCharData[playerid][SkinIndex];
			if(clickedid == CharCreateTD[playerid][ClothesLeft])
				skin_idx--;
			else
				skin_idx++;
				
			if(skin_idx < 0)
				skin_idx = 0;
			
			if(CreatingCharData[playerid][Gender] == MALE) 
			{
				if(skin_idx == sizeof(MaleSkins))
					skin_idx = sizeof(MaleSkins) - 1;
					
				CreatingCharData[playerid][Skin] = MaleSkins[skin_idx];
			} 
			else if(CreatingCharData[playerid][Gender] == FEMALE) 
			{
				if(skin_idx == sizeof(FemaleSkins))
					skin_idx = sizeof(FemaleSkins) - 1;
				
				CreatingCharData[playerid][Skin] = FemaleSkins[skin_idx];
			}
			CreatingCharData[playerid][SkinIndex] = skin_idx;
			SetPlayerSkin(playerid, CreatingCharData[playerid][Skin]);
			
			new skinid_str[4];
			valstr(skinid_str, CreatingCharData[playerid][Skin]);
			PlayerTextDrawSetString(playerid, CharCreateTD[playerid][ClothesValue], skinid_str);
			
			CancelSelectTextDraw(playerid);
			SelectTextDraw(playerid, 0xFFFFFFFF);
		} 
		else if(clickedid == CharSelectTD[playerid][LeftArrow]
			|| clickedid == CharSelectTD[playerid][RightArrow]) 
		{
			if(clickedid == CharSelectTD[playerid][LeftArrow])
				SelectedCharIndex[playerid]--;
			else
				SelectedCharIndex[playerid]++;
			//assert(0 <= SelectedCharIndex[playerid] < MAX_CHARACTERS);
			
			if(SelectedCharIndex[playerid] == (MAX_CHARACTERS - 1)) // last character
				PlayerTextDrawHide(playerid, CharSelectTD[playerid][RightArrow]);
			else
				PlayerTextDrawShow(playerid, CharSelectTD[playerid][RightArrow]);
			
			if(SelectedCharIndex[playerid] == 0) // first character
				PlayerTextDrawHide(playerid, CharSelectTD[playerid][LeftArrow]);
			else
				PlayerTextDrawShow(playerid, CharSelectTD[playerid][LeftArrow]);

			
			new skin, name[MAX_PLAYER_NAME];
			if(CachedChars[playerid][ SelectedCharIndex[playerid] ][ID] != 0) //character/row exists at selected index 
			{
				PlayerTextDrawHide(playerid, CharSelectTD[playerid][CreateChar]);
				PlayerTextDrawShow(playerid, CharSelectTD[playerid][SelectChar]);
				PlayerTextDrawShow(playerid, CharSelectTD[playerid][DeleteChar]);
				skin = CachedChars[playerid][ SelectedCharIndex[playerid] ][Skin];
				format(name, sizeof name, CachedChars[playerid][ SelectedCharIndex[playerid] ][Name]);
			}
			else
			{
				PlayerTextDrawShow(playerid, CharSelectTD[playerid][CreateChar]);
				PlayerTextDrawHide(playerid, CharSelectTD[playerid][SelectChar]);
				PlayerTextDrawHide(playerid, CharSelectTD[playerid][DeleteChar]);
				skin = 299;
				name = "Empty Slot";
			} 
			
			SetPlayerSkin(playerid, skin);
			PlayerTextDrawSetString(playerid, CharSelectTD[playerid][CurrentChar], name);
			CancelSelectTextDraw(playerid);
			SelectTextDraw(playerid, 0xFFFFFFFF);
		} 
		else if(clickedid == CharSelectTD[playerid][CreateChar]) 
		{
			SetTimerEx("StartCharacterCreation", 100, false, "i", playerid);
			CancelSelectTextDraw(playerid);
			SelectTextDraw(playerid, 0xFFFFFFFF);
		} 
		else if(clickedid == CharCreateTD[playerid][SpawnLeft]
			|| clickedid == CharCreateTD[playerid][SpawnRight])
		{
			new spawn_idx = CreatingCharData[playerid][SpawnIndex];
			if(clickedid == CharCreateTD[playerid][SpawnLeft])
				spawn_idx--;
			else
				spawn_idx++;
				
			if(spawn_idx < 0)
				spawn_idx = 0;
			else if(spawn_idx == sizeof(SpawnLocations))
				spawn_idx = sizeof(SpawnLocations) - 1;

			CreatingCharData[playerid][SpawnIndex] = spawn_idx;
			PlayerTextDrawSetString(playerid, CharCreateTD[playerid][SpawnValue], SpawnLocations[spawn_idx][Name]);
			
			CancelSelectTextDraw(playerid);
			SelectTextDraw(playerid, 0xFFFFFFFF);
		}
		else if(clickedid == CharCreateTD[playerid][ButtonBox]) 
		{
			CancelSelectTextDraw(playerid);
			if(strcmp(CreatingCharData[playerid][Name], "SELECT") == 0) 
			{
				SendClientMessage(playerid, X11_GRAY, "Please choose a name for your character before continuing");
				SelectTextDraw(playerid, 0xFFFFFFFF);
			}
			else
			{
				new query_str[256];
				new sidx = CreatingCharData[playerid][SpawnIndex];
				
				mysql_format(mysql, query_str, sizeof query_str,
					"INSERT INTO `characters` "\
					"(`AccountID`, `Name`, `Skin`, `Gender`, `PosX`, `PosY`, `PosZ`, `PosA`) "\
					"VALUES (%d, '%e', %d, %d, %f, %f, %f, %f)",
					Account[playerid][ID], CreatingCharData[playerid][Name],
					CreatingCharData[playerid][Skin], _:CreatingCharData[playerid][Gender],
					SpawnLocations[sidx][PosX], SpawnLocations[sidx][PosY],
					SpawnLocations[sidx][PosZ], SpawnLocations[sidx][PosA]);
				//TODO: insert age
				mysql_tquery(mysql, query_str, "MysqlCharacterStore", "d", playerid);
			}
		} 
		else if(clickedid == CharSelectTD[playerid][SelectChar]) 
		{
			new query_str[128];
			format(query_str, sizeof query_str, 
				"SELECT *, `Gender`+0 AS `gender_idx` FROM `characters` WHERE `ID` = %d",
				CachedChars[playerid][ SelectedCharIndex[playerid] ][ID]);
			mysql_tquery(mysql, query_str, "MysqlCharacterLoad", "d", playerid);
			
			CancelSelectTextDraw(playerid);
		} 
		else if(clickedid == CharSelectTD[playerid][DeleteChar]) 
		{
			ShowPlayerDialog(playerid, DIALOG_CHARACTER_DELETE, DIALOG_STYLE_MSGBOX, "Delete Character", "Are you sure you want to delete this character?", "Yes", "Cancel");
			CancelSelectTextDraw(playerid);
		}
	}
	return 1;
}

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) 
{
	if(dialogid == DIALOG_CHAR_CREATE_SELECT_NAME) 
	{
		if(strfind(inputtext, "_") != -1)
		{
			format(CreatingCharData[playerid][Name], MAX_PLAYER_NAME, inputtext);
			PlayerTextDrawSetString(playerid, CharCreateTD[playerid][NameValue], inputtext);
		} 
		else 
		{
			ShowPlayerDialog(playerid, DIALOG_CHAR_CREATE_SELECT_NAME, DIALOG_STYLE_INPUT, 
				"Select a name", 
				"Please select a name for character in format: FirstName_LastName", "Ok", "");
		}
		SelectTextDraw(playerid, 0xFFFFFFFF);
	} 
	else if(dialogid == DIALOG_CHARACTER_DELETE) 
	{
		if(response) 
		{
			new query[128];
			mysql_format(mysql, query, sizeof(query), 
				"DELETE FROM `characters` WHERE `ID` = %d", 
				CachedChars[playerid][ SelectedCharIndex[playerid] ][ID]);
			mysql_tquery(mysql, query);
			
			CachedChars[playerid][ SelectedCharIndex[playerid] ][ID] = 0;
			format(CachedChars[playerid][ SelectedCharIndex[playerid] ][Name],
				MAX_PLAYER_NAME, "Empty Slot");
			CachedChars[playerid][ SelectedCharIndex[playerid] ][Skin] = 299;
			
			PlayerTextDrawSetString(playerid, CharSelectTD[playerid][CurrentChar], "Empty Slot");
			PlayerTextDrawHide(playerid, CharSelectTD[playerid][SelectChar]);
			PlayerTextDrawHide(playerid, CharSelectTD[playerid][DeleteChar]);
			PlayerTextDrawShow(playerid, CharSelectTD[playerid][CreateChar]);
		}
		SelectTextDraw(playerid, 0xFFFFFFFF);
	}
	return 1;
}

hook OnPlayerConnect(playerid) 
{
	CreateCharacterSelectionMenu(playerid);
	CreateCharacterCreationMenu(playerid);
	return 1;
}

hook OnPlayerDisconnect(playerid, reason) 
{
	DestroyCharacterSelectionMenu(playerid);
	DestroyCharacterCreationMenu(playerid);
	
	for(new i = 0; i < MAX_CHARACTERS; i++) 
	{
		for(new CharacterData:e; e != CharacterData; ++e)
			Character[playerid][e] = 0;
			
		for(new CachedCharData:e; e != CachedCharData; ++e)
			CachedChars[playerid][i][e] = 0;
	}
	return 1;
}

hook OnPlayerSpawn(playerid)
{
	if(Account[playerid][LoggedIn] && Character[playerid][ID] != 0)
		SetTimerEx("OnCharacterSpawn", 0, false, "d", playerid);
	return 1;
}


forward MysqlCharacterLoad(playerid) ;
public MysqlCharacterLoad(playerid) 
{
	//assert(cache_num_rows() == 1);
	Character[playerid][ID] = 				cache_get_field_content_int(0, "ID");
	cache_get_field_content(0, "Name", Character[playerid][Name], .max_len = MAX_PLAYER_NAME);
	Character[playerid][Gender] =			GenderType:cache_get_field_content_int(0, "gender_idx");
	Character[playerid][CharacterLocked] =	cache_get_field_content_int(0, "CharacterLocked");
	Character[playerid][Skin] = 			cache_get_field_content_int(0, "Skin");
	Character[playerid][Level] = 			cache_get_field_content_int(0, "Level");
	Character[playerid][Money] = 			cache_get_field_content_int(0, "Money");
	Character[playerid][BankAmount] = 		cache_get_field_content_int(0, "BankAmount");
	Character[playerid][BankSavings] = 		cache_get_field_content_int(0, "BankSavings");
	Character[playerid][SavingsCooldown] = 	cache_get_field_content_int(0, "SavingsCooldown");
	Character[playerid][FactionID] =		cache_get_field_content_int(0, "FactionID");
	Character[playerid][Tier] = 			cache_get_field_content_int(0, "Tier");
	cache_get_field_content(0, "PhoneNumber", Character[playerid][PhoneNumber], .max_len = 11);
	Character[playerid][DrivingLicense] = 	cache_get_field_content_int(0, "DrivingLicense");
	Character[playerid][WeaponLicense] = 	cache_get_field_content_int(0, "HoldingWeapon");
	Character[playerid][hWeaponAmmo] = 		cache_get_field_content_int(0, "hWeaponAmmo");
	Character[playerid][TrouserSlot] = 		cache_get_field_content_int(0, "TrouserSlot");
	Character[playerid][TrouserAmmo] = 		cache_get_field_content_int(0, "TrouserAmmo");
	Character[playerid][JacketSlot] = 		cache_get_field_content_int(0, "JacketSlot");
	Character[playerid][JacketAmmo] = 		cache_get_field_content_int(0, "JacketAmmo");
	Character[playerid][BizOwner] = 		cache_get_field_content_int(0, "BizOwner");
	Character[playerid][Medikits] = 		cache_get_field_content_int(0, "Medikits");
	Character[playerid][HasGPS] = 			cache_get_field_content_int(0, "HasGPS");
	Character[playerid][HasCiggy] = 		cache_get_field_content_int(0, "HasCiggy");
	Character[playerid][HasMask] = 			cache_get_field_content_int(0, "HasMask");
	Character[playerid][SQL_ToyInitiate] = 	cache_get_field_content_int(0, "SQL_ToyInitiate");
	Character[playerid][VehSlot1] = 		cache_get_field_content_int(0, "VehSlot1");
	Character[playerid][VehSlot2] = 		cache_get_field_content_int(0, "VehSlot2");
	Character[playerid][PosX] = 			cache_get_field_content_float(0, "PosX");
	Character[playerid][PosY] = 			cache_get_field_content_float(0, "PosY");
	Character[playerid][PosZ] = 			cache_get_field_content_float(0, "PosZ");
	Character[playerid][PosA] = 			cache_get_field_content_float(0, "PosA");
	Character[playerid][PosInt] = 			cache_get_field_content_int(0, "PosInt");
	Character[playerid][PosVW] = 			cache_get_field_content_int(0, "PosVW");
	
	HideCharacterSelectionMenu(playerid);
	HideCreateCharacterMenu(playerid);
	
	SetTimerEx("OnCharacterSpawn", 0, false, "d", playerid);
	
	return 1;
}

forward OnCharacterSpawn(playerid);
public OnCharacterSpawn(playerid)
{
	SetCameraBehindPlayer(playerid);
	SetPlayerSkin(playerid, Character[playerid][Skin]);
	SetPlayerPos(playerid, Character[playerid][PosX], Character[playerid][PosY], Character[playerid][PosZ]);
	SetPlayerFacingAngle(playerid, Character[playerid][PosA]);
	SetPlayerInterior(playerid, Character[playerid][PosInt]);
	SetPlayerVirtualWorld(playerid, Character[playerid][PosVW]);
	TogglePlayerControllable(playerid, 1);
	
	return 1;
}

forward MysqlCharacterStore(playerid);
public MysqlCharacterStore(playerid)
{
	new cchar_idx = SelectedCharIndex[playerid];
	
	CachedChars[playerid][cchar_idx][ID] = cache_insert_id();
	format(CachedChars[playerid][cchar_idx][Name], MAX_PLAYER_NAME, CreatingCharData[playerid][Name]);
	CachedChars[playerid][cchar_idx][Skin] = CreatingCharData[playerid][Skin];
	
	for(new CreatingCharEnum:e; e != CreatingCharEnum; e++)
		CreatingCharData[playerid][e] = 0;
	
	HideCreateCharacterMenu(playerid);
	ShowCharacterSelectionMenu(playerid);
	return 1;
}

GetPlayerAgeEx(date[11]) // Format: day.month.year
{
	new dateTimestamp = DateToTimestamp(date);
	new currentTimestamp = gettime();

	if(dateTimestamp <= 0) 
		return 0;
	if(dateTimestamp > currentTimestamp)
		return 0;
	new age = (currentTimestamp - dateTimestamp) / 31536000;

	//printf("Date: %s, dateTimestamp: %d, Age: %d", date, dateTimestamp, age);
	return age;
}


forward MysqlTempCharsLoad(playerid);
public MysqlTempCharsLoad(playerid)
{
	for(new r; r != cache_num_rows(); r++)
	{
		CachedChars[playerid][r][ID] = cache_get_row_int(r, 0);
		cache_get_row(r, 1, CachedChars[playerid][r][Name], .max_len = MAX_PLAYER_NAME);
		CachedChars[playerid][r][Skin] = cache_get_row_int(r, 2);
	}
	
	InitCharacterSelection(playerid);
	return 1;
}

InitCharacterSelection(playerid)
{
	SetSpawnInfo(playerid, 0, 299, 264.6288, 77.5742, 1001.0391, 286.2208, 0, 0, 0, 0, 0, 0);
	SpawnPlayer(playerid);
	SetPlayerInterior(playerid, 6);
	SetPlayerVirtualWorld(playerid, playerid + 1000);
	TogglePlayerControllable(playerid, 0);
	SetPlayerCameraPos(playerid, 265.925415, 77.9514, 1002.343505);
	SetPlayerCameraLookAt(playerid, 264.976004, 77.675198, 1002.194067);
	
	SetTimerEx("DelayedStartCharacterSelection", 500, false, "i", playerid);
	return 1;
}

StartCharacterSelection(playerid)
{
	new query_str[128];
	mysql_format(mysql, query_str, sizeof query_str, 
		"SELECT `ID`, `Name`, `Skin` FROM `characters` WHERE `AccountID` = %d LIMIT " #MAX_CHARACTERS,
		Account[playerid][ID]);
	mysql_tquery(mysql, query_str, "MysqlTempCharsLoad", "d", playerid);
}

forward StartCharacterCreation(playerid);
public StartCharacterCreation(playerid) 
{
	for(new CreatingCharEnum:e; e != CreatingCharEnum; e++)
		CreatingCharData[playerid][e] = 0;
	
	CreatingCharData[playerid][Gender] = MALE;
	CreatingCharData[playerid][Age] = 18;
	CreatingCharData[playerid][Skin] = 299;
	format(CreatingCharData[playerid][Name], MAX_PLAYER_NAME, "SELECT");
	SetPlayerSkin(playerid, CreatingCharData[playerid][Skin]);
	
	HideCharacterSelectionMenu(playerid);
	ShowCreateCharacterMenu(playerid);
	
	SetTimerEx("DelaySelection", 100, false, "i", playerid);
	return 1;
}

forward DelayedStartCharacterSelection(playerid);
public DelayedStartCharacterSelection(playerid) 
{
	ShowCharacterSelectionMenu(playerid);
	return 1;
}

forward DelaySelection(playerid);
public DelaySelection(playerid) 
{
	SelectTextDraw(playerid, 0xFFFFFFFF);
	TogglePlayerSpectating(playerid, false);
	return 1;
}

HideCharacterSelectionMenu(playerid) 
{
	for(new CharSelectionEnum:e; e != CharSelectionEnum; e++)
		PlayerTextDrawHide(playerid, CharSelectTD[playerid][e]);
}

ShowCharacterSelectionMenu(playerid)
{
	PlayerTextDrawShow(playerid, CharSelectTD[playerid][Box]);
	PlayerTextDrawShow(playerid, CharSelectTD[playerid][Textdraw0]);
	PlayerTextDrawShow(playerid, CharSelectTD[playerid][Textdraw1]);

	if(CachedChars[playerid][0][ID] == 0) 
	{
		PlayerTextDrawShow(playerid, CharSelectTD[playerid][CreateChar]);
		PlayerTextDrawSetString(playerid, CharSelectTD[playerid][CurrentChar], "Empty Slot");
	} 
	else 
	{
		PlayerTextDrawShow(playerid, CharSelectTD[playerid][SelectChar]);
		PlayerTextDrawShow(playerid, CharSelectTD[playerid][DeleteChar]);
		PlayerTextDrawSetString(playerid, CharSelectTD[playerid][CurrentChar], CachedChars[playerid][0][Name]);
		SetPlayerSkin(playerid, CachedChars[playerid][0][Skin]);
	}

	PlayerTextDrawShow(playerid, CharSelectTD[playerid][CurrentChar]);

	SelectedCharIndex[playerid] = 0;

	PlayerTextDrawShow(playerid, CharSelectTD[playerid][RightArrow]);
	SelectTextDraw(playerid, 0xFFFFFFFF);
}

DestroyCharacterSelectionMenu(playerid) 
{
	for(new CharSelectionEnum:e; e != CharSelectionEnum; e++)
		PlayerTextDrawDestroy(playerid, CharSelectTD[playerid][e]);
}


HideCreateCharacterMenu(playerid) 
{
	for(new CharacterCreationEnum:e; e != CharacterCreationEnum; e++)
		PlayerTextDrawHide(playerid, CharCreateTD[playerid][e]);
}

ShowCreateCharacterMenu(playerid)
{
	for(new CharacterCreationEnum:e; e != CharacterCreationEnum; e++)
		PlayerTextDrawShow(playerid, CharCreateTD[playerid][e]);

}

DestroyCharacterCreationMenu(playerid)
{
	for(new CharacterCreationEnum:e; e != CharacterCreationEnum; e++)
		PlayerTextDrawDestroy(playerid, CharCreateTD[playerid][e]);
}

CreateCharacterSelectionMenu(playerid) 
{
	CharSelectTD[playerid][Box] = CreatePlayerTextDraw(playerid, 429.666687, 273.203704, "usebox");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][Box], 0.000000, 16.495685);
	PlayerTextDrawTextSize(playerid, CharSelectTD[playerid][Box], 219.333221, 0.000000);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][Box], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][Box], 0);
	PlayerTextDrawUseBox(playerid, CharSelectTD[playerid][Box], true);
	PlayerTextDrawBoxColor(playerid, CharSelectTD[playerid][Box], 102);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][Box], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][Box], 0);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][Box], 0);

	CharSelectTD[playerid][Textdraw0] = CreatePlayerTextDraw(playerid, 238.000015, 270.459106, "Welcome to East Side Gaming");
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][Textdraw0], 1);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][Textdraw0], 2);
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][Textdraw0], 0.285666, 2.180738);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][Textdraw0], -1);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][Textdraw0], -1);
	PlayerTextDrawSetProportional(playerid, CharSelectTD[playerid][Textdraw0], 1);
	PlayerTextDrawSetSelectable(playerid, CharSelectTD[playerid][Textdraw0], 0);
	PlayerTextDrawBackgroundColor(playerid, CharSelectTD[playerid][Textdraw0], 51);

	CharSelectTD[playerid][Textdraw1] = CreatePlayerTextDraw(playerid, 238.666732, 289.540679, "Please choose one of the options below:");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][Textdraw1], 0.199999, 1.496296);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][Textdraw1], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][Textdraw1], -2139062017);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][Textdraw1], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][Textdraw1], -1);
	PlayerTextDrawBackgroundColor(playerid, CharSelectTD[playerid][Textdraw1], 51);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][Textdraw1], 2);
	PlayerTextDrawSetProportional(playerid, CharSelectTD[playerid][Textdraw1], 1);

	CharSelectTD[playerid][CreateChar] = CreatePlayerTextDraw(playerid, 258.333251, 330.436950, "Create Character");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][CreateChar], 0.309332, 2.363259);
	PlayerTextDrawTextSize(playerid, CharSelectTD[playerid][CreateChar], 450.0, 20.0);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][CreateChar], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][CreateChar], 8388863);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][CreateChar], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][CreateChar], 1);
	PlayerTextDrawBackgroundColor(playerid, CharSelectTD[playerid][CreateChar], 255);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][CreateChar], 2);
	PlayerTextDrawSetProportional(playerid, CharSelectTD[playerid][CreateChar], 1);
	PlayerTextDrawSetSelectable(playerid, CharSelectTD[playerid][CreateChar], true);

	CharSelectTD[playerid][SelectChar] = CreatePlayerTextDraw(playerid, 258.333343, 304.059082, "Select Character");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][SelectChar], 0.335666, 2.479406);
	PlayerTextDrawTextSize(playerid, CharSelectTD[playerid][SelectChar], 450.0, 20.0);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][SelectChar], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][SelectChar], -5963521);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][SelectChar], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][SelectChar], -1);
	PlayerTextDrawBackgroundColor(playerid, CharSelectTD[playerid][SelectChar], 255);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][SelectChar], 2);
	PlayerTextDrawSetProportional(playerid, CharSelectTD[playerid][SelectChar], 1);
	PlayerTextDrawSetSelectable(playerid, CharSelectTD[playerid][SelectChar], true);

	CharSelectTD[playerid][DeleteChar] = CreatePlayerTextDraw(playerid, 259.999725, 360.399810, "Delete Character");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][DeleteChar], 0.308332, 2.296891);
	PlayerTextDrawTextSize(playerid, CharSelectTD[playerid][DeleteChar], 450.0, 20.0);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][DeleteChar], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][DeleteChar], -16776961);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][DeleteChar], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][DeleteChar], 1);
	PlayerTextDrawBackgroundColor(playerid, CharSelectTD[playerid][DeleteChar], 255);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][DeleteChar], 2);
	PlayerTextDrawSetProportional(playerid, CharSelectTD[playerid][DeleteChar], 1);
	PlayerTextDrawSetSelectable(playerid, CharSelectTD[playerid][DeleteChar], true);

	CharSelectTD[playerid][LeftArrow] = CreatePlayerTextDraw(playerid, 224.333343, 397.807250, "ld_beat:left");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][LeftArrow], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharSelectTD[playerid][LeftArrow], 21.999984, 25.303730);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][LeftArrow], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][LeftArrow], -1);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][LeftArrow], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][LeftArrow], 0);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][LeftArrow], 4);
	PlayerTextDrawSetSelectable(playerid, CharSelectTD[playerid][LeftArrow], true);

	CharSelectTD[playerid][RightArrow] = CreatePlayerTextDraw(playerid, 404.000122, 399.881347, "ld_beat:right");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][RightArrow], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharSelectTD[playerid][RightArrow], 21.333311, 22.400024);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][RightArrow], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][RightArrow], -1);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][RightArrow], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][RightArrow], 0);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][RightArrow], 4);
	PlayerTextDrawSetSelectable(playerid, CharSelectTD[playerid][RightArrow], true);

	CharSelectTD[playerid][CurrentChar] = CreatePlayerTextDraw(playerid, 282.000030, 399.466552, "Empty Slot");
	PlayerTextDrawLetterSize(playerid, CharSelectTD[playerid][CurrentChar], 0.311666, 1.973333);
	PlayerTextDrawAlignment(playerid, CharSelectTD[playerid][CurrentChar], 1);
	PlayerTextDrawColor(playerid, CharSelectTD[playerid][CurrentChar], -1);
	PlayerTextDrawSetShadow(playerid, CharSelectTD[playerid][CurrentChar], 0);
	PlayerTextDrawSetOutline(playerid, CharSelectTD[playerid][CurrentChar], 1);
	PlayerTextDrawBackgroundColor(playerid, CharSelectTD[playerid][CurrentChar], 51);
	PlayerTextDrawFont(playerid, CharSelectTD[playerid][CurrentChar], 2);
	PlayerTextDrawSetProportional(playerid, CharSelectTD[playerid][CurrentChar], 1);
}

CreateCharacterCreationMenu(playerid) 
{
	CharCreateTD[playerid][Box] = CreatePlayerTextDraw(playerid, 209.999984, 123.455566, "usebox");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Box], 0.000000, 28.473434);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][Box], 11.999996, 0.000000);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Box], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Box], 0);
	PlayerTextDrawUseBox(playerid, CharCreateTD[playerid][Box], true);
	PlayerTextDrawBoxColor(playerid, CharCreateTD[playerid][Box], 102);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Box], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Box], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Box], 0);

	CharCreateTD[playerid][Title] = CreatePlayerTextDraw(playerid, 11.333326, 113.659210, "Create a character");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Title], 0.337666, 1.807407);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Title], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Title], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Title], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Title], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Title], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Title], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Title], 1);

	CharCreateTD[playerid][Info] = CreatePlayerTextDraw(playerid, 22.666643, 136.474090, "Please click the arrows below to create your custom character");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Info], 0.308333, 1.359406);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][Info], 214.666702, -84.207382);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Info], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Info], -1);
	PlayerTextDrawUseBox(playerid, CharCreateTD[playerid][Info], true);
	PlayerTextDrawBoxColor(playerid, CharCreateTD[playerid][Info], -256);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Info], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Info], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Info], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Info], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Info], 1);

	CharCreateTD[playerid][Name] = CreatePlayerTextDraw(playerid, 22.999980, 185.837020, "Name:");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Name], 0.310000, 1.869629);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Name], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Name], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Name], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Name], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Name], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Name], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Name], 1);

	CharCreateTD[playerid][Gender] = CreatePlayerTextDraw(playerid, 22.333326, 212.385208, "Gender:");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Gender], 0.289999, 1.952592);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Gender], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Gender], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Gender], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Gender], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Gender], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Gender], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Gender], 1);

	CharCreateTD[playerid][Age] = CreatePlayerTextDraw(playerid, 21.666648, 238.518478, "Age:");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Age], 0.338333, 2.077037);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Age], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Age], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Age], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Age], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Age], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Age], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Age], 1);

	CharCreateTD[playerid][Clothes] = CreatePlayerTextDraw(playerid, 22.000007, 267.555572, "Clothes:");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Clothes], 0.321666, 2.118518);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Clothes], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Clothes], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Clothes], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Clothes], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Clothes], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Clothes], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Clothes], 1);

	CharCreateTD[playerid][GenderLeft] = CreatePlayerTextDraw(playerid, 105.999946, 215.703704, "ld_beat:left");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][GenderLeft], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][GenderLeft], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][GenderLeft], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][GenderLeft], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][GenderLeft], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][GenderLeft], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][GenderLeft], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][GenderLeft], true);

	CharCreateTD[playerid][AgeLeft] = CreatePlayerTextDraw(playerid, 106.999946, 244.325897, "ld_beat:left");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][AgeLeft], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][AgeLeft], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][AgeLeft], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][AgeLeft], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][AgeLeft], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][AgeLeft], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][AgeLeft], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][AgeLeft], true);

	CharCreateTD[playerid][ClothesLeft] = CreatePlayerTextDraw(playerid, 106.666671, 271.703674, "ld_beat:left");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][ClothesLeft], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][ClothesLeft], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][ClothesLeft], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][ClothesLeft], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][ClothesLeft], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][ClothesLeft], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][ClothesLeft], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][ClothesLeft], true);

	CharCreateTD[playerid][GenderRight] = CreatePlayerTextDraw(playerid, 192.333480, 213.629592, "ld_beat:right");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][GenderRight], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][GenderRight], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][GenderRight], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][GenderRight], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][GenderRight], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][GenderRight], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][GenderRight], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][GenderRight], true);

	CharCreateTD[playerid][AgeRight] = CreatePlayerTextDraw(playerid, 192.000183, 243.911117, "ld_beat:right");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][AgeRight], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][AgeRight], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][AgeRight], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][AgeRight], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][AgeRight], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][AgeRight], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][AgeRight], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][AgeRight], true);

	CharCreateTD[playerid][ClothesRight] = CreatePlayerTextDraw(playerid, 191.000045, 274.192596, "ld_beat:right");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][ClothesRight], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][ClothesRight], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][ClothesRight], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][ClothesRight], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][ClothesRight], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][ClothesRight], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][ClothesRight], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][ClothesRight], true);

	CharCreateTD[playerid][Spawn] = CreatePlayerTextDraw(playerid, 22.333330, 296.177703, "SPAWN:");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][Spawn], 0.336666, 2.222222);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][Spawn], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][Spawn], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][Spawn], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][Spawn], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][Spawn], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][Spawn], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][Spawn], 1);

	CharCreateTD[playerid][SpawnLeft] = CreatePlayerTextDraw(playerid, 108.666633, 301.155548, "ld_beat:left");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][SpawnLeft], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][SpawnLeft], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][SpawnLeft], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][SpawnLeft], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][SpawnLeft], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][SpawnLeft], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][SpawnLeft], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][SpawnLeft], true);

	CharCreateTD[playerid][SpawnRight] = CreatePlayerTextDraw(playerid, 190.333374, 301.644439, "ld_beat:right");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][SpawnRight], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][SpawnRight], 14.333329, 17.007415);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][SpawnRight], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][SpawnRight], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][SpawnRight], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][SpawnRight], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][SpawnRight], 4);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][SpawnRight], true);

	CharCreateTD[playerid][ButtonText] = CreatePlayerTextDraw(playerid, 36.333316, 343.881530, "CREATE MY CHARACTER");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][ButtonText], 0.303666, 2.031405);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][ButtonText], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][ButtonText], -5963521);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][ButtonText], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][ButtonText], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][ButtonText], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][ButtonText], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][ButtonText], 1);

	CharCreateTD[playerid][ButtonBox] = CreatePlayerTextDraw(playerid, 28.000000, 338.744445, "usebox");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][ButtonBox], 0.000000, 3.324487);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][ButtonBox], 196.000000, 75.000000);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][ButtonBox], 1);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][ButtonBox], 0);
	PlayerTextDrawUseBox(playerid, CharCreateTD[playerid][ButtonBox], true);
	PlayerTextDrawBoxColor(playerid, CharCreateTD[playerid][ButtonBox], 102);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][ButtonBox], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][ButtonBox], 0);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][ButtonBox], 0);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][ButtonBox], true);

	CharCreateTD[playerid][NameValue] = CreatePlayerTextDraw(playerid, 156.666656, 186.666687, "SELECT");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][NameValue], 0.230000, 1.537777);
	PlayerTextDrawTextSize(playerid, CharCreateTD[playerid][NameValue], 50.0, 20.0);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][NameValue], 2);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][NameValue], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][NameValue], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][NameValue], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][NameValue], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][NameValue], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][NameValue], 1);
	PlayerTextDrawSetSelectable(playerid, CharCreateTD[playerid][NameValue], true);

	CharCreateTD[playerid][GenderValue] = CreatePlayerTextDraw(playerid, 158.000045, 214.874053, "MALE");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][GenderValue], 0.268331, 1.413333);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][GenderValue], 2);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][GenderValue], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][GenderValue], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][GenderValue], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][GenderValue], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][GenderValue], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][GenderValue], 1);

	CharCreateTD[playerid][AgeValue] = CreatePlayerTextDraw(playerid, 156.000015, 241.422195, "18");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][AgeValue], 0.241666, 1.517035);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][AgeValue], 2);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][AgeValue], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][AgeValue], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][AgeValue], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][AgeValue], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][AgeValue], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][AgeValue], 1);

	CharCreateTD[playerid][ClothesValue] = CreatePlayerTextDraw(playerid, 156.666702, 272.118499, "299");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][ClothesValue], 0.208331, 1.475554);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][ClothesValue], 2);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][ClothesValue], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][ClothesValue], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][ClothesValue], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][ClothesValue], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][ClothesValue], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][ClothesValue], 1);

	CharCreateTD[playerid][SpawnValue] = CreatePlayerTextDraw(playerid, 158.333343, 300.740753, "AIRPORT");
	PlayerTextDrawLetterSize(playerid, CharCreateTD[playerid][SpawnValue], 0.179998, 1.517035);
	PlayerTextDrawAlignment(playerid, CharCreateTD[playerid][SpawnValue], 2);
	PlayerTextDrawColor(playerid, CharCreateTD[playerid][SpawnValue], -1);
	PlayerTextDrawSetShadow(playerid, CharCreateTD[playerid][SpawnValue], 0);
	PlayerTextDrawSetOutline(playerid, CharCreateTD[playerid][SpawnValue], 1);
	PlayerTextDrawBackgroundColor(playerid, CharCreateTD[playerid][SpawnValue], 51);
	PlayerTextDrawFont(playerid, CharCreateTD[playerid][SpawnValue], 2);
	PlayerTextDrawSetProportional(playerid, CharCreateTD[playerid][SpawnValue], 1);
	
}
