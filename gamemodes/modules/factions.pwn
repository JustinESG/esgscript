#define MAX_FACTIONS (100)

#define MAX_FACTION_NAME (32)
#define MAX_ABBRV_LENGTH (6)

enum FactionType
{
	NONE,
	LAW,
	GOVERNMENT,
	CIVILIAN
};
enum FactionEnum
{
	//MySQL data
	ID,
	Name[MAX_FACTION_NAME + 1],
	ShortName[MAX_ABBRV_LENGTH + 1],
	FactionType:Type,
	
	//PAWN data
	bool:Chat
};

new Faction[MAX_FACTIONS][FactionEnum];
new Iterator:Factions<MAX_FACTIONS>;



hook OnSystemInit()
{
	mysql_tquery(mysql, "SELECT * FROM `factions`", "MysqlFactionsLoad");
	return 1;
}

forward MysqlFactionsLoad();
public MysqlFactionsLoad()
{
	new rows = cache_num_rows();
	if(rows == 0)
		return printf("[FACTION] There are no factions to load.");
	
	for(new i; i != rows; i++)
	{
		Iter_Add(Factions, i);
	   	Faction[i][ID] = cache_get_field_content_int(i, "ID");
		
	    cache_get_field_content(i, "Name", Faction[i][Name], 
			.max_len = MAX_FACTION_NAME);
	    cache_get_field_content(i, "ShortName", Faction[i][ShortName], 
			.max_len = MAX_ABBRV_LENGTH);
		
	    Faction[i][Type] = FactionType:cache_get_field_content_int(i, "Type");

       	printf("[FACTION] Loaded \"%s\" [%s]", 
			Faction[i][Name], Faction[i][ShortName]);
	}
	
	return true;
}


/*
 *  Commands
 */

CMD:factiontow(playerid, params[])
{
	new 
		factionid = Character[playerid][FactionID], 
		string[128];
	
	if(factionid == 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not in a faction!");
	
	if(Character[playerid][Tier] < 2 && Account[playerid][Admin] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You don't have the correct privileges to do this!");
	
	
	for(new i; i < MAX_VEHICLES; i ++)
	{
	    if(Vehicle[i][FactionID] == factionid)
	    {
			SetVehicleToRespawn(i);
	    }
	}
	
    format(string, sizeof(string), 
		"[FACTION] (%d) %s's has just towed all faction vehicles.", 
		playerid, Character[playerid][Name]);
    SendFactionMessage(playerid, factionid, string);
	
	return true;
}

CMD:factionlist(playerid, params[])
{
	if(Iter_Count(Factions) == 0)
		return SendClientMessage(playerid, COLOR_BLUE, 
		"No factions available.");
		
	
	new string[128];
	
	SendClientMessage(playerid, COLOR_BLUE, "Faction list:");
	foreach(new i : Factions)
	{
		format(string, sizeof(string), 
			"[ID]:{E0E0E0} %d [%s] \"%s\" [Online: [%d]]", 
			Faction[i][ID], Faction[i][ShortName], Faction[i][Name], 
			GetFactionMemberCount(Faction[i][ID]));
		SendClientMessage(playerid, COLOR_BLUE, string);
	}
	return true;
}

CMD:deletefaction(playerid, params[])
{
	new 
		factionid, 
		string[128];
	
	if(Account[playerid][Admin] < 2) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an admin!");
	
	if(sscanf(params, "i", factionid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /deletefaction [id]");
	
	
	if(DeleteFaction(factionid))
	{
		format(string, sizeof(string), 
			"[ADMIN WATCH] (%d) %s has removed faction ID %d.", 
			playerid, Account[playerid][Name], factionid);
		SendAdminMsgEx(string);
		
		format(string, sizeof(string), 
			"[ADMIH]{E0E0E0} You have deleted faction ID %d.", 
			factionid);
		SendClientMessage(playerid, COLOR_BLUE, string);
	}
	else
	{
		SendClientMessage(playerid, COLOR_ERROR, 
			"[ERROR]{E0E0E0} Invalid faction ID!");
	}
	return true;
}

CMD:createfaction(playerid, params[])
{
	new 
		string[128], 
		name[MAX_FACTION_NAME + 1], 
		short_name[MAX_ABBRV_LENGTH + 1], 
		type;
	
	if(Account[playerid][Admin] < 2) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an admin!");
	
	if(sscanf(params, "s[33]s[7]i", name, short_name, type))
	{
		return SendClientMessage(playerid, COLOR_ERROR, 
			"[ERROR]{E0E0E0} /createfaction [name] [abbreviation] [type]");
	}
	
	
	new fid = CreateFaction(name, short_name, FactionType:type);
	
	format(string, sizeof(string), 
		"[ADMIN]:{E0E0E0} You have created faction ID: %d [%s] " \
		"\"%s\" with type %d.", 
		fid, short_name, name, type);
	SendClientMessage(playerid, COLOR_BLUE, string);
	
	return true;
}

CMD:factionname(playerid, params[])
{
	new
		factionid = Character[playerid][FactionID],
		fname[MAX_FACTION_NAME + 1],
		string[256];
	
	if(factionid != 0 && Character[playerid][Tier] > 0)
	{
		if(sscanf(params, "s[33]", fname)) 
			return SendClientMessage(playerid, COLOR_ERROR, 
			"[ERROR]{E0E0E0} /factionname [name]");
	}
	else if(Account[playerid][Admin] > 0)
	{
		if(sscanf(params, "is[33]", factionid, fname)) 
			return SendClientMessage(playerid, COLOR_ERROR, 
			"[ERROR]{E0E0E0} /factionname [factionid] [name]");
	}
	else if(factionid == 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not in a faction!");
	else
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You don't have the correct privileges to do this!");
	
	if(strlen(fname) < 4 || strlen(fname) > MAX_FACTION_NAME)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} A faction name has to be between " \
		"4 and 32 characters in length.");
	
	new fidx = GetFactionIndexById(factionid);
	if(fidx == -1)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} Invalid faction ID!");
	
	
	format(Faction[fidx][Name], MAX_FACTION_NAME, fname);

	format(string, sizeof(string), 
		"[FACTION]{E0E0E0} The faction's name has been changed to %s.", 
		fname);
	SendClientMessage(playerid, COLOR_BLUE, string);

	mysql_format(mysql, string, sizeof(string), 
		"UPDATE `factions` SET `Name` = '%e' WHERE `ID` = %d", 
		fname, factionid);
	mysql_tquery(mysql, string);

	return true;
}

CMD:factionrank(playerid, params[])
{
	new
		targetid, 
		factionrank[16], 
		string[128], 
		factionid = Character[playerid][FactionID];
	
	if(factionid == 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not in a faction!");
	
	if(Character[playerid][Tier] < 1 && Account[playerid][Admin] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You don't have the correct priviledges to do this!");
	
	if(sscanf(params, "us[16]", targetid, factionrank)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /factionrank [playerid/name] [rank]");
	
	if(!IsPlayerConnected(targetid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	if(strlen(factionrank) < 4 || strlen(factionrank) > 16) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} A faction rank has to be between 4 and 16 characters in length.");
	
	if(Character[targetid][FactionID] != factionid)
	 	return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} This player isn't in your faction!");
	
	
	format(Character[targetid][Rank], 16, factionrank);
	
    format(string, sizeof(string), 
		"[FACTION] (%d) %s's rank has been changed to %s by (%d) %s.", 
		targetid, Account[targetid][Name], factionrank, 
		playerid, Account[playerid][Name]);
    SendFactionMessage(playerid, factionid, string);

	format(string, sizeof(string), 
		"[FACTION]{E0E0E0} Your faction rank has been changed to %s.", 
		factionrank);
	SendClientMessage(targetid, COLOR_BLUE, string);

	return true;
}

CMD:factiontier(playerid, params[])
{
	new 
		targetid, 
		factiontier, 
		string[128], 
		factionid = Character[playerid][FactionID];
	
	if(factionid == 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not in a faction!");
	
	if(Character[playerid][Tier] < 1 && Account[playerid][Admin] == 0) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You don't have the correct privileges to do this!");
	
	if(sscanf(params, "ui", targetid, factiontier)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /factiontier [playerid/name] [tier]");
	
	if(!IsPlayerConnected(targetid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	if(Character[targetid][FactionID] != factionid)
	 	return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} This player isn't in your faction!");
	
	
	Character[targetid][Tier] = factiontier;
	
    format(string, sizeof(string), 
		"[FACTION] (%d) %s's tier has been changed to %d by (%d) %s.", 
		targetid, Character[targetid][Name], factiontier,
		playerid, Account[playerid][Name]);
    SendFactionMessage(playerid, factionid, string);
	
	format(string, sizeof(string), 
		"[FACTION]{E0E0E0} Your faction tier has been changed to %d.",
		factiontier);
	SendClientMessage(targetid, COLOR_BLUE, string);
	
	return true;
}

CMD:setfaction(playerid, params[])
{
	new 
		targetid, 
		factionid, 
		string[128];

	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an admin!");
		
	if(sscanf(params, "ui", targetid, factionid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /setfaction [playerid/name] [factionid]");
	
	if(!IsPlayerConnected(targetid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	new fidx = GetFactionIndexById(factionid);
	if(fidx == -1)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} Invalid faction ID!");
		
	
	format(string, sizeof string, 
		"UPDATE `characters` SET `FactionID` = %d WHERE `ID` = %d",
		factionid, Character[targetid][ID]);
	mysql_tquery(mysql, string);
	
	Character[targetid][FactionID] = factionid;
	format(Character[targetid][Rank], 16, "Rank");
	
	format(string, sizeof(string), 
		"[FACTION]{E0E0E0} Admin %s has placed you in faction ID %d (%s).",
		Account[playerid][Name], factionid, Faction[fidx][Name]);
	SendClientMessage(playerid, COLOR_BLUE, string);
	
	return true;
}

CMD:factionchat(playerid, params[])
{
	new 
		string[128], 
		factionid = Character[playerid][FactionID];
	
	if(factionid == 0) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not in a faction!");
	
	if(Character[playerid][Tier] < 1 && Account[playerid][Admin] < 2) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You don't have the correct privileges to do this!");
	
	
	new fidx = GetFactionIndexById(factionid);
	Faction[fidx][Chat] = !Faction[fidx][Chat];

    format(string, sizeof(string), 
		"[FACTION] The faction's chat has been turned %s.",
		Faction[fidx][Chat] ? "on" : "off");
    SendFactionMessage(playerid, factionid, string);

	return true;
}

CMD:faction(playerid, params[])
{
	new 
		text[128], string[256], 
		factionid = Character[playerid][FactionID],
		fidx = GetFactionIndexById(factionid);

	if(factionid == 0 || fidx == -1) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not in a faction!");
	
    if(sscanf(params, "s[128]", text)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /f(action) [text]");
	
	if(!Faction[fidx][Chat]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} This faction's faction chat is turned off.");
	
	
    format(string, sizeof(string), "[ (%d) %s %s: %s ]", 
		playerid, Character[playerid][Rank],
		Character[playerid][Name], text);
    SendFactionMessage(playerid, factionid, string);
	
	return true;
}

CMD:f(playerid, params[])
	return cmd_faction(playerid, params);

	
	
GetFactionIndexById(factionid)
{
	foreach(new i : Factions)
	{
		if(Faction[i][ID] == factionid)
			return i;
	}
	return -1;
}

GetFactionMemberCount(factionid)
{
	new count;
	foreach(new i: Player)
	{
	    if(Character[i][FactionID] == factionid)
	        count++;
	}
	
	return count;
}

SendFactionMessage(playerid, factionid, text[])
{
	foreach(new i: Player)
	{
	    if(Character[i][FactionID] == factionid)
	        ReturnSplittedMessage(i, playerid, COLOR_FACTION, text, 1);
	}
}

IsFactionSkin(skinid)
{
	static const skins[] = {
		70, 71, 163, 164, 165, 166, 265, 266, 267, 274, 275, 276, 277,
		278, 279, 280, 281, 282, 282, 283, 288, 284, 285, 286, 287
	};

	for(new i; i != sizeof skins; i++)
	{
		if(skins[i] == skinid)
			return true;
	}
	return false;
}

CreateFaction(
	const name[MAX_FACTION_NAME + 1], 
	const short_name[MAX_ABBRV_LENGTH + 1], 
	FactionType:type)
{
	new query[128];
	
	mysql_format(mysql, query, sizeof(query), 
		"INSERT INTO `factions` (`Name`, `ShortName`, `Type`) " \
		"VALUES ('%e', '%e', %d)", 
		name, short_name, _:type);
	new Cache:res = mysql_query(mysql, query);
	
	new idx = Iter_Free(Factions);
	Iter_Add(Factions, idx);
	
	Faction[idx][ID] = cache_insert_id();
	format(Faction[idx][Name], MAX_FACTION_NAME, "%s", name);
	format(Faction[idx][ShortName], MAX_ABBRV_LENGTH, "%s", short_name);
	Faction[idx][Type] = type;
	
	cache_delete(res);
	
	printf("Faction \"%s\" (%d) has been created.", name, Faction[idx][ID]);
	return Faction[idx][ID];
}

DeleteFaction(factionid)
{
	new fidx = GetFactionIndexById(factionid);
	if(fidx == -1)
		return 0;
	
	new query[128];
	format(query, sizeof query, "DELETE FROM `factions` WHERE ID = %d", factionid);
	mysql_tquery(mysql, query);
	
	for(new FactionEnum:e; e != FactionEnum; e++)
		Faction[fidx][e] = 0;
	
	Iter_Remove(Factions, fidx);
	return 1;
}
