#include <YSI4\YSI_Coding\y_hooks>

//////////////////////////////
//////////// ENUMS ///////////
//////////////////////////////

enum AccountData
{
	//data stored in MySQL
	ID,
	Name[MAX_PLAYER_NAME],
	Admin,
	
	//normal variables
	bool:LoggedIn,
	bool:NewUser,
	
	LoginAttempts
	//LoginTimer,
	//LoginTimeLeft,
}

new Account[MAX_PLAYERS][AccountData];

////////////////////////////////////////////////////////////////////////////////
/////////////////////////// SA-MP HOOKED FUNCTIONS /////////////////////////////
////////////////////////////////////////////////////////////////////////////////

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    switch(dialogid)
    {
		case DIALOG_LOGIN:
	    {
	        if(!response) 
				return Kick(playerid);
			
			new 
				hashed_pass[129],
				stored_hpass[129];
		    WP_Hash(hashed_pass, sizeof hashed_pass, inputtext);
			GetPVarString(playerid, "AccountPass", stored_hpass, sizeof stored_hpass);

	        if(!strcmp(hashed_pass, stored_hpass, true))
	        {
				DeletePVar(playerid, "AccountPass");
				
				new query_str[128];
	            mysql_format(MysqlUcp, query_str, sizeof(query_str), 
					"SELECT * FROM `accounts` WHERE `user_id` = %d LIMIT 1", Account[playerid][ID]);
	            mysql_tquery(MysqlUcp, query_str, "MysqlAccountLoad", "i", playerid);
				
				//KillTimer(LoginTimer[playerid]);
				
				TextDrawShowForPlayer(playerid, LogoTD);
	            TextDrawShowForPlayer(playerid, ClockTD);
				// StartLevelingTimer(playerid);
				
				Account[playerid][LoginAttempts] = 0;
				Account[playerid][LoggedIn] = true;
	        }
	        else
			{
			    if(Account[playerid][LoginAttempts] >= 2)
			    {
			        SendClientMessage(playerid, COLOR_ERROR, "Too many failed login attempts!");
			        SetTimerEx("KickPlayer", 1000, false, "i", playerid);

			        Account[playerid][LoggedIn] = false;
			        Account[playerid][LoginAttempts] = 0;
			    }
				else
				{
					Account[playerid][LoginAttempts]++;
					ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Authenticate", 
						ROYAL_BLUE "Your account already exists. Please\n"\
						"authenticate by typing your password below.\n\n"\
						"You have entered an " RED "incorrect" ROYAL_BLUE " password!\n", 
						"Login", "Quit");
				}
			}
	    }

	   	case DIALOG_REGISTER:
	    {
	        if(!response) 
				return Kick(playerid);
	
			ShowPlayerDialog(playerid, DIALOG_REGISTER_EMAIL, DIALOG_STYLE_INPUT, "Enter e-mail address",
				ROYAL_BLUE "Please enter your e-mail address in the field below.\n"\
				"We guarantee you, that you won't recieve any spam\n"\
				"mail from us. This step is required to ensure your security.",
				"Enter", "Abort");
		}
	   	case DIALOG_REGISTER_EMAIL:
	    {
			if(!response)
				return Kick(playerid);
			
			//TODO: check if e-mail is valid (regex?)
			if(strlen(inputtext) < 7)
			{
				ShowPlayerDialog(playerid, DIALOG_REGISTER_EMAIL, DIALOG_STYLE_INPUT, "Enter e-mail address",
					ROYAL_BLUE "Please enter your e-mail address in the field below.\n"\
					RED "Invalid e-mail address!",
					"Enter", "Abort");
				return 1;
			}
			
			static email_repeat[MAX_PLAYERS];
			if(email_repeat[playerid] == 1)
			{
				new email_addr[128];
				
				email_repeat[playerid] = 0;
				GetPVarString(playerid, "AccRegEmail", email_addr, sizeof email_addr);
				if(!strcmp(email_addr, inputtext))
				{
					ShowPlayerDialog(playerid, DIALOG_REGISTER_PASS, DIALOG_STYLE_PASSWORD, "Enter password", 
						ROYAL_BLUE "Please enter your password in the field below.\n"\
						"Make sure that your password is longer than 5 characters.", 
						"Finish", "Back");
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_REGISTER_EMAIL, DIALOG_STYLE_INPUT, "Enter e-mail address",
						ROYAL_BLUE "Please enter your e-mail address in the field below.\n"\
						RED "Your e-mail addresses do not match!",
						"Enter", "Abort");
				}
			}
			else
			{
				email_repeat[playerid] = 1;
				SetPVarString(playerid, "AccRegEmail", inputtext);
				ShowPlayerDialog(playerid, DIALOG_REGISTER_EMAIL, DIALOG_STYLE_INPUT, "Enter e-mail address",
					ROYAL_BLUE "Please repeat your e-mail address.",
					"Enter", "Abort");
			}
		}
	   	case DIALOG_REGISTER_PASS:
	    {
	        if(!response)
			{
				ShowPlayerDialog(playerid, DIALOG_REGISTER_EMAIL, DIALOG_STYLE_INPUT, "Enter e-mail address",
					ROYAL_BLUE "Please enter your e-mail address in the field below.\n"\
					"We guarantee you, that you won't recieve any spam\n"\
					"mail from us. This step is required to ensure your security.",
					"Enter", "Abort");
				return 1;
			}
			
	        if(strlen(inputtext) < 5) 
			{
				ShowPlayerDialog(playerid, DIALOG_REGISTER_PASS, DIALOG_STYLE_PASSWORD, "Enter password", 
					ROYAL_BLUE "Please enter your password in the field below.\n"\
					RED "Your password is too short!", 
					"Finish", "Back");
				return 1;
			}
	        if(strlen(inputtext) >= 64) 
			{
				ShowPlayerDialog(playerid, DIALOG_REGISTER_PASS, DIALOG_STYLE_PASSWORD, "Enter password", 
					ROYAL_BLUE "Please enter your password in the field below.\n"\
					RED "Your password is too long!", 
					"Finish", "Back");
				return 1;
			}
			
			new hashed_pass[129];
			static pass_repeat[MAX_PLAYERS];
			if(pass_repeat[playerid] == 1)
			{
				new password[129];
				
				pass_repeat[playerid] = 0;
				GetPVarString(playerid, "AccRegPass", password, sizeof password);
				WP_Hash(hashed_pass, sizeof hashed_pass, inputtext);
				if(!strcmp(password, hashed_pass))
				{
					new 
						email_addr[128],
						query_str[512];
					
					GetPVarString(playerid, "AccRegEmail", email_addr, sizeof email_addr);
					
					mysql_format(MysqlUcp, query_str, sizeof(query_str), 
						"INSERT INTO `accounts` (`username`, `password`, `email`) VALUES ('%e', '%s', '%e')",
						Account[playerid][Name], hashed_pass, email_addr);
					mysql_tquery(MysqlUcp, query_str, "MysqlAccountStore", "d", playerid);
					
					Account[playerid][LoggedIn] = true;
					Account[playerid][NewUser] = true;
					
					DeletePVar(playerid, "AccRegPass");
					DeletePVar(playerid, "AccRegEmail");
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_REGISTER_PASS, DIALOG_STYLE_PASSWORD, "Enter password", 
						ROYAL_BLUE "Please enter your password in the field below.\n"\
						RED "Your passwords do not match!", 
						"Finish", "Back");
				}
			}
			else
			{
				pass_repeat[playerid] = 1;
				WP_Hash(hashed_pass, sizeof hashed_pass, inputtext);
				SetPVarString(playerid, "AccRegPass", hashed_pass);
				ShowPlayerDialog(playerid, DIALOG_REGISTER_PASS, DIALOG_STYLE_PASSWORD, "Enter password", 
					ROYAL_BLUE "Please re-enter your password.", 
					"Finish", "Back");
			}
	    }
	    
	    case DIALOG_NOTWHITELISTED: Kick(playerid);
	}
	return 1;
}

hook OnPlayerConnect(playerid)
{
	GetPlayerName(playerid, Account[playerid][Name], MAX_PLAYER_NAME);

	new query_str[256];
	mysql_format(MysqlUcp, query_str, sizeof(query_str), 
		"SELECT a.`user_id`, a.`password`, b.`reason`, b.`date` " \
		"FROM `accounts` AS a LEFT JOIN `bans` AS b " \
		"ON (a.`user_id` = b.`user_id`) WHERE a.`username` = '%e' LIMIT 1", Account[playerid][Name]);
	mysql_tquery(MysqlUcp, query_str, "MysqlAccountCheck", "i", playerid);
	
	SetupStartCamera(playerid);
	return 1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	//StopLevelingTimer(playerid);
	//SavePlayerData(playerid);

	//reset account variables
	for(new AccountData:e; e != AccountData; ++e)
		Account[playerid][e] = 0;
	
	return 1;
}

public OnPlayerRequestClass(playerid, classid) 
{
	if(Account[playerid][LoggedIn])
		SpawnPlayer(playerid);
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	return 0;
}


////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// PUBLIC FUNCTIONS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////

forward MysqlAccountCheck(playerid);
public MysqlAccountCheck(playerid)
{
	if(cache_num_rows()) 
	{
		new ban_reason[64];
		cache_get_row(0, 2, ban_reason);
		if(!strcmp(ban_reason, "NULL")) //user is registered
		{
			new password[129];
			cache_get_row(0, 1, password);
			SetPVarString(playerid, "AccountPass", password);
			
			Account[playerid][ID] = cache_get_row_int(0, 0);
			
			new dialog_str[512];
			format(dialog_str, sizeof(dialog_str), 
				ROYAL_BLUE "Welcome back, " FIREBRICK "%s" ROYAL_BLUE "!\n"\
				ROYAL_BLUE "This account is registered and requires a password.\n"\
				"If this is your account please login by entering the \n"\
				"password you registered with.\nIf you have forgotten \n"\
				"your password please contact support by going to \n\""\
				SPRING_GREEN "eastsidegaming.net/support/" ROYAL_BLUE "\".\n"\
				DODGER_BLUE "Enjoy your stay!",
				Account[playerid][Name]);
			ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Authenticate", dialog_str, "Login", "Quit");
		}
		else //user is banned
		{
			new 
				str[256], 
				sub_str[64],
				date_str[32];
			
			cache_get_row(0, 3, date_str);
			if(!strcmp(ban_reason, "NULL"))
				format(sub_str, sizeof sub_str, "permanently");
			else
				format(sub_str, sizeof sub_str, "until %s", date_str);
			
			format(str, sizeof str,
				WHITE "You have been banned %s.\n" \
				"Reason: \"%s\"\n" \
				"You can contact our support at \"eastsidegaming.net/support/\" for futher assistance.", 
				sub_str, ban_reason);
			ShowPlayerDialog(playerid, DIALOG_NOTWHITELISTED, DIALOG_STYLE_MSGBOX, 
				"You are banned!", str, "OK", "");
			
			SetTimerEx("KickPlayer", 1000, false, "d", playerid);
		}
	}
	else //user is not registered
	{
		if(gServerInfo[Closed])
		{
			SendClientMessage(playerid, -1, 
				"["#HEX_INFO"INFO{FFFFFF}] The server is in closed testing. \
				Please check out our website "#SERVER_URL" for information about our opening.");
			SetTimerEx("KickPlayer", 1000, false, "d", playerid);
		}
		else if(gServerInfo[WhiteListOnly])
		{
			ShowPlayerDialog(playerid, DIALOG_NOTWHITELISTED, DIALOG_STYLE_MSGBOX, "Notice",
				"{FFFFFF}Sorry, your account is not currently registered nor whitelisted.\n"\
				"Please register on our UCP at ucp.eastsidegaming.net.", "OK", "");
			SetTimerEx("KickPlayer", 1000, false, "d", playerid);
		}
		else
		{
			new dialog_str[512];
			format(dialog_str, sizeof(dialog_str), 
				ROYAL_BLUE "Welcome, " FIREBRICK "%s" ROYAL_BLUE "!\n"\
				"Your account is not registered. We are now \n"\
				"going to guide you through the registration \n"\
				"process. Press \"" SPRING_GREEN "Register" ROYAL_BLUE "\" to start.\n"\
				"Our staff will never ask you for your password.\n"\
				"If you encounter any problems you can contact \n"\
				"our support at \"" SPRING_GREEN "eastsidegaming.net/support/" ROYAL_BLUE "\".\n"\
				DODGER_BLUE "Enjoy your stay!", 
				Account[playerid][Name]);
			ShowPlayerDialog(playerid, DIALOG_REGISTER, DIALOG_STYLE_MSGBOX, "Registration", dialog_str, "Register", "Quit");
		}
	}
	return 1;
}

forward MysqlAccountLoad(playerid);
public MysqlAccountLoad(playerid)
{
	Account[playerid][Admin] = cache_get_field_content_int(0, "admlvl");
	
	StartCharacterSelection(playerid);
    return 1;
}

forward MysqlAccountStore(playerid);
public MysqlAccountStore(playerid)
{
	Account[playerid][ID] = cache_insert_id();
	
	StartCharacterSelection(playerid);
	return 1;
}


static SetupStartCamera(playerid)
{
	SetTimerEx("DelayedCameraSetup", 200, false, "d", playerid);
}

forward DelayedCameraSetup(playerid);
public DelayedCameraSetup(playerid)
{
	TogglePlayerControllable(playerid, false);
	SetPlayerVirtualWorld(playerid, playerid + 10);
	SetPlayerCameraPos(playerid, -2662.6516, 113.5546, 64.8456);
	SetPlayerCameraLookAt(playerid, -2661.8169, 112.9963, 64.5806);
	
	SetPlayerPos(playerid, -2662.6516, 113.5546, 68.8456);
	return 1;
}
