#define COLOR_RADIO 			(0xB1D490FF)

new RadioSlot[MAX_PLAYERS];

CMD:radio(playerid, params[])
{
    new text[92], string[144];

	if(Player[playerid][HasRadio] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You need a radio in order to perform this command!");

    if(sscanf(params, "s[92]", text))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /r(adio) [text]");

	if(Player[playerid][RadioFreq][RadioSlot[playerid]] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're currently not authed to a proper frequency. Use /radiofreq and /radioslot to adjust it!");

    format(string, sizeof(string), "%s says (radio): %s", GetRoleplayName(playerid), text);
    SendLocalMessage(playerid, 0xEBEBEBAA, string, 3.0);

    format(string, sizeof(string), "[RADIO] [Slot %d] %s: %s", RadioSlot[playerid], GetRoleplayName(playerid), text);
    SendRadioMessage(playerid, Player[playerid][RadioFreq][RadioSlot[playerid]], string);

    return true;
}

CMD:r(playerid, params[])
	return cmd_radio(playerid, params);

CMD:radioslot(playerid, params[])
{
	new slot, string[90];

	if(Player[playerid][HasRadio] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You need a radio in order to perform this command!");

	if(sscanf(params, "i", slot))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /changeslot [slot]");

	if(slot < 1 || slot > 2)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You can only select between slot 1 or 2!");

	RadioSlot[playerid] = slot;

	format(string, sizeof(string), "[INFO]{E0E0E0} You have changed your radio slot to \"%d\".", slot);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:radiofreq(playerid, params[])
{
	new freq, slot, string[90];

	if(Player[playerid][HasRadio] == 0)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You need a radio in order to perform this command!");

	if(sscanf(params, "ii", slot, freq))
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} /radiofreq [slot] [freq]");

	if(slot < 1 || slot > 2)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You can only select between slot 1 or 2!");

	if(freq < 1000 || freq > 9999)
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} A frequency can't be less than 1000, or more than 9999!");

	Player[playerid][RadioFreq][slot] = freq;

	format(string, sizeof(string), "[INFO]{E0E0E0} You have changed your radio frequency to \"%d\" in slot \"%d\".", freq, slot);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:radioslots(playerid, params[])
{
	new string[90];
	
	SendClientMessage(playerid, COLOR_BLUE, "Your radio frequencies:");
	
	for(new i; i < 2; i ++)
	{
		format(string, sizeof(string), "[INFO]{E0E0E0} Slot %d: Frequency: %d", i, Player[playerid][RadioFreq][i]);
		SendClientMessage(playerid, COLOR_BLUE, string);
	}
	
	return true;
}

SendRadioMessage(playerid, freq, text[])
{
	foreach(new i: Player)
	{
	    for(new ix; ix < 2; ix ++)
	    {
	    	if(Player[i][RadioFreq][ix] == freq)
	    	{
				ReturnSplittedMessage(i, playerid, COLOR_RADIO, text, 1);
	    	}
		}
	}

	return true;
}
