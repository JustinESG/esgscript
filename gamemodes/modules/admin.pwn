// Admin script color includes
#define COLOR_ADMALERT X11_TAN1
#define COLOR_ADMCOLOR X11_DARK_ORCHID

enum AdminRanksEnum
{
	Level,
	RankName[64]
};

static const AdminRanks[][AdminRanksEnum] =
{
	{0, "None"},
	{1, "Junior Moderator"},
	{2, "Moderator"},
	{3, "Senior Moderator"},
	{4, "Head Moderator"},
	{5, "Junior Executive Officer"},
	{6, "Executive Officer"},
	{7, "Senior Executive Officer"},
	{8, "Head Executive Officer"},
	{9, "Lead Executive Officer"},
	{10, "Assistant Chief Executive Officer"},
	{11, "Chief Executive Officer"}
};

static bool: PendingReport[MAX_PLAYERS];
static bool: AnimDebugEnabled[MAX_PLAYERS];
static bool: OnDuty[MAX_PLAYERS];

static ReportExpireTimer[MAX_PLAYERS];
static AnimDebugTimer[MAX_PLAYERS];



CMD:gotopos(playerid, params[])
{
	new Float:x, Float:y, Float:z;
	
	if(!Account[playerid][Admin])
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "fff", x, y, z))
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /gotopos [x-coordinate] [y-coordinate] [z-coordinate]");
	
	
	SetPlayerPos(playerid, x, y, z);
	return 1;
}

CMD:unban(playerid, params[])
{
	new target_name[MAX_PLAYER_NAME];
	
	if(Account[playerid][Admin] < 3)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "s[" #MAX_PLAYER_NAME "]", target_name))
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /unban [player name]");
	
	
	inline BannedPlayerFind()
	{
		if(!cache_num_rows())
			return SendClientMessage(playerid, COLOR_ERROR, 
			"[ERROR]{E0E0E0} Player does not exist or is not banned!");
		
		new str[128];
		format(str, sizeof str,
			"DELETE FROM `bans` WHERE `id` = %d",
			cache_get_row_int(0, 0));
		mysql_tquery(MysqlUcp, str);
		
		format(str, sizeof(str), 
			"[AMSG] (%d) %s has unbanned player %s.",
			playerid, Account[playerid][Name], target_name);
		SendAdminMsgEx(str);
	}
	
	new query_str[256];
	mysql_format(MysqlUcp, query_str, sizeof query_str,
		"SELECT bans.`id` FROM `bans`, `accounts` " \
		"WHERE accounts.`user_id` = bans.`user_id` " \
		"AND accounts.`username` = '%e'",
		target_name);
	mysql_tquery_inline(MysqlUcp, query_str, using inline BannedPlayerFind, "");
	
	return 1;
}

CMD:ban(playerid, params[])
{
	new 
		targetid,
		reason_str[64],
		hours;
	
	if(Account[playerid][Admin] < 3)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "uis[64]", targetid, hours, reason_str))
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /ban [player] [hours (0 = perm.)] [reason]");
	
	if(!IsPlayerConnected(targetid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	if(hours < 0)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} Invalid number of hours!");
	
	
	new str[512], sub_str[64];
	if(hours)
	{
		format(sub_str, sizeof sub_str,
			"DATE_ADD(NOW(), INTERVAL %d HOUR)", hours);
	}
	else
		format(sub_str, sizeof sub_str, "NULL");
	
	mysql_format(MysqlUcp, str, sizeof str, 
		"INSERT INTO `bans` (`user_id`, `admin_id`, `reason`, `date`) " \
		"VALUES (%d, %d, '%e', %s)",
		Account[targetid][ID], Account[playerid][ID], reason_str, sub_str);
	mysql_tquery(MysqlUcp, str);
	
	SetTimerEx("KickPlayer", 1000, false, "d", targetid);
	
	
	if(hours)
		format(sub_str, sizeof sub_str, "for %d hours", hours);
	else
		format(sub_str, sizeof sub_str, "permanently");
	
	format(str, sizeof str, 
		"[INFO]{E0E0E0} Admin (%d) %s has banned you %s from the server.",
		playerid, Account[playerid][Name], sub_str);
	SendClientMessage(targetid, COLOR_BLUE, str);
	
	format(str, sizeof str, 
		"[INFO]{E0E0E0} Reason: %s", reason_str);
	SendClientMessage(targetid, COLOR_BLUE, str);
	
	format(str, sizeof str, 
		"[AMSG] (%d) %s has banned player (%d) %s %s from the server.", 
		playerid, Account[playerid][Name],
		targetid, Account[targetid][Name], sub_str);
	SendAdminMsgEx(str);	
	return 1;
}

CMD:kick(playerid, params[])
{
	new targetid;
	
	if(Account[playerid][Admin] < 2)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "u", targetid))
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /kick [player(id)]");
	
	if(!IsPlayerConnected(targetid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	
	new str[128];
	format(str, sizeof str, 
		"[INFO]{E0E0E0} Admin (%d) %s has kicked you from the server.",
		playerid, Account[playerid][Name]);
	SendClientMessage(targetid, COLOR_BLUE, str);
	
	format(str, sizeof str, 
		"[AMSG] (%d) %s has kicked player (%d) %s from the server.", 
		playerid, Account[playerid][Name], targetid, Account[targetid][Name]);
	SendAdminMsgEx(str);	
	
	SetTimerEx("KickPlayer", 500, false, "d", targetid);
	return 1;
}

CMD:staff(playerid, params[])
{
	if(Iter_Count(Player) == 0)
		return SendClientMessage(playerid, COLOR_ADMALERT, 
		"[ADMIN]{E0E0E0} There are currently no staff members online.");
	
	foreach(new i: Player)
	{
		if(Account[i][Admin])
		{
			new str[128];
			
			format(str, sizeof(str), "[ADMIN]{E0E0E0}: %s (%d) %s ",
				AdminRanks[ Account[i][Admin] ][RankName], i, Account[i][Name]);
			if(IsOnAdminDuty(i))
				strcat(str, "{61B06A}[ON DUTY]{E0E0E0}");
			else
				strcat(str, "{CF6767}[OFF DUTY]{E0E0E0}");
			
		    SendClientMessage(playerid, COLOR_BLUE, str);
		}
	}
	
	return 1;
}

CMD:report(playerid, params[])
{
	new text[128], str[128];

	if(PendingReport[playerid]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]:{E0E0E0} You already have a pending report. " \
		"Please wait for an admin to take it.");

	if(sscanf(params, "s[128]", text)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /report [information]");
	
	if(strlen(text) > 80) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} Short summaries please! No more than 80 characters please!");

	
	format(str, sizeof(str), 
		"[REPORT]: %s (%d) has made a report with the following information:", 
		Account[playerid][Name], playerid);
	SendAdminMsgEx(str);

	format(str, sizeof(str), 
		"{A3A3A3}\"%s\"{63C9FF}. To accept this report, please use {F59F2F}/takereport %d{63C9FF}.", 
		text, playerid);
	SendAdminMsgEx(str);
	
	PendingReport[playerid] = true;
	ReportExpireTimer[playerid] = SetTimerEx("ReportExpire", 300000, false, "i", playerid);
	
	return true;
}

CMD:ahelp(playerid, params[])
{
	if(!Account[playerid][Admin])
		return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]:{E0E0E0} You're not an administrator!");

	SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 1]:{E0E0E0} /ahelp, /acmds, /achat, /a(dmin)duty, /takereport, /factionname, /factionrank, /factiontier");
	SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 1]:{E0E0E0} /setfaction, /factionchat, /entercar, /putplayerincar, /getvehicle, /gotocar, /removeplayerfromcar");

	if(Account[playerid][Admin] > 1)
	{
		SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 2]:{E0E0E0} /animdebug, /acreateveh, /asaveveh, /gotocar");
		SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 2]:{E0E0E0} /bizgoto, /bizmove, /bizsettype, /bizsettname, /atmgoto, /atmmove");
 		SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 2]:{E0E0E0} /gotobooth, /editbooth, /confirmboothpos");
	}

	if(Account[playerid][Admin] > 2)
	{
		SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 3]:{E0E0E0} /bizcreate, /bizdelete, /createbooth");
		SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 3]:{E0E0E0} /createatm, /deleteatm /confirmatmpos,");
		SendClientMessage(playerid, COLOR_ERROR, "[LEVEL 3]:{E0E0E0} /createfaction, /detefaction");

	}

	return true;
}

//CMD:acdms(playerid, params[]) return cmd_ahelp(playerid, params);

CMD:achat(playerid, params[])
{
	new text[128];

	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "s[128]", text)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /achat [text]");
	
	if(strlen(text) > 80) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You can't use more than 80 characters!");
	
	
	SendAdminMessage(playerid, text);
	return true;
}

CMD:aduty(playerid, params[])
{
	new str[128];

	if(!Account[playerid][Admin]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");

	
	OnDuty[playerid] = !OnDuty[playerid];
	format(str, sizeof(str), "[AMSG] (%d) %s is now %s admin duty!", 
		playerid, Account[playerid][Name], IsOnAdminDuty(playerid) ? "on" : "off");
    SendAdminMsgEx(str);

	return true;
}

//CMD:aduty(playerid, params[]) return cmd_adminduty(playerid, params);

CMD:takereport(playerid, params[])
{
	new targetpid, string[128];
	
	if(!Account[playerid][Admin])
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	if(sscanf(params, "u", targetpid)) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} /takereport [playerid]");
	
	if(!IsPlayerConnected(targetpid))
		return SendClientMessage(playerid, COLOR_ERROR,
		"[ERROR]{E0E0E0} Invalid player ID!");
	
	if(!PendingReport[targetpid]) 
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} The playerid you have selected does not have a pending report.");
	
	
	format(string, sizeof(string), 
		"[REPORT MSG]: %s (%d) is now reviewing your report. Please stand by and contact them.", 
		Account[playerid][Name], playerid);
	SendClientMessage(targetpid, COLOR_BLUE, string);
	
	format(string, sizeof(string), 
		"[MSG]: You are now reviewing %s (%d)'s report. They will contact you as soon as possible.", 
		Account[targetpid][Name], targetpid);
	SendClientMessage(playerid, COLOR_ADMALERT, string);

	format(string, sizeof(string), 
		"[REPORT MSG]: %s (%d) is now reviewing %s (%d)'s report.", 
		Account[playerid][Name], playerid, Account[targetpid][Name], targetpid);
	SendAdminMsgEx(string);
	print(string);
	
	KillTimer(ReportExpireTimer[targetpid]);
	PendingReport[targetpid] = false;
	
	return true;
}


CMD:animdebug(playerid, params[])
{
	if(Account[playerid][Admin] < 2)
		return SendClientMessage(playerid, COLOR_ERROR, 
		"[ERROR]{E0E0E0} You're not an administrator!");
	
	
	AnimDebugEnabled[playerid] = !AnimDebugEnabled[playerid];
	
	printf("AdmCmd: (%d) %s has just %s animation debugging.", 
		playerid, Account[playerid][Name], AnimDebugEnabled[playerid] ? "enabled" : "disabled");
	
	new str[128];
	format(str, sizeof str, 
		"[ANIM DEBUG]:{E0E0E0} Animation debugging {FF6347}%s{E0E0E0}.",
		AnimDebugEnabled[playerid] ? "enabled" : "disabled");
	SendClientMessage(playerid, COLOR_ERROR, str);
	
	if(AnimDebugEnabled[playerid])
		AnimDebugTimer[playerid] = SetTimerEx("AnimDebug", 1000, true, "i", playerid);
	else
		KillTimer(AnimDebugTimer[playerid]);
	
	return true;
}

// =-=-=-=-=-=-= Start Custom Functions =-=-=-=-=-=-= //

IsOnAdminDuty(playerid)
{
	return OnDuty[playerid];
}

SendAdminMsgEx(text[])
{
	foreach(new i: Player)
	{
	    if(Account[i][Admin] > 0)
	        SendClientMessage(i, COLOR_BLUE, text);
	}
	return true;
}

SendAdminMessage(playerid, text[])
{
	new string[512];
	
	if(strlen(text) > 144) 
		return printf(
			"SendAdminMessage param returned false due to strlen exceeding the limit! %d/144", 
			strlen(text));
	
	
	format(string, sizeof(string), 
		"[Admin]{E0E0E0} %s (%d) %s: %s", 
		AdminRanks[ Account[playerid][Admin] ][RankName], playerid, 
		Account[playerid][Name], text);
	
	foreach(new i: Player)
	{
	    if(Account[i][Admin] > 0)
	        ReturnSplittedMessage(i, playerid, COLOR_BLUE, string, 1);
	}
	return true;
}

IsVehicleOccupied(vehicleid)
{
	foreach(new i: Player)
	{
		if(IsPlayerInAnyVehicle(i) && GetPlayerVehicleID(i) == vehicleid)
		    return true;
	}
	return false;
}

GetVehicleDriver(vehicleid)
{
	foreach(new i: Player)
	{
		if(IsPlayerInAnyVehicle(i) && GetPlayerVehicleID(i) == vehicleid && GetPlayerVehicleSeat(i) == 0)
		    return i;
	}
	return false;
}

// =-=-=-=-=-=-= Start Custom Public Functions =-=-=-=-=-=-= //

forward AnimDebug(playerid);
public AnimDebug(playerid) // FFFF00
{
    if(GetPlayerAnimationIndex(playerid))
    {
        new animlib[32], animname[32], msg[128];
        GetAnimationName(
			GetPlayerAnimationIndex(playerid), 
			animlib, sizeof(animlib), 
			animname, sizeof(animname));
		
        format(msg, sizeof(msg), 
			"[ANIM DEBUG]:{E0E0E0} Library: {FF6347}''%s'' {E0E0E0}Name: {FF6347}''%s''", 
			animlib, animname);
        SendClientMessage(playerid, COLOR_ERROR, msg);
    }
    return true;
}

forward ReportExpire(playerid);
public ReportExpire(playerid)
{
	new string[128];
	
	PendingReport[playerid] = false;
	SendClientMessage(playerid, COLOR_ADMALERT, 
		"[REPORT MSG]: Your report has just expired. You can now send reports again.");
	
	format(string, sizeof(string), 
		"[REPORT MSG]: %s (%d)'s report has expired and can no longer be assigned.", 
		Account[playerid][Name], playerid);
	SendAdminMsgEx(string);
	
	return true;
}
