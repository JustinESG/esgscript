#include <YSI4\YSI_Coding\y_hooks>

#define MENU_WATCHES    		(450)
#define MENU_GLASSES            (451)
#define MENU_HATS	            (453)
#define MENU_MASK	            (454)
#define MENU_HAIR	            (455)

#define DIALOG_TOYSEL           (450)
#define MAX_USER_TOYS           (10) // loops start from 1, so +1 for digit (6 = 5)

enum ToyInfo
{
	UserID,
	AttachID[MAX_USER_TOYS],
	Slot[MAX_USER_TOYS],

	tIndex[MAX_USER_TOYS],
	tModelID[MAX_USER_TOYS],
	tBoneID[MAX_USER_TOYS]
};

new toyPosition[MAX_USER_TOYS][95];
new PlayerToy[MAX_PLAYERS][ToyInfo];

new bool: EditingAttachedObject[MAX_PLAYERS];

enum ToyEnum
{
	ModelID,
	Price
};

new WatchArray[][ToyEnum] =
{
	{19039}, {19040}, {19041}, {19042}, {19043},
	{19044}, {19045}, {19046}, {19049}, {19050},
	{19051}, {19052}, {19053}
};

new GlassesArray[][ToyEnum] =
{
	{19006}, {19007}, {19008}, {19009}, {19010}, {19011},
	{19012}, {19013}, {19014}, {19015}, {19016}, {19017},
	{19018}, {19019}, {19020}, {19021}, {19022}, {19023},
	{19024}, {19025}, {19026}, {19027}, {19028}, {19029},
	{19030}, {19031}, {19032}, {19033}, {19034}, {19035}
};

new CapArray[][ToyEnum] =
{
	{18939}, {18940}, {18941}, {18942}, {18943}, {18953},
	{18954}, {18955}, {18956}, {18957}, {18958}, {18959},
	{18960}, {18961}, {18964}, {18965}, {18966}
};

new BowlerHatArray[][ToyEnum] =
{
	{18944}, {18945}, {18946}, {18947}, {18948}, {18949},
	{18950}, {18951}, {18968}, {18969}, {18970}, {18971},
	{18972}, {18973}
};

new BandanaArray[][ToyEnum] =
{
	{18891}, {18892}, {18893}, {18894}, {18895}, {18896},
	{18897}, {18898}, {18899}, {18900}, {18901}, {18902},
	{18903}, {18904}, {18905}, {18906}, {18907}, {18908},
	{18909}, {18910}
};

new MaskArray[][ToyEnum] =
{
	{18911}, {18912}, {18913}, {18914}, {18915}, {18916},
	{18917}, {18918}, {18919}, {18920}, {19036}, {19037},
	{19038}, {19472}
};

new HairArray[][ToyEnum] =
{
	{19516}, {19517}, {19518}, {19519}, {19274}, {18640},
	{18975}, {19077}, {19136}
};

hook OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == DIALOG_TOYSEL)
	{
	    if(response)
	    {
	        switch(listitem)
	        {
				case 0: // Watches
				{
			    	new xtoy[sizeof(WatchArray)];

					for(new i; i < sizeof(WatchArray); i ++)
					{
						xtoy[i] = WatchArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Watches", MENU_WATCHES, 16.0, 0.0, -55.0);
				}
				
				case 1: // Glasses
				{
			    	new xtoy[sizeof(GlassesArray)];

					for(new i; i < sizeof(GlassesArray); i ++)
					{
						xtoy[i] = GlassesArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Glasses", MENU_GLASSES, 16.0, 0.0, -55.0);
				}

				case 2: // Caps
				{
			    	new xtoy[sizeof(CapArray)];

					for(new i; i < sizeof(CapArray); i ++)
					{
						xtoy[i] = CapArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Caps", MENU_HATS, 16.0, 0.0, -55.0);
				}

				case 3: // Bowlers
				{
			    	new xtoy[sizeof(BowlerHatArray)];

					for(new i; i < sizeof(BowlerHatArray); i ++)
					{
						xtoy[i] = BowlerHatArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Bowler Hats", MENU_HATS, 16.0, 0.0, -55.0);
				}

				case 4: // Bandana
				{
			    	new xtoy[sizeof(BandanaArray)];

					for(new i; i < sizeof(BandanaArray); i ++)
					{
						xtoy[i] = BandanaArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Bandanas", MENU_HATS, 16.0, 0.0, -55.0);
				}
				
				case 5: // Masks
				{
			    	new xtoy[sizeof(MaskArray)];

					for(new i; i < sizeof(MaskArray); i ++)
					{
						xtoy[i] = MaskArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Masks", MENU_MASK, 16.0, 0.0, -55.0);
				}

				case 6: // Hair
				{
			    	new xtoy[sizeof(HairArray)];

					for(new i; i < sizeof(HairArray); i ++)
					{
						xtoy[i] = HairArray[i][ModelID];
					}

			        ShowModelSelectionMenuEx(playerid, xtoy, sizeof(xtoy), "Hair", MENU_HAIR, 16.0, 0.0, -55.0);
				}
			}
	    }
	}
	return 1;
}

hook OnCharacterSpawn(playerid)
{
	if(Character[playerid][SQL_ToyInitiate] != 0)
	{
		SQL_LoadAttachment(playerid);
	}
	return 1;
}

hook OnPlayerDisconnect(playerid, reason)
{
	for(new i = 1; i < MAX_USER_TOYS; i ++)
	{
    	SavePlayerAttachedObjects(playerid, i);
    }
	return 1;
}


CMD:reparsetoyinfo(playerid, params[])
{
	new string[100];
	
	for(new i ; i < MAX_USER_TOYS - 1; i ++)
	{
		format(string, sizeof(string), "[User ID: %d] [Attach ID: %d] [Slot: %d] [tIndex: %d] [tModelID: %d] [tBoneID: %d]", PlayerToy[playerid][UserID],
		PlayerToy[playerid][AttachID][i], PlayerToy[playerid][Slot][i], PlayerToy[playerid][tIndex][i], PlayerToy[playerid][tModelID][i], PlayerToy[playerid][tBoneID][i]);

		SendClientMessage(playerid, COLOR_BLUE, string);
	}
	
	return true;
}

CMD:toys(playerid, params[])
{
	if(EditingAttachedObject[playerid])
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You're already editing an attached object!");

	ShowPlayerDialog(playerid, DIALOG_TOYSEL, DIALOG_STYLE_LIST, "Select a toy", "Watches\nGlasses\nCaps\nHats\nBandanas\nMasks\nHair", "Select", "Cancel");

	return true;
}

hook OnPlayerModelSelectionEx(playerid, response, extraid, modelid)
{
	if(extraid == MENU_WATCHES)
	{
	    if(response)
	    {
	        // Index 0-5
			SetPlayerAttachedObject(playerid, 0, modelid, 5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 1);
	        EditAttachedObject(playerid, 0);
	    
	        EditingAttachedObject[playerid] = true;
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Please adjust the object to your needs. Unrealistic objects will result in punishment!");
	    }
	}

	if(extraid == MENU_GLASSES)
	{
	    if(response)
	    {
			SetPlayerAttachedObject(playerid, 1, modelid, 2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 1);
	        EditAttachedObject(playerid, 1);

	        EditingAttachedObject[playerid] = true;
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Please adjust the object to your needs. Unrealistic objects will result in punishment!");
	    }
	}

	if(extraid == MENU_HATS)
	{
	    if(response)
	    {
			SetPlayerAttachedObject(playerid, 2, modelid, 2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 1);
	        EditAttachedObject(playerid, 2);

	        EditingAttachedObject[playerid] = true;
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Please adjust the object to your needs. Unrealistic objects will result in punishment!");
	    }
	}

	if(extraid == MENU_MASK)
	{
	    if(response)
	    {
			SetPlayerAttachedObject(playerid, 3, modelid, 2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 1);
	        EditAttachedObject(playerid, 3);

	        EditingAttachedObject[playerid] = true;
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Please adjust the object to your needs. Unrealistic objects will result in punishment!");
	    }
	}

	if(extraid == MENU_HAIR)
	{
	    if(response)
	    {
			SetPlayerAttachedObject(playerid, 4, modelid, 2, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 1);
	        EditAttachedObject(playerid, 4);

	        EditingAttachedObject[playerid] = true;
			SendClientMessage(playerid, COLOR_BLUE, "[INFO]{E0E0E0} Please adjust the object to your needs. Unrealistic objects will result in punishment!");
	    }
	}
	return 1;
}

hook OnPlayerEditAttachedObj(playerid, response, index, modelid, boneid, Float:fOffsetX, Float:fOffsetY,
       Float:fOffsetZ, Float:fRotX, Float:fRotY, Float:fRotZ, Float:fScaleX, Float:fScaleY, Float:fScaleZ)
{
	new slot = 999, string[95];
	
	for(new i; i < MAX_USER_TOYS -1; i ++)
	{
		if(PlayerToy[playerid][tModelID][i] == 0)
		{
		    slot = i;
			break;
		}
	}
	
	if(slot == 999)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You have no slot left! Please remove a toy before trying again!");
	
	if(response)
	{
	    if(GetPlayerMoneyEx(playerid) <= 2500)
		{
			RemovePlayerAttachedObject(playerid, index);
        	EditingAttachedObject[playerid] = false;
	    	return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have enough money! You need at least $2500 for a toy!");
		}
	
	    EditingAttachedObject[playerid] = false;
	
		PlayerToy[playerid][tIndex][slot] = index;
		PlayerToy[playerid][tModelID][slot] = modelid;
		PlayerToy[playerid][tBoneID][slot] = boneid;

		format(string, sizeof(string), "%f, %f, %f, %f, %f, %f, %f, %f, %f", fOffsetX, fOffsetY, fOffsetZ, fRotX, fRotY, fRotZ, fScaleX, fScaleY, fScaleZ);
		toyPosition[slot] = string;
	}
	
	else if(!response)
	{
        RemovePlayerAttachedObject(playerid, index);
        EditingAttachedObject[playerid] = false;
	}
	return 1;
}

SQL_LoadAttachment(playerid)
{
	new query[200];
	
	mysql_format(mysql, query, sizeof(query), "SELECT * FROM `player_attach` WHERE `UserID` = '%d'", Player[playerid][ID]);
    mysql_function_query(mysql, query, true, "SQL_LoadAttachments", "");
}

forward SQL_LoadAttachments(playerid);
public SQL_LoadAttachments(playerid)
{
	new rows, fields;
	cache_get_data(rows, fields, mysql);

	if(!cache_num_rows()) return printf("[TOYS] cache_get_row_count returned false. There are no player toys to load for playerid %d.", playerid);
	printf("[Toys] cache_get_row_count returned %d rows (player toys) to load.", cache_get_row_count());

 	for(new i; i != cache_get_row_count(); i++)
	{
		PlayerToy[playerid][UserID] = 				cache_get_field_content_int(i, "UserID");

   		PlayerToy[playerid][AttachID][i] =          cache_insert_id();
		PlayerToy[playerid][Slot][i]     = 			cache_get_field_content_int(i, "Slot");
		
		PlayerToy[playerid][tIndex][i]   = 			cache_get_field_content_int(i, "tIndex");
		PlayerToy[playerid][tModelID][i] = 			cache_get_field_content_int(i, "tModelID");
		PlayerToy[playerid][tBoneID][i]  = 			cache_get_field_content_int(i, "tBoneID");

		cache_get_field_content(i, "tPosition", toyPosition[i]);
		
		printf("Loaded attachment ID %d with slot %d, indexID %d, modelID %d, boneID %d at pos %s", PlayerToy[playerid][AttachID][i],
		PlayerToy[playerid][Slot][i], PlayerToy[playerid][tIndex][i], PlayerToy[playerid][tModelID][i], PlayerToy[playerid][tBoneID][i], toyPosition[i]);
	}

	return true;
}

CMD:buytoyslot(playerid, params[])
{
	new string[144], slot, price = 2000 * Player[playerid][SQL_ToyInitiate];

	if(price == 0)
	    price = 1000;

	if(Player[playerid][SQL_ToyInitiate] >= MAX_USER_TOYS - 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You already have all available slots! You can't buy any more!");

	if(sscanf(params, "i", slot))
	    return SendClientMessage(playerid, COLOR_ERROR, "/buytoyslot [1-9]");

	if(GetPlayerMoneyEx(playerid) < price)
	{
	    format(string, sizeof(string), "You don't have enough money for this slot! You need at least $%d. You have $%d.", price, GetPlayerMoneyEx(playerid));
		return SendClientMessage(playerid, COLOR_ERROR, string);
	}

	Player[playerid][SQL_ToyInitiate] ++;

	SQL_AddPlayerAttachedObjects(playerid, slot);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have bought slot %d for %d!", slot, price);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:weartoy(playerid, params[])
{
	new mainstr[90], tempstr[10], slot;

	if(sscanf(params, "i", slot))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /weartoy [0-9]");

	if(slot < 0 || slot > MAX_USER_TOYS - 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} There aren't this many slots available!");

	if(PlayerToy[playerid][tModelID][slot] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a toy in this slot!");

	new Float: tOffSetX, Float: tOffSetY, Float: tOffSetZ, Float: tRotX, Float: tRotY, Float: tRotZ, Float: tScaleX, Float: tScaleY, Float: tScaleZ;

	format(mainstr, sizeof(mainstr), "%s", toyPosition[slot]);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tOffSetX = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tOffSetY = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tOffSetZ = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",", true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tRotX = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tRotY = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tRotZ = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",", true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tScaleX = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tScaleY = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, 8);
	tScaleZ = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",", true));

	SetPlayerAttachedObject(playerid, PlayerToy[playerid][tIndex][slot], PlayerToy[playerid][tModelID][slot],
	PlayerToy[playerid][tBoneID][slot], tOffSetX, tOffSetY, tOffSetZ, tRotX, tRotY, tRotZ, tScaleX, tScaleY, tScaleZ);

	format(mainstr, sizeof(mainstr), "[INFO]{E0E0E0} You have put on your toy from slot ID: %d.", slot);
	SendClientMessage(playerid, COLOR_BLUE, mainstr);

	return true;
}

CMD:hidetoy(playerid, params[])
{
	new string[144], slot;

	if(sscanf(params, "i", slot))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /removetoy [0-9]");

	if(slot < 0 || slot > MAX_USER_TOYS - 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} There aren't this many slots available!");

	if(PlayerToy[playerid][tModelID][slot] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a toy in this slot!");

	RemovePlayerAttachedObject(playerid, slot);

	format(string, sizeof(string), "[INFO]{E0E0E0} You have hid your toy.", slot);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:removetoy(playerid, params[])
{
	new string[144], slot;

	if(sscanf(params, "i", slot))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /removetoy [0-9]");

	if(slot < 0 || slot > MAX_USER_TOYS - 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} There aren't this many slots available!");

	if(PlayerToy[playerid][tModelID][slot] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a toy in this slot!");

	RemovePlayerAttachedObject(playerid, slot);
	PlayerToy[playerid][tModelID][slot] = 0;
	
	format(string, sizeof(string), "[INFO]{E0E0E0} You have deleted the toy in slot %d. New items can now be stored here.", slot);
	SendClientMessage(playerid, COLOR_BLUE, string);

	return true;
}

CMD:edittoy(playerid, params[])
{
	new mainstr[90], tempstr[10], slot;

	if(sscanf(params, "i", slot))
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} /edittoy [0-9]");

	if(slot < 0 || slot > MAX_USER_TOYS - 1)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} There aren't this many slots available!");

	if(PlayerToy[playerid][tModelID][slot] == 0)
	    return SendClientMessage(playerid, COLOR_ERROR, "[ERROR]{E0E0E0} You don't have a toy in this slot!");

	new Float: tOffSetX, Float: tOffSetY, Float: tOffSetZ, Float: tRotX, Float: tRotY, Float: tRotZ, Float: tScaleX, Float: tScaleY, Float: tScaleZ;

	format(mainstr, sizeof(mainstr), "%s", toyPosition[slot]);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tOffSetX = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tOffSetY = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tOffSetZ = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",", true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tRotX = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tRotY = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tRotZ = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",", true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tScaleX = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, strfind(mainstr, ",", true));
	tScaleY = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",",  true) + 1);

	strmid(tempstr, mainstr, 0, 8);
	tScaleZ = floatstr(tempstr), strdel(mainstr, 0, strfind(mainstr, ",", true));

	SetPlayerAttachedObject(playerid, PlayerToy[playerid][tIndex][slot], PlayerToy[playerid][tModelID][slot],
	PlayerToy[playerid][tBoneID][slot], tOffSetX, tOffSetY, tOffSetZ, tRotX, tRotY, tRotZ, tScaleX, tScaleY, tScaleZ);

	EditAttachedObject(playerid, PlayerToy[playerid][tIndex][slot]);

	format(mainstr, sizeof(mainstr), "[INFO]{E0E0E0} You are editing your toy from slot ID: %d.", slot);
	SendClientMessage(playerid, COLOR_BLUE, mainstr);

	return true;
}

SQL_AddPlayerAttachedObjects(playerid, slot)
{
	new query[400];

	mysql_format(mysql, query, sizeof(query), "INSERT INTO `player_attach` (`AttachID`, `Slot`, `UserID`, `tIndex`, `tModelID`, `tBoneID`, `tPosition`) VALUES ('', '%d', '%d', '%d', '%d', '%d', '%s')",
 	slot, Player[playerid][ID], PlayerToy[playerid][tIndex][slot],   PlayerToy[playerid][tModelID][slot],  PlayerToy[playerid][tBoneID][slot], toyPosition[slot]);

	mysql_tquery(mysql, query, "", "");

	return true;
}

SavePlayerAttachedObjects(playerid, slot)
{
	new query[600], enumid = slot - 1;

	mysql_format(mysql, query, sizeof(query), "UPDATE `player_attach` SET `tIndex` = '%d', `tModelID` = '%d', `tBoneID` = '%d', `tPosition` = '%s' WHERE `UserID` = '%d' AND `Slot` = '%d'",
	PlayerToy[playerid][tIndex][enumid], PlayerToy[playerid][tModelID][enumid], PlayerToy[playerid][tBoneID][enumid], toyPosition[enumid], Player[playerid][ID], slot);

	mysql_tquery(mysql, query, "", "");

	return true;
}
