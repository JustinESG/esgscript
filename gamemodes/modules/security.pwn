#include <YSI4\YSI_Coding\y_hooks>

/*
	Serverside security
*/


#if defined SERVER_DEVELOPMENT
#define SERVER_IP "159.253.7.154"
#else
#define SERVER_IP "127.0.0.1"
#endif

hook OnGameModeInit() {
	if(strcmp(GetServerIp(), SERVER_IP, false))
	{
		SendRconCommand("exit");
	}
	return 1;
}


GetServerIp()
{
    static
        sIp[16]
    ;
    GetServerVarAsString("bind", sIp, sizeof(sIp));
    return sIp;
}
