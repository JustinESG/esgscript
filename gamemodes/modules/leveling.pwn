new LevelTimer[MAX_PLAYERS];

StartLevelingTimer(playerid)
{
	LevelTimer[playerid] = SetTimerEx("LevelUp", 3600000, true, "i", playerid);

	return true;
}

StopLevelingTimer(playerid)
{
	KillTimer(LevelTimer[playerid]);

	return true;
}

forward LevelUp(playerid);
public LevelUp(playerid)
{
	new string[144];

	Player[playerid][Experience] ++;
	Account[playerid][Hours] ++;

	Account[playerid][BankAmount] += 1500;

	new savingscalc = Player[playerid][BankSavings] / 2;
	Account[playerid][BankSavings] += savingscalc / 9;

	format(string, sizeof(string), "Experience point gained! [{61C75B}%d/%d{E0E0E0}] You currently have {61C75B}%d{E0E0E0} hours played.", Player[playerid][Experience], Player[playerid][ExpLeft], Player[playerid][Hours]);
	SendClientMessage(playerid, 0xE0E0E0FF, string);

	format(string, sizeof(string), "Hourly reward: ${61C75B}1500{E0E0E0} Bank info: ${61C75B}%d{E0E0E0}. Savings: ${61C75B}%d{E0E0E0}. (+ {61C75B}%d{E0E0E0})", Player[playerid][BankAmount], Player[playerid][BankSavings], savingscalc / 9);
	SendClientMessage(playerid, 0xE0E0E0FF, string);

	if(Player[playerid][Experience] >= Player[playerid][ExpLeft])
	{
	    Player[playerid][Experience] = 0;
	    Player[playerid][ExpLeft] = Player[playerid][ExpLeft] * 2;

	    Player[playerid][Level] ++;

		format(string, sizeof(string), "Congratulations, you have just leveled up! You are now level {61C75B}%d{E0E0E0}.", Player[playerid][Level]);
    	SendClientMessage(playerid, 0xE0E0E0FF, string);

	}

	if(Player[playerid][SavingsCooldown] != 0)
	{
		Player[playerid][SavingsCooldown] --;
		    
	    format(string, sizeof(string), "You have %d paydays remaining until you can access your savings!", Player[playerid][SavingsCooldown]);
    	SendClientMessage(playerid, 0xE0E0E0FF, string);
	}

	return true;
}
