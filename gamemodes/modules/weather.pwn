enum ClockInfo
{
	Minutes,
	Hours
};

new Time[ClockInfo];

InitialiseClock()
{
	LoadClockTextDraw();
	
	WeatherTimer();
	
	SetTimer("ClockTimer", 1800000, true);
	SetTimer("WeatherTimer", 900000, true);

	Time[Hours] = 14;
	Time[Minutes] = 00;

	SetWorldTime(Time[Hours]);

	FixClockTextDraw();

	return true;
}

FixClockTextDraw()
{
	new tempstr[15];

	format(tempstr, sizeof(tempstr), "%d:%d", Time[Hours], Time[Minutes]);
	
	if(Time[Hours] < 10)
	{
	    strins(tempstr, "0", 0, 1);
	}

	if(Time[Minutes] < 10)
	{
	    new point = strfind(tempstr, ":", true);

	    strins(tempstr, "0", point + 1, 1);
	}
	
	TextDrawSetString(ClockTD, tempstr);
	
	return true;
}

forward ClockTimer();
public ClockTimer()
{
	Time[Minutes] ++;

	if(Time[Minutes] > 59)
	{
	    Time[Hours] ++;
		SetWorldTime(Time[Hours]);

		Time[Minutes] = 0;

		if(Time[Hours] >= 23)
		{
		    Time[Hours] = 0;
		}
	}

	FixClockTextDraw();

	return true;
}

forward WeatherTimer();
public WeatherTimer()
{
	switch(random(8))
	{
	    // Sunny
		case 0: SetWeather(2);
		case 1: SetWeather(5);
		case 2: SetWeather(11);
		case 3: SetWeather(14);

		// Foggy/rain
		case 4: SetWeather(9); // foggy
		case 5: SetWeather(8); // rain
		case 6: SetWeather(4); // cloudy LS
		case 7: SetWeather(7); // cloudy SF
	}

	return true;
}


LoadClockTextDraw()
{
	ClockTD = TextDrawCreate(549.333190, 30.385173, "14:30");
	TextDrawLetterSize(ClockTD, 0.502000, 2.068740);
	TextDrawAlignment(ClockTD, 1);
	TextDrawColor(ClockTD, 0xE0E0E0FF);
	TextDrawSetShadow(ClockTD, 0);
	TextDrawSetOutline(ClockTD, 1);
	TextDrawBackgroundColor(ClockTD, 51);
	TextDrawFont(ClockTD, 2);
	TextDrawSetProportional(ClockTD, 1);
}
