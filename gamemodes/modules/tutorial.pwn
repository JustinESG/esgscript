#include <YSI4\YSI_Coding\y_hooks>

new PlayerText: GenderSelTD[4][MAX_PLAYERS];

StartTutorial(playerid)
{
	SetPlayerVirtualWorld(playerid, 951);

	SetPlayerCameraPos(playerid, -1494.9956, 776.1202, 54.7659);
	SetPlayerCameraLookAt(playerid, -1495.8458, 776.6442, 54.6110);

	LoadGenderSelectionDraws(playerid);
	ShowGenderSelectionDraws(playerid);

	SelectTextDraw(playerid, 0xffffff);
}

EndTutorial(playerid)
{
	TogglePlayerSpectating(playerid, false);
	HideGenderSelectionDraws(playerid);

	CancelSelectTextDraw(playerid);
	SetPlayerVirtualWorld(playerid, 0);
	
//	SetCameraBehindPlayer(playerid);
	
	SetPlayerSkin(playerid, Player[playerid][SkinID]);
	SendClientMessage(playerid, COLOR_BLUE, "[WELCOME]{E0E0E0} Welcome to "SERVER_NAME"! If you require any help, feel free to use /ask!");

	Newreg[playerid] = false;
	Player[playerid][Registered] = 1;
}

hook OnPlayerClickPlayerTD(playerid, PlayerText:playertextid)
{
	if(playertextid == GenderSelTD[2][playerid])
	{
		Player[playerid][Gender] = 1;
		Player[playerid][SkinID] = 240;

		EndTutorial(playerid);
	}

	if(playertextid == GenderSelTD[3][playerid])
	{
		Player[playerid][Gender] = 2;
		Player[playerid][SkinID] = 226;

		EndTutorial(playerid);
	}
	return 1;
}

HideGenderSelectionDraws(playerid)
{
	for(new i; i < sizeof(GenderSelTD); i ++)
	{
		PlayerTextDrawHide(playerid, GenderSelTD[i][playerid]);
	}
}

ShowGenderSelectionDraws(playerid)
{
	for(new i; i < sizeof(GenderSelTD); i ++)
	{
		PlayerTextDrawShow(playerid, GenderSelTD[i][playerid]);
	}
}

LoadGenderSelectionDraws(playerid)
{
	GenderSelTD[0][playerid] = CreatePlayerTextDraw(playerid, 322, 170, "null");
	PlayerTextDrawFont(playerid, GenderSelTD[0][playerid], TEXT_DRAW_FONT_MODEL_PREVIEW);
	PlayerTextDrawUseBox(playerid, GenderSelTD[0][playerid], true);
	PlayerTextDrawBackgroundColor(playerid, GenderSelTD[0][playerid], 0);
	PlayerTextDrawTextSize(playerid, GenderSelTD[0][playerid], 135, 135);
	PlayerTextDrawSetPreviewModel(playerid, GenderSelTD[0][playerid], 233);

	GenderSelTD[1][playerid] = CreatePlayerTextDraw(playerid, 195, 170, "null");
	PlayerTextDrawFont(playerid, GenderSelTD[1][playerid], TEXT_DRAW_FONT_MODEL_PREVIEW);
	PlayerTextDrawUseBox(playerid, GenderSelTD[1][playerid], true);
	PlayerTextDrawBackgroundColor(playerid, GenderSelTD[1][playerid], 0);
	PlayerTextDrawTextSize(playerid, GenderSelTD[1][playerid], 135, 135);
	PlayerTextDrawSetPreviewModel(playerid, GenderSelTD[1][playerid], 299);

	GenderSelTD[2][playerid] = CreatePlayerTextDraw(playerid, 242.333328, 297.007385, "Male");
	PlayerTextDrawLetterSize(playerid, GenderSelTD[2][playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, GenderSelTD[2][playerid], 290.0, 12.0);
	PlayerTextDrawAlignment(playerid, GenderSelTD[2][playerid], 1);
	PlayerTextDrawColor(playerid, GenderSelTD[2][playerid], -1);
	PlayerTextDrawSetShadow(playerid, GenderSelTD[2][playerid], 0);
	PlayerTextDrawSetOutline(playerid, GenderSelTD[2][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, GenderSelTD[2][playerid], 51);
	PlayerTextDrawFont(playerid, GenderSelTD[2][playerid], 2);
	PlayerTextDrawSetProportional(playerid, GenderSelTD[2][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, GenderSelTD[2][playerid], 1);

	GenderSelTD[3][playerid] = CreatePlayerTextDraw(playerid, 355.000030, 297.007476, "Female");
	PlayerTextDrawLetterSize(playerid, GenderSelTD[3][playerid], 0.449999, 1.600000);
	PlayerTextDrawTextSize(playerid, GenderSelTD[3][playerid], 430.0, 12.0);
	PlayerTextDrawAlignment(playerid, GenderSelTD[3][playerid], 1);
	PlayerTextDrawColor(playerid, GenderSelTD[3][playerid], -1);
	PlayerTextDrawSetShadow(playerid, GenderSelTD[3][playerid], 0);
	PlayerTextDrawSetOutline(playerid, GenderSelTD[3][playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, GenderSelTD[3][playerid], 51);
	PlayerTextDrawFont(playerid, GenderSelTD[3][playerid], 2);
	PlayerTextDrawSetProportional(playerid, GenderSelTD[3][playerid], 1);
	PlayerTextDrawSetSelectable(playerid, GenderSelTD[3][playerid], 1);
}
